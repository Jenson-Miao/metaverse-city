/**
 * 电屏蔽效果
 */
/* eslint-disable */
import * as maptalks from 'maptalks';
import * as THREE from 'three';
import { BaseObject } from 'maptalks.three';


const showElectricShield = false
const lnglats = [
  [116.4720069, 39.9789455],
  [116.4766441, 39.9912067],
];

export default class ElectricShield{
  constructor ({threeLayer}) {
    this.meshes = []
    lnglats.forEach((lnglat) => {
      const mesh = new ElectricShieldObject(lnglat, {
        radius: 500,
      }, threeLayer);
      this.meshes.push(mesh);
    });
    if(showElectricShield){

      threeLayer.addMesh(this.meshes);
    }

  }
  getGuiParams () {
    return  {
      showElectricShield
    }
  }

  guiFun (gui, params, threeLayer) {
    const _this = this
    // 是否增加电屏蔽效果
    gui.add(params, 'showElectricShield').onChange(() => {
      if (params.showElectricShield) {
        threeLayer.addMesh(_this.meshes)
      } else {
        threeLayer.removeMesh(_this.meshes)
      }
    }).name('电屏蔽')
  }
}

// default values
const OPTIONS = {
  speed: 0.02,
  radius: 1,
  altitude: 0,
  // interactive: false
};
class ElectricShieldObject extends BaseObject {
  constructor(coordinate, options, layer) {
    options = maptalks.Util.extend({}, OPTIONS, options, {
      layer,
      coordinate,
    });
    super();
    // Initialize internal configuration
    // https://github.com/maptalks/maptalks.three/blob/1e45f5238f500225ada1deb09b8bab18c1b52cf2/src/BaseObject.js#L135
    this._initOptions(options);
    const {
      altitude,
      radius,
    } = options;
    // generate geometry
    const r = layer.distanceToVector3(radius, radius).x;
    const geometry = new THREE.SphereBufferGeometry(r, 50, 50, 0, Math.PI * 2);

    // Initialize internal object3d
    // https://github.com/maptalks/maptalks.three/blob/1e45f5238f500225ada1deb09b8bab18c1b52cf2/src/BaseObject.js#L140
    // this._createMesh(geometry, material);
    const material = this.getMaterial();
    this._createGroup();
    const mesh = new THREE.Mesh(geometry, material);
    this.getObject3d().add(mesh);
    // set object3d position
    const z = layer.distanceToVector3(altitude, altitude).x;
    const position = layer.coordinateToVector3(coordinate, z);
    this.getObject3d().position.copy(position);
    this.getObject3d().rotation.x = Math.PI / 2;
  }

  getMaterial() {
    const ElectricShield = {
      uniforms: {
        time: {
          type: 'f',
          value: 0,
        },
        color: {
          type: 'c',
          value: new THREE.Color('#cc66ff'),
        },
        opacity: {
          type: 'f',
          value: 1,
        },
      },
      vertexShaderSource: '\n  precision lowp float;\n  precision lowp int;\n  '
        .concat(
          THREE.ShaderChunk.fog_pars_vertex,
          '\n  varying vec2 vUv;\n  void main() {\n    vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );\n    vUv = uv;\n    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);\n    ',
        )
        .concat(THREE.ShaderChunk.fog_vertex, '\n  }\n'),
      fragmentShaderSource: `
		                #if __VERSION__ == 100
		                 #extension GL_OES_standard_derivatives : enable
		                #endif
		                uniform vec3 color;
		                uniform float opacity;
		                uniform float time;
		                varying vec2 vUv;
		                #define pi 3.1415926535
		                #define PI2RAD 0.01745329252
		                #define TWO_PI (2. * PI)
		                float rands(float p){
		                    return fract(sin(p) * 10000.0);
		                }
		                float noise(vec2 p){
		                    float t = time / 20000.0;
		                    if(t > 1.0) t -= floor(t);
		                    return rands(p.x * 14. + p.y * sin(t) * 0.5);
		                }
		                vec2 sw(vec2 p){
		                    return vec2(floor(p.x), floor(p.y));
		                }
		                vec2 se(vec2 p){
		                    return vec2(ceil(p.x), floor(p.y));
		                }
		                vec2 nw(vec2 p){
		                    return vec2(floor(p.x), ceil(p.y));
		                }
		                vec2 ne(vec2 p){
		                    return vec2(ceil(p.x), ceil(p.y));
		                }
		                float smoothNoise(vec2 p){
		                    vec2 inter = smoothstep(0.0, 1.0, fract(p));
		                    float s = mix(noise(sw(p)), noise(se(p)), inter.x);
		                    float n = mix(noise(nw(p)), noise(ne(p)), inter.x);
		                    return mix(s, n, inter.y);
		                }
		                float fbm(vec2 p){
		                    float z = 2.0;
		                    float rz = 0.0;
		                    vec2 bp = p;
		                    for(float i = 1.0; i < 6.0; i++){
		                    rz += abs((smoothNoise(p) - 0.5)* 2.0) / z;
		                    z *= 2.0;
		                    p *= 2.0;
		                    }
		                    return rz;
		                }
		                void main() {
		                    vec2 uv = vUv;
		                    vec2 uv2 = vUv;
		                    if (uv.y < 0.5) {
		                    discard;
		                    }
		                    uv *= 4.;
		                    float rz = fbm(uv);
		                    uv /= exp(mod(time * 2.0, pi));
		                    rz *= pow(15., 0.9);
		                    gl_FragColor = mix(vec4(color, opacity) / rz, vec4(color, 0.1), 0.2);
		                    if (uv2.x < 0.05) {
		                    gl_FragColor = mix(vec4(color, 0.1), gl_FragColor, uv2.x / 0.05);
		                    }
		                    if (uv2.x > 0.95){
		                    gl_FragColor = mix(gl_FragColor, vec4(color, 0.1), (uv2.x - 0.95) / 0.05);
		                    }
		                }`,
    };
    const material = new THREE.ShaderMaterial({
      uniforms: ElectricShield.uniforms,
      defaultAttributeValues: {},
      vertexShader: ElectricShield.vertexShaderSource,
      fragmentShader: ElectricShield.fragmentShaderSource,
      blending: THREE.AdditiveBlending,
      depthWrite: !1,
      depthTest: !0,
      side: THREE.DoubleSide,
      transparent: !0,
    });
    return material;
  }

  _animation() {
    const ball = this.getObject3d().children[0];
    const { speed } = this.getOptions();
    ball.material.uniforms.time.value += speed;
  }
}
