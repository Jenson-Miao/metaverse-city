
import * as maptalks from 'maptalks'
import * as THREE from 'three'
import buildings from '@/assets/data_buildings'
import buildingTexture from '@/assets/building.png'

/**
 * 建筑物
 * @type {{topColor: string, outLine: boolean, interactive: boolean}}
 */
const topColor = '#fff'
// https://maptalks.org/maptalks.js/api/0.x/CanvasLayer.html
var OPTIONS = {

  interactive: true, // 是否交互
  // topColor: topColor,
  outLine: true // 建筑物轮廓线
}
// 建筑贴图
const texture = new THREE.TextureLoader().load(buildingTexture)
texture.needsUpdate = true // 使用贴图时进行更新
texture.wrapS = texture.wrapT = THREE.RepeatWrapping
texture.repeat.set(1, 4)

// https://zhuanlan.zhihu.com/p/199353080
export default class Buildings {
  time = {
    value: 10
  }

  StartTime = {
    value: 10
  }

  isStart = true

  constructor ({ threeLayer, map }) {
    // 将Buildings中的数据，添加到features中
    var features = []
    this.meshes = []
    buildings.features.forEach(function (b) {
      features = features.concat(b)
    })
    this.material = this.getMaterial()
    // 看是否需要交互
    if (OPTIONS.interactive) {
      const highlightmaterial = this.getMaterial(2)
      const chooseMaterial = this.getMaterial(3)
      features.forEach((f) => {
        const geometry = maptalks.GeoJSON.toGeometry(f)
        const height = this.getHeight(geometry)
        let meterial = this.material
        if (['望京SOHO中心T1', '方恒国际B座', '望京储气罐'].includes(geometry.getProperties().name)) {
          meterial = highlightmaterial
        }

        const mesh = threeLayer.toExtrudePolygon(geometry, Object.assign(OPTIONS, { height }), meterial)
        // tooltip test
        mesh.setToolTip(height, {
          showTimeout: 0,
          eventsPropagation: true,
          dx: 10
        })

        // infowindow test
        mesh.setInfoWindow({
          content: 'hello world,height:' + height,
          title: 'message',
          animationDuration: 0,
          autoOpenOn: false
        })

        mesh.getInfoWindow().addTo(map);
        ((meterial) => {
          ['click', 'mousemove', 'mouseout', 'mouseover', 'mousedown', 'mouseup', 'dblclick', 'contextmenu'].forEach(function (eventType) {
            mesh.on(eventType, function (e) {
              if (e.type === 'mouseout') {
                this.setSymbol(meterial)
              }
              if (e.type === 'mouseover') {
                this.setSymbol(chooseMaterial)
              }
            })
          })
        })(meterial)
        if (topColor) {
          const topColor = new THREE.Color(OPTIONS.topColor)
          const bufferGeometry = mesh.getObject3d().geometry
          const geometry = new THREE.Geometry().fromBufferGeometry(bufferGeometry)
          const { vertices, faces, faceVertexUvs } = geometry
          for (let i = 0, len = faces.length; i < len; i++) {
            const { a, b, c } = faces[i]
            const p1 = vertices[a]; const p2 = vertices[b]; const p3 = vertices[c]
            // top face
            if (p1.z > 0 && p2.z > 0 && p3.z > 0) {
              const vertexColors = faces[i].vertexColors
              for (let j = 0, len1 = vertexColors.length; j < len1; j++) {
                vertexColors[j].r = topColor.r
                vertexColors[j].g = topColor.g
                vertexColors[j].b = topColor.b
              }
              const uvs = faceVertexUvs[0][i]
              for (let j = 0, len1 = uvs.length; j < len1; j++) {
                uvs[j].x = 0
                uvs[j].y = 0
              }
            }
          }
          mesh.getObject3d().geometry = new THREE.BufferGeometry().fromGeometry(geometry)
          bufferGeometry.dispose()
          geometry.dispose()
        }
        this.meshes.push(mesh)
      })
    } else {
      // 整体渲染
      var polygons = features.map(f => {
        const polygon = maptalks.GeoJSON.toGeometry(f)

        // var levels = f?.properties?.levels || 1
        polygon.setProperties({
          height: this.getHeight(polygon)
        })
        return polygon
      })
      const mesh = threeLayer.toExtrudePolygons(polygons, OPTIONS, this.material)
      // const mesh = new THREE.Mesh(polygons, this.material)
      if (topColor) {
        const topColor = new THREE.Color(OPTIONS.topColor)
        const bufferGeometry = mesh.getObject3d().geometry
        const geometry = new THREE.Geometry().fromBufferGeometry(bufferGeometry)
        const { vertices, faces, faceVertexUvs } = geometry
        for (let i = 0, len = faces.length; i < len; i++) {
          const { a, b, c } = faces[i]
          const p1 = vertices[a]; const p2 = vertices[b]; const p3 = vertices[c]
          // top face
          if (p1.z > 0 && p2.z > 0 && p3.z > 0) {
            const vertexColors = faces[i].vertexColors
            for (let j = 0, len1 = vertexColors.length; j < len1; j++) {
              vertexColors[j].r = topColor.r
              vertexColors[j].g = topColor.g
              vertexColors[j].b = topColor.b
            }
            const uvs = faceVertexUvs[0][i]
            for (let j = 0, len1 = uvs.length; j < len1; j++) {
              uvs[j].x = 0
              uvs[j].y = 0
            }
          }
        }
        mesh.getObject3d().geometry = new THREE.BufferGeometry().fromGeometry(geometry)
        bufferGeometry.dispose()
        geometry.dispose()
      }

      this.meshes.push(mesh)
    }
    // this.meshes._type = 'building'
    threeLayer.addMesh(this.meshes)
  }

  getHeight (polygon) {
    let height = (Math.random() + 0.1) * 200
    // 因没有高层数据。 这个地方手动突出地标建筑高度
    if (polygon.getProperties().name === '望京SOHO中心T1') {
      height = 400
    }
    if (polygon.getProperties().name === '方恒国际B座') {
      height = 400
    }
    if (polygon.getProperties().name === '望京储气罐') {
      height = 500
    }
    return height
  }

  /**
   *
   * @param type 1：普通材质，2：高亮材质，3：选中材质
   * @returns {THREE.MeshPhongMaterial}
   */
  getMaterial (type = 1) {
    // var Shader = {
    //   vertexShader: '\nvarying vec3 vp;\nvoid main(){\nvp = position;\ngl_Position=projectionMatrix * modelViewMatrix * vec4(position, 1.0);}',
    //   fragmentShader: `
    //     varying vec3 vp;
    //     uniform vec3 u_color;
    //     uniform vec3 u_tcolor;
    //     uniform float u_r;
    //     uniform float u_length;
    //     uniform float u_max;
    //     float getLeng(float x, float y){
    //         return  sqrt((x-0.0)*(x-0.0)+(y-0.0)*(y-0.0));
    //     }
    //     void main(){
    //         float uOpacity = 0.3;
    //         vec3 vColor = u_color;
    //         float uLength = getLeng(vp.x,vp.z);
    //         if ( uLength <= u_r && uLength > u_r - u_length ) {
    //             float op = sin( (u_r - uLength) / u_length ) ;
    //             uOpacity = op;
    //             if( vp.y<0.0){
    //                 vColor = u_color * op;
    //             }else{
    //                 vColor = u_tcolor;
    //             };
    //         }
    //         gl_FragColor = vec4(vColor,uOpacity);
    //     }
    // `
    // }
    // MeshPhongMaterial ShaderMaterial
    let config
    switch (type) {
      case 1:
        config = {
          // vertexShader: Shader.vertexShader,
          // fragmentShader: Shader.fragmentShader,
          // side: THREE.DoubleSide,
          // uniforms: {
          //   u_color: { value: new THREE.Color('#5588aa') },
          //   u_tcolor: { value: new THREE.Color('#f55c1a') },
          //   u_r: { value: 0.25 },
          //   u_length: { value: 20 }, // 扫过区域
          //   u_max: { value: 300 }// 扫过最大值
          // },
          map: texture,
          transparent: false // 是否透明
          // color: '#fff'
        }
        break
      case 2:
        config = {
          map: texture,
          transparent: false, // 是否透明
          emissive: '#fff', // 材质的放射（光）颜色，基本上是不受其他光照影响的固有颜色。默认为黑色。
          emissiveMap: texture, // 设置放射（发光）贴图。默认值为null。放射贴图颜色由放射颜色和强度所调节。 如果你有一个放射贴图，请务必将放射颜色设置为黑色以外的其他颜色。
          emissiveIntensity: 1, // 放射光强度。调节发光颜色。默认为1。
          shininess: 100
        // color: '#fff'
        }
        break
      case 3:
        config = {
          transparent: false, // 是否透明
          emissive: '#fff',
          emissiveMap: texture,
          emissiveIntensity: 1,
          shininess: 100
        // color: '#fff'
        }
        break
    }

    const material = new THREE.MeshPhongMaterial(config)
    material.vertexColors = THREE.VertexColors
    return material
  }

  getGuiParams () {
    return {
      showBuilding: true,
      // color: this.material.color.getStyle(),
      opacity: 1
    }
  }

  guiFun (gui, params, threeLayer) {
    const _this = this
    if (!_this.material) return
    // 是否增加建筑
    gui.add(params, 'showBuilding').onChange(() => {
      if (params.showBuilding) {
        threeLayer.addMesh(_this.meshes)
      } else {
        threeLayer.removeMesh(_this.meshes)
      }
    }).name('建筑物')
    // 建筑颜色
    // gui.addColor(params, 'color').onChange(function () {
    //   _this.material.color.set(params.color)
    // }).name('建筑颜色')
    // 建筑透明度
    gui.add(params, 'opacity', 0, 1).onChange(function () {
      _this.material.opacity = params.opacity
    }).name('建筑透明度')
  }

  // _animation () {
  //   const dalte = 0.01
  //   this.material.uniforms.u_r.value += dalte * 10
  //   if (this.material.uniforms.u_r.value >= 60) {
  //     this.material.uniforms.u_r.value = 20
  //   }
  // }
}
