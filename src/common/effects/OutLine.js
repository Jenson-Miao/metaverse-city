
import { BaseObject } from 'maptalks.three'
import * as maptalks from 'maptalks'
import * as THREE from 'three'

/**
 * 建筑轮廓
 * @type {{altitude: number, interactive: boolean}}
 */

// default values
var OPTIONS = {
  altitude: 0,
  interactive: false
}
const showSurroundLine = false
// https://zhuanlan.zhihu.com/p/199353080
export default class OutLine {
  constructor ({ threeLayer }) {
    this.material = this.getMaterial()
    const allMeshs = threeLayer.getMeshes()
    this.meshes = []
    allMeshs.forEach(mesh => {
      if (mesh.type === 'ExtrudePolygon' || mesh.type === 'ExtrudePolygons') {
        const outLine = new OutLineObject(mesh, threeLayer, this.material)
        this.meshes.push(outLine)
      }
    })
    if (showSurroundLine) {
      threeLayer.addMesh(this.meshes)
    }
  }

  getMaterial () {
    // https://zhuanlan.zhihu.com/p/199353080
    const lineMaterial = new THREE.LineBasicMaterial({
      // 线的颜色
      color: 'rgb(15,159,190)',
      transparent: true,
      linewidth: 1,
      opacity: 0.7
      // depthTest: true,
    })
    // 解决z-flighting
    lineMaterial.polygonOffset = true
    lineMaterial.depthTest = true
    lineMaterial.polygonOffsetFactor = 1
    lineMaterial.polygonOffsetUnits = 1.0
    return lineMaterial
  }

  getGuiParams () {
    return this.material ? {
      surroundLine: showSurroundLine,
      lineColor: this.material.color.getStyle(),
      lineOpacity: this.material.opacity
    } : {}
  }

  guiFun (gui, params, threeLayer) {
    const _this = this
    if (!_this.material) return
    // 是否增加建筑轮廓线
    gui.add(params, 'surroundLine').onChange(() => {
      if (params.surroundLine) {
        threeLayer.addMesh(_this.meshes)
      } else {
        threeLayer.removeMesh(_this.meshes)
      }
    }).name('建筑物轮廓线')
    // 建筑边框线动态调整
    gui.addColor(params, 'lineColor').onChange(function () {
      _this.material.color.set(params.lineColor)
    }).name('轮廓线颜色')
    // 边框线的透明度
    gui.add(params, 'lineOpacity', 0, 1).onChange(function () {
      _this.material.opacity = params.lineOpacity
    }).name('轮廓线透明度')
  }

  _animation () {
    // const ball = this.getObject3d().children[0];
    // const { speed } = this.getOptions();
    // ball.material.uniforms.time.value += speed;
  }
}
class OutLineObject extends BaseObject {
  constructor (mesh, threeLayer, material) {
    const options = maptalks.Util.extend({}, OPTIONS, { layer: threeLayer })
    super()
    this._initOptions(options)
    const edges = new THREE.EdgesGeometry(mesh.getObject3d().geometry, 1)
    const lineS = new THREE.LineSegments(edges, material)
    this._createGroup()
    this.getObject3d().add(lineS)
    // set object3d position
    this.getObject3d().position.copy(mesh.getObject3d().position)
  }
}
