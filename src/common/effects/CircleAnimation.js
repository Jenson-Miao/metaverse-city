/* eslint-disable */
/**
 * 光圈-用于提示事件
 */
import * as maptalks from 'maptalks';
import * as THREE from 'three';
import { BaseObject } from 'maptalks.three';


const colorDic = { warning: '#ffff00', danger: '#ff2400', normal: '#90ee90' };
const showCircleAnimation = false
const lnglats = [{
  positioning:[116.4720069, 39.9789455],
  type:'normal'
},{
  positioning:[116.4766441, 39.9912067],
  type:'normal'
},{
  positioning:[116.4549013, 39.9756327],
  type:'warning'
},{
  positioning:[116.4599713, 39.9679038],
  type:'danger'
},{
  positioning:[116.4768056, 39.9612205],
  type:'danger'
}]
export default class CircleAnimation{
  constructor ({ threeLayer }) {
    const SIZE = 512;

    this.meshes = lnglats.map((lnglat) => {
      const canvas = document.createElement('canvas');
      canvas.width = canvas.height = SIZE;
      const ctx = canvas.getContext('2d');
      const material = this.getMaterial(canvas, ctx, SIZE, lnglat.type);
      const circle = new CircleAnimationObject(lnglat.positioning, {
        radius: 100,
      }, material, threeLayer);

      // tooltip test
      circle.setToolTip(`id:${circle.getId()}`, {
        showTimeout: 0,
        eventsPropagation: true,
        dx: 10,
      });

      // infowindow test
      circle.setInfoWindow({
        content: `id:${circle.getId()}`,
        title: 'message',
        animationDuration: 0,
        autoOpenOn: false,
      });

      return circle;
    });
    if(showCircleAnimation){

      threeLayer.addMesh(this.meshes);
    }
  }
  getMaterial(canvas, ctx, SIZE, reminderType) {
    let fillColor = colorDic[reminderType];
    console.log(reminderType,fillColor)
    let material;
    ctx.clearRect(0, 0, SIZE, SIZE);
    ctx.save();

    ctx.beginPath();
    ctx.fillStyle = fillColor;
    ctx.arc(SIZE / 2, SIZE / 2, 60, 0, Math.PI * 2);
    ctx.closePath();
    ctx.fill();

    ctx.strokeStyle = fillColor;
    ctx.lineWidth = 20;
    ctx.beginPath();
    ctx.arc(SIZE / 2, SIZE / 2, 120, 0, Math.PI * 2);
    ctx.closePath();
    ctx.stroke();

    ctx.shadowColor = '#fff';
    ctx.shadowBlur = 100;
    ctx.beginPath();
    ctx.arc(SIZE / 2, SIZE / 2, 200, 0, Math.PI * 2);
    ctx.closePath();
    ctx.stroke();

    ctx.restore();

    if (!material) {
      const texture = new THREE.Texture(canvas);
      texture.needsUpdate = true; // 使用贴图时进行更新
      material = new THREE.MeshPhongMaterial({
        map: texture,
        // side: THREE.DoubleSide,
        transparent: true,
      });
    } else {
      material.map.needsUpdate = true;
    }
    return material;
  }
  getGuiParams () {
    return  {
      showCircleAnimation
    }
  }

  guiFun (gui, params, threeLayer) {
    const _this = this
    // 是否增加光圈
    gui.add(params, 'showCircleAnimation').onChange(() => {
      if (params.showCircleAnimation) {
        threeLayer.addMesh(_this.meshes)
      } else {
        threeLayer.removeMesh(_this.meshes)
      }
    }).name('光圈提示')
  }
}




// default values
const OPTIONS = {
  radius: 100,
  altitude: 0,
};

 class CircleAnimationObject extends BaseObject {
  constructor(coordinate, options, material, layer) {
    options = maptalks.Util.extend({}, OPTIONS, options, { layer, coordinate });
    super();
    // Initialize internal configuration
    // https://github.com/maptalks/maptalks.three/blob/1e45f5238f500225ada1deb09b8bab18c1b52cf2/src/BaseObject.js#L135
    this._initOptions(options);
    const { altitude, radius } = options;
    // generate geometry
    const r = layer.distanceToVector3(radius, radius).x;
    const geometry = new THREE.CircleBufferGeometry(r, 50);

    // Initialize internal object3d
    // https://github.com/maptalks/maptalks.three/blob/1e45f5238f500225ada1deb09b8bab18c1b52cf2/src/BaseObject.js#L140
    this._createMesh(geometry, material);

    // set object3d position
    const z = layer.distanceToVector3(altitude, altitude).x;
    const position = layer.coordinateToVector3(coordinate, z);
    this.getObject3d().position.copy(position);
    this._scale = 1;
    // this.getObject3d().rotation.x = -Math.PI;
  }

  // test animation
  _animation() {
    this._scale = (this._scale > 1 ? 0 : this._scale);
    this._scale += 0.03;
    this.getObject3d().scale.set(this._scale, this._scale, this._scale);
  }
}
