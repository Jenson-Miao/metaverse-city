/* eslint-disable*/
/**
 * 发光圆锥+跳动效果
 */
import * as THREE from 'three';
import * as maptalks from 'maptalks';
import { BaseObject } from 'maptalks.three';

const OPTIONS = {
  radius: 0.2, // 圆锥底部的半径 0.2
  height: 0.4, // 圆锥的高度 0.4
  radialSegments: 4, // 圆锥侧面周围的分段数
  defaultZ: 1, // z轴为0的时候起作用
  jumpSpeed: 0.05, // 跳动速度
  // ...
};
const lnglats = [{
  positioning:[116.4720069, 39.9789455],
  type:'normal'
},{
  positioning:[116.4766441, 39.9912067],
  type:'normal'
},{
  positioning:[116.4549013, 39.9756327],
  type:'warning'
},{
  positioning:[116.4599713, 39.9679038],
  type:'danger'
},{
  positioning:[116.4768056, 39.9612205],
  type:'danger'
}]
const showConeJump = false

export default class ConeJump {
  constructor({threeLayer}){
    this.meshes = []
    lnglats.forEach(item=>{
      const mesh = new ConeJumpObject(item.positioning,{},threeLayer,item.type)
      this.meshes.push(mesh)
    })
    if(showConeJump){
      threeLayer.addMesh(this.meshes)
    }
  }
  getGuiParams () {
    return  {
      showConeJump
    }
  }

  guiFun (gui, params, threeLayer) {
    const _this = this
    // 是否增加跳跃圆锥提示
    gui.add(params, 'showConeJump').onChange(() => {
      if (params.showConeJump) {
        threeLayer.addMesh(_this.meshes)
      } else {
        threeLayer.removeMesh(_this.meshes)
      }
    }).name('圆锥提示')
  }

}
const colorDic = { warning: '#ffff00', danger: '#ff2400', normal: '#90ee90' };


let flag = 0;

class ConeJumpObject extends BaseObject {
  constructor(coordinate, options, layer, colorType) {
    options = maptalks.Util.extend({}, OPTIONS, options, {
      layer,
      coordinate,
    });
    super();
    this._initOptions(options);

    const {
      radius,
      height,
      radialSegments,
      defaultZ,
    } = options;

    const geometry = new THREE.ConeGeometry(radius, height, radialSegments);
    const colorValue = colorDic[colorType];

    const material = new THREE.MeshBasicMaterial({
      color: colorValue,
      transparent: true,
      opacity: 0.8,
      side: THREE.DoubleSide,
      depthWrite: false,
      lightMapIntensity: 100,
      wireframeLinecap: 'square',
      // wireframe: true // 将几何体渲染为线框
      // vertexColors: THREE.VertexColors
    });

    const pointLight = new THREE.PointLight('red', 3, 5);
    const cone = new THREE.Mesh(geometry, material);
    cone.add(pointLight);
    this._createGroup();
    this.getObject3d().add(cone);

    const v = layer.coordinateToVector3(coordinate, 0);
    v.z === 0 ? v.z = defaultZ : 0;
    this.getObject3d().position.copy(v);
    this.getObject3d().rotation.x = -Math.PI / 2;
  }

  _animation() {
    this.getObject3d().children.forEach((children) => {
      children.rotation.y += 0.1; // 旋转

      // 到达零界点改变运动轨迹
      if (Math.floor(children.position.y) === -2) { // 顶部
        flag = 1;
      } else if (Math.floor(children.position.y) === 0) { // 底部
        flag = 0;
      }

      if (flag === 1) {
        children.position.setY(children.position.y + OPTIONS.jumpSpeed);
      } else {
        children.position.setY(children.position.y - OPTIONS.jumpSpeed);
      }
    });
  }
}
