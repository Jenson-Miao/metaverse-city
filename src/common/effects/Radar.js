import * as THREE from 'three'

const fragBasic = 'precision mediump float;' +
  '\n\nfloat atan2(float y, float x){' +
  '\n float t0, t1, t2, t3, t4;' +
  '\n  t3 = abs(x);\n  t1 = abs(y);' +
  '\n  t0 = max(t3, t1);' +
  '\n  t1 = min(t3, t1);' +
  '\n  t3 = float(1) / t0;' +
  '\n  t3 = t1 * t3;' +
  '\n  t4 = t3 * t3;' +
  '\n  t0 = -float(0.013480470);' +
  '\n  t0 = t0 * t4 + float(0.057477314);' +
  '\n  t0 = t0 * t4 - float(0.121239071);' +
  '\n  t0 = t0 * t4 + float(0.195635925);' +
  '\n  t0 = t0 * t4 - float(0.332994597);' +
  '\n  t0 = t0 * t4 + float(0.999995630);' +
  '\n  t3 = t0 * t3;' +
  '\n  t3 = (abs(y) > abs(x)) ? float(1.570796327) - t3 : t3;' +
  '\n  t3 = (x < 0.0) ?  float(3.141592654) - t3 : t3;' +
  '\n  t3 = (y < 0.0) ? -t3 : t3;' +
  '\n  return t3;\n}\n// 计算距离\nfloat distanceTo(vec2 src, vec2 dst) {' +
  '\n  floatdx =src.x-dst.x;' +
  '\n float dy = src.y - dst.y;' +
  '\n float dv = dx * dx + dy * dy;' +
  '\n return sqrt(dv);\n}\n#define PI 3.14159265359\n#define PI2 6.28318530718\nuniform vec3 u_color;\nuniform float time;\nuniform float u_opacity;\nuniform float u_radius;\nuniform float u_width;\nuniform float u_speed;\nvarying vec2 v_position;'
const Shader = {
  vertexShader: `
    varying vec2 v_position;

    void main() {
        v_position = vec2(position.x, position.y);
        gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    }`,
  fragmentShader: `
    ${fragBasic}
    void main() {
        float d_time = u_speed * time;
        float angle = atan2(v_position.x, v_position.y) + PI;

        float angleT = mod(angle + d_time, PI2);
        float width = u_width;

        float d_opacity = 0.0;
        // 当前位置离中心位置
        float length = distanceTo(vec2(0.0, 0.0), v_position);

        float bw = 5.0;
        if (length < u_radius && length > u_radius - bw) {
            float o = (length - (u_radius - bw)) / bw;
            d_opacity = sin(o * PI);
        }
        if (length < u_radius - bw / 1.1) {
            d_opacity = 1.0 - angleT / PI * (PI / width);
        }
        if (length > u_radius) { d_opacity = 0.0; }

        gl_FragColor = vec4(u_color, d_opacity * u_opacity);
    }`
}
export default function (opts) {
  const {
    radius = 50,
    color = '#fff',
    speed = 1,
    opacity = 1,
    angle = Math.PI,
    position = {
      x: 0,
      y: 0,
      z: 0
    },
    rotation = {
      x: -Math.PI / 2,
      y: 0,
      z: 0
    }
  } = opts

  const width = radius * 2

  const geometry = new THREE.PlaneBufferGeometry(width, width, 1, 1)

  const material = new THREE.ShaderMaterial({
    uniforms: {
      u_radius: {
        value: radius
      },
      u_speed: {
        value: speed
      },
      u_opacity: {
        value: opacity
      },
      u_width: {
        value: angle
      },
      u_color: {
        value: new THREE.Color(color)
      },
      time: {
        value: 0
      }
    },
    transparent: true,
    depthWrite: false,
    side: THREE.DoubleSide,
    vertexShader: Shader.vertexShader,
    fragmentShader: Shader.fragmentShader
  })

  const mesh = new THREE.Mesh(geometry, material)

  mesh.rotation.set(rotation.x, rotation.y, rotation.z)
  mesh.position.copy(position)

  return mesh
}
