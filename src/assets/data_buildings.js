/* eslint-disable import/prefer-default-export */
const buildings = {
  type: 'FeatureCollection',
  name: 'wangjing_buildings',
  crs: { type: 'name', properties: { name: 'urn:ogc:def:crs:OGC:1.3:CRS84' } },
  features: [
    {
      type: 'Feature',
      properties: {
        osm_id: '108380301', code: 1500, fclass: 'building', name: '佳境天成', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4701514, 39.9787829], [116.4701753, 39.9793893], [116.4703295, 39.9794246], [116.4704363, 39.9793747], [116.4704634, 39.9792764], [116.4705717, 39.9792772], [116.4706267, 39.9788407], [116.4704953, 39.9788068], [116.4704982, 39.9787168], [116.4701514, 39.9787829]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '108381020', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4635531, 39.9851466], [116.4637474, 39.9853379], [116.4640452, 39.9851605], [116.4638509, 39.9849691], [116.4636967, 39.985061], [116.4635531, 39.9851466]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '108381022', code: 1500, fclass: 'building', name: 'Wangjing Tower A', type: 'office'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4638434, 39.9845582], [116.4639407, 39.9846408], [116.4640629, 39.9847446], [116.4642922, 39.9845862], [116.4640727, 39.9843997], [116.4638434, 39.9845582]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '128976660', code: 1500, fclass: 'building', name: 'Ausotel Dayu Beijing', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4695511, 39.978152], [116.4696048, 39.9782958], [116.4700125, 39.9781931], [116.4700769, 39.978304], [116.4703612, 39.9784233], [116.4704148, 39.978452], [116.470565, 39.9782712], [116.4703451, 39.9781807], [116.4702539, 39.9780204], [116.4701949, 39.977967], [116.4695511, 39.978152]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '128977563', code: 1500, fclass: 'building', name: 'Courtyard by Marriott Beijing Northeast', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4686724, 39.9768628], [116.4687409, 39.9769471], [116.4696743, 39.9779917], [116.4698745, 39.9778189], [116.4694472, 39.9774319], [116.469351, 39.9772373], [116.4693162, 39.9772504], [116.4691445, 39.9769039], [116.4690935, 39.9768812], [116.469056, 39.9769285], [116.4687368, 39.9767785], [116.4686724, 39.9768628]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '129621692', code: 1500, fclass: 'building', name: 'Bldg. 1', type: 'hospital'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4790792, 39.9727681], [116.4794974, 39.9731599], [116.4797839, 39.9729803], [116.479665, 39.9728689], [116.4795523, 39.9729396], [116.479253, 39.9726592], [116.4790792, 39.9727681]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '129621693', code: 1500, fclass: 'building', name: 'Bldg. 2', type: 'hospital'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4784574, 39.9738092], [116.4785995, 39.9739414], [116.479491, 39.9733787], [116.4793489, 39.9732465], [116.4784574, 39.9738092]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '129621694', code: 1500, fclass: 'building', name: 'Gatehouse – Dental', type: 'hospital'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.479532, 39.9735187], [116.4796372, 39.9736163], [116.4799015, 39.9734491], [116.4797963, 39.9733515], [116.4796865, 39.973421], [116.4796077, 39.9734708], [116.479532, 39.9735187]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '129621695', code: 1500, fclass: 'building', name: 'Family Medicine', type: 'hospital'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4797907, 39.9733302], [116.479889, 39.9734253], [116.4802793, 39.9731884], [116.480181, 39.9730933], [116.4797907, 39.9733302]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '136146034', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4766493, 39.9753261], [116.476767, 39.9754412], [116.4774231, 39.9750106], [116.4774409, 39.9749358], [116.4774183, 39.9748645], [116.477357, 39.9748221], [116.4771834, 39.9749409], [116.4771842, 39.9749854], [116.4766493, 39.9753261]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '137116654', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4735371, 40.0017432], [116.4744227, 40.0017464], [116.4744234, 40.0016279], [116.4735378, 40.0016247], [116.4735371, 40.0017432]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '137116655', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4746207, 40.0016898], [116.4754253, 40.0016816], [116.4754189, 40.0015532], [116.4746214, 40.0015594], [116.4746207, 40.0016898]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '137116656', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4729362, 40.0022569], [116.4742387, 40.0022518], [116.4742291, 40.0021172], [116.4729416, 40.0021213], [116.4729362, 40.0022569]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '142658380', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4713799, 39.9762628], [116.4717942, 39.9766258], [116.4721732, 39.9763717], [116.4718682, 39.9761045], [116.4717589, 39.9760087], [116.4713799, 39.9762628]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '142658381', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4719054, 39.975811], [116.4719615, 39.9758607], [116.4721153, 39.9760005], [116.4722736, 39.9761528], [116.4721701, 39.9762221], [116.472252, 39.976294], [116.4723935, 39.9761993], [116.4725628, 39.976086], [116.4724224, 39.9759628], [116.4725047, 39.9759071], [116.4724588, 39.9758663], [116.4723777, 39.9759235], [116.4721208, 39.9756804], [116.4719054, 39.975811]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '142658382', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4722366, 39.9756691], [116.4724588, 39.9758663], [116.4725047, 39.9759071], [116.4726403, 39.9760275], [116.472996, 39.9757923], [116.4725922, 39.9754339], [116.4722366, 39.9756691]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '142658383', code: 1500, fclass: 'building', name: 'Crowne Plaza Lido Hotel', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4718869, 39.9767502], [116.4726384, 39.9774388], [116.4730581, 39.9771576], [116.4730397, 39.9769418], [116.4729395, 39.9768996], [116.472395, 39.976411], [116.4718869, 39.9767502]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '142658385', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4717589, 39.9760087], [116.4718682, 39.9761045], [116.4719651, 39.976037], [116.4720057, 39.9760739], [116.4721153, 39.9760005], [116.4719615, 39.9758607], [116.4717599, 39.9760018], [116.4717589, 39.9760087]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '142658386', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4723935, 39.9761993], [116.4726724, 39.9764385], [116.4733273, 39.9760147], [116.4734736, 39.9761475], [116.4732047, 39.9763216], [116.4733601, 39.9764627], [116.4736527, 39.9762733], [116.4738176, 39.9764229], [116.4734401, 39.9766672], [116.4735848, 39.9767985], [116.4741603, 39.976426], [116.4733721, 39.9757108], [116.4726573, 39.9761734], [116.4725628, 39.976086], [116.4723935, 39.9761993]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '142658387', code: 1500, fclass: 'building', name: 'Metropark Lido Hotel', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4727592, 39.9766103], [116.4729296, 39.9767692], [116.4731616, 39.9767912], [116.4733332, 39.9769351], [116.4735103, 39.9768323], [116.4733426, 39.9766623], [116.4733761, 39.9765281], [116.4732206, 39.9763595], [116.4729872, 39.9763555], [116.4728102, 39.9764705], [116.4727592, 39.9766103]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '169612330', code: 1500, fclass: 'building', name: '中化', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4476663, 39.9807904], [116.4477775, 39.980893], [116.447805, 39.9808755], [116.4478533, 39.9809201], [116.4480012, 39.9808262], [116.4478418, 39.9806789], [116.4476663, 39.9807904]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '169688482', code: 1500, fclass: 'building', name: '111号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4488466, 39.9998129], [116.4488556, 40.0000915], [116.449214, 40.0000847], [116.449205, 39.9998061], [116.4488466, 39.9998129]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '169688483', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4501294, 40.0004371], [116.4501515, 40.0007316], [116.4505063, 40.000716], [116.4504842, 40.0004215], [116.4501294, 40.0004371]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '169688484', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4482332, 39.9994111], [116.4482349, 39.999714], [116.4486363, 39.9997126], [116.4486346, 39.9994098], [116.4482332, 39.9994111]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '169688485', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4495341, 40.0001406], [116.449541, 40.0004234], [116.4499185, 40.0004181], [116.4499117, 40.0001352], [116.4495341, 40.0001406]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '170246461', code: 1500, fclass: 'building', name: '望京华彩商业中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.462027, 40.0097597], [116.4637751, 40.0097716], [116.4637829, 40.0091033], [116.4620348, 40.0090914], [116.462027, 40.0097597]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '170294793', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4492445, 39.9990437], [116.4492492, 39.9993818], [116.4496167, 39.9993788], [116.449612, 39.9990407], [116.4492445, 39.9990437]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '170294794', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4500232, 39.9990423], [116.4500278, 39.9993797], [116.4503945, 39.9993768], [116.4503899, 39.9990393], [116.4500232, 39.9990423]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '190470568', code: 1500, fclass: 'building', name: '颐堤港', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4844382, 39.9694022], [116.4845281, 39.9695451], [116.4847043, 39.9696029], [116.485965, 39.9695916], [116.4859788, 39.9693204], [116.486226, 39.9688992], [116.4862991, 39.9686431], [116.4862695, 39.9683809], [116.4861726, 39.9681658], [116.4862556, 39.9680415], [116.4862385, 39.9679145], [116.4858534, 39.9677919], [116.4856794, 39.9677329], [116.485369, 39.9676277], [116.4849708, 39.9681599], [116.484674, 39.9685717], [116.4845059, 39.9689732], [116.4844477, 39.9692453], [116.4844382, 39.9694022]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '219923469', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5010617, 39.9922951], [116.5017054, 39.9923609], [116.5017, 39.9924308], [116.5020434, 39.9924554], [116.5022244, 39.9918174], [116.5018824, 39.9917734], [116.5019361, 39.9915432], [116.5017322, 39.9914939], [116.5014587, 39.9915063], [116.5014104, 39.9916131], [116.5012441, 39.9918227], [116.5012012, 39.9920321], [116.5011797, 39.9921719], [116.5010617, 39.9922951]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '221268083', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4737805, 40.0031277], [116.4737946, 40.0032331], [116.4743498, 40.0031896], [116.4743357, 40.0030842], [116.4737805, 40.0031277]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '221268084', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4745597, 40.0030318], [116.4745754, 40.0031394], [116.4754072, 40.0030681], [116.4753915, 40.0029606], [116.4745597, 40.0030318]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '221268085', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4729336, 40.0031951], [116.4729488, 40.0033018], [116.4736867, 40.00324], [116.4736715, 40.0031334], [116.4729336, 40.0031951]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224835303', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.460458, 39.9873007], [116.460753, 39.9876049], [116.4609837, 39.9874898], [116.460694, 39.9871815], [116.460458, 39.9873007]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224835304', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4608818, 39.9877405], [116.461276, 39.9881638], [116.4613565, 39.9881227], [116.4614772, 39.9882419], [116.4622899, 39.9877549], [116.4621692, 39.9876234], [116.4614611, 39.9880323], [116.4610856, 39.9876316], [116.4608818, 39.9877405]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224835305', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4612197, 39.9871692], [116.4615469, 39.9874939], [116.4617562, 39.9873911], [116.4614236, 39.9870582], [116.4612197, 39.9871692]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224835306', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4619493, 39.9873254], [116.4622443, 39.9876419], [116.4624374, 39.9875391], [116.4621585, 39.9872103], [116.4619493, 39.9873254]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224838021', code: 1500, fclass: 'building', name: 'A', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4654509, 39.9835233], [116.4655387, 39.9835284], [116.4656078, 39.9835323], [116.4657519, 39.9835406], [116.4658002, 39.9835434], [116.466013, 39.9835556], [116.4660273, 39.9834096], [116.4659604, 39.9834057], [116.4659628, 39.9833816], [116.4658578, 39.9833756], [116.465855, 39.9834046], [116.4655452, 39.9833867], [116.4655387, 39.9834529], [116.4654583, 39.9834483], [116.4654509, 39.9835233]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224838022', code: 1500, fclass: 'building', name: 'B', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4650982, 39.98377], [116.4651936, 39.983776], [116.4652609, 39.9837802], [116.4657255, 39.9838094], [116.4657768, 39.9838126], [116.4659485, 39.9838234], [116.4659619, 39.9836652], [116.4657889, 39.9836542], [116.4657436, 39.9836514], [116.465499, 39.9836358], [116.4654213, 39.9836309], [116.4652162, 39.9836179], [116.4652162, 39.9836796], [116.4651036, 39.9836755], [116.4650982, 39.98377]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224838023', code: 1500, fclass: 'building', name: 'C', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4653718, 39.9840392], [116.4656952, 39.9840601], [116.4657464, 39.9840634], [116.4659136, 39.9840742], [116.4659324, 39.9839118], [116.4657681, 39.9839026], [116.465722, 39.9839], [116.4653852, 39.983881], [116.4653718, 39.9840392]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224838024', code: 1500, fclass: 'building', name: 'D', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.464771, 39.9840043], [116.4648507, 39.9840084], [116.4649233, 39.9840122], [116.4653262, 39.9840331], [116.4653423, 39.9838769], [116.4651427, 39.9838658], [116.4650674, 39.9838616], [116.4648605, 39.9838493], [116.4648568, 39.9839057], [116.4647817, 39.9838995], [116.464771, 39.9840043]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224838025', code: 1500, fclass: 'building', name: 'E', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4651116, 39.9842961], [116.4656105, 39.984329], [116.4656051, 39.9844153], [116.4657473, 39.9844236], [116.4657473, 39.9843372], [116.4658921, 39.9843455], [116.4659109, 39.9841687], [116.4657407, 39.9841594], [116.4656849, 39.9841563], [116.465125, 39.9841256], [116.4651116, 39.9842961]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224838026', code: 1500, fclass: 'building', name: 'F', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.464433, 39.984253], [116.465058, 39.9842941], [116.4650652, 39.9842154], [116.4650684, 39.9841803], [116.4650741, 39.9841173], [116.4647918, 39.9841002], [116.4646974, 39.9840945], [116.4645322, 39.9840845], [116.4645269, 39.9841626], [116.4644384, 39.9841605], [116.464433, 39.984253]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224838027', code: 1500, fclass: 'building', name: 'Siemens Limited China', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4663709, 39.9843681], [116.4669932, 39.9844112], [116.4670468, 39.984218], [116.4670415, 39.9840043], [116.4669744, 39.9837638], [116.4667893, 39.9835666], [116.4664675, 39.9834124], [116.4664567, 39.9835111], [116.4663709, 39.9843681]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224838028', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4658358, 39.9850935], [116.4660531, 39.9851079], [116.4660906, 39.9848243], [116.466214, 39.9848346], [116.466265, 39.9843393], [116.4660236, 39.984329], [116.4660101, 39.98444], [116.4658975, 39.98444], [116.4658358, 39.9850935]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224839732', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4646974, 39.9840945], [116.4647918, 39.9841002], [116.4649233, 39.9840122], [116.4648507, 39.9840084], [116.4646974, 39.9840945]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224839733', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4650652, 39.9842154], [116.4651776, 39.9842256], [116.4651803, 39.9841866], [116.4650684, 39.9841803], [116.4650652, 39.9842154]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224839734', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4650674, 39.9838616], [116.4651427, 39.9838658], [116.4652609, 39.9837802], [116.4651936, 39.983776], [116.4650674, 39.9838616]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224839736', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.465722, 39.9839], [116.4657681, 39.9839026], [116.4657768, 39.9838126], [116.4657255, 39.9838094], [116.465722, 39.9839]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224839737', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4654213, 39.9836309], [116.465499, 39.9836358], [116.4656078, 39.9835323], [116.4655387, 39.9835284], [116.4654213, 39.9836309]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845781', code: 1500, fclass: 'building', name: '花家地北里5号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4588051, 39.9859298], [116.4591645, 39.9862422], [116.460122, 39.9863162], [116.4604815, 39.9861004], [116.4603313, 39.9860059], [116.4601328, 39.9861374], [116.4592906, 39.9860839], [116.4589687, 39.9858312], [116.4588051, 39.9859298]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845783', code: 1500, fclass: 'building', name: '花家地北里6号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4594676, 39.9859383], [116.4595652, 39.9860351], [116.4600762, 39.985736], [116.4602657, 39.9859075], [116.4604322, 39.9858151], [116.4602082, 39.9856172], [116.4600245, 39.985604], [116.4594676, 39.9859383]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845785', code: 1500, fclass: 'building', name: '花家地北里7号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4590427, 39.9857404], [116.4591575, 39.9858459], [116.4597719, 39.985472], [116.4598293, 39.985494], [116.4598925, 39.98556], [116.4600532, 39.9854632], [116.4599326, 39.9853576], [116.4597317, 39.98534], [116.4590427, 39.9857404]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845787', code: 1500, fclass: 'building', name: 'A108', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4597627, 39.9886797], [116.4600868, 39.988985], [116.4602836, 39.9888623], [116.4599596, 39.9885571], [116.4597627, 39.9886797]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845788', code: 1500, fclass: 'building', name: 'A110', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4601275, 39.9890489], [116.4602556, 39.98916], [116.4608914, 39.9887637], [116.4607584, 39.9886488], [116.4601275, 39.9890489]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845790', code: 1500, fclass: 'building', name: 'A115', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4608165, 39.9889197], [116.4611974, 39.9892714], [116.4613645, 39.9891651], [116.4609836, 39.9888134], [116.4608165, 39.9889197]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845792', code: 1500, fclass: 'building', name: 'A116', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4604315, 39.9896535], [116.4605481, 39.9897596], [116.4612022, 39.9893379], [116.4610857, 39.9892318], [116.4604315, 39.9896535]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845793', code: 1500, fclass: 'building', name: 'A111', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4599049, 39.9892026], [116.4602961, 39.9895571], [116.4604437, 39.9894615], [116.4600525, 39.989107], [116.4599049, 39.9892026]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845795', code: 1500, fclass: 'building', name: 'A112', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4603569, 39.9892736], [116.460478, 39.9893847], [116.46089, 39.989121], [116.4607689, 39.98901], [116.4603569, 39.9892736]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845797', code: 1500, fclass: 'building', name: 'A107', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4595226, 39.988517], [116.4596481, 39.9886301], [116.460305, 39.9882025], [116.4601795, 39.9880893], [116.4595226, 39.988517]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845799', code: 1500, fclass: 'building', name: 'A106', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4596143, 39.9877793], [116.4599806, 39.9881184], [116.4601556, 39.9880074], [116.4597893, 39.9876683], [116.4596143, 39.9877793]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845801', code: 1500, fclass: 'building', name: 'A105', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4592653, 39.9882841], [116.4593698, 39.9883821], [116.4598233, 39.9880983], [116.4597188, 39.9880003], [116.4592653, 39.9882841]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845802', code: 1500, fclass: 'building', name: 'A103', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4590061, 39.9880407], [116.4591062, 39.9881392], [116.4596314, 39.9878259], [116.4595313, 39.9877274], [116.4590061, 39.9880407]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845804', code: 1500, fclass: 'building', name: 'A102', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.458756, 39.9881559], [116.4590525, 39.9884524], [116.4592174, 39.9883545], [116.4589228, 39.9880619], [116.458756, 39.9881559]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845805', code: 1500, fclass: 'building', name: 'A109', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4591375, 39.988508], [116.4594446, 39.9887904], [116.4595982, 39.9886923], [116.4592911, 39.9884099], [116.4591375, 39.988508]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845807', code: 1500, fclass: 'building', name: 'A101', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4585267, 39.9879677], [116.4586208, 39.9880505], [116.4590922, 39.9877496], [116.4592914, 39.9877518], [116.459324, 39.987656], [116.4590291, 39.9876518], [116.4585267, 39.9879677]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845809', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4584475, 39.9889539], [116.458556, 39.9890553], [116.4590312, 39.9887567], [116.4589227, 39.9886554], [116.4584475, 39.9889539]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845810', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4580072, 39.9885926], [116.4583482, 39.9888953], [116.4585162, 39.9887843], [116.4581752, 39.9884816], [116.4580072, 39.9885926]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845812', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.458123, 39.9882277], [116.4586834, 39.9886818], [116.4588574, 39.9885732], [116.4583163, 39.9881093], [116.458123, 39.9882277]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845814', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.457685, 39.9885436], [116.4578718, 39.9886275], [116.4582405, 39.9883613], [116.458098, 39.9882454], [116.457685, 39.9885436]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845815', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4574948, 39.9889108], [116.4582306, 39.9891485], [116.4583194, 39.9889871], [116.4575837, 39.9887494], [116.4574948, 39.9889108]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845817', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4580857, 39.9896106], [116.4582739, 39.9896687], [116.4585064, 39.9892271], [116.4583182, 39.989169], [116.4580857, 39.9896106]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845820', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4575368, 39.9894024], [116.4579942, 39.9895504], [116.4580844, 39.9893925], [116.4576141, 39.9892494], [116.4575368, 39.9894024]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845821', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4571568, 39.9895652], [116.4573822, 39.9896294], [116.4576463, 39.9890371], [116.4574338, 39.9889829], [116.4571568, 39.9895652]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845822', code: 1500, fclass: 'building', name: 'B206', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4613436, 39.9904867], [116.4616679, 39.9908027], [116.4618271, 39.9907068], [116.4615027, 39.9903908], [116.4613436, 39.9904867]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845823', code: 1500, fclass: 'building', name: 'B209', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4618231, 39.9906831], [116.4619207, 39.9907719], [116.4625795, 39.9903466], [116.462482, 39.9902579], [116.4618231, 39.9906831]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845825', code: 1500, fclass: 'building', name: 'B207', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4616261, 39.9904075], [116.4617352, 39.9905083], [116.462156, 39.990241], [116.4620469, 39.9901402], [116.4616261, 39.9904075]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845827', code: 1500, fclass: 'building', name: 'B208', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4619725, 39.9899519], [116.4623174, 39.9902566], [116.462464, 39.9901591], [116.4621191, 39.9898545], [116.4619725, 39.9899519]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845828', code: 1500, fclass: 'building', name: 'B201', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4609337, 39.9901896], [116.4611302, 39.9903554], [116.4612772, 39.9902531], [116.4610806, 39.9900873], [116.4609337, 39.9901896]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845830', code: 1500, fclass: 'building', name: 'B205', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4612767, 39.9900895], [116.4613831, 39.9901823], [116.4620179, 39.9897551], [116.4619115, 39.9896623], [116.4612767, 39.9900895]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845833', code: 1500, fclass: 'building', name: 'B202', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.460753, 39.9899695], [116.4608745, 39.9900828], [116.4615343, 39.9896676], [116.4614128, 39.9895543], [116.460753, 39.9899695]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845834', code: 1500, fclass: 'building', name: 'B203', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4615067, 39.9895343], [116.4616638, 39.9896839], [116.46181, 39.9895938], [116.4616529, 39.9894442], [116.4615067, 39.9895343]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845837', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4629242, 39.9875339], [116.4629325, 39.9878603], [116.4633267, 39.9878543], [116.4633183, 39.987528], [116.4629242, 39.9875339]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845838', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4629921, 39.9870088], [116.4633891, 39.987014], [116.4633957, 39.9867203], [116.4629987, 39.986715], [116.4629921, 39.9870088]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845841', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4636869, 39.987539], [116.4640758, 39.9875421], [116.4640797, 39.9872546], [116.4636908, 39.9872515], [116.4636869, 39.987539]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845842', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4635401, 39.9878672], [116.4640683, 39.9878718], [116.4640704, 39.9877279], [116.4635422, 39.9877234], [116.4635401, 39.9878672]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '224845844', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4644827, 39.9894262], [116.4644827, 39.9895783], [116.4652928, 39.9895742], [116.4653035, 39.9894221], [116.4644827, 39.9894262]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '228097688', code: 1500, fclass: 'building', name: 'Hairun', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4757845, 39.9752214], [116.4759052, 39.9753283], [116.4766831, 39.9748267], [116.476557, 39.9747116], [116.4764256, 39.9748021], [116.4763424, 39.9747363], [116.4761145, 39.9748822], [116.4761976, 39.9749562], [116.4761037, 39.9750199], [116.4760179, 39.974946], [116.4758087, 39.9750857], [116.4758865, 39.9751536], [116.4757845, 39.9752214]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '229639113', code: 1500, fclass: 'building', name: 'Wangjing Tower Annex', type: 'office'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4636448, 39.9849531], [116.4636967, 39.985061], [116.4638509, 39.9849691], [116.463879, 39.9849138], [116.4639022, 39.9846878], [116.4639407, 39.9846408], [116.4638434, 39.9845582], [116.4636582, 39.9846896], [116.4636448, 39.9849531]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '229843886', code: 1500, fclass: 'building', name: '南湖中园119', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4531431, 40.0023339], [116.4538445, 40.0026993], [116.4539644, 40.0025643], [116.453263, 40.0021988], [116.4531431, 40.0023339]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '229843887', code: 1500, fclass: 'building', name: '南湖中园118', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4522242, 40.0016426], [116.4523002, 40.0018625], [116.4529806, 40.002222], [116.4530861, 40.0021132], [116.4524548, 40.0017556], [116.4524066, 40.0015994], [116.4522242, 40.0016426]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '234383857', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4818446, 39.9826094], [116.4828648, 39.9826199], [116.4828667, 39.9825106], [116.4818465, 39.9825001], [116.4818446, 39.9826094]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '234384423', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4824284, 39.9822573], [116.482997, 39.982266], [116.482999, 39.9821879], [116.4824304, 39.9821792], [116.4824284, 39.9822573]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '234384424', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4812955, 39.9825075], [116.4816717, 39.9828448], [116.4815894, 39.9828988], [116.4817499, 39.9830427], [116.4819107, 39.9829374], [116.4819082, 39.9828964], [116.4817746, 39.9827766], [116.4817967, 39.9827621], [116.4814211, 39.9824253], [116.4812955, 39.9825075]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '234384425', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4814993, 39.9822573], [116.482033, 39.9822634], [116.4820384, 39.9822367], [116.4823925, 39.9822429], [116.4823978, 39.9821483], [116.482033, 39.9821401], [116.4820357, 39.9821586], [116.4815059, 39.9821437], [116.4814993, 39.9822573]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '234390623', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4810116, 39.9826842], [116.4813849, 39.9830153], [116.481521, 39.9829252], [116.4811477, 39.9825941], [116.4810116, 39.9826842]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '234390901', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4806811, 39.9829019], [116.4810721, 39.9832327], [116.481184, 39.983155], [116.480793, 39.9828243], [116.4806811, 39.9829019]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '237584821', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4466749, 39.9766787], [116.44684, 39.9768543], [116.447094, 39.9767141], [116.4471476, 39.9767711], [116.4472468, 39.9767164], [116.4469725, 39.9764245], [116.4468821, 39.9764744], [116.4469378, 39.9765336], [116.4466749, 39.9766787]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '239157891', code: 1500, fclass: 'building', name: 'C8', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4866086, 39.9883967], [116.4898988, 39.9884488], [116.4899064, 39.9881676], [116.4866161, 39.9881155], [116.4866086, 39.9883967]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '241408770', code: 1500, fclass: 'building', name: '望京西园319号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4670576, 39.9978545], [116.4673302, 39.9981277], [116.4677005, 39.9979127], [116.4674326, 39.9976381], [116.4670576, 39.9978545]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '241408942', code: 1500, fclass: 'building', name: '望京西园320号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4673302, 39.9981277], [116.4675531, 39.9983532], [116.4679235, 39.9981382], [116.4677005, 39.9979127], [116.4673302, 39.9981277]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '241409175', code: 1500, fclass: 'building', name: '望京西园321号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4677557, 39.9985226], [116.4680256, 39.998768], [116.468369, 39.9985506], [116.4681034, 39.9983011], [116.4677557, 39.9985226]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '241410647', code: 1500, fclass: 'building', name: '望京西园323号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.468924, 39.9998219], [116.4692086, 39.9998355], [116.4692355, 39.999505], [116.4689508, 39.9994914], [116.468924, 39.9998219]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '241410648', code: 1500, fclass: 'building', name: '望京西园323号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4682676, 39.999188], [116.4683903, 39.9993077], [116.4684986, 39.9994133], [116.4688188, 39.9992206], [116.4685878, 39.9989954], [116.4682676, 39.999188]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '241410650', code: 1500, fclass: 'building', name: '望京西园322号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4680256, 39.998768], [116.4682654, 39.9989902], [116.4686088, 39.9987727], [116.468369, 39.9985506], [116.4680256, 39.998768]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '241752062', code: 1500, fclass: 'building', name: '9号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.458909, 40.0048579], [116.4589098, 40.0050572], [116.4594569, 40.0050558], [116.459456, 40.0048565], [116.458909, 40.0048579]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '241752065', code: 1500, fclass: 'building', name: '8号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4581411, 40.0048697], [116.4581418, 40.0050615], [116.4587331, 40.0050602], [116.4587324, 40.0048684], [116.4581411, 40.0048697]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '242224113', code: 1500, fclass: 'building', name: '7号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.457084, 40.0050532], [116.4572482, 40.0051892], [116.4574794, 40.0050211], [116.4579584, 40.0050203], [116.4579638, 40.0048559], [116.4573587, 40.0048483], [116.457084, 40.0050532]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '242312467', code: 1500, fclass: 'building', name: '望京西园328号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4671, 39.999542], [116.4673682, 39.9998171], [116.4676515, 39.9996584], [116.4673737, 39.9993803], [116.4671, 39.999542]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '242312469', code: 1500, fclass: 'building', name: '望京西园330号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4662853, 39.9988179], [116.4665543, 39.9990782], [116.4668578, 39.9988941], [116.4665888, 39.9986338], [116.4662853, 39.9988179]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '242312470', code: 1500, fclass: 'building', name: '望京西园329号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4666207, 39.9991789], [116.4668808, 39.999447], [116.467237, 39.9992442], [116.466977, 39.9989761], [116.4666207, 39.9991789]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '242312471', code: 1500, fclass: 'building', name: '望京西园327号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4673682, 39.9998171], [116.4676115, 40.0000721], [116.4678949, 39.9999134], [116.4676515, 39.9996584], [116.4673682, 39.9998171]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '242551934', code: 1500, fclass: 'building', name: '4号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4584871, 40.0063032], [116.4592623, 40.0063042], [116.459264, 40.0061571], [116.4584889, 40.0061561], [116.4584871, 40.0063032]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '242551935', code: 1500, fclass: 'building', name: '5号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4577114, 40.0063017], [116.4583725, 40.0063072], [116.4583746, 40.0061532], [116.4577136, 40.0061476], [116.4577114, 40.0063017]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244002112', code: 1500, fclass: 'building', name: '望京花园122号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4637163, 40.0056028], [116.4637171, 40.0057685], [116.4639351, 40.0057678], [116.4639343, 40.0056021], [116.4637163, 40.0056028]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244002113', code: 1500, fclass: 'building', name: '望京花园121号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4639255, 40.0054652], [116.4641411, 40.005473], [116.4641531, 40.0052783], [116.4639375, 40.0052705], [116.4639255, 40.0054652]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244002116', code: 1500, fclass: 'building', name: '#120', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4641334, 40.0050346], [116.4641334, 40.005199], [116.4649711, 40.0052039], [116.4649703, 40.005388], [116.4643373, 40.0053756], [116.4643319, 40.0055195], [116.4649695, 40.0055316], [116.4650985, 40.0053793], [116.4650905, 40.0051579], [116.4649542, 40.005051], [116.4641334, 40.0050346]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244002117', code: 1500, fclass: 'building', name: '望京花园123号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4639025, 40.0060785], [116.4640962, 40.0060797], [116.4640979, 40.0059124], [116.4639043, 40.0059112], [116.4639025, 40.0060785]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244002118', code: 1500, fclass: 'building', name: '望京花园119号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4634638, 40.0051078], [116.4637784, 40.0051115], [116.4637762, 40.005222], [116.4639569, 40.0052241], [116.4639613, 40.0049979], [116.4634661, 40.0049921], [116.4634638, 40.0051078]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244002482', code: 1500, fclass: 'building', name: '#117', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4625174, 40.0049957], [116.4625182, 40.0052437], [116.4628328, 40.0052431], [116.462832, 40.0049951], [116.4625174, 40.0049957]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244002483', code: 1500, fclass: 'building', name: '#116', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4619234, 40.0052878], [116.4620914, 40.0052889], [116.4622209, 40.0051353], [116.4624462, 40.0051394], [116.4624462, 40.0049873], [116.4620385, 40.0049853], [116.4619274, 40.0050807], [116.4619234, 40.0052878]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244002486', code: 1500, fclass: 'building', name: '#118', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4628483, 40.004977], [116.4628528, 40.0052265], [116.4630848, 40.0052241], [116.4630828, 40.0051155], [116.4631575, 40.0051147], [116.4631549, 40.0049738], [116.4628483, 40.004977]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244002488', code: 1500, fclass: 'building', name: '#115', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4619204, 40.0055523], [116.4621299, 40.0055537], [116.4622416, 40.0055545], [116.4622447, 40.0052899], [116.4620914, 40.0052889], [116.4619234, 40.0052878], [116.4619204, 40.0055523]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '244010337', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4911217, 39.9960044], [116.4913261, 39.9961838], [116.4914859, 39.996077], [116.4912816, 39.9958976], [116.4911217, 39.9960044]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '255736506', code: 1500, fclass: 'building', name: '人濟大廈', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4686898, 39.9766769], [116.4688826, 39.9767387], [116.4689127, 39.9766835], [116.4688226, 39.9766439], [116.4687198, 39.9766218], [116.4686898, 39.9766769]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '256668610', code: 1500, fclass: 'building', name: '东芝医疗器械', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4991557, 39.9796633], [116.4991599, 39.9799422], [116.5000181, 39.9799346], [116.5000139, 39.9796557], [116.4991557, 39.9796633]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '256668688', code: 1500, fclass: 'building', name: '中国电子科技集团11研究所', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4981714, 39.9813217], [116.4981779, 39.9818012], [116.4996407, 39.9817895], [116.4996342, 39.98131], [116.4981714, 39.9813217]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '264412048', code: 1500, fclass: 'building', name: '锅炉房', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.457571, 39.9797336], [116.4586614, 39.97974], [116.4586434, 39.9791063], [116.4582525, 39.9792015], [116.4579179, 39.9793316], [116.457581, 39.9793784], [116.457571, 39.9797336]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '264412330', code: 1500, fclass: 'building', name: '家乐福', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4541355, 39.9777914], [116.4542157, 39.9780703], [116.4545797, 39.9781816], [116.4554854, 39.9784307], [116.4557658, 39.9779245], [116.45601, 39.9774213], [116.4554713, 39.9772375], [116.4547833, 39.9770544], [116.4542335, 39.977419], [116.4541355, 39.9777914]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '264415710', code: 1500, fclass: 'building', name: '花家地为民农贸市场', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4599881, 39.9778576], [116.4607285, 39.9785009], [116.4609675, 39.9783624], [116.4610025, 39.9780541], [116.4604545, 39.9775404], [116.4599881, 39.9778576]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '271474595', code: 1500, fclass: 'building', name: '301号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4687282, 39.9948714], [116.4691409, 39.9951854], [116.4699223, 39.9952268], [116.470602, 39.9947717], [116.4704685, 39.9946546], [116.4698429, 39.9950735], [116.4692203, 39.9950418], [116.4688614, 39.9947687], [116.4687282, 39.9948714]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '271474596', code: 1500, fclass: 'building', name: '304号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4679089, 39.9941203], [116.4682991, 39.9944626], [116.4687152, 39.9944845], [116.4691217, 39.9942094], [116.468992, 39.9940969], [116.4686422, 39.9943337], [116.4684103, 39.9943239], [116.4680617, 39.9940181], [116.4679089, 39.9941203]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '271474597', code: 1500, fclass: 'building', name: '305号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4670974, 39.9934095], [116.4674828, 39.9937375], [116.467883, 39.9937618], [116.4683058, 39.993477], [116.468179, 39.9933666], [116.4678163, 39.9936109], [116.4675685, 39.9935987], [116.4672348, 39.9933148], [116.4670974, 39.9934095]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '271474598', code: 1500, fclass: 'building', name: '首开广场', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4662369, 39.9921141], [116.4667706, 39.9925995], [116.4680044, 39.9918031], [116.4674052, 39.9912581], [116.4667284, 39.991695], [116.4667939, 39.9917546], [116.4662369, 39.9921141]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '271474599', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4659383, 39.9906646], [116.4661049, 39.9908126], [116.4665109, 39.9905444], [116.4663443, 39.9903964], [116.4659383, 39.9906646]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '271474600', code: 1500, fclass: 'building', name: '东亚望京中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4662321, 39.9909844], [116.4663987, 39.9911351], [116.4668254, 39.9908583], [116.4666588, 39.9907075], [116.4662321, 39.9909844]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '271475401', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4632979, 39.9924068], [116.4634355, 39.9925294], [116.4637668, 39.9923111], [116.4644379, 39.9923495], [116.4645658, 39.9924661], [116.464743, 39.992352], [116.4645459, 39.9921722], [116.4637167, 39.9921308], [116.4632979, 39.9924068]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '3624964', code: 1500, fclass: 'building', name: '首都师范大学附属中学', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4648443, 39.9936611], [116.4649509, 39.9937603], [116.4650172, 39.9937185], [116.4653195, 39.9937409], [116.4653284, 39.9936706], [116.4654202, 39.9936774], [116.465429, 39.9936081], [116.465526, 39.9936153], [116.4658399, 39.9934243], [116.4656503, 39.9932414], [116.4656221, 39.9932585], [116.4654034, 39.9930474], [116.4649539, 39.9933208], [116.4650172, 39.9933819], [116.464979, 39.9934051], [116.4649512, 39.9936022], [116.4648538, 39.9935942], [116.4648443, 39.9936611]], [[116.4649779, 39.9935973], [116.4650011, 39.9934409], [116.465102, 39.9934497], [116.465466, 39.9932224], [116.4655534, 39.9933046], [116.4655393, 39.9933134], [116.4655076, 39.9935276], [116.4654004, 39.9935183], [116.46539, 39.9935889], [116.4653019, 39.9935813], [116.4652916, 39.9936516], [116.4649923, 39.9936256], [116.4649962, 39.9935989], [116.4649779, 39.9935973]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '272729931', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4506836, 40.0001789], [116.4506875, 40.0003006], [116.4511153, 40.0003016], [116.4511534, 40.0003347], [116.4511494, 40.0004554], [116.4512623, 40.0004544], [116.4512623, 40.0004312], [116.4513069, 40.0004322], [116.4513095, 40.0003639], [116.4512675, 40.0003659], [116.4512675, 40.0003026], [116.4513082, 40.0002704], [116.4511967, 40.000188], [116.4511494, 40.0002201], [116.4510733, 40.0002181], [116.4510746, 40.0001779], [116.4506836, 40.0001789]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '272729932', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4506967, 40.0009308], [116.4506993, 40.0010515], [116.4508411, 40.0010505], [116.4508397, 40.0010826], [116.451236, 40.0010816], [116.4513122, 40.0010213], [116.4513122, 40.0009208], [116.4513555, 40.0009178], [116.4513541, 40.0008434], [116.4512951, 40.0008434], [116.4512977, 40.0008233], [116.451198, 40.0008253], [116.4512006, 40.0008484], [116.4511809, 40.0008524], [116.4511836, 40.0009057], [116.4512019, 40.0009067], [116.4512046, 40.000956], [116.4511403, 40.0009942], [116.4510917, 40.0009952], [116.451093, 40.000955], [116.4508253, 40.000961], [116.4508201, 40.0009258], [116.4506967, 40.0009308]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '272729933', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4506613, 39.9994516], [116.4506665, 39.9995723], [116.450992, 39.9995602], [116.4512229, 39.9995642], [116.4512229, 39.9994597], [116.4506613, 39.9994516]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '272729934', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4506403, 39.9999382], [116.4511547, 39.9999301], [116.4512439, 39.9998738], [116.4512544, 39.9996808], [116.4511284, 39.9996889], [116.4511232, 39.9997773], [116.4510655, 39.9998175], [116.4506455, 39.9998135], [116.4506403, 39.9999382]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '272729935', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4507665, 40.0005592], [116.4507673, 40.0006743], [116.4509111, 40.0006737], [116.4509114, 40.000713], [116.4513211, 40.0007114], [116.4513203, 40.0005873], [116.4509106, 40.0005889], [116.4508923, 40.000589], [116.4508921, 40.0005586], [116.4507665, 40.0005592]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '272730019', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4493088, 39.9984295], [116.4493162, 39.9988091], [116.4504501, 39.9988176], [116.4504557, 39.9984309], [116.4493088, 39.9984295]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '281972818', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4506685, 40.0028371], [116.4506746, 40.003015], [116.4512532, 40.0030033], [116.451247, 40.0028254], [116.4506685, 40.0028371]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '282200143', code: 1500, fclass: 'building', name: '望京储气罐', type: 'industrial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4413823, 39.9904173], [116.4413823, 39.9915598], [116.441672, 39.9920119], [116.4429916, 39.9920284], [116.4429809, 39.9904173], [116.4413823, 39.9904173]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '295970757', code: 1500, fclass: 'building', name: '228号楼', type: 'house'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4543278, 39.9967053], [116.4543339, 39.9969036], [116.4550549, 39.9969036], [116.4550303, 39.9967053], [116.4543278, 39.9967053]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '295970870', code: 1500, fclass: 'building', name: '227号楼', type: 'house'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4533234, 39.9966865], [116.4533234, 39.9969036], [116.4541614, 39.9969178], [116.4541737, 39.9967006], [116.4533234, 39.9966865]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297128592', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4434774, 39.9728194], [116.4434837, 39.9730944], [116.4438476, 39.9730895], [116.4438413, 39.9728145], [116.4434774, 39.9728194]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297128593', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4440965, 39.9727958], [116.4441041, 39.9730759], [116.444472, 39.97307], [116.4444644, 39.97279], [116.4440965, 39.9727958]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297128594', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4436976, 39.9731277], [116.4436982, 39.9732314], [116.4437416, 39.9732313], [116.4437421, 39.9733088], [116.4441467, 39.9733073], [116.4441465, 39.9732743], [116.4442179, 39.9732741], [116.4442176, 39.9732364], [116.4442792, 39.9732362], [116.4442785, 39.9731256], [116.4436976, 39.9731277]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297128596', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4430265, 39.972383], [116.4430307, 39.9726654], [116.4434049, 39.9726621], [116.4434007, 39.9723797], [116.4430265, 39.972383]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297128597', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4426378, 39.9719331], [116.4426414, 39.9722209], [116.4430122, 39.9722182], [116.4430086, 39.9719304], [116.4426378, 39.9719331]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297128598', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4445204, 39.9723896], [116.4445232, 39.9726687], [116.444896, 39.9726665], [116.4448931, 39.9723874], [116.4445204, 39.9723896]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297128599', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4434435, 39.9717657], [116.4434442, 39.9720023], [116.4445289, 39.9720004], [116.4445282, 39.9717639], [116.4434435, 39.9717657]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314730', code: 1500, fclass: 'building', name: 'Daimler Tower', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4754862, 39.9926418], [116.4756681, 39.9932281], [116.4760098, 39.9931523], [116.4758233, 39.9925768], [116.4754862, 39.9926418]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314731', code: 1500, fclass: 'building', name: 'Microsoft Tower', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4766719, 39.9928534], [116.476869, 39.9934289], [116.4771914, 39.9933641], [116.4769942, 39.9927886], [116.4766719, 39.9928534]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314732', code: 1500, fclass: 'building', name: 'Caterpillar Tower', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4762247, 39.9921128], [116.4764151, 39.9926936], [116.4767397, 39.9926312], [116.4765492, 39.9920504], [116.4762247, 39.9921128]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314733', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4788141, 39.9904206], [116.4795018, 39.9904808], [116.4795246, 39.9903283], [116.4788368, 39.9902681], [116.4788141, 39.9904206]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314734', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4795719, 39.9900031], [116.4802253, 39.9900618], [116.4802488, 39.9899084], [116.4795954, 39.9898498], [116.4795719, 39.9900031]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314735', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4803096, 39.9896493], [116.4806429, 39.9896721], [116.4806602, 39.9895234], [116.4803269, 39.9895006], [116.4803096, 39.9896493]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314736', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4807385, 39.9900217], [116.4810679, 39.9900409], [116.4810826, 39.989893], [116.4807532, 39.9898737], [116.4807385, 39.9900217]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314737', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4811637, 39.9903596], [116.4815261, 39.9903843], [116.4815431, 39.9902376], [116.4811808, 39.9902129], [116.4811637, 39.9903596]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314738', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4815867, 39.9907394], [116.4819374, 39.9907632], [116.4819543, 39.9906162], [116.4816037, 39.9905924], [116.4815867, 39.9907394]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314739', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4819667, 39.9910881], [116.4823074, 39.9911142], [116.4823263, 39.9909693], [116.4819856, 39.9909432], [116.4819667, 39.9910881]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314740', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4822034, 39.9917246], [116.4825252, 39.9917704], [116.4825616, 39.9916211], [116.4822396, 39.9915753], [116.4822034, 39.9917246]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314741', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4815191, 39.992065], [116.4823862, 39.9921366], [116.4824657, 39.992081], [116.4825261, 39.9917639], [116.4823437, 39.9917405], [116.482322, 39.9918488], [116.4815566, 39.9917922], [116.4815191, 39.992065]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314743', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4797547, 39.991302], [116.4801101, 39.9913324], [116.4801328, 39.9911767], [116.4797774, 39.9911463], [116.4797547, 39.991302]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314744', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4803165, 39.9910687], [116.4810083, 39.9911268], [116.48103, 39.9909754], [116.4803382, 39.9909172], [116.4803165, 39.9910687]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297314745', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4811343, 39.9914085], [116.4814881, 39.9914375], [116.4815085, 39.9912912], [116.4811547, 39.9912622], [116.4811343, 39.9914085]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315477', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4673234, 39.9844685], [116.4677141, 39.9848202], [116.4678391, 39.9847387], [116.4674483, 39.984387], [116.4673234, 39.9844685]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315478', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4676513, 39.9842758], [116.4681308, 39.9847145], [116.468126, 39.9847847], [116.4678184, 39.9849764], [116.4679188, 39.9850709], [116.4682949, 39.9848365], [116.4683022, 39.9846923], [116.4677662, 39.984202], [116.4676513, 39.9842758]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315479', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4669945, 39.9846836], [116.4673855, 39.9850385], [116.4674971, 39.9849663], [116.4671061, 39.9846115], [116.4669945, 39.9846836]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315480', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4666663, 39.9848872], [116.467071, 39.9852408], [116.4671827, 39.9851657], [116.466778, 39.9848122], [116.4666663, 39.9848872]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315481', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4673972, 39.9858316], [116.4677006, 39.9861175], [116.4678004, 39.9860552], [116.4675509, 39.9858202], [116.4675436, 39.9857697], [116.4676383, 39.9857061], [116.4675479, 39.9856271], [116.4673996, 39.9857267], [116.4673972, 39.9858316]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315482', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4676539, 39.985564], [116.4680327, 39.9859075], [116.4681412, 39.9858372], [116.4677624, 39.9854938], [116.4676539, 39.985564]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315483', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4679677, 39.9853638], [116.4683259, 39.9856945], [116.4684358, 39.9856246], [116.4680776, 39.9852939], [116.4679677, 39.9853638]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315484', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4682845, 39.9851519], [116.4686531, 39.9854886], [116.4687618, 39.9854187], [116.4683933, 39.985082], [116.4682845, 39.9851519]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315485', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4685702, 39.9849594], [116.4689335, 39.985311], [116.4691035, 39.9852068], [116.4686807, 39.984891], [116.4685702, 39.9849594]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315486', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4690316, 39.9853328], [116.4694937, 39.9857734], [116.4696477, 39.9856816], [116.4690966, 39.9852803], [116.4690316, 39.9853328]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315487', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4677731, 39.9861872], [116.4681621, 39.986524], [116.4685771, 39.9862426], [116.4682051, 39.9859205], [116.4681122, 39.9859835], [116.4683636, 39.9862012], [116.4681494, 39.9863464], [116.467881, 39.986114], [116.4677731, 39.9861872]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315488', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4683756, 39.9857629], [116.4688794, 39.9861978], [116.4689994, 39.9861162], [116.4684956, 39.9856813], [116.4683756, 39.9857629]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315489', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4686973, 39.9855399], [116.4692014, 39.9859877], [116.4693153, 39.9859125], [116.4688112, 39.9854646], [116.4686973, 39.9855399]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315490', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4697123, 39.9859261], [116.4702347, 39.986274], [116.4703428, 39.9861787], [116.4698204, 39.9858308], [116.4697123, 39.9859261]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315491', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4689524, 39.9863813], [116.4690654, 39.986482], [116.4696772, 39.986079], [116.4695642, 39.9859783], [116.4689524, 39.9863813]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '3954968', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4684209, 39.9865823], [116.4689064, 39.9870036], [116.4690194, 39.9869272], [116.468946, 39.9868634], [116.4690827, 39.986771], [116.4691076, 39.9867926], [116.4690732, 39.9868159], [116.4691243, 39.9868603], [116.4692454, 39.9867784], [116.4687536, 39.9863518], [116.4686373, 39.9864305], [116.4686855, 39.9864723], [116.4687168, 39.9864512], [116.4687401, 39.9864714], [116.4686162, 39.9865552], [116.4685482, 39.9864962], [116.4684209, 39.9865823]], [[116.4687591, 39.9866885], [116.4688909, 39.9866029], [116.468907, 39.9866175], [116.4688591, 39.9866485], [116.4689149, 39.9866989], [116.4689673, 39.9866648], [116.4690139, 39.9867069], [116.4688774, 39.9867955], [116.4687591, 39.9866885]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315494', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4700278, 39.9867842], [116.4701331, 39.9868782], [116.4705372, 39.9866124], [116.4704319, 39.9865184], [116.4700278, 39.9867842]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315495', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4697355, 39.9870826], [116.4698351, 39.9871712], [116.470076, 39.9870123], [116.4699765, 39.9869237], [116.4697355, 39.9870826]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315496', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4645183, 39.9890935], [116.4645194, 39.989203], [116.4652593, 39.9891988], [116.4652582, 39.9890893], [116.4645183, 39.9890935]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315497', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4645105, 39.9887522], [116.4645108, 39.988851], [116.4652539, 39.9888495], [116.4652535, 39.9887508], [116.4645105, 39.9887522]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315498', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4645149, 39.9885037], [116.4652705, 39.9885043], [116.4652706, 39.9884046], [116.464515, 39.988404], [116.4645149, 39.9885037]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315499', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4645096, 39.9880509], [116.464511, 39.9881578], [116.4652716, 39.9881518], [116.4652702, 39.9880449], [116.4645096, 39.9880509]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315500', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4654176, 39.9891023], [116.4654176, 39.9891964], [116.4659748, 39.9891964], [116.4659748, 39.9891023], [116.4654176, 39.9891023]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315501', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4654198, 39.988848], [116.4661796, 39.9888493], [116.4661798, 39.9887498], [116.4654201, 39.9887486], [116.4654198, 39.988848]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315502', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.465425, 39.9883993], [116.4654253, 39.9885014], [116.4661796, 39.9884999], [116.4661793, 39.9883978], [116.465425, 39.9883993]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315503', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4654335, 39.9880579], [116.4654351, 39.9881571], [116.4661823, 39.9881501], [116.4661807, 39.9880508], [116.4654335, 39.9880579]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297315504', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4649032, 39.9873751], [116.4654513, 39.987856], [116.4655444, 39.9877937], [116.4649963, 39.9873128], [116.4649032, 39.9873751]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316335', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4639095, 39.9854103], [116.4644676, 39.9859029], [116.464587, 39.9858235], [116.4640288, 39.985331], [116.4639095, 39.9854103]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316336', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4642526, 39.9851832], [116.4648134, 39.985681], [116.4649325, 39.9856022], [116.4643718, 39.9851044], [116.4642526, 39.9851832]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316337', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4645889, 39.9849607], [116.4651482, 39.9854599], [116.4652664, 39.9853822], [116.4647071, 39.9848829], [116.4645889, 39.9849607]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316338', code: 1500, fclass: 'building', name: '西八间北里', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.464475, 39.9860229], [116.4649182, 39.9864142], [116.4650496, 39.9863269], [116.4646064, 39.9859355], [116.464475, 39.9860229]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316339', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4648409, 39.9857856], [116.4652781, 39.9861926], [116.4654138, 39.986107], [116.4649767, 39.9857], [116.4648409, 39.9857856]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316340', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4652113, 39.9855417], [116.4656685, 39.9859418], [116.4657718, 39.9858725], [116.4653146, 39.9854724], [116.4652113, 39.9855417]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316341', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4650083, 39.9864672], [116.4653214, 39.9867517], [116.4654232, 39.986686], [116.4651101, 39.9864015], [116.4650083, 39.9864672]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316342', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4653786, 39.9867328], [116.4654605, 39.986809], [116.4656333, 39.9867001], [116.4655513, 39.9866238], [116.4653786, 39.9867328]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316343', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4656411, 39.9865681], [116.4657252, 39.9866441], [116.465931, 39.9865104], [116.4658468, 39.9864344], [116.4656411, 39.9865681]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316344', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4657304, 39.9859895], [116.4661307, 39.986354], [116.4659585, 39.986465], [116.4660429, 39.9865418], [116.4662001, 39.9864405], [116.4663288, 39.9863575], [116.4658441, 39.9859161], [116.4657304, 39.9859895]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316345', code: 1500, fclass: 'building', name: '4号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4658347, 39.985592], [116.4663899, 39.9860931], [116.4665105, 39.9860146], [116.4659554, 39.9855135], [116.4658347, 39.985592]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316346', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4668851, 39.988235], [116.4673385, 39.9886578], [116.4674607, 39.9885809], [116.4670072, 39.9881581], [116.4668851, 39.988235]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316347', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4662519, 39.9877094], [116.4667456, 39.9881538], [116.4668486, 39.9880867], [116.4663549, 39.9876423], [116.4662519, 39.9877094]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316348', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4665837, 39.987497], [116.4670728, 39.9879413], [116.4671716, 39.9878774], [116.4666825, 39.9874332], [116.4665837, 39.987497]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316349', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4669087, 39.987291], [116.4673954, 39.9877215], [116.4674918, 39.9876575], [116.4670051, 39.987227], [116.4669087, 39.987291]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316350', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4671445, 39.9869888], [116.4677791, 39.9875324], [116.4678845, 39.9874601], [116.4672499, 39.9869166], [116.4671445, 39.9869888]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316351', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.465613, 39.987094], [116.4659936, 39.9874322], [116.4661284, 39.9873431], [116.4657478, 39.9870049], [116.465613, 39.987094]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316352', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4660572, 39.9873935], [116.4661845, 39.9875093], [116.4662865, 39.9874435], [116.4661592, 39.9873276], [116.4660572, 39.9873935]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316353', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4661657, 39.9876297], [116.4662298, 39.9876865], [116.4665027, 39.9875061], [116.4664387, 39.9874492], [116.4661657, 39.9876297]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316354', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4695389, 39.9819043], [116.4701183, 39.9824341], [116.4705809, 39.9821371], [116.470436, 39.9820047], [116.4701427, 39.9821929], [116.4697082, 39.9817956], [116.4695389, 39.9819043]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316355', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4701723, 39.9815121], [116.4707724, 39.9820435], [116.4709119, 39.9819509], [116.4703118, 39.9814196], [116.4701723, 39.9815121]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316356', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4709784, 39.9817292], [116.4711545, 39.9817386], [116.4711959, 39.9817761], [116.4713644, 39.9816705], [116.471369, 39.9815332], [116.4712373, 39.981417], [116.4711928, 39.981451], [116.4710075, 39.9814428], [116.4709784, 39.9817292]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316357', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4685105, 39.9839258], [116.4701144, 39.9851002], [116.471229, 39.9843479], [116.4708582, 39.9840263], [116.4698607, 39.9847], [116.469069, 39.9841211], [116.470114, 39.983396], [116.4697485, 39.9830781], [116.4685105, 39.9839258]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316359', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4704364, 39.9832984], [116.4708262, 39.9836189], [116.47078, 39.9836519], [116.471072, 39.983892], [116.4714211, 39.9836428], [116.4707393, 39.9830822], [116.4704364, 39.9832984]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316360', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4699706, 39.9829646], [116.4702984, 39.9832633], [116.470437, 39.9831739], [116.4701093, 39.9828752], [116.4699706, 39.9829646]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316361', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4702528, 39.9827456], [116.470348, 39.9828301], [116.4705015, 39.9827286], [116.4707309, 39.982743], [116.4708449, 39.9828461], [116.470951, 39.9827772], [116.4707873, 39.9826291], [116.4704507, 39.9826147], [116.4702528, 39.9827456]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316362', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4713427, 39.9828085], [116.4716195, 39.9830527], [116.4717357, 39.9829754], [116.4717401, 39.9828381], [116.4716733, 39.9828369], [116.4716525, 39.982813], [116.4715798, 39.982813], [116.4715613, 39.9828253], [116.4714565, 39.9827328], [116.4713427, 39.9828085]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316597', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4708673, 39.9823396], [116.4711934, 39.9826212], [116.4713744, 39.9824981], [116.4712829, 39.982419], [116.4712576, 39.9824296], [116.4712186, 39.9823961], [116.4712347, 39.9823697], [116.4712301, 39.9823381], [116.4711979, 39.9823099], [116.4712278, 39.9822431], [116.4711315, 39.9821599], [116.4708673, 39.9823396]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316598', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4712766, 39.9820669], [116.4714165, 39.9821911], [116.4714521, 39.9821676], [116.4716441, 39.9823379], [116.4717997, 39.982235], [116.4715882, 39.9820474], [116.4716429, 39.9820111], [116.4715224, 39.9819042], [116.4712766, 39.9820669]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316975', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.477993, 39.9976297], [116.4786334, 39.9976339], [116.4786348, 39.9975176], [116.4779943, 39.9975133], [116.477993, 39.9976297]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316976', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4786183, 39.9982511], [116.4795145, 39.9982269], [116.4795125, 39.9980769], [116.4786217, 39.998098], [116.4786183, 39.9982511]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316977', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4796155, 39.997699], [116.4796167, 39.9978216], [116.4802721, 39.9978179], [116.480271, 39.9976953], [116.4796155, 39.997699]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316978', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.480265, 39.9972217], [116.4809276, 39.9972234], [116.4809281, 39.9970981], [116.4802655, 39.9970964], [116.480265, 39.9972217]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316979', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4795445, 39.9967179], [116.4795454, 39.9968129], [116.4801629, 39.9968093], [116.480162, 39.9967144], [116.4795445, 39.9967179]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316980', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4786361, 39.9971181], [116.4786364, 39.9972217], [116.4795161, 39.9972201], [116.4795157, 39.9971164], [116.4786361, 39.9971181]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316981', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4774469, 39.9957591], [116.4774513, 39.9959064], [116.4780896, 39.9958953], [116.4780853, 39.9957481], [116.4774469, 39.9957591]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316982', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4779542, 39.9950349], [116.4785319, 39.9950434], [116.478535, 39.9949176], [116.4779574, 39.994909], [116.4779542, 39.9950349]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316983', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4785432, 39.9946293], [116.4793255, 39.9946364], [116.4793248, 39.9946814], [116.4799742, 39.9946872], [116.4799762, 39.9945624], [116.4793805, 39.994557], [116.4793812, 39.9945073], [116.4785452, 39.9944998], [116.4785432, 39.9946293]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316984', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4785765, 39.9959651], [116.4794303, 39.9959673], [116.4794309, 39.9958199], [116.4785771, 39.9958177], [116.4785765, 39.9959651]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297316985', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4794329, 39.9956294], [116.4799998, 39.995632], [116.4800008, 39.9954922], [116.4794339, 39.9954897], [116.4794329, 39.9956294]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317467', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4782837, 39.9935859], [116.4784541, 39.9936006], [116.4784883, 39.9933671], [116.4783179, 39.9933525], [116.4782837, 39.9935859]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317468', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4784051, 39.9937844], [116.4799073, 39.9939095], [116.4799276, 39.9937663], [116.4784255, 39.9936411], [116.4784051, 39.9937844]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317469', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4799054, 39.9937467], [116.4800842, 39.9937591], [116.4801168, 39.9934851], [116.479938, 39.9934726], [116.4799054, 39.9937467]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317470', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4782136, 39.9929578], [116.4783939, 39.992976], [116.4784176, 39.9928378], [116.4782373, 39.9928197], [116.4782136, 39.9929578]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317471', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4783396, 39.9931469], [116.4789203, 39.9932041], [116.4789423, 39.9930726], [116.4783617, 39.9930155], [116.4783396, 39.9931469]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317472', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4792071, 39.9932177], [116.4798445, 39.9932754], [116.479864, 39.9931489], [116.4792266, 39.9930912], [116.4792071, 39.9932177]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317473', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4785208, 39.9914416], [116.479065, 39.9915595], [116.4791511, 39.9913261], [116.4789552, 39.9912836], [116.478928, 39.9913573], [116.4785798, 39.9912818], [116.4785208, 39.9914416]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317474', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4784129, 39.9918405], [116.4784173, 39.9919802], [116.4784481, 39.9920845], [116.4784832, 39.9921148], [116.4790653, 39.9920643], [116.4790367, 39.9918624], [116.4789401, 39.9918725], [116.4789467, 39.9919331], [116.4785447, 39.991965], [116.4785249, 39.9918136], [116.4785469, 39.9918136], [116.4785425, 39.9917429], [116.4788066, 39.9917135], [116.4787911, 39.9916132], [116.4784415, 39.9916504], [116.4784239, 39.991721], [116.4784129, 39.9918405]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317475', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4784741, 39.9925371], [116.4787454, 39.9925543], [116.4787525, 39.9924892], [116.4790273, 39.9925066], [116.4790345, 39.9924398], [116.4793179, 39.9924578], [116.4793311, 39.9923355], [116.4790653, 39.9923187], [116.4790584, 39.9923827], [116.4787711, 39.9923645], [116.4787647, 39.9924243], [116.4784882, 39.9924068], [116.4784741, 39.9925371]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317476', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4795188, 39.9924306], [116.4798321, 39.9924631], [116.4798521, 39.9923506], [116.4797927, 39.9923444], [116.4798172, 39.9922061], [116.4796292, 39.9921866], [116.479604, 39.9923287], [116.4795381, 39.9923218], [116.4795188, 39.9924306]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317477', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4801598, 39.9961275], [116.4801619, 39.9962426], [116.4808654, 39.9962348], [116.4808633, 39.9961198], [116.4801598, 39.9961275]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317478', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4810364, 39.9963459], [116.4816352, 39.9963483], [116.4816361, 39.9962089], [116.4810374, 39.9962065], [116.4810364, 39.9963459]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317479', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4799425, 39.9927998], [116.4802742, 39.9928292], [116.4802919, 39.9927121], [116.4802162, 39.9927054], [116.4802375, 39.9925641], [116.4800595, 39.9925483], [116.480039, 39.9926836], [116.4799611, 39.9926767], [116.4799425, 39.9927998]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317480', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4806082, 39.9928678], [116.4809381, 39.9928929], [116.4809518, 39.9927869], [116.4808887, 39.9927821], [116.4809057, 39.9926512], [116.4807151, 39.9926367], [116.4806987, 39.9927633], [116.4806225, 39.9927575], [116.4806082, 39.9928678]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317481', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4810386, 39.9925543], [116.4813641, 39.9925805], [116.4813799, 39.9924649], [116.4813168, 39.9924598], [116.4813363, 39.9923172], [116.4811538, 39.9923025], [116.4811351, 39.992439], [116.4810553, 39.9924326], [116.4810386, 39.9925543]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317482', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4816846, 39.9926678], [116.4825281, 39.9927183], [116.4825414, 39.9925874], [116.481698, 39.9925369], [116.4816846, 39.9926678]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317483', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4823324, 39.9929715], [116.4825, 39.9929827], [116.4825275, 39.9927404], [116.48236, 39.9927293], [116.4823324, 39.9929715]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317484', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4808494, 39.9933111], [116.4814902, 39.9933634], [116.4815075, 39.9932392], [116.4808667, 39.9931869], [116.4808494, 39.9933111]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317485', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.480535, 39.993781], [116.4807167, 39.9937937], [116.4807487, 39.9935264], [116.4805669, 39.9935137], [116.480535, 39.993781]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317486', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.48069, 39.9939543], [116.4822098, 39.994086], [116.482231, 39.9939428], [116.4807112, 39.9938111], [116.48069, 39.9939543]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317487', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4821809, 39.9939321], [116.4823546, 39.9939472], [116.4823737, 39.9938177], [116.4822, 39.9938027], [116.4821809, 39.9939321]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317488', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.481805, 39.9933933], [116.4823413, 39.9934396], [116.4823608, 39.9933075], [116.4818245, 39.9932611], [116.481805, 39.9933933]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317489', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.482245, 39.9935962], [116.4824102, 39.9936094], [116.4824316, 39.9934525], [116.4822664, 39.9934393], [116.482245, 39.9935962]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317490', code: 1500, fclass: 'building', name: '新荟城', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4753883, 39.9972782], [116.4755547, 39.9974306], [116.4755258, 39.9974694], [116.4761445, 39.9980681], [116.4767379, 39.9976635], [116.4760142, 39.9969594], [116.4757501, 39.9971257], [116.4753883, 39.9972782]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317717', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4541188, 40.0027636], [116.4548187, 40.0031094], [116.4549159, 40.0029939], [116.454216, 40.0026481], [116.4541188, 40.0027636]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317718', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4552314, 40.0031368], [116.4552332, 40.0032625], [116.4561663, 40.0032546], [116.4561654, 40.0031923], [116.4563937, 40.0031904], [116.456392, 40.0030685], [116.4561529, 40.0030705], [116.4561537, 40.003129], [116.4552314, 40.0031368]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317719', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4755968, 39.9998652], [116.4756786, 40.0000232], [116.4766969, 39.9997138], [116.4766151, 39.9995558], [116.4755968, 39.9998652]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317720', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4767986, 39.9995062], [116.4768999, 39.9996366], [116.4778286, 39.9992135], [116.4777273, 39.9990831], [116.4767986, 39.9995062]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317721', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4776766, 39.9985397], [116.4777728, 39.9989636], [116.4780065, 39.9989325], [116.4779104, 39.9985086], [116.4776766, 39.9985397]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317722', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4767568, 39.9982516], [116.4768373, 39.9983982], [116.4772805, 39.9982553], [116.4773817, 39.9984396], [116.4775499, 39.9983853], [116.477442, 39.998189], [116.4773068, 39.9982326], [116.4772329, 39.9980981], [116.4767568, 39.9982516]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317723', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4762531, 39.9986723], [116.4763121, 39.9987558], [116.4764529, 39.9986827], [116.476496, 39.9987488], [116.4763439, 39.9988132], [116.4764052, 39.9989106], [116.4767458, 39.9987767], [116.4766845, 39.9986653], [116.4765414, 39.998728], [116.4764983, 39.9986601], [116.4766277, 39.9985958], [116.4765642, 39.9985105], [116.4764279, 39.9985784], [116.4762531, 39.9986723]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '3955006', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4754991, 39.9988358], [116.4755059, 39.9988915], [116.4757512, 39.998888], [116.4757625, 39.9989089], [116.4756013, 39.9989837], [116.4757535, 39.9991768], [116.4762917, 39.9989489], [116.4762099, 39.9988602], [116.4761554, 39.9988845], [116.4761145, 39.9988202], [116.476144, 39.9988132], [116.4761486, 39.9987314], [116.4759102, 39.9987332], [116.475926, 39.9987697], [116.4757103, 39.9987645], [116.4757194, 39.9987975], [116.4755287, 39.9987958], [116.4755264, 39.9988306], [116.4754991, 39.9988358]], [[116.4758193, 39.998881], [116.4760532, 39.9988358], [116.4760714, 39.9988602], [116.4760305, 39.9988793], [116.4760623, 39.9989193], [116.475892, 39.9989854], [116.4758193, 39.998881]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317732', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.473055, 39.9985892], [116.4730591, 39.9987429], [116.4734486, 39.998737], [116.4734446, 39.9985833], [116.473055, 39.9985892]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317733', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4738229, 39.998492], [116.4738245, 39.9986557], [116.4742317, 39.9986533], [116.4742301, 39.9984896], [116.4738229, 39.998492]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317734', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4747143, 39.9983322], [116.4747177, 39.9984887], [116.4750128, 39.9984849], [116.4750094, 39.9983285], [116.4747143, 39.9983322]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297317735', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4752492, 39.998024], [116.4752535, 39.998176], [116.4755496, 39.9981711], [116.4755453, 39.9980192], [116.4752492, 39.998024]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318073', code: 1500, fclass: 'building', name: '金汉王科技大厦', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4701238, 40.0116975], [116.4714261, 40.0117392], [116.471471, 40.0109165], [116.4701687, 40.0108748], [116.4701238, 40.0116975]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318590', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4725317, 40.0069428], [116.4735863, 40.0071754], [116.4736158, 40.0070968], [116.4725612, 40.0068643], [116.4725317, 40.0069428]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318591', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4726315, 40.0065994], [116.4737041, 40.0068141], [116.4737383, 40.0067139], [116.4726657, 40.0064992], [116.4726315, 40.0065994]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318592', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4725818, 40.0062972], [116.4726518, 40.0063111], [116.4729864, 40.0053354], [116.4726362, 40.0053315], [116.4725818, 40.0062972]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318593', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4729675, 40.0049767], [116.4737379, 40.0049795], [116.4737386, 40.0048581], [116.4729682, 40.0048553], [116.4729675, 40.0049767]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318594', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4728053, 40.0061001], [116.4738847, 40.0063187], [116.4739187, 40.0062204], [116.4728393, 40.0060018], [116.4728053, 40.0061001]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318595', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4729399, 40.0057062], [116.4739916, 40.0059583], [116.4740298, 40.0058649], [116.472978, 40.0056128], [116.4729399, 40.0057062]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318596', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4730525, 40.0053731], [116.4737801, 40.0055217], [116.4738127, 40.005428], [116.4730851, 40.0052793], [116.4730525, 40.0053731]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318597', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4739627, 40.0070262], [116.4746836, 40.0071737], [116.4747139, 40.007087], [116.473993, 40.0069394], [116.4739627, 40.0070262]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318598', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4741267, 40.0065968], [116.4750231, 40.0067871], [116.4750646, 40.0066724], [116.4741683, 40.0064821], [116.4741267, 40.0065968]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318599', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4742706, 40.0061785], [116.4751653, 40.0063598], [116.4752044, 40.0062466], [116.4743097, 40.0060653], [116.4742706, 40.0061785]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318600', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4743969, 40.0057657], [116.475286, 40.0059495], [116.4753262, 40.0058354], [116.4744371, 40.0056516], [116.4743969, 40.0057657]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318601', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4749338, 40.0071729], [116.4760523, 40.0071755], [116.4760527, 40.0070794], [116.4749342, 40.0070769], [116.4749338, 40.0071729]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318602', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.475422, 40.0066856], [116.4754223, 40.0067874], [116.4761764, 40.0067862], [116.4761761, 40.0066843], [116.475422, 40.0066856]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318603', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4754841, 40.0062216], [116.4754859, 40.0063326], [116.4762352, 40.0063254], [116.4762334, 40.0062144], [116.4754841, 40.0062216]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318604', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4754931, 40.0058841], [116.4762528, 40.0058959], [116.4762559, 40.0057784], [116.4754962, 40.0057665], [116.4754931, 40.0058841]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318605', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4755247, 40.0054068], [116.4762687, 40.0054109], [116.4762698, 40.0052862], [116.4755259, 40.0052821], [116.4755247, 40.0054068]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318606', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4750529, 40.0053978], [116.4754154, 40.0054027], [116.4754183, 40.0052759], [116.4750558, 40.005271], [116.4750529, 40.0053978]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318607', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4750104, 40.0050001], [116.4753763, 40.0050015], [116.4753771, 40.0048852], [116.4750111, 40.0048837], [116.4750104, 40.0050001]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318608', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.475006, 40.0046943], [116.4753734, 40.0046983], [116.4753756, 40.004578], [116.4750082, 40.004574], [116.475006, 40.0046943]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318609', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4741033, 40.0046742], [116.4741036, 40.0046941], [116.474105, 40.0047962], [116.4748769, 40.0047922], [116.4748786, 40.0046689], [116.4741033, 40.0046742]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318610', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4739018, 40.0046949], [116.4739052, 40.0048234], [116.474105, 40.00482], [116.474105, 40.0047962], [116.4741036, 40.0046941], [116.4739018, 40.0046949]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318611', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4739055, 40.0050475], [116.4739065, 40.0051748], [116.4741072, 40.0051738], [116.4741075, 40.0052024], [116.47488, 40.0051987], [116.4748789, 40.0050702], [116.4741057, 40.0050739], [116.4741055, 40.0050465], [116.4739055, 40.0050475]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318612', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4755271, 40.0049894], [116.4762702, 40.00499], [116.4762703, 40.004871], [116.4755273, 40.0048704], [116.4755271, 40.0049894]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297318613', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4755233, 40.0045672], [116.4755237, 40.0046834], [116.4762687, 40.0046817], [116.4762682, 40.0045654], [116.4755233, 40.0045672]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297319221', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4765745, 40.0109093], [116.4767366, 40.0111681], [116.4771869, 40.0110027], [116.4770248, 40.0107439], [116.4765745, 40.0109093]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297319222', code: 1500, fclass: 'building', name: '双鹤药业', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4754808, 40.0105937], [116.4755212, 40.010737], [116.4755807, 40.0108574], [116.4756444, 40.0109452], [116.4757039, 40.010872], [116.4757592, 40.0107532], [116.4763753, 40.010859], [116.4764178, 40.0107142], [116.4758101, 40.0106035], [116.4758314, 40.0104896], [116.4764645, 40.010488], [116.4764667, 40.0103415], [116.4758293, 40.010335], [116.4758165, 40.0102211], [116.4764369, 40.0101072], [116.4763965, 40.0099607], [116.475746, 40.0101052], [116.4756934, 40.0100088], [116.4756503, 40.0099327], [116.4756019, 40.0099792], [116.4755654, 40.0100863], [116.4755203, 40.0101723], [116.4755046, 40.0102768], [116.4754808, 40.0105937]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297319223', code: 1500, fclass: 'building', name: '安捷伦大厦', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4768854, 40.0095525], [116.4772101, 40.0102018], [116.4774587, 40.0101288], [116.4771834, 40.0095783], [116.47777, 40.0091611], [116.4775843, 40.0090079], [116.4770001, 40.0094234], [116.4770854, 40.0094938], [116.4768854, 40.0095525]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297319224', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4775297, 40.0107729], [116.47776, 40.0109978], [116.4777374, 40.0110186], [116.4778007, 40.0111137], [116.4780242, 40.011086], [116.4781507, 40.0110549], [116.478302, 40.0110099], [116.4784194, 40.0109476], [116.4784895, 40.0109044], [116.4784511, 40.0108594], [116.478652, 40.0107383], [116.4786679, 40.0107038], [116.4783269, 40.0103907], [116.4780852, 40.0105377], [116.4780288, 40.0104824], [116.4775297, 40.0107729]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297319226', code: 1500, fclass: 'building', name: '爱立信大厦 ET1', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4779419, 40.0089822], [116.4783951, 40.009245], [116.4792801, 40.0083056], [116.4788958, 40.0080081], [116.4779419, 40.0089822]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297319227', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4783287, 40.0100601], [116.4789382, 40.0106446], [116.4796263, 40.0102236], [116.4794703, 40.010074], [116.4789395, 40.0103988], [116.478486, 40.0099639], [116.4783287, 40.0100601]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297833330', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4982082, 39.9819961], [116.4982098, 39.9826911], [116.5000477, 39.9826887], [116.5000462, 39.9819937], [116.4982082, 39.9819961]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297833331', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5007149, 39.9821408], [116.5007371, 39.9826932], [116.5021236, 39.9826932], [116.5021264, 39.9820728], [116.5008869, 39.9820749], [116.5008148, 39.9820941], [116.5007149, 39.9821408]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297833332', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5010202, 39.9817607], [116.5019973, 39.9817672], [116.5020026, 39.9813003], [116.5010255, 39.9812938], [116.5010202, 39.9817607]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297833333', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5008136, 39.9796824], [116.5008195, 39.9801355], [116.5017609, 39.9801283], [116.501755, 39.9796753], [116.5008136, 39.9796824]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297833334', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4941347, 39.9788805], [116.4943733, 39.9788807], [116.4943744, 39.9781181], [116.4941358, 39.9781179], [116.4941347, 39.9788805]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '297833335', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4944474, 39.9788834], [116.4957168, 39.9788927], [116.4957264, 39.9781206], [116.494457, 39.9781112], [116.4944474, 39.9788834]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '315698646', code: 1500, fclass: 'building', name: '博泰嘉华国际大酒店', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4727984, 40.0116197], [116.472834, 40.0117357], [116.4728807, 40.0118573], [116.4731002, 40.0118037], [116.4734504, 40.0117357], [116.4738552, 40.0116795], [116.4743543, 40.011604], [116.474302, 40.0114168], [116.4727984, 40.0116197]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '321588748', code: 1500, fclass: 'building', name: 'Audi China Building (ACB)', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4932685, 39.9843396], [116.4939159, 39.9843422], [116.4939227, 39.9833734], [116.4932752, 39.9833707], [116.4932685, 39.9843396]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '322694460', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4930356, 39.9859331], [116.4930527, 39.9860086], [116.4930937, 39.9860785], [116.4931561, 39.9861383], [116.493236, 39.9861844], [116.4933283, 39.9862138], [116.4934272, 39.9862247], [116.4935266, 39.9862164], [116.4936202, 39.9861895], [116.4937021, 39.9861455], [116.4937672, 39.9860873], [116.4938113, 39.9860186], [116.4938317, 39.9859436], [116.4938272, 39.9858671], [116.4937979, 39.9857938], [116.4937458, 39.9857284], [116.4936741, 39.985675], [116.4935873, 39.9856369], [116.4934909, 39.9856166], [116.4933909, 39.9856153], [116.4932937, 39.985633], [116.4932052, 39.9856688], [116.4931311, 39.9857203], [116.4930761, 39.9857843], [116.4930436, 39.9858567], [116.4930356, 39.9859331]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '327007845', code: 1500, fclass: 'building', name: '望京SOHO中心T1', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.474525, 39.9956755], [116.4745688, 39.995756], [116.4746395, 39.9957863], [116.4747228, 39.9957907], [116.4748175, 39.995769], [116.4750633, 39.9956439], [116.4754246, 39.9954226], [116.4759914, 39.9951282], [116.4765438, 39.9948873], [116.4767149, 39.9948047], [116.476645, 39.9947585], [116.4764979, 39.9947266], [116.4763111, 39.9947015], [116.4761005, 39.9946962], [116.4758851, 39.9947207], [116.4756786, 39.9947827], [116.4754865, 39.9948506], [116.4752955, 39.9949453], [116.474898, 39.9951567], [116.4746948, 39.9952896], [116.4746132, 39.9954028], [116.4745576, 39.9955327], [116.474525, 39.9956755]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '327007846', code: 1500, fclass: 'building', name: '望京SOHO中心T2', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4735662, 39.9954738], [116.4736363, 39.9957701], [116.4737273, 39.9961498], [116.4738031, 39.9967104], [116.4738114, 39.9968254], [116.4738968, 39.9968404], [116.4741144, 39.9966415], [116.4742573, 39.9964769], [116.4743294, 39.9963231], [116.4743801, 39.9960433], [116.4743839, 39.9957718], [116.4742411, 39.9954852], [116.4740984, 39.9952863], [116.47393, 39.9951774], [116.4737836, 39.9951606], [116.4735861, 39.9953315], [116.4735662, 39.9954738]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '327007847', code: 1500, fclass: 'building', name: '望京SOHO中心T3', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4740115, 39.9951807], [116.4741002, 39.9952173], [116.4743478, 39.9952199], [116.4746431, 39.9951458], [116.4749566, 39.9949464], [116.4750729, 39.994807], [116.4751105, 39.9946835], [116.4751461, 39.994453], [116.4751074, 39.994384], [116.4750324, 39.9943628], [116.4749034, 39.9944011], [116.4745293, 39.9947311], [116.4741615, 39.9950073], [116.474024, 39.99513], [116.4740115, 39.9951807]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '329203953', code: 1500, fclass: 'building', name: 'Hairun', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4765946, 39.9746242], [116.4766831, 39.9748267], [116.4769137, 39.9748039], [116.4771834, 39.9749409], [116.477357, 39.9748221], [116.4773827, 39.9746903], [116.4773192, 39.9745855], [116.4772101, 39.9745231], [116.4771482, 39.9745031], [116.4771059, 39.9745718], [116.4770375, 39.9745506], [116.4769984, 39.9746254], [116.4769463, 39.974608], [116.4769691, 39.9745393], [116.4768681, 39.9744932], [116.4767834, 39.9744919], [116.4766483, 39.9745418], [116.4766597, 39.9746105], [116.4765946, 39.9746242]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '330321986', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4732498, 39.9859453], [116.4735048, 39.9861755], [116.475478, 39.9849212], [116.4752803, 39.9847485], [116.4747386, 39.984753], [116.4747208, 39.9849591], [116.4739339, 39.985465], [116.4736234, 39.9854878], [116.4736373, 39.9856877], [116.4732498, 39.9859453]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '330398332', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4747804, 39.9824978], [116.4751479, 39.9828293], [116.4753389, 39.9827049], [116.475257, 39.9826311], [116.4751562, 39.9826967], [116.4749585, 39.9825183], [116.4750573, 39.982454], [116.4749854, 39.9823891], [116.4751798, 39.9822626], [116.4751307, 39.9822183], [116.4751543, 39.982203], [116.4751141, 39.9821667], [116.4748776, 39.9823205], [116.4749042, 39.9823446], [116.4748239, 39.9823969], [116.4748706, 39.9824391], [116.4747804, 39.9824978]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '330398333', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4764043, 39.9836552], [116.4767688, 39.9839826], [116.4768366, 39.9839383], [116.476472, 39.9836109], [116.4764043, 39.9836552]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '332374051', code: 1500, fclass: 'building', name: '叶青大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4615412, 40.0105508], [116.4615412, 40.0113315], [116.4625888, 40.0113207], [116.4625817, 40.0114996], [116.4630418, 40.0114942], [116.4630276, 40.0116134], [116.4638063, 40.011608], [116.4638204, 40.0111092], [116.46261, 40.0111418], [116.4626383, 40.0104912], [116.4615907, 40.0104858], [116.4615412, 40.0105508]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '335846848', code: 1500, fclass: 'building', name: '施耐德（中国）电气有限公司', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.482087, 40.0054672], [116.4822311, 40.0058426], [116.4827616, 40.0057366], [116.4827429, 40.0057097], [116.4826155, 40.0053371], [116.4825664, 40.0053393], [116.482087, 40.0054672]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '335846899', code: 1500, fclass: 'building', name: '梅赛德斯-奔驰（中国）研发中心', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4819214, 40.0060933], [116.4819515, 40.0062831], [116.4824772, 40.0062241], [116.4824906, 40.0063498], [116.4822026, 40.006378], [116.4822395, 40.0065652], [116.4828422, 40.0065062], [116.4827484, 40.0060087], [116.4819214, 40.0060933]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '335846940', code: 1500, fclass: 'building', name: '美团网', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4829125, 40.0059805], [116.4829895, 40.0064908], [116.4835922, 40.0064293], [116.4835587, 40.0062446], [116.4832741, 40.0062703], [116.483254, 40.0061395], [116.4837663, 40.0060933], [116.4837496, 40.0059061], [116.4829125, 40.0059805]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '338447008', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4779643, 39.9774811], [116.4785597, 39.9780443], [116.4786777, 39.9779703], [116.4780662, 39.9774236], [116.4779643, 39.9774811]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '338447009', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4783129, 39.9772468], [116.4789299, 39.9778141], [116.4790425, 39.9777401], [116.478431, 39.9771769], [116.4783129, 39.9772468]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '338447010', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.478667, 39.9770207], [116.4792732, 39.9775757], [116.4793966, 39.977514], [116.4787743, 39.9769508], [116.478667, 39.9770207]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '338447011', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4777351, 39.9772853], [116.4778718, 39.9774055], [116.4780373, 39.9772949], [116.4779006, 39.9771747], [116.4777351, 39.9772853]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '338447012', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4780984, 39.9770618], [116.4782164, 39.9771769], [116.4784041, 39.9770741], [116.4782808, 39.9769508], [116.4780984, 39.9770618]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '338447013', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4784631, 39.9768398], [116.4785865, 39.9769385], [116.4787421, 39.9768357], [116.4786187, 39.9767288], [116.4784631, 39.9768398]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '340037142', code: 1500, fclass: 'building', name: '数码港大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4726256, 40.0108488], [116.4726837, 40.0112404], [116.4736839, 40.0111347], [116.473695, 40.0112342], [116.4741914, 40.0112156], [116.4741895, 40.0111778], [116.4741657, 40.0109897], [116.4739863, 40.0109901], [116.4739596, 40.0107558], [116.4726256, 40.0108488]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '344381310', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.473871, 39.978027], [116.4747093, 39.9783118], [116.4747805, 39.9781982], [116.4739422, 39.9779119], [116.473871, 39.978027]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '344381311', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4742644, 39.9777664], [116.4751107, 39.9780467], [116.4751719, 39.9779376], [116.4743218, 39.9776543], [116.4742644, 39.9777664]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '344381312', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4746361, 39.9775164], [116.4754982, 39.9777997], [116.4755713, 39.9776831], [116.4747034, 39.9773952], [116.4746361, 39.9775164]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '344381313', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4751324, 39.978124], [116.4753756, 39.9783452], [116.4754883, 39.978274], [116.4752451, 39.9780573], [116.4751324, 39.978124]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '344381314', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4747488, 39.9783603], [116.4749683, 39.9785724], [116.4750889, 39.9785027], [116.4748694, 39.9782921], [116.4747488, 39.9783603]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '344381315', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4755318, 39.97788], [116.4757532, 39.9780846], [116.4758679, 39.9780134], [116.4756445, 39.9778134], [116.4755318, 39.97788]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '348160245', code: 1500, fclass: 'building', name: '花家地南里7号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4633354, 39.9782988], [116.46389, 39.9783369], [116.4641999, 39.9786037], [116.4639321, 39.9787649], [116.4640239, 39.9788675], [116.4643414, 39.9786711], [116.4643567, 39.9785597], [116.4639551, 39.9782373], [116.4639092, 39.9782168], [116.4633469, 39.9781757], [116.4633354, 39.9782988]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '349940678', code: 1500, fclass: 'building', name: 'Element Fresh', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4747819, 39.9744787], [116.4751679, 39.9748177], [116.4753884, 39.9746702], [116.4750024, 39.9743313], [116.4747819, 39.9744787]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '352424115', code: 1500, fclass: 'building', name: '融科望京中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4767949, 39.9963948], [116.4768057, 39.9965049], [116.4772993, 39.9969536], [116.4773834, 39.9969432], [116.4776247, 39.9971759], [116.4777549, 39.9971011], [116.4771013, 39.9965173], [116.4770905, 39.9963594], [116.4773535, 39.9961226], [116.4771718, 39.9960333], [116.4767949, 39.9963948]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '353135637', code: 1500, fclass: 'building', name: '中央美术学院美术馆', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4585239, 39.9829862], [116.4589424, 39.9831835], [116.4596826, 39.9825505], [116.4596826, 39.9823039], [116.4594359, 39.9822957], [116.4591355, 39.9826656], [116.4588136, 39.9824601], [116.4585668, 39.9824601], [116.458546, 39.9827151], [116.4585239, 39.9829862]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357285450', code: 1500, fclass: 'building', name: '嘉润花园A座', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.472361, 39.9851132], [116.4724678, 39.9851647], [116.4726695, 39.9851738], [116.4727683, 39.9851132], [116.4727723, 39.9849817], [116.4727762, 39.9848496], [116.4726813, 39.9847678], [116.4724796, 39.9847617], [116.4723729, 39.9848193], [116.472361, 39.9851132]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357285451', code: 1500, fclass: 'building', name: '嘉润花园B座', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4729304, 39.9844698], [116.4729317, 39.9845873], [116.4729328, 39.9846979], [116.4730265, 39.984777], [116.4731022, 39.9847777], [116.4732089, 39.9847788], [116.4733265, 39.9847015], [116.4733386, 39.9844679], [116.4732257, 39.9843852], [116.4730385, 39.9843852], [116.4729304, 39.9844698]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357285452', code: 1500, fclass: 'building', name: '嘉润花园C座', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4717099, 39.9844807], [116.4718333, 39.9845139], [116.4718189, 39.9846133], [116.4719727, 39.9846849], [116.4720806, 39.9845489], [116.4722917, 39.9845506], [116.4722943, 39.9843594], [116.4721574, 39.9843245], [116.472167, 39.984238], [116.4719659, 39.9841838], [116.4719084, 39.9843091], [116.4718018, 39.9842804], [116.4717099, 39.9844807]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357285453', code: 1500, fclass: 'building', name: '嘉润花园D座', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4722895, 39.9840246], [116.4724023, 39.9840927], [116.4723519, 39.9842343], [116.4725212, 39.984316], [116.4726472, 39.9841626], [116.4728654, 39.9841904], [116.472923, 39.9839256], [116.4727442, 39.9839028], [116.4727795, 39.9837406], [116.4725531, 39.9837117], [116.4725181, 39.983873], [116.4723951, 39.9838573], [116.4722895, 39.9840246]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357285454', code: 1500, fclass: 'building', name: '嘉润花园', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4726813, 39.9847678], [116.4727762, 39.9848496], [116.4727723, 39.9849817], [116.4728967, 39.9849885], [116.4730955, 39.9848531], [116.4731022, 39.9847777], [116.4730265, 39.984777], [116.4729328, 39.9846979], [116.4729317, 39.9845873], [116.4728768, 39.9845755], [116.4727001, 39.9846991], [116.4726813, 39.9847678]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357824304', code: 1500, fclass: 'building', name: '中国民航管理干部学院', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4717117, 39.983065], [116.4717117, 39.9832316], [116.4721664, 39.9832559], [116.4724669, 39.9835346], [116.472807, 39.9833316], [116.4724155, 39.9829862], [116.4722534, 39.982965], [116.4722692, 39.9826651], [116.471858, 39.9826529], [116.4718224, 39.983065], [116.4717117, 39.983065]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357824305', code: 1500, fclass: 'building', name: '凯美佳', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4719766, 39.9822075], [116.4723088, 39.9824863], [116.4724195, 39.9824105], [116.4727358, 39.9826681], [116.4726567, 39.9827317], [116.4729533, 39.9830074], [116.4730245, 39.9829711], [116.4730719, 39.9830105], [116.4731313, 39.9829741], [116.4731866, 39.9830226], [116.4735227, 39.9828196], [116.4730759, 39.9824499], [116.4726528, 39.9822712], [116.4722929, 39.9819742], [116.4719766, 39.9822075]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357824306', code: 1500, fclass: 'building', name: '望京街道社区服务站', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4724709, 39.9835983], [116.4725381, 39.9836528], [116.4729573, 39.9833922], [116.472898, 39.9833316], [116.4724709, 39.9835983]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357824308', code: 1500, fclass: 'building', name: '蓝色家园', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4729335, 39.9834892], [116.4729415, 39.9836377], [116.4730838, 39.9837376], [116.473375, 39.9837505], [116.4733408, 39.9842042], [116.4736298, 39.9844715], [116.4738312, 39.9843436], [116.4738747, 39.9838588], [116.4733272, 39.9832732], [116.4729335, 39.9834892]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '357824311', code: 1500, fclass: 'building', name: '民航国际活动中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4734921, 39.9832692], [116.4739702, 39.9836921], [116.4742429, 39.9835111], [116.4737649, 39.9830882], [116.4734921, 39.9832692]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846519', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4687984, 39.9816301], [116.4691591, 39.9819567], [116.4692992, 39.9818659], [116.4689385, 39.9815393], [116.4687984, 39.9816301]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846520', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4691064, 39.9814451], [116.4694703, 39.9817765], [116.4696152, 39.9816831], [116.4692513, 39.9813516], [116.4691064, 39.9814451]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846521', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4694269, 39.9812194], [116.4698093, 39.9815617], [116.4699678, 39.9814577], [116.4695854, 39.9811155], [116.4694269, 39.9812194]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846522', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4697545, 39.9809803], [116.4701354, 39.981338], [116.4702925, 39.9812398], [116.4699115, 39.9808821], [116.4697545, 39.9809803]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846523', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4701486, 39.9807586], [116.4704972, 39.9810881], [116.4706725, 39.9809792], [116.4703239, 39.9806496], [116.4701486, 39.9807586]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846524', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4705556, 39.9811275], [116.4707591, 39.9813207], [116.4709587, 39.9811973], [116.4707552, 39.981004], [116.4705556, 39.9811275]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846525', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4685119, 39.9813881], [116.4686905, 39.9815597], [116.4688289, 39.9814751], [116.4686503, 39.9813035], [116.4685119, 39.9813881]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846526', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4688301, 39.9811911], [116.4690031, 39.9813615], [116.4691518, 39.9812729], [116.4689788, 39.9811025], [116.4688301, 39.9811911]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846527', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.469148, 39.9809905], [116.4693183, 39.9811585], [116.4694885, 39.9810573], [116.4693182, 39.9808892], [116.469148, 39.9809905]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846528', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4694827, 39.9807907], [116.4696585, 39.9809525], [116.4698301, 39.9808431], [116.4696543, 39.9806813], [116.4694827, 39.9807907]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360846529', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4697068, 39.9806386], [116.4698787, 39.9807911], [116.4702154, 39.9805684], [116.4700436, 39.9804159], [116.4697068, 39.9806386]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360847042', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.473059, 40.0119577], [116.4732325, 40.0126318], [116.4738486, 40.0125388], [116.4736751, 40.0118647], [116.473059, 40.0119577]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360847044', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4737308, 40.0118512], [116.4738926, 40.0125293], [116.4745634, 40.0124354], [116.4744016, 40.0117573], [116.4737308, 40.0118512]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '360847046', code: 1500, fclass: 'building', name: '中国石化加油站（广泽桥）', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4723511, 40.0124487], [116.4723511, 40.01265], [116.4725872, 40.01265], [116.4728336, 40.0126506], [116.4728269, 40.0124531], [116.4723511, 40.0124487]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361015030', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4818093, 40.0048477], [116.4818321, 40.004975], [116.4821956, 40.0049368], [116.4822271, 40.0051124], [116.4820401, 40.005132], [116.4820669, 40.0052814], [116.4825491, 40.0052332], [116.4825664, 40.0053393], [116.4826155, 40.0053371], [116.4827429, 40.0057097], [116.4829087, 40.0056923], [116.4829158, 40.005732], [116.4836528, 40.0056544], [116.4836145, 40.0054411], [116.4833363, 40.0054704], [116.4833137, 40.0053444], [116.4834912, 40.0053257], [116.4834628, 40.0051671], [116.4835507, 40.0051578], [116.4835297, 40.0050406], [116.4828092, 40.0051164], [116.4827434, 40.0047494], [116.4818093, 40.0048477]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361015036', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4815936, 40.0056703], [116.4816849, 40.0059233], [116.4818092, 40.005897], [116.4817179, 40.005644], [116.4815936, 40.0056703]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361015038', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4838799, 40.0058051], [116.4839173, 40.0060641], [116.4840624, 40.0060518], [116.484025, 40.0057928], [116.4838799, 40.0058051]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361015040', code: 1500, fclass: 'building', name: '昆泰酒店', type: 'hotel'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4845773, 40.005395], [116.4846893, 40.0055719], [116.4851609, 40.0053712], [116.4854701, 40.0050596], [116.4852574, 40.004934], [116.4849758, 40.005219], [116.4845773, 40.005395]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016376', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4728057, 40.0077775], [116.4736317, 40.0077824], [116.4736332, 40.0076371], [116.4728071, 40.0076322], [116.4728057, 40.0077775]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016378', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4725145, 40.0077867], [116.4725183, 40.0078977], [116.4726911, 40.0078943], [116.4726873, 40.0077832], [116.4725145, 40.0077867]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016379', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4725811, 40.0082476], [116.4730665, 40.0082571], [116.4730697, 40.0081596], [116.4725844, 40.0081501], [116.4725811, 40.0082476]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016381', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4735535, 40.0082683], [116.4744076, 40.0082731], [116.474409, 40.0081253], [116.473555, 40.0081205], [116.4735535, 40.0082683]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016382', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4736994, 40.0076422], [116.4737007, 40.0077795], [116.4745684, 40.0077746], [116.474567, 40.0076372], [116.4736994, 40.0076422]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016383', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4730408, 40.0079147], [116.4730428, 40.0080295], [116.4732534, 40.0080273], [116.4732543, 40.0080786], [116.4735008, 40.0080761], [116.4734997, 40.0080116], [116.473751, 40.008009], [116.4737492, 40.0079063], [116.4734955, 40.0079089], [116.4734963, 40.0079532], [116.473243, 40.0079558], [116.4732422, 40.0079126], [116.4730408, 40.0079147]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016384', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4740419, 40.0079167], [116.4740465, 40.008028], [116.4745113, 40.0080165], [116.4745066, 40.0079053], [116.4740419, 40.0079167]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016385', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4747985, 40.0077739], [116.4752148, 40.0077795], [116.4752174, 40.0076665], [116.4748011, 40.0076609], [116.4747985, 40.0077739]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016386', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.475324, 40.0077689], [116.4757886, 40.0077711], [116.4757895, 40.0076614], [116.4753249, 40.0076591], [116.475324, 40.0077689]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361016388', code: 1500, fclass: 'building', name: '摩托罗拉大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.476892, 40.0047379], [116.4768958, 40.0051879], [116.4770262, 40.0051877], [116.477017, 40.0057569], [116.4779617, 40.0056641], [116.4779383, 40.0053494], [116.4780153, 40.0053308], [116.4794394, 40.0051198], [116.4804655, 40.0047893], [116.4806444, 40.0046479], [116.4806506, 40.0044704], [116.4805053, 40.0043343], [116.4802809, 40.0043824], [116.4802953, 40.0045261], [116.4792967, 40.0048364], [116.4781636, 40.0050271], [116.4779599, 40.0050354], [116.4777195, 40.0050394], [116.4772511, 40.0049405], [116.476892, 40.0047379]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361099414', code: 1500, fclass: 'building', name: '爱立信大厦 ET2', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4787855, 40.009288], [116.4792488, 40.0095544], [116.4796201, 40.0091697], [116.4802034, 40.0085652], [116.479723, 40.0083635], [116.4787855, 40.009288]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361379955', code: 1500, fclass: 'building', name: '南湖中园202号楼', type: 'house'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4530436, 39.9993001], [116.4539127, 39.9993042], [116.453918, 39.9991357], [116.453049, 39.9991316], [116.4530436, 39.9993001]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714057', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4617164, 40.008732], [116.4623544, 40.0087401], [116.4623597, 40.0084936], [116.4617217, 40.0084855], [116.4617164, 40.008732]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714058', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4624966, 40.0086902], [116.4630352, 40.0087042], [116.4630447, 40.0084875], [116.4625062, 40.0084736], [116.4624966, 40.0086902]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714059', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4631852, 40.0087427], [116.4638235, 40.0087521], [116.4638299, 40.0084989], [116.4631915, 40.0084895], [116.4631852, 40.0087427]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714060', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4619685, 40.0080911], [116.4625191, 40.0080928], [116.4625202, 40.0078617], [116.4619697, 40.0078601], [116.4619685, 40.0080911]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714061', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4631162, 40.0078711], [116.4631205, 40.0080808], [116.4636404, 40.0080746], [116.4636362, 40.0078649], [116.4631162, 40.0078711]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714062', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4625308, 40.0077592], [116.4630813, 40.007764], [116.4630843, 40.0075663], [116.4625337, 40.0075615], [116.4625308, 40.0077592]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714063', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4661426, 40.0091076], [116.466678, 40.0091097], [116.4666813, 40.008629], [116.4661459, 40.0086269], [116.4661426, 40.0091076]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714064', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4644207, 40.0090266], [116.4647192, 40.0090282], [116.4647234, 40.0085498], [116.4644249, 40.0085483], [116.4644207, 40.0090266]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714068', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4653327, 40.0093139], [116.4653486, 40.0097629], [116.4655975, 40.0097577], [116.4655994, 40.0098134], [116.4662046, 40.0098007], [116.4663239, 40.0097517], [116.4664177, 40.0096375], [116.4664119, 40.0094721], [116.4661912, 40.0094767], [116.4661953, 40.0095946], [116.4656147, 40.0096066], [116.4656042, 40.0093082], [116.4653327, 40.0093139]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714069', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4643101, 40.0095306], [116.4643108, 40.0098086], [116.464429, 40.0098084], [116.4644285, 40.0096547], [116.4645716, 40.0096545], [116.4645719, 40.009765], [116.4648099, 40.0097647], [116.4648096, 40.0096505], [116.4649514, 40.0096503], [116.4649518, 40.0098039], [116.4650707, 40.0098037], [116.46507, 40.0095294], [116.4643101, 40.0095306]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714070', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4643359, 40.0094432], [116.4647151, 40.0094497], [116.4647258, 40.0090825], [116.4643466, 40.009076], [116.4643359, 40.0094432]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714071', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4643627, 40.0082072], [116.4643666, 40.0084503], [116.4647565, 40.0084467], [116.4647526, 40.0082036], [116.4643627, 40.0082072]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714072', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4643411, 40.0081564], [116.4647226, 40.0081595], [116.4647248, 40.0079964], [116.4645097, 40.0079946], [116.464511, 40.0079029], [116.4647258, 40.0079046], [116.4647278, 40.0077552], [116.4643697, 40.0077523], [116.4643678, 40.0078944], [116.4644227, 40.0078949], [116.4644214, 40.0079939], [116.4643434, 40.0079933], [116.4643411, 40.0081564]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714074', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4643622, 40.0076031], [116.464363, 40.0076958], [116.4647624, 40.0076937], [116.4647605, 40.007473], [116.4644465, 40.0074746], [116.4644476, 40.0076027], [116.4643622, 40.0076031]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714075', code: 1500, fclass: 'building', name: '北京惠兰医院', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4670661, 40.0093267], [116.4671432, 40.0093407], [116.4671391, 40.0096891], [116.4672082, 40.0098479], [116.4673711, 40.0099008], [116.4680233, 40.0099053], [116.4680264, 40.0096368], [116.4674448, 40.0096328], [116.4674482, 40.0093428], [116.4670661, 40.0093267]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714076', code: 1500, fclass: 'building', name: '中材国际大厦', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4683039, 40.0096783], [116.4692561, 40.0096849], [116.4692599, 40.0093691], [116.4683076, 40.0093625], [116.4683039, 40.0096783]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714077', code: 1500, fclass: 'building', name: '国际楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4675531, 40.0076393], [116.4675594, 40.0092083], [116.4678258, 40.0092077], [116.4678194, 40.0076387], [116.4675531, 40.0076393]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714078', code: 1500, fclass: 'building', name: '竞时健康运动中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4689398, 40.0075974], [116.4689492, 40.0081474], [116.4697919, 40.0081387], [116.4697861, 40.0075933], [116.4689398, 40.0075974]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714080', code: 1500, fclass: 'building', name: '礼堂', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4693623, 40.0088095], [116.4693722, 40.0091128], [116.4695635, 40.0091133], [116.4695479, 40.009234], [116.4696855, 40.00926], [116.4697961, 40.00925], [116.4699299, 40.009208], [116.4700335, 40.0090893], [116.4700489, 40.0089358], [116.4700033, 40.0088077], [116.4698379, 40.008698], [116.4696326, 40.0086882], [116.4695436, 40.0087027], [116.4695483, 40.0087955], [116.4693623, 40.0088095]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714081', code: 1500, fclass: 'building', name: '学生宿舍', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4700401, 40.0079297], [116.4700433, 40.0081452], [116.4710305, 40.0081364], [116.4710273, 40.0079209], [116.4700401, 40.0079297]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '361714082', code: 1500, fclass: 'building', name: '学生宿舍', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4700111, 40.0075936], [116.4700131, 40.0077412], [116.4705519, 40.0077368], [116.4710028, 40.0077332], [116.4710007, 40.0075857], [116.4700111, 40.0075936]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '370143557', code: 1500, fclass: 'building', name: '中轻大厦A座', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4844156, 40.0042066], [116.4845187, 40.0043607], [116.484936, 40.0041915], [116.4854023, 40.0041502], [116.4853827, 40.0039659], [116.4848771, 40.0040073], [116.4844156, 40.0042066]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '384794249', code: 1500, fclass: 'building', name: '捷通', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4993139, 39.9833624], [116.4993204, 39.9837712], [116.5000025, 39.9837648], [116.4999961, 39.9833561], [116.4993139, 39.9833624]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '401198331', code: 1500, fclass: 'building', name: '望京智选假日酒店北京', type: 'hotel'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.476907, 39.98957], [116.477496, 39.9901179], [116.4777528, 39.9899559], [116.4771637, 39.989408], [116.476907, 39.98957]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '403580680', code: 1500, fclass: 'building', name: 'C9', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4865501, 39.9893404], [116.489811, 39.9893819], [116.4898218, 39.9886971], [116.4865609, 39.9886598], [116.4865501, 39.9893404]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '406567179', code: 1500, fclass: 'building', name: '芳园里36号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.477728, 39.9704156], [116.478223, 39.9708594], [116.4783071, 39.9707806], [116.4778308, 39.9703583], [116.477728, 39.9704156]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '406567351', code: 1500, fclass: 'building', name: '芳园里38号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4780475, 39.9701966], [116.4784665, 39.9705697], [116.4785712, 39.9705083], [116.4781399, 39.9701352], [116.4780475, 39.9701966]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '406702438', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4761793, 39.9763421], [116.4761868, 39.9765103], [116.4763733, 39.9765054], [116.4763657, 39.9763372], [116.4761793, 39.9763421]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '406702527', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4765524, 39.9761239], [116.4765571, 39.9762794], [116.476751, 39.9762759], [116.4767463, 39.9761204], [116.4765524, 39.9761239]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '406702528', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.476887, 39.9758743], [116.476895, 39.976044], [116.4771013, 39.9760384], [116.4770934, 39.9758686], [116.476887, 39.9758743]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '406702529', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4772723, 39.9758127], [116.4774481, 39.9758138], [116.4774498, 39.975644], [116.477274, 39.9756429], [116.4772723, 39.9758127]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '406702530', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4776158, 39.9754187], [116.4776168, 39.9755811], [116.4778122, 39.9755804], [116.477811, 39.9754179], [116.4776158, 39.9754187]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '406719546', code: 1500, fclass: 'building', name: null, type: 'industrial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4841281, 39.9724317], [116.4866267, 39.9724751], [116.4866055, 39.9714716], [116.4864073, 39.9714662], [116.4863932, 39.9723178], [116.4843475, 39.9722744], [116.4843475, 39.9718405], [116.4854163, 39.9711353], [116.4864073, 39.9711678], [116.4864144, 39.9712329], [116.4866197, 39.9712329], [116.4866126, 39.9710105], [116.4853243, 39.9709888], [116.4841422, 39.9717482], [116.4841281, 39.9724317]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '425253579', code: 1500, fclass: 'building', name: '北京诺金酒店', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4741013, 39.9752583], [116.4741035, 39.9753401], [116.4741293, 39.9754131], [116.4742044, 39.9754683], [116.4743014, 39.9754973], [116.4744377, 39.9755367], [116.4745303, 39.9755765], [116.4746935, 39.9757286], [116.4747015, 39.9757355], [116.474868, 39.9756304], [116.4742688, 39.9750943], [116.474195, 39.9751038], [116.474123, 39.975186], [116.4741013, 39.9752583]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '432582209', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.464789, 39.9925381], [116.4652003, 39.99292], [116.4652756, 39.9928724], [116.4648644, 39.9924905], [116.464789, 39.9925381]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805704', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.478375, 39.9699766], [116.4788578, 39.9704042], [116.4789758, 39.9703466], [116.4784823, 39.9699108], [116.478375, 39.9699766]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805705', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.478729, 39.9707824], [116.479126, 39.9711359], [116.4792118, 39.9710619], [116.4788256, 39.9707002], [116.478729, 39.9707824]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805706', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4789651, 39.9704864], [116.4794479, 39.9708975], [116.4795766, 39.9708399], [116.4790616, 39.9704206], [116.4789651, 39.9704864]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805707', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4783274, 39.9709428], [116.4787799, 39.9713434], [116.47889, 39.9712839], [116.4784179, 39.9708564], [116.4783274, 39.9709428]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805708', code: 1500, fclass: 'building', name: '37号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4771916, 39.9699376], [116.4776098, 39.9703078], [116.4776883, 39.9702562], [116.4773343, 39.9699602], [116.4774201, 39.9699108], [116.478214, 39.9699273], [116.4782147, 39.9698166], [116.4774169, 39.9697896], [116.4771916, 39.9699376]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805709', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4788242, 39.9699366], [116.478873, 39.9699896], [116.4789056, 39.9699646], [116.4789421, 39.9699989], [116.4789218, 39.9700238], [116.4789543, 39.9700519], [116.4790438, 39.970002], [116.4790153, 39.9699709], [116.4790438, 39.9699491], [116.4789869, 39.9698805], [116.4789259, 39.9699148], [116.4788893, 39.969893], [116.4788242, 39.9699366]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805710', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4790113, 39.9701142], [116.47906, 39.9701485], [116.4790885, 39.970136], [116.4791373, 39.9701672], [116.4791048, 39.9701921], [116.4791413, 39.9702201], [116.4792389, 39.9701578], [116.4792064, 39.9701391], [116.4792389, 39.970108], [116.4791739, 39.9700519], [116.4791251, 39.9700768], [116.4790926, 39.9700488], [116.4790113, 39.9701142]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805711', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4792511, 39.970298], [116.4793771, 39.9704102], [116.4795398, 39.9703167], [116.4794097, 39.9701983], [116.4792511, 39.970298]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805712', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4794544, 39.9704912], [116.4795804, 39.9706033], [116.479743, 39.970485], [116.4796129, 39.9703728], [116.4794544, 39.9704912]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805713', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.479739, 39.9707249], [116.4798406, 39.9708277], [116.4800032, 39.9707217], [116.4798813, 39.9706158], [116.479739, 39.9707249]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433805831', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4792278, 39.9698652], [116.4792332, 39.9699268], [116.479319, 39.9699844], [116.4794531, 39.9699823], [116.4794585, 39.9698713], [116.4792278, 39.9698652]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807507', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4795351, 39.9698807], [116.4795351, 39.9699917], [116.4802407, 39.9699917], [116.4802438, 39.9698642], [116.4795351, 39.9698807]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807508', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4803516, 39.969987], [116.4812513, 39.9699846], [116.4812482, 39.9698854], [116.4803547, 39.9698713], [116.4803516, 39.969987]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807509', code: 1500, fclass: 'building', name: null, type: 'industrial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4797447, 39.9703057], [116.4798833, 39.970438], [116.4801575, 39.9702632], [116.4800158, 39.9701263], [116.4797447, 39.9703057]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807510', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4799427, 39.9708258], [116.4804241, 39.9712679], [116.4805338, 39.9711946], [116.4800595, 39.970739], [116.4799427, 39.9708258]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807511', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4802825, 39.9706033], [116.4807886, 39.9710536], [116.480916, 39.9709831], [116.4804042, 39.9705181], [116.4802825, 39.9706033]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807512', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4807959, 39.9705309], [116.4807959, 39.9706368], [116.4817756, 39.9706493], [116.4817838, 39.9705434], [116.4807959, 39.9705309]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807513', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4804828, 39.9701819], [116.4804869, 39.9703003], [116.4813488, 39.970291], [116.4816415, 39.9703128], [116.4816618, 39.9702037], [116.4813569, 39.9701757], [116.4804828, 39.9701819]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807514', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4794496, 39.9710654], [116.4797929, 39.9713691], [116.4799203, 39.9714831], [116.4800477, 39.9714234], [116.479938, 39.9713149], [116.4795664, 39.9709677], [116.4794496, 39.9710654]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807515', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4798158, 39.9717963], [116.4799177, 39.9718909], [116.4803093, 39.971673], [116.4806794, 39.9720348], [116.4808457, 39.9719895], [116.4803093, 39.9714674], [116.4798158, 39.9717963]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807516', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4805346, 39.9713564], [116.481012, 39.971784], [116.4811247, 39.9717305], [116.4806258, 39.9712742], [116.4805346, 39.9713564]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807517', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4808135, 39.9721088], [116.4810389, 39.9723308], [116.4811676, 39.9722855], [116.4809316, 39.9720471], [116.4808135, 39.9721088]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807518', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4811247, 39.9718909], [116.4814734, 39.9722033], [116.4815753, 39.972154], [116.4812051, 39.971821], [116.4811247, 39.9718909]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '433807519', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4808967, 39.9711169], [116.4811516, 39.9713664], [116.4813073, 39.9712742], [116.4810525, 39.9710246], [116.4808967, 39.9711169]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436059510', code: 1500, fclass: 'building', name: '南湖中园201号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4518635, 39.9989008], [116.4523766, 39.9992437], [116.4528823, 39.9992548], [116.452886, 39.9991099], [116.4523984, 39.9991015], [116.4520018, 39.9987977], [116.4518635, 39.9989008]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436059511', code: 1500, fclass: 'building', name: '南湖中园205号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4518489, 39.9987392], [116.4520491, 39.9987364], [116.4520418, 39.9982709], [116.4518562, 39.9982764], [116.4518489, 39.9987392]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436059512', code: 1500, fclass: 'building', name: '南湖中园203号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.452322, 39.9987447], [116.4523256, 39.998859], [116.4527295, 39.9988451], [116.4527331, 39.9987447], [116.452322, 39.9987447]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436059515', code: 1500, fclass: 'building', name: '南湖中园206号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.452231, 39.9983155], [116.4522383, 39.9984911], [116.4527441, 39.9984883], [116.4527513, 39.9983155], [116.452231, 39.9983155]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436059517', code: 1500, fclass: 'building', name: '南湖中园208号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4518712, 39.9982381], [116.4520324, 39.9982431], [116.4520369, 39.9981179], [116.4522467, 39.9981145], [116.4527791, 39.9981027], [116.4527879, 39.9979504], [116.452123, 39.9979757], [116.4520435, 39.9979876], [116.4520015, 39.9979571], [116.451933, 39.9980079], [116.4519485, 39.9980299], [116.4519353, 39.9980959], [116.4518734, 39.9980908], [116.4518712, 39.9982381]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436059518', code: 1500, fclass: 'building', name: '南湖中园204号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4529912, 39.9987725], [116.4530058, 39.9989258], [116.453799, 39.998937], [116.4537954, 39.9987893], [116.4529912, 39.9987725]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436059519', code: 1500, fclass: 'building', name: '南湖中园207号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4529803, 39.9984659], [116.4529803, 39.9986164], [116.4535334, 39.9986164], [116.4535261, 39.9984548], [116.4529803, 39.9984659]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436059520', code: 1500, fclass: 'building', name: '南湖中园209号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4530494, 39.9981342], [116.4531622, 39.9981593], [116.4534278, 39.9982318], [116.453577, 39.9982791], [116.4536898, 39.9983293], [116.4537044, 39.9985997], [116.4538257, 39.9985943], [116.4538289, 39.9985669], [116.453833, 39.9985494], [116.45389, 39.9985607], [116.4538998, 39.9984852], [116.4538346, 39.9984765], [116.453833, 39.9984484], [116.45389, 39.9984528], [116.4538946, 39.9983809], [116.4538415, 39.9983758], [116.4538128, 39.9983166], [116.4538769, 39.9983047], [116.4538393, 39.9982353], [116.4537907, 39.9982523], [116.4537112, 39.9981863], [116.4537399, 39.9981524], [116.4536538, 39.9981203], [116.4536251, 39.9981575], [116.4535897, 39.9981456], [116.453614, 39.9981152], [116.4535145, 39.9980784], [116.4534932, 39.9981072], [116.4534596, 39.9980962], [116.4534824, 39.9980617], [116.4533894, 39.9980337], [116.4533682, 39.9980636], [116.4533385, 39.9980519], [116.4533548, 39.9980205], [116.4532683, 39.9979943], [116.4532491, 39.9980261], [116.453213, 39.9980155], [116.4532342, 39.9979822], [116.4531403, 39.9979527], [116.4531331, 39.9979753], [116.4531004, 39.997967], [116.4530567, 39.9980255], [116.4530494, 39.9981342]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061050', code: 1500, fclass: 'building', name: '南湖中园派出所', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4560625, 39.9977615], [116.4562262, 39.9977637], [116.4562275, 39.9977086], [116.4563124, 39.9977097], [116.4563144, 39.9976219], [116.4562634, 39.9976052], [116.4562658, 39.9975029], [116.4560685, 39.9975002], [116.4560625, 39.9977615]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061052', code: 1500, fclass: 'building', name: '南湖中园210号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4541645, 39.999306], [116.4551032, 39.9993339], [116.4551142, 39.999122], [116.4541681, 39.9990886], [116.4541645, 39.999306]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061053', code: 1500, fclass: 'building', name: '南湖中园212号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4542773, 39.9989269], [116.4550487, 39.9989297], [116.4550705, 39.9987513], [116.4542809, 39.9987401], [116.4542773, 39.9989269]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061054', code: 1500, fclass: 'building', name: '南湖中园213号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4552888, 39.9988572], [116.4559511, 39.9988684], [116.4559583, 39.99869], [116.4552925, 39.9986844], [116.4552888, 39.9988572]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061055', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4552815, 39.9993199], [116.456042, 39.9993311], [116.4560529, 39.9992335], [116.4561512, 39.9992084], [116.4562203, 39.999175], [116.456264, 39.9991192], [116.4563077, 39.9990523], [116.4563077, 39.9989799], [116.4563077, 39.998938], [116.4552925, 39.998952], [116.4552815, 39.9993199]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061056', code: 1500, fclass: 'building', name: '南湖中园216号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4553107, 39.9983415], [116.4553107, 39.9984837], [116.4559947, 39.9984753], [116.456002, 39.9983415], [116.4553107, 39.9983415]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061057', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4553034, 39.9981018], [116.4558892, 39.9981074], [116.4559984, 39.9981102], [116.4560493, 39.998138], [116.4560893, 39.9981826], [116.456093, 39.9983192], [116.4562931, 39.9983192], [116.456304, 39.9980293], [116.4562312, 39.9979429], [116.4560857, 39.9979067], [116.455831, 39.9979095], [116.4555326, 39.9979039], [116.4553216, 39.9979039], [116.4553034, 39.9981018]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061058', code: 1500, fclass: 'building', name: '南湖中园217号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4560784, 39.99886], [116.4562931, 39.9988544], [116.4562931, 39.9983722], [116.4561003, 39.998361], [116.4560784, 39.99886]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061059', code: 1500, fclass: 'building', name: '旺角购物广场', type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4570154, 40.0001001], [116.4570552, 40.0007804], [116.4570287, 40.0010173], [116.457064, 40.0010309], [116.4575279, 40.0010444], [116.4574926, 40.000425], [116.4577532, 40.0001848], [116.4577753, 39.9999343], [116.4572761, 39.9999275], [116.4570154, 40.0001001]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061061', code: 1500, fclass: 'building', name: '南湖中园215号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.454544, 39.9984317], [116.4545513, 39.998585], [116.455068, 39.9985878], [116.4550825, 39.9984233], [116.454544, 39.9984317]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061062', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4541801, 39.9982644], [116.4541947, 39.9986212], [116.4543402, 39.9986156], [116.4543402, 39.998323], [116.4549806, 39.9981195], [116.4549333, 39.9979745], [116.4543038, 39.9982087], [116.4541801, 39.9982644]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061063', code: 1500, fclass: 'building', name: '南湖中园222号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4529903, 39.9972163], [116.4530048, 39.9974114], [116.4533469, 39.9974003], [116.4533214, 39.997208], [116.4529903, 39.9972163]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061064', code: 1500, fclass: 'building', name: '南湖中园223号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4535725, 39.9971327], [116.4535761, 39.9973222], [116.4539072, 39.9973195], [116.4539036, 39.9971299], [116.4535725, 39.9971327]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061065', code: 1500, fclass: 'building', name: '南湖中园224号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4541146, 39.9971411], [116.4541328, 39.9973362], [116.4544931, 39.9973222], [116.4544858, 39.9971383], [116.4541146, 39.9971411]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061066', code: 1500, fclass: 'building', name: '南湖中园225号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.454684, 39.9973478], [116.4546866, 39.9974013], [116.4550434, 39.9973869], [116.4550407, 39.9971609], [116.4546893, 39.9971793], [116.454684, 39.9973478]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061075', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4517831, 39.9962042], [116.4517861, 39.9963144], [116.451882, 39.9963695], [116.4522356, 39.9961261], [116.4529968, 39.9961353], [116.4530208, 39.9959287], [116.4522176, 39.9959379], [116.4520438, 39.9960205], [116.4517831, 39.9962042]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061076', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4517981, 39.9964108], [116.451834, 39.99687], [116.4519839, 39.9968792], [116.4519719, 39.9964384], [116.4517981, 39.9964108]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061077', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4518101, 39.9969251], [116.451822, 39.9973521], [116.4519959, 39.9973751], [116.4519899, 39.9969343], [116.4518101, 39.9969251]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061078', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4517861, 39.997522], [116.451822, 39.9975495], [116.4520258, 39.9977194], [116.4526372, 39.9977378], [116.452793, 39.9977194], [116.452793, 39.9975541], [116.4521517, 39.9975587], [116.4519599, 39.9974072], [116.4517861, 39.997522]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061079', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4521217, 39.9973245], [116.4521637, 39.997398], [116.452769, 39.9973934], [116.452763, 39.9972281], [116.4521277, 39.9972189], [116.4521217, 39.9973245]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061080', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4521217, 39.9968792], [116.4521277, 39.9970536], [116.4527211, 39.9970491], [116.4527331, 39.9968792], [116.4521217, 39.9968792]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061081', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4521397, 39.9964613], [116.4521517, 39.9966312], [116.4527391, 39.9966358], [116.452757, 39.9964705], [116.4521397, 39.9964613]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061082', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4541955, 39.9969251], [116.4549447, 39.9969113], [116.4549387, 39.9967185], [116.4542015, 39.9967047], [116.4541955, 39.9969251]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061083', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4540541, 39.996403], [116.4540567, 39.9965569], [116.4547683, 39.9965499], [116.4547657, 39.996396], [116.4540541, 39.996403]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061084', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4532425, 39.9967139], [116.4532485, 39.9969297], [116.4540517, 39.9969159], [116.4540636, 39.996723], [116.4532425, 39.9967139]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061086', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4529728, 39.9963879], [116.4529788, 39.9969113], [116.4532485, 39.9969297], [116.4532186, 39.9963695], [116.4529728, 39.9963879]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061087', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4531402, 39.9959677], [116.4531435, 39.9961676], [116.4540122, 39.9961591], [116.4540088, 39.9959592], [116.4531402, 39.9959677]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '436061088', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4541329, 39.996036], [116.4541336, 39.9961867], [116.4548582, 39.9961846], [116.4548575, 39.9960339], [116.4541329, 39.996036]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '437399877', code: 1500, fclass: 'building', name: '启明国际大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4682186, 40.0107698], [116.4682293, 40.0110859], [116.4692047, 40.0110664], [116.469194, 40.0107503], [116.4682186, 40.0107698]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '437400047', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4671069, 40.0111553], [116.4671077, 40.0114863], [116.4680636, 40.011485], [116.4680628, 40.0111541], [116.4671069, 40.0111553]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '437400048', code: 1500, fclass: 'building', name: '津港大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4671689, 40.0106962], [116.4671689, 40.0109587], [116.4673717, 40.0109587], [116.4673765, 40.0107443], [116.4676903, 40.0107406], [116.4676903, 40.010992], [116.4679993, 40.010992], [116.4680041, 40.0106185], [116.4675358, 40.0106259], [116.4672461, 40.0106111], [116.4671689, 40.0106962]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '441370192', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4490621, 40.0061653], [116.4491297, 40.0061662], [116.4491261, 40.0063131], [116.4494886, 40.0063183], [116.4494991, 40.0058921], [116.449151, 40.0058871], [116.4491493, 40.0059579], [116.4490672, 40.0059567], [116.4490621, 40.0061653]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '6574579', code: 1500, fclass: 'building', name: '科创大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4802851, 40.0064749], [116.4804001, 40.0068421], [116.4804963, 40.0068244], [116.4805378, 40.0069569], [116.4813378, 40.0074016], [116.4817212, 40.0073594], [116.481771, 40.0076248], [116.4825175, 40.0075425], [116.4824499, 40.0073092], [116.4823073, 40.007021], [116.4817116, 40.0070867], [116.4811683, 40.0067641], [116.4809593, 40.0068025], [116.4808257, 40.0063756], [116.4802851, 40.0064749]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '442181975', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4564293, 39.9682484], [116.456668, 39.9684683], [116.4568021, 39.9684108], [116.4566921, 39.9682648], [116.4568128, 39.9681888], [116.4569738, 39.9683429], [116.4574512, 39.9681456], [116.4570971, 39.967827], [116.4569818, 39.9678907], [116.4571454, 39.9681086], [116.4570274, 39.96816], [116.4568316, 39.9679894], [116.4564293, 39.9682484]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '442181976', code: 1500, fclass: 'building', name: '比如世界购物中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4560377, 39.9673891], [116.4565848, 39.9678969], [116.4569308, 39.967679], [116.4568799, 39.9676399], [116.4568155, 39.9676122], [116.4567404, 39.9675895], [116.4566546, 39.9675906], [116.4565875, 39.967607], [116.4565205, 39.9676368], [116.4564856, 39.9676111], [116.4565392, 39.9675587], [116.4565848, 39.9674909], [116.4566036, 39.9674117], [116.4566036, 39.9673501], [116.4565929, 39.9672802], [116.4565687, 39.9672092], [116.4565231, 39.9671527], [116.4564708, 39.9671846], [116.4564212, 39.9671445], [116.4560377, 39.9673891]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '442181977', code: 1500, fclass: 'building', name: '比如世界购物中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4556162, 39.9680589], [116.4558669, 39.9682844], [116.4564457, 39.9679066], [116.4559412, 39.9674527], [116.4557467, 39.9675796], [116.4560005, 39.967808], [116.4556162, 39.9680589]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '442181978', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4567855, 39.968577], [116.4569494, 39.9688007], [116.4572028, 39.9686916], [116.4572173, 39.9687114], [116.4574133, 39.968627], [116.4573986, 39.9686069], [116.4576487, 39.9684993], [116.4574851, 39.968276], [116.4567855, 39.968577]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '442693688', code: 1500, fclass: 'building', name: '家盒子', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4508639, 40.0079194], [116.4508898, 40.0079194], [116.450889, 40.0080548], [116.4513269, 40.0080562], [116.4513276, 40.0079198], [116.4513602, 40.0079199], [116.4513614, 40.0076827], [116.4513216, 40.0076826], [116.4513223, 40.0075508], [116.4508864, 40.0075494], [116.4508857, 40.0076791], [116.4508652, 40.007679], [116.4508639, 40.0079194]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '446529977', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4534304, 39.9854282], [116.454256, 39.9856486], [116.4543084, 39.9855334], [116.4536291, 39.9853521], [116.4537362, 39.9851166], [116.4545695, 39.9853391], [116.4546235, 39.9852202], [116.4538458, 39.9850126], [116.4538288, 39.9850499], [116.4536268, 39.984996], [116.4534304, 39.9854282]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '446529980', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4549237, 39.9858176], [116.4557559, 39.9860432], [116.4559491, 39.985625], [116.4557338, 39.9855666], [116.4557496, 39.9855324], [116.4549778, 39.9853231], [116.4549281, 39.9854307], [116.4557543, 39.9856547], [116.4556471, 39.9858866], [116.4549759, 39.9857046], [116.4549237, 39.9858176]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '446529982', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4551823, 39.9851286], [116.4561797, 39.9853984], [116.4563715, 39.9849821], [116.4561511, 39.9849225], [116.4561688, 39.984884], [116.4554027, 39.9846768], [116.4553541, 39.9847822], [116.4561832, 39.9850064], [116.4560755, 39.9852404], [116.4552355, 39.9850132], [116.4551823, 39.9851286]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '446529983', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4536999, 39.9847222], [116.4546709, 39.984987], [116.454715, 39.9848922], [116.4538729, 39.9846626], [116.4539871, 39.9844168], [116.4548535, 39.9846531], [116.4549068, 39.9845384], [116.4539114, 39.9842669], [116.4536999, 39.9847222]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '449494044', code: 1500, fclass: 'building', name: null, type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4763327, 39.9881871], [116.4778696, 39.9896112], [116.4779447, 39.9896461], [116.478001, 39.9896708], [116.4780815, 39.9896811], [116.4781539, 39.9896913], [116.4782263, 39.9896831], [116.4795433, 39.9888303], [116.4796103, 39.9885138], [116.4793877, 39.9883124], [116.4792428, 39.9883905], [116.4788807, 39.9885775], [116.4786849, 39.9886638], [116.4772526, 39.9875438], [116.4770783, 39.9877021], [116.4769978, 39.9876445], [116.4767323, 39.9878377], [116.4763327, 39.9881871]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '449494190', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4745278, 39.9882877], [116.4749079, 39.988627], [116.4752793, 39.9883827], [116.4748991, 39.9880434], [116.4745278, 39.9882877]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '449494302', code: 1500, fclass: 'building', name: '方恒国际B座', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4747127, 39.988999], [116.4750624, 39.9893256], [116.4753375, 39.9891528], [116.4749877, 39.9888261], [116.4747127, 39.988999]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '449660710', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.476166, 39.9898257], [116.4763084, 39.989956], [116.4764106, 39.9898905], [116.4768285, 39.9896224], [116.4766861, 39.9894921], [116.476166, 39.9898257]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '449664880', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4665886, 39.988951], [116.4666035, 39.989054], [116.4667617, 39.9890304], [116.4668636, 39.9891157], [116.466971, 39.9890561], [116.4668703, 39.9889718], [116.4669227, 39.9888527], [116.4667883, 39.9888146], [116.4667255, 39.9889338], [116.4665886, 39.988951]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '449665620', code: 1500, fclass: 'building', name: 'Capital mall', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4615203, 39.9925265], [116.4619136, 39.9925417], [116.4621368, 39.9923817], [116.4621422, 39.9922779], [116.4623368, 39.9921051], [116.462902, 39.991686], [116.4628938, 39.9915968], [116.4629762, 39.9915876], [116.4630309, 39.9915465], [116.4630481, 39.9915084], [116.4630395, 39.9914578], [116.46299, 39.9914107], [116.4630133, 39.9908547], [116.4628741, 39.9907378], [116.4628402, 39.9906323], [116.4627414, 39.9906032], [116.4626471, 39.9906206], [116.46259, 39.9907125], [116.4624064, 39.9908198], [116.4623719, 39.9914894], [116.462013, 39.9917752], [116.4617287, 39.9919766], [116.4615892, 39.9921175], [116.461526, 39.9923153], [116.4615203, 39.9925265]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '452372058', code: 1500, fclass: 'building', name: '合生·麒麟新天地', type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4708054, 39.9956751], [116.4711916, 39.9960079], [116.4713901, 39.9959052], [116.471787, 39.9962668], [116.4722698, 39.9959792], [116.4721089, 39.9956627], [116.4716154, 39.9951572], [116.4708054, 39.9956751]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '452372060', code: 1500, fclass: 'building', name: '利星行中心E', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4671694, 39.9900379], [116.4686899, 39.9913938], [116.4695782, 39.9907922], [116.4688143, 39.9901199], [116.4683709, 39.9903985], [116.4676351, 39.9897592], [116.4671694, 39.9900379]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455243469', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4765477, 39.9764825], [116.4771573, 39.9770281], [116.4771261, 39.9770486], [116.4773773, 39.9772735], [116.4774803, 39.9772059], [116.4772355, 39.9769869], [116.4772691, 39.9769648], [116.4766529, 39.9764134], [116.4765477, 39.9764825]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455243470', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4769017, 39.9762466], [116.47752, 39.9767979], [116.4776249, 39.9767288], [116.4770067, 39.9761775], [116.4769017, 39.9762466]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455243471', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4772643, 39.9760136], [116.4778823, 39.9765615], [116.477849, 39.9765835], [116.4780998, 39.9768059], [116.4782088, 39.9767337], [116.4779582, 39.9765116], [116.4779915, 39.9764895], [116.4773732, 39.9759414], [116.4772643, 39.9760136]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455243472', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4776218, 39.9757595], [116.478228, 39.9763072], [116.4781928, 39.9763301], [116.478445, 39.9765579], [116.4785496, 39.9764899], [116.4783061, 39.9762699], [116.4783348, 39.9762512], [116.47772, 39.9756957], [116.4776218, 39.9757595]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455243473', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4779725, 39.9755285], [116.4785799, 39.9760747], [116.4785493, 39.9760947], [116.4787771, 39.9762995], [116.4788931, 39.9762237], [116.4786646, 39.9760183], [116.4786993, 39.9759957], [116.4780925, 39.9754501], [116.4779725, 39.9755285]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455245820', code: 1500, fclass: 'building', name: '花家地北里10号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4582197, 39.9853679], [116.4585339, 39.9853812], [116.4585469, 39.9852004], [116.4582327, 39.9851871], [116.4582197, 39.9853679]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455245821', code: 1500, fclass: 'building', name: '花家地北里12号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4577225, 39.9848811], [116.4580245, 39.9849006], [116.4580436, 39.9846872], [116.4577416, 39.9846801], [116.4577225, 39.9848811]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455245822', code: 1500, fclass: 'building', name: '花家地北里13号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4583073, 39.9845055], [116.4586215, 39.9845188], [116.4586346, 39.984338], [116.4583203, 39.9843247], [116.4583073, 39.9845055]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267196', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4792141, 39.9768961], [116.4794209, 39.9768969], [116.4794204, 39.9769759], [116.4796408, 39.9769768], [116.4796416, 39.9768479], [116.4794206, 39.976847], [116.4794211, 39.9767735], [116.4792149, 39.9767727], [116.4792141, 39.9768961]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267197', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4796223, 39.9772892], [116.4798291, 39.97729], [116.4798286, 39.977369], [116.480049, 39.9773699], [116.4800499, 39.9772409], [116.4798288, 39.9772401], [116.4798293, 39.9771666], [116.4796232, 39.9771658], [116.4796223, 39.9772892]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267198', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.480033, 39.9776555], [116.4802398, 39.9776563], [116.4802392, 39.9777353], [116.4804596, 39.9777362], [116.4804605, 39.9776072], [116.4802395, 39.9776063], [116.48024, 39.9775328], [116.4800338, 39.977532], [116.480033, 39.9776555]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267199', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4794398, 39.9775542], [116.4796859, 39.9777815], [116.4798003, 39.9777088], [116.4795541, 39.9774815], [116.4794398, 39.9775542]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267200', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.478738, 39.9766924], [116.4789289, 39.9768724], [116.479344, 39.9766137], [116.4791531, 39.9764337], [116.478738, 39.9766924]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267201', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4733233, 39.9783785], [116.4735301, 39.9783793], [116.4735296, 39.9784583], [116.47375, 39.9784592], [116.4737509, 39.9783302], [116.4735298, 39.9783294], [116.4735303, 39.9782559], [116.4733242, 39.9782551], [116.4733233, 39.9783785]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267202', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.473758, 39.9787782], [116.4739648, 39.978779], [116.4739643, 39.978858], [116.4741847, 39.9788589], [116.4741856, 39.97873], [116.4739645, 39.9787291], [116.473965, 39.9786556], [116.4737588, 39.9786548], [116.473758, 39.9787782]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267203', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4742062, 39.9791868], [116.474413, 39.9791876], [116.4744125, 39.9792666], [116.4746329, 39.9792675], [116.4746338, 39.9791386], [116.4744127, 39.9791377], [116.4744132, 39.9790642], [116.4742071, 39.9790634], [116.4742062, 39.9791868]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267204', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.474668, 39.9795895], [116.4748748, 39.9795903], [116.4748742, 39.9796693], [116.4750946, 39.9796702], [116.4750955, 39.9795412], [116.4748745, 39.9795403], [116.474875, 39.9794669], [116.4746688, 39.979466], [116.474668, 39.9795895]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267205', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4751065, 39.980001], [116.4753133, 39.9800019], [116.4753128, 39.9800809], [116.4755332, 39.9800817], [116.4755341, 39.9799528], [116.475313, 39.9799519], [116.4753135, 39.9798784], [116.4751073, 39.9798776], [116.4751065, 39.980001]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267206', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4750307, 39.9790744], [116.4756492, 39.9796334], [116.4757549, 39.9795647], [116.4751364, 39.9790057], [116.4750307, 39.9790744]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267207', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4750126, 39.9788331], [116.475147, 39.9789514], [116.475321, 39.9788354], [116.4751866, 39.9787171], [116.4750126, 39.9788331]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267208', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.47537, 39.9785985], [116.4755044, 39.9787168], [116.4756784, 39.9786008], [116.475544, 39.9784824], [116.47537, 39.9785985]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267209', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4757229, 39.9783594], [116.4758573, 39.9784778], [116.4760313, 39.9783617], [116.4758969, 39.9782434], [116.4757229, 39.9783594]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267210', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.476194, 39.9782474], [116.4763284, 39.9783657], [116.4765024, 39.9782497], [116.476368, 39.9781314], [116.476194, 39.9782474]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267211', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4757437, 39.9786028], [116.4763637, 39.9791525], [116.4763315, 39.9791739], [116.4765761, 39.9793907], [116.4766804, 39.9793217], [116.4764363, 39.9791052], [116.4764621, 39.9790881], [116.4758416, 39.9785379], [116.4757437, 39.9786028]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '455267212', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4753831, 39.9788436], [116.4760039, 39.9794029], [116.4759753, 39.9794216], [116.4762229, 39.9796446], [116.4763266, 39.979577], [116.476081, 39.9793557], [116.4761143, 39.979334], [116.4754916, 39.9787729], [116.4753831, 39.9788436]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '456993300', code: 1500, fclass: 'building', name: '望馨商业中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4556282, 40.0078059], [116.4558056, 40.0078091], [116.455795, 40.0081517], [116.4561549, 40.0081582], [116.4561741, 40.0075396], [116.4561793, 40.0073701], [116.4561248, 40.0073691], [116.4556421, 40.0073603], [116.4556282, 40.0078059]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '458019741', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4753349, 39.9895467], [116.4756962, 39.9898677], [116.4759394, 39.9897071], [116.475578, 39.9893861], [116.4753349, 39.9895467]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '460150620', code: 1500, fclass: 'building', name: '滨河一号 1号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.49792, 39.9697553], [116.4979227, 39.969891], [116.4993657, 39.9698766], [116.499363, 39.969743], [116.49792, 39.9697553]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '460150621', code: 1500, fclass: 'building', name: '滨河一号 3号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.497912, 39.9692085], [116.4979146, 39.9693463], [116.499355, 39.969338], [116.499355, 39.9692024], [116.497912, 39.9692085]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '460150622', code: 1500, fclass: 'building', name: '滨河一号 2号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.500047, 39.9697389], [116.500047, 39.9698745], [116.5014927, 39.9698745], [116.50149, 39.9697409], [116.500047, 39.9697389]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '460150624', code: 1500, fclass: 'building', name: '滨河一号 4号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5000497, 39.96919], [116.5000497, 39.9693216], [116.5014793, 39.9693175], [116.501482, 39.96919], [116.5000497, 39.96919]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '460150626', code: 1500, fclass: 'building', name: '滨河一号 公寓楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4972602, 39.9693894], [116.4973594, 39.9693894], [116.4973541, 39.9698828], [116.4976437, 39.9698828], [116.4976491, 39.9690585], [116.4972655, 39.9690544], [116.4972602, 39.9693894]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '461106335', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4714211, 40.0010023], [116.47143, 40.0012337], [116.4716032, 40.0012405], [116.4717764, 40.0014004], [116.4717453, 40.0020673], [116.4719985, 40.0020775], [116.4720207, 40.0012779], [116.4716565, 40.0010057], [116.4714211, 40.0010023]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '461446736', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4729992, 39.9942373], [116.4733244, 39.9942373], [116.4733895, 39.9942933], [116.4735684, 39.9939445], [116.473487, 39.9939258], [116.4731293, 39.9941501], [116.4729992, 39.9942373]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '461446738', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4742549, 39.9967331], [116.4742903, 39.9967706], [116.4744183, 39.9967381], [116.4746888, 39.9966487], [116.474753, 39.9966125], [116.4747596, 39.9963835], [116.4747179, 39.9960295], [116.4746836, 39.995958], [116.4746269, 39.9959606], [116.4745776, 39.9960232], [116.474494, 39.9963571], [116.4742962, 39.9966614], [116.4742549, 39.9967331]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '474561467', code: 1500, fclass: 'building', name: '中轻大厦B座', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4856228, 40.0041387], [116.4862741, 40.0042617], [116.4863246, 40.0041211], [116.4856779, 40.0040122], [116.4856228, 40.0041387]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '476057394', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4570633, 40.0075094], [116.4570986, 40.0076829], [116.4578764, 40.0075678], [116.4578195, 40.0074133], [116.4570633, 40.0075094]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '476057395', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4569496, 40.0096962], [116.4576799, 40.0097372], [116.4577041, 40.009592], [116.4569631, 40.0095448], [116.4569496, 40.0096962]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '476057396', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4579531, 40.0079457], [116.4580026, 40.0080975], [116.4583212, 40.0080324], [116.4586185, 40.0079999], [116.4588887, 40.0080122], [116.4591048, 40.008076], [116.4591454, 40.0079217], [116.4589512, 40.0078698], [116.4586539, 40.0078535], [116.4583283, 40.0078915], [116.4579531, 40.0079457]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '476057397', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4592422, 40.0086715], [116.4599946, 40.0086724], [116.4599974, 40.0085246], [116.4592493, 40.0085142], [116.4592422, 40.0086715]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '476057398', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4593277, 40.0076805], [116.460065, 40.0076965], [116.4600639, 40.0075558], [116.459349, 40.0075341], [116.4593277, 40.0076805]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '476057399', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4569706, 40.0085859], [116.4569777, 40.0087594], [116.4577598, 40.0087296], [116.4577563, 40.0085588], [116.4569706, 40.0085859]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '476057400', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4578888, 40.0094652], [116.4582922, 40.0095086], [116.4587585, 40.0094987], [116.4590009, 40.0094673], [116.4589597, 40.0092915], [116.4587421, 40.0093294], [116.458287, 40.009344], [116.4579553, 40.0093016], [116.4578888, 40.0094652]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '476057401', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4592133, 40.0095853], [116.4592345, 40.0097262], [116.459978, 40.0096416], [116.45995, 40.0095025], [116.4592133, 40.0095853]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178036', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4535768, 39.9964372], [116.4535769, 39.9964544], [116.4536688, 39.9964541], [116.4536687, 39.9964369], [116.4535768, 39.9964372]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178037', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4534594, 39.9964372], [116.4534595, 39.9964523], [116.4535515, 39.996452], [116.4535514, 39.9964369], [116.4534594, 39.9964372]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178038', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4533367, 39.9964372], [116.4533368, 39.9964538], [116.4534321, 39.9964536], [116.453432, 39.9964369], [116.4533367, 39.9964372]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178039', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.453332, 39.9964602], [116.4533321, 39.9964857], [116.4533879, 39.9964855], [116.4533878, 39.99646], [116.453332, 39.9964602]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178040', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4534453, 39.9964571], [116.4534454, 39.9964888], [116.4534959, 39.9964886], [116.4534957, 39.9964569], [116.4534453, 39.9964571]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178041', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.453511, 39.9964545], [116.4535111, 39.9964919], [116.453567, 39.9964917], [116.4535668, 39.9964544], [116.453511, 39.9964545]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178042', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4536995, 39.9964361], [116.4536996, 39.9964559], [116.4537862, 39.9964556], [116.4537861, 39.9964359], [116.4536995, 39.9964361]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178043', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4537497, 39.9964586], [116.4537499, 39.9964877], [116.453801, 39.9964876], [116.4538008, 39.9964585], [116.4537497, 39.9964586]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178044', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4538691, 39.9964642], [116.4538692, 39.9964898], [116.4539089, 39.9964897], [116.4539088, 39.9964641], [116.4538691, 39.9964642]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178045', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4538114, 39.9964361], [116.4538116, 39.9964564], [116.4539062, 39.9964561], [116.4539061, 39.9964359], [116.4538114, 39.9964361]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178046', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4538194, 39.9963488], [116.4538196, 39.9963891], [116.4539009, 39.9963889], [116.4539007, 39.9963485], [116.4538194, 39.9963488]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178047', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4537013, 39.9963406], [116.4537016, 39.9963907], [116.453783, 39.9963904], [116.4537827, 39.9963403], [116.4537013, 39.9963406]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178048', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4535813, 39.9963463], [116.4535816, 39.9963896], [116.453677, 39.9963893], [116.4536768, 39.996346], [116.4535813, 39.9963463]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178049', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.45346, 39.9963488], [116.4534602, 39.9963901], [116.4535456, 39.9963899], [116.4535453, 39.9963485], [116.45346, 39.9963488]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '478178050', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4533426, 39.9963489], [116.4533428, 39.9963903], [116.4534349, 39.99639], [116.4534347, 39.9963487], [116.4533426, 39.9963489]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635768', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4808058, 40.0077962], [116.4808876, 40.0078474], [116.480925, 40.0078123], [116.4808433, 40.0077611], [116.4808058, 40.0077962]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635769', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4809057, 40.0078486], [116.4809694, 40.0078885], [116.4810035, 40.0078565], [116.4809399, 40.0078166], [116.4809057, 40.0078486]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635770', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4809747, 40.0078053], [116.4810195, 40.0078333], [116.4810954, 40.0077622], [116.4810507, 40.0077341], [116.4809747, 40.0078053]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635771', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4807958, 40.0077014], [116.4809366, 40.0077896], [116.481002, 40.0077283], [116.4808612, 40.0076402], [116.4807958, 40.0077014]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635772', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4803122, 40.0075259], [116.4804271, 40.0075934], [116.4804866, 40.0075341], [116.4803717, 40.0074666], [116.4803122, 40.0075259]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635773', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4801432, 40.0077406], [116.4807153, 40.0080771], [116.4807601, 40.0080323], [116.4801881, 40.0076958], [116.4801432, 40.0077406]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635774', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4803525, 40.0076769], [116.4808249, 40.0079548], [116.4808728, 40.007907], [116.4804004, 40.0076291], [116.4803525, 40.0076769]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635775', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4809586, 40.0079257], [116.4812315, 40.0080862], [116.4815407, 40.0077778], [116.4812678, 40.0076173], [116.4809586, 40.0079257]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635776', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.47974, 40.007297], [116.4799925, 40.0074455], [116.4803498, 40.0070892], [116.480239, 40.0070241], [116.4801488, 40.0071141], [116.4800071, 40.0070307], [116.47974, 40.007297]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '479635777', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.479992, 40.0069788], [116.4801383, 40.0070708], [116.4802054, 40.0070082], [116.4800591, 40.0069162], [116.479992, 40.0069788]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '490488976', code: 1500, fclass: 'building', name: '360大厦', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.484134, 39.9818496], [116.4845021, 39.9818415], [116.4844985, 39.9813967], [116.4841375, 39.9813967], [116.484134, 39.9818496]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '500995351', code: 1500, fclass: 'building', name: '雕塑楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4593508, 39.9805119], [116.4593861, 39.9809358], [116.4600025, 39.9809057], [116.4599672, 39.9804818], [116.4593508, 39.9805119]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '508313189', code: 1500, fclass: 'building', name: '方恒国际中心A座', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4740519, 39.988533], [116.4742538, 39.988715], [116.4745719, 39.9885079], [116.47437, 39.9883258], [116.4740519, 39.988533]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '522062192', code: 1500, fclass: 'building', name: '京客隆超市', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4958965, 39.9691245], [116.4959141, 39.9693081], [116.4959787, 39.9693045], [116.4960008, 39.9695356], [116.4960902, 39.9695305], [116.4961087, 39.969724], [116.4962142, 39.9697181], [116.4962351, 39.9699365], [116.4963229, 39.9699315], [116.4963356, 39.9700635], [116.496636, 39.9700466], [116.4965441, 39.969088], [116.4958965, 39.9691245]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '529168264', code: 1500, fclass: 'building', name: '北京东隅酒店', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.486041, 39.9677164], [116.4861681, 39.9678552], [116.486345, 39.9675808], [116.4863289, 39.967274], [116.4861447, 39.9672621], [116.4861221, 39.9675037], [116.486041, 39.9677164]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '529169996', code: 1500, fclass: 'building', name: '大众汽车体验中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4847446, 39.9669608], [116.4877526, 39.9671756], [116.4878001, 39.9667844], [116.4847921, 39.9665696], [116.4847446, 39.9669608]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '529862389', code: 1500, fclass: 'building', name: 'Volkswagen Training Academy Building 1&2', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4922198, 39.9806814], [116.4929262, 39.9806891], [116.4929313, 39.9804125], [116.4922249, 39.9804049], [116.4922198, 39.9806814]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '529862390', code: 1500, fclass: 'building', name: 'Volkswagen Training Academy Building 3', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4923968, 39.9802695], [116.4929573, 39.9802718], [116.4929596, 39.9799575], [116.4923991, 39.9799552], [116.4923968, 39.9802695]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '529862391', code: 1500, fclass: 'building', name: 'Roo`s Club', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4871542, 39.9806488], [116.4872835, 39.9806504], [116.4872878, 39.9803993], [116.48716, 39.9803983], [116.4871542, 39.9806488]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532216104', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4709439, 39.9761807], [116.4716225, 39.9768302], [116.471762, 39.9767398], [116.4711585, 39.9761931], [116.4709439, 39.9761807]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532216105', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.470673, 39.9739197], [116.4711933, 39.9743945], [116.4714777, 39.9742157], [116.4713409, 39.9740883], [116.4713892, 39.9740307], [116.4711343, 39.9738005], [116.4710539, 39.9738211], [116.4709654, 39.9737409], [116.470673, 39.9739197]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532218241', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4780761, 39.978722], [116.4780784, 39.9789079], [116.4783599, 39.9789058], [116.4783608, 39.9789793], [116.4786348, 39.9789773], [116.4786325, 39.9787956], [116.4783674, 39.9787976], [116.4783665, 39.9787199], [116.4780761, 39.978722]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532218243', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4785848, 39.9786103], [116.4787135, 39.978598], [116.4787189, 39.9785158], [116.4791051, 39.9785199], [116.4791105, 39.9783472], [116.4786974, 39.9783431], [116.4785902, 39.9783924], [116.4785848, 39.9786103]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532218245', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4791105, 39.9779526], [116.4791212, 39.9782362], [116.4792607, 39.9782403], [116.47925, 39.9781746], [116.4796255, 39.9781746], [116.4801941, 39.9786679], [116.4804033, 39.9785363], [116.4796899, 39.9778868], [116.4792446, 39.9778992], [116.4791105, 39.9779526]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532218247', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4805657, 39.9778862], [116.4809949, 39.9782676], [116.4811186, 39.9781859], [116.4806894, 39.9778044], [116.4805657, 39.9778862]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532218248', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4810329, 39.9782988], [116.4814938, 39.9787204], [116.4816221, 39.978638], [116.4811612, 39.9782165], [116.4810329, 39.9782988]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532218249', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4815594, 39.9787614], [116.4816988, 39.9788847], [116.4816801, 39.9791724], [116.4818195, 39.9791786], [116.4818249, 39.9788271], [116.481672, 39.9786833], [116.4815594, 39.9787614]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532219136', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4761207, 39.9773169], [116.4765831, 39.9777423], [116.4767016, 39.9776666], [116.4762392, 39.9772413], [116.4761207, 39.9773169]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532220302', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.476093, 39.9804919], [116.4770157, 39.981314], [116.4773215, 39.9811167], [116.4771847, 39.9809914], [116.4770023, 39.9811106], [116.4763827, 39.9805536], [116.4765731, 39.9804159], [116.4764256, 39.9802802], [116.476093, 39.9804919]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513261', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4704495, 39.9671061], [116.4708408, 39.9671204], [116.4708596, 39.9668203], [116.4704682, 39.9668059], [116.4704495, 39.9671061]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513262', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4710252, 39.9667872], [116.4714222, 39.9668077], [116.4714492, 39.9664995], [116.4710522, 39.9664791], [116.4710252, 39.9667872]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513264', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4717114, 39.9710883], [116.4718766, 39.9712351], [116.4723969, 39.9708913], [116.4722316, 39.9707444], [116.4717114, 39.9710883]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513265', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4722807, 39.9707192], [116.4724433, 39.9708756], [116.4727483, 39.9706895], [116.4725857, 39.9705331], [116.4722807, 39.9707192]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513266', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4698177, 39.9724115], [116.4699514, 39.9725325], [116.4704738, 39.9721936], [116.4703402, 39.9720725], [116.4698177, 39.9724115]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513267', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4692433, 39.9727749], [116.4693803, 39.9729013], [116.4698958, 39.9725731], [116.4697589, 39.9724467], [116.4692433, 39.9727749]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513268', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4724675, 39.9717846], [116.4727257, 39.9718799], [116.4729048, 39.9715948], [116.4726467, 39.9714995], [116.4724675, 39.9717846]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513269', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4729177, 39.9721113], [116.4732634, 39.9721132], [116.4732662, 39.9718112], [116.4729206, 39.9718093], [116.4729177, 39.9721113]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513270', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4734198, 39.9716771], [116.4736966, 39.9719411], [116.4739788, 39.9717674], [116.4737019, 39.9715033], [116.4734198, 39.9716771]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513271', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4727655, 39.9713577], [116.4728996, 39.9714728], [116.473141, 39.9713043], [116.4733073, 39.9714317], [116.4734521, 39.9713248], [116.473259, 39.9711686], [116.4731142, 39.9711275], [116.4729854, 39.9711522], [116.4727655, 39.9713577]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513272', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4740534, 39.9710971], [116.474241, 39.9712428], [116.4744613, 39.9710762], [116.4742736, 39.9709305], [116.4740534, 39.9710971]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513273', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4735867, 39.9706695], [116.4737853, 39.9708295], [116.4740162, 39.9706611], [116.4738176, 39.9705011], [116.4735867, 39.9706695]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532513274', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4743754, 39.9703814], [116.4745569, 39.9705456], [116.4747645, 39.9704108], [116.4745899, 39.9702499], [116.4743754, 39.9703814]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532515221', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4750152, 39.9719802], [116.475523, 39.9724194], [116.4757036, 39.9722968], [116.4751957, 39.9718576], [116.4750152, 39.9719802]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532515222', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4750263, 39.9759816], [116.4750667, 39.9761118], [116.4752484, 39.976252], [116.4754632, 39.9762828], [116.4756059, 39.9762393], [116.4756819, 39.9761669], [116.4756934, 39.9760386], [116.4756311, 39.9759421], [116.4754779, 39.9758208], [116.4753202, 39.9757772], [116.4751657, 39.9757902], [116.4750752, 39.9758516], [116.4750263, 39.9759816]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532516947', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4707767, 39.9755447], [116.4710025, 39.9757451], [116.4712717, 39.9755669], [116.4710459, 39.9753665], [116.4707767, 39.9755447]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532516948', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4701566, 39.9754234], [116.4703828, 39.9756195], [116.4706573, 39.9754336], [116.4704311, 39.9752374], [116.4701566, 39.9754234]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532548130', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4724273, 39.9736236], [116.4724863, 39.9736747], [116.4724644, 39.9736895], [116.4725141, 39.9737326], [116.4725776, 39.9736895], [116.4726094, 39.973717], [116.4727358, 39.9736313], [116.4726228, 39.9735334], [116.4726035, 39.9735465], [116.4725405, 39.9734919], [116.4724877, 39.9735277], [116.4725232, 39.9735585], [116.4724273, 39.9736236]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532764844', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4787406, 39.9675268], [116.4798028, 39.9683373], [116.4799318, 39.968238], [116.4788696, 39.9674275], [116.4787406, 39.9675268]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532764845', code: 1500, fclass: 'building', name: '芳园里46号', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4755017, 39.9698961], [116.4755048, 39.9703193], [116.4759129, 39.9703175], [116.4759098, 39.9698944], [116.4755017, 39.9698961]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532764846', code: 1500, fclass: 'building', name: '芳园里47号', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4761306, 39.9702056], [116.476544, 39.9702095], [116.4765506, 39.9698072], [116.4761372, 39.9698032], [116.4761306, 39.9702056]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532765719', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4712908, 39.9696343], [116.4716545, 39.9699675], [116.4718258, 39.9698577], [116.4714621, 39.9695245], [116.4712908, 39.9696343]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '532765763', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4707313, 39.9693127], [116.4709147, 39.9694845], [116.4710151, 39.9694215], [116.4708317, 39.9692497], [116.4707313, 39.9693127]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '533788766', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4780182, 39.9877647], [116.4789033, 39.9885004], [116.4792252, 39.9883114], [116.4790911, 39.9881675], [116.4789087, 39.9882662], [116.4782435, 39.9876949], [116.4782542, 39.9874729], [116.4780236, 39.9874729], [116.4780182, 39.9877647]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '533788767', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4756525, 39.9875346], [116.4759314, 39.9877812], [116.4760602, 39.9877894], [116.4763821, 39.987588], [116.4764035, 39.9874154], [116.4762104, 39.987403], [116.4761782, 39.9875346], [116.4762426, 39.9875469], [116.4761031, 39.987662], [116.4759583, 39.9876579], [116.47591, 39.987588], [116.4759636, 39.987551], [116.4758349, 39.98744], [116.4756525, 39.9875346]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '533788768', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4764278, 39.9872592], [116.4766431, 39.9872678], [116.4766639, 39.9869448], [116.4767177, 39.986985], [116.4767819, 39.9869345], [116.4767953, 39.9869119], [116.4775919, 39.9869509], [116.4775758, 39.9869756], [116.4776509, 39.9870367], [116.4777099, 39.9869941], [116.4776849, 39.987311], [116.4778764, 39.9873199], [116.4778898, 39.9871512], [116.4778192, 39.9871479], [116.4778333, 39.9869694], [116.477718, 39.9868811], [116.477565, 39.9868715], [116.4775704, 39.9868207], [116.4773783, 39.9868086], [116.4773733, 39.9868552], [116.477035, 39.986834], [116.4770401, 39.986787], [116.4768666, 39.9867761], [116.4768606, 39.9868316], [116.4766987, 39.9868215], [116.4765592, 39.9869098], [116.4764278, 39.9872592]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '533789597', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4741754, 39.9861735], [116.4748505, 39.9867742], [116.4755392, 39.9868231], [116.4755548, 39.9866921], [116.4750222, 39.986655], [116.4743509, 39.9860577], [116.4741754, 39.9861735]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '533789599', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4755529, 39.986333], [116.4758207, 39.9865901], [116.4763885, 39.9862429], [116.4761207, 39.9859858], [116.4755529, 39.986333]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '533789601', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4755229, 39.9852016], [116.4765502, 39.9861636], [116.4767697, 39.986026], [116.4757424, 39.9850639], [116.4755229, 39.9852016]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '533789605', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4726556, 39.9870405], [116.4730263, 39.987396], [116.4732125, 39.987282], [116.4728417, 39.9869265], [116.4726556, 39.9870405]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '535281051', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4711808, 39.975279], [116.4716971, 39.9757334], [116.4719427, 39.9755695], [116.472003, 39.9756225], [116.4721347, 39.9755347], [116.4715582, 39.9750272], [116.4711808, 39.975279]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '536121105', code: 1500, fclass: 'building', name: 'No.2', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4932124, 39.9899398], [116.4940774, 39.9907443], [116.4944274, 39.9905233], [116.4935624, 39.9897189], [116.4932124, 39.9899398]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '536620347', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4810402, 39.9817423], [116.4814093, 39.9820563], [116.4815139, 39.9819806], [116.4813067, 39.9817898], [116.4814442, 39.9816922], [116.481448, 39.9816317], [116.4816673, 39.9816324], [116.4816653, 39.9816513], [116.482031, 39.9816609], [116.4820404, 39.9816298], [116.482328, 39.9816346], [116.482328, 39.9815124], [116.4814184, 39.9814861], [116.4810402, 39.9817423]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '536621350', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.488161, 39.9808035], [116.488161, 39.981192], [116.4890986, 39.981192], [116.4890986, 39.9808035], [116.488161, 39.9808035]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '539413230', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4921851, 39.9789169], [116.4930621, 39.9789253], [116.4930937, 39.9769963], [116.4922165, 39.9769879], [116.4921851, 39.9789169]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '539413232', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4913201, 39.9769797], [116.492025, 39.9769807], [116.4920253, 39.9768303], [116.4913204, 39.9768294], [116.4913201, 39.9769797]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '539413655', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4892202, 39.9775928], [116.490199, 39.9776004], [116.4902011, 39.9774373], [116.4898876, 39.9774349], [116.4898901, 39.9772519], [116.4892249, 39.9772467], [116.4892202, 39.9775928]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '539416693', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4842595, 39.9750108], [116.4844172, 39.9750132], [116.4844883, 39.9749567], [116.4852833, 39.9749689], [116.4852922, 39.9746284], [116.4854194, 39.9746303], [116.4854337, 39.9740865], [116.4842842, 39.9740688], [116.4842595, 39.9750108]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '539418172', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4844511, 39.9739211], [116.4849633, 39.9739222], [116.4849645, 39.9736058], [116.4844523, 39.9736047], [116.4844511, 39.9739211]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '539418173', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4841901, 39.9727871], [116.4841901, 39.9734099], [116.4846273, 39.9734099], [116.4846273, 39.9727871], [116.4841901, 39.9727871]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '541459603', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4737399, 40.0025805], [116.4737454, 40.0026696], [116.474321, 40.0026491], [116.4743156, 40.00256], [116.4737399, 40.0025805]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '541459604', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4728749, 40.0025885], [116.4728804, 40.0026818], [116.4736465, 40.0026552], [116.473641, 40.002562], [116.4728749, 40.0025885]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '541459605', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4745228, 40.0023898], [116.4745253, 40.0025024], [116.4753732, 40.0024914], [116.4753594, 40.0018678], [116.4751769, 40.0018702], [116.4751882, 40.0023812], [116.4745228, 40.0023898]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '541459606', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4728366, 40.0014374], [116.4728688, 40.0017374], [116.4734267, 40.001721], [116.4734168, 40.0015982], [116.4730297, 40.0016059], [116.473019, 40.0013429], [116.4728944, 40.0013847], [116.4728366, 40.0014374]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '541459607', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4732621, 40.0011456], [116.4732808, 40.0012398], [116.474372, 40.0011124], [116.4743533, 40.0010183], [116.4732621, 40.0011456]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '541459608', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4745266, 40.000974], [116.4745405, 40.0010718], [116.4754297, 40.0009977], [116.4754158, 40.0008998], [116.4745266, 40.000974]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '545375953', code: 1500, fclass: 'building', name: '南湖东园2区210号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4569149, 39.9981437], [116.4571142, 39.9981474], [116.4571406, 39.9980205], [116.4577624, 39.9980186], [116.4577768, 39.9978733], [116.4571286, 39.997877], [116.4570038, 39.9979046], [116.4569533, 39.9979874], [116.4569149, 39.9981437]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '554491577', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4891791, 39.9741804], [116.4891818, 39.9745288], [116.4894017, 39.9745278], [116.489399, 39.9741794], [116.4891791, 39.9741804]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '563841888', code: 1500, fclass: 'building', name: '望京绿地中心A座', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4798885, 39.9993284], [116.4800909, 39.9995118], [116.4802827, 39.9993876], [116.4804418, 39.9995318], [116.4806646, 39.9993876], [116.480511, 39.9992484], [116.4807079, 39.9991209], [116.480639, 39.9990584], [116.4805182, 39.998949], [116.4803333, 39.9990688], [116.4801474, 39.9989003], [116.4799245, 39.9990446], [116.4800922, 39.9991966], [116.4798885, 39.9993284]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564589319', code: 1500, fclass: 'building', name: '教学楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4685703, 40.0024129], [116.4687876, 40.0024173], [116.4687993, 40.0023015], [116.468826, 40.0023039], [116.4688318, 40.0023569], [116.4692684, 40.0023584], [116.4692722, 40.0023996], [116.4696068, 40.0024144], [116.4696145, 40.0022096], [116.4688996, 40.0022006], [116.468801, 40.0021993], [116.4687318, 40.0021404], [116.4685933, 40.0021419], [116.4685703, 40.0024129]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564589326', code: 1500, fclass: 'building', name: '望京西园一区供热站', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4695426, 40.0018392], [116.469899, 40.0018496], [116.4699035, 40.0017593], [116.469547, 40.0017489], [116.4695426, 40.0018392]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564923383', code: 1500, fclass: 'building', name: '对外经济贸易大学附属中学（高中部）学生食堂', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4517633, 39.9945361], [116.4522587, 39.9945389], [116.4522616, 39.9942422], [116.4518331, 39.9942398], [116.4517662, 39.9942394], [116.4517633, 39.9945361]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564923385', code: 1500, fclass: 'building', name: '对外经济贸易大学附属中学（高中部）学生宿舍楼', type: 'dormitory'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4522885, 39.9947068], [116.4529069, 39.9947076], [116.4529073, 39.9945364], [116.4525338, 39.9945359], [116.4522889, 39.9945355], [116.4522885, 39.9947068]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564923391', code: 1500, fclass: 'building', name: '器材库', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4538522, 39.9933636], [116.4538529, 39.9934342], [116.453932, 39.9934336], [116.4539312, 39.9933631], [116.4538522, 39.9933636]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564923392', code: 1500, fclass: 'building', name: '对外经济贸易大学附属中学（高中部）领操台', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4538056, 39.9935035], [116.4538084, 39.9941478], [116.4539114, 39.9941479], [116.453927, 39.9934992], [116.4538056, 39.9935035]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564923401', code: 1500, fclass: 'building', name: '对外经济贸易大学附属中学（高中部）高三年级教学楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.452348, 39.9941322], [116.4523481, 39.994198], [116.4523496, 39.9942628], [116.4529054, 39.9942628], [116.4529054, 39.9941228], [116.4528387, 39.9941228], [116.4528387, 39.9940943], [116.4523496, 39.9940943], [116.452348, 39.9941322]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564923402', code: 1500, fclass: 'building', name: '对外经济贸易大学附属中学（高中部）教学楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4520011, 39.9936413], [116.4520019, 39.9939591], [116.4520024, 39.9941985], [116.4523037, 39.9941981], [116.4523481, 39.994198], [116.452348, 39.9941322], [116.4523106, 39.9941323], [116.4522647, 39.9941323], [116.4522635, 39.9936416], [116.4520011, 39.9936413]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564923403', code: 1500, fclass: 'building', name: '对外经济贸易大学附属中学（高中部）高一高二年级教学楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4516933, 39.9934479], [116.4516937, 39.9935968], [116.4517479, 39.9935967], [116.451748, 39.9936417], [116.4520011, 39.9936413], [116.4522635, 39.9936416], [116.4523131, 39.9936415], [116.4524242, 39.9936414], [116.4524239, 39.993518], [116.4523883, 39.9935181], [116.4523881, 39.9934469], [116.4516933, 39.9934479]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564923411', code: 1500, fclass: 'building', name: '对外经济贸易大学附属中学（高中部）体育馆', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4524893, 39.9931675], [116.4524915, 39.9935123], [116.4529188, 39.9935107], [116.4529166, 39.9931659], [116.4524893, 39.9931675]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '564923414', code: 1500, fclass: 'building', name: '对外经济贸易大学附属中学（高中部）阶梯教室', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.452484, 39.9937428], [116.4527258, 39.9937454], [116.452729, 39.9935667], [116.4524872, 39.9935641], [116.452484, 39.9937428]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '8060830', code: 1500, fclass: 'building', name: '北京陈经纶中学分校（初三年级部）教学楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4569555, 39.9973997], [116.4569734, 39.9974113], [116.4569803, 39.9974694], [116.4569803, 39.9974789], [116.4569913, 39.9975], [116.4570065, 39.9975127], [116.4570451, 39.9975233], [116.4570754, 39.9975169], [116.457081, 39.9974948], [116.4570989, 39.9974768], [116.4571471, 39.9974747], [116.4571554, 39.9975011], [116.4573664, 39.9975022], [116.457365, 39.9974747], [116.4577345, 39.9974768], [116.4577235, 39.9973163], [116.4575208, 39.9973194], [116.4575194, 39.997293], [116.4574946, 39.9972909], [116.4574684, 39.9972941], [116.4574698, 39.9973437], [116.4570051, 39.9973332], [116.4570065, 39.9973744], [116.4569582, 39.997386], [116.4569555, 39.9973997]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '565023537', code: 1500, fclass: 'building', name: '中福百货（鑫隆购物）', type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4571075, 39.9989298], [116.4571131, 39.9990697], [116.4572153, 39.9990739], [116.4572089, 39.9991669], [116.4572068, 39.9992125], [116.4572183, 39.9992449], [116.4572472, 39.9992715], [116.4572587, 39.999295], [116.457326, 39.9993436], [116.4573529, 39.9993554], [116.4573702, 39.9993716], [116.4574807, 39.9993785], [116.4574788, 39.9993959], [116.4575818, 39.9994026], [116.4575895, 39.9993878], [116.4577494, 39.9993954], [116.4577466, 39.9994296], [116.458026, 39.9994676], [116.4580825, 39.9992957], [116.4577768, 39.9992551], [116.4577791, 39.998939], [116.4577031, 39.9989436], [116.4577086, 39.998897], [116.4571834, 39.998892], [116.4571753, 39.9989263], [116.4571075, 39.9989298]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '565023541', code: 1500, fclass: 'building', name: '报刊亭', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4563063, 39.9998216], [116.4563519, 39.9998222], [116.4563524, 39.9997986], [116.4563068, 39.999798], [116.4563063, 39.9998216]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '565034638', code: 1500, fclass: 'building', name: '朝阳区消防救援支队望京中队', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4569653, 39.995035], [116.4571633, 39.9950396], [116.4571753, 39.9947348], [116.4575236, 39.9947429], [116.4575307, 39.9945629], [116.4569845, 39.9945502], [116.4569653, 39.995035]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '565100571', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4531054, 39.9975583], [116.4531128, 39.9976491], [116.4532662, 39.9976418], [116.4532589, 39.9975511], [116.4531054, 39.9975583]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '565100572', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4569109, 39.9972019], [116.4571292, 39.9972036], [116.457132, 39.9969913], [116.4569137, 39.9969896], [116.4569109, 39.9972019]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '575391583', code: 1500, fclass: 'building', name: '超图大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4993112, 39.9845759], [116.4999697, 39.9845765], [116.4999703, 39.984188], [116.4993118, 39.9841875], [116.4993112, 39.9845759]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '576542703', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4678861, 40.0021505], [116.4679577, 40.0021523], [116.467952, 40.0022854], [116.4682865, 40.0022937], [116.468284, 40.0023528], [116.4683214, 40.0023814], [116.4683424, 40.0023867], [116.4683727, 40.002385], [116.4684171, 40.0023581], [116.4684321, 40.0021319], [116.4683167, 40.0021274], [116.46827, 40.0020863], [116.4681883, 40.0020791], [116.4681626, 40.0020523], [116.4680505, 40.0020451], [116.4680365, 40.0020183], [116.4678919, 40.0020147], [116.4678861, 40.0021505]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '576542704', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4674606, 40.0015695], [116.467599, 40.0015749], [116.4675961, 40.0016194], [116.4676534, 40.0016217], [116.4676498, 40.0016748], [116.4683302, 40.0017016], [116.4683401, 40.0015553], [116.4676976, 40.0015299], [116.4677141, 40.0012839], [116.4674804, 40.0012747], [116.4674606, 40.0015695]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '576542705', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.467796, 40.0013009], [116.46837, 40.0013324], [116.4683843, 40.0011803], [116.4678103, 40.0011488], [116.467796, 40.0013009]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '576542706', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4685301, 40.0013239], [116.4691093, 40.0013528], [116.4691209, 40.0012167], [116.4685417, 40.0011877], [116.4685301, 40.0013239]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '576542707', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4685387, 40.0009465], [116.4692264, 40.0009728], [116.4692131, 40.0011773], [116.469439, 40.0011859], [116.4694538, 40.0009585], [116.4694128, 40.0009569], [116.4693812, 40.0009175], [116.4693138, 40.0009149], [116.4693156, 40.0008863], [116.4692526, 40.0008839], [116.4692584, 40.0007949], [116.4685504, 40.0007678], [116.4685387, 40.0009465]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '576542708', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.467472, 40.001161], [116.4676961, 40.0011723], [116.467715, 40.0009534], [116.4683892, 40.0009875], [116.4684025, 40.0008333], [116.4676596, 40.0007957], [116.4676534, 40.0008677], [116.4676179, 40.0008659], [116.4676147, 40.0009032], [116.4675532, 40.0009001], [116.4675512, 40.0009239], [116.4675325, 40.0009167], [116.46753, 40.0009459], [116.4674907, 40.0009439], [116.467472, 40.001161]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '602954941', code: 1500, fclass: 'building', name: '荣之联', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4993336, 39.9853916], [116.4999854, 39.9853927], [116.4999866, 39.9849858], [116.4993348, 39.9849846], [116.4993336, 39.9853916]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '605959253', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4710778, 39.9708148], [116.4713722, 39.9710732], [116.4719132, 39.9707112], [116.4715353, 39.9703796], [116.4713981, 39.9704714], [116.4714816, 39.9705446], [116.4710778, 39.9708148]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '611563760', code: 1500, fclass: 'building', name: '百分百商城', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4825813, 39.9664137], [116.482651, 39.9664178], [116.4826403, 39.9664836], [116.4828656, 39.9666481], [116.4833376, 39.9666481], [116.483343, 39.966315], [116.4826027, 39.9662945], [116.4825813, 39.9664137]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '611563761', code: 1500, fclass: 'building', name: 'Indigo One', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4844268, 39.9677531], [116.4849864, 39.9679576], [116.4852302, 39.9675656], [116.4846706, 39.9673611], [116.4844268, 39.9677531]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616216819', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4965228, 39.9777817], [116.4966342, 39.978316], [116.4968309, 39.9782919], [116.4967195, 39.9777576], [116.4965228, 39.9777817]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616216821', code: 1500, fclass: 'building', name: '驼房营村委会', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4965047, 39.9770833], [116.4966172, 39.9776446], [116.4968276, 39.9776198], [116.496715, 39.9770586], [116.4965047, 39.9770833]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616216822', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4947409, 39.9768271], [116.4956274, 39.9768376], [116.4956301, 39.9767036], [116.4954216, 39.9767011], [116.4954222, 39.9766704], [116.4947443, 39.9766623], [116.4947409, 39.9768271]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616216823', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4944252, 39.977294], [116.4956765, 39.9773039], [116.495681, 39.9769719], [116.4944297, 39.976962], [116.4944252, 39.977294]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616216824', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4940801, 39.9778683], [116.4946836, 39.9778693], [116.4947493, 39.9778456], [116.4950001, 39.9778508], [116.4950283, 39.9778662], [116.4956667, 39.9778775], [116.4956814, 39.9775291], [116.495039, 39.9775301], [116.4950109, 39.9775486], [116.4947869, 39.9775507], [116.4947534, 39.977526], [116.4940868, 39.977526], [116.4940801, 39.9778683]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616216825', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.49395, 39.9765056], [116.4956853, 39.9765137], [116.4956882, 39.9761602], [116.4939528, 39.976152], [116.49395, 39.9765056]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616216826', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4939604, 39.9759234], [116.4956924, 39.9759295], [116.4956945, 39.975579], [116.4939625, 39.9755729], [116.4939604, 39.9759234]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616216827', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4939552, 39.9753583], [116.4957147, 39.9753672], [116.4957179, 39.9749982], [116.4939583, 39.9749893], [116.4939552, 39.9753583]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616216828', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4939931, 39.9747219], [116.4957391, 39.9747292], [116.4957416, 39.9743705], [116.4939956, 39.9743632], [116.4939931, 39.9747219]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616217504', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4916377, 39.9707585], [116.4916382, 39.9709101], [116.4927353, 39.9709079], [116.4927347, 39.9707563], [116.4916377, 39.9707585]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616217505', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4918933, 39.9717918], [116.4930498, 39.9717919], [116.4930498, 39.9715997], [116.4927527, 39.9715997], [116.4921985, 39.9715996], [116.4918934, 39.9715996], [116.4918933, 39.9717918]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616217506', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.491906, 39.9726193], [116.4930513, 39.9726201], [116.4930517, 39.9722747], [116.4919064, 39.972274], [116.491906, 39.9726193]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616217507', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.491722, 39.9735504], [116.4929484, 39.9735523], [116.4929494, 39.9731833], [116.4917229, 39.9731815], [116.491722, 39.9735504]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616217508', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4917126, 39.9743374], [116.4927781, 39.9743388], [116.492779, 39.97392], [116.4917135, 39.9739186], [116.4917126, 39.9743374]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616217509', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4916795, 39.9750164], [116.4928771, 39.9750225], [116.4928799, 39.9746947], [116.4916823, 39.9746886], [116.4916795, 39.9750164]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616217510', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4905978, 39.9734093], [116.4913715, 39.973415], [116.491375, 39.9731343], [116.4906013, 39.9731286], [116.4905978, 39.9734093]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '616217511', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4905635, 39.9740626], [116.4906572, 39.9740627], [116.4906567, 39.9744173], [116.490568, 39.9744172], [116.4905675, 39.9748239], [116.4913474, 39.9748245], [116.4913479, 39.9744122], [116.4909142, 39.9744119], [116.4909147, 39.9740574], [116.4913407, 39.9740577], [116.4913412, 39.9736508], [116.490564, 39.9736502], [116.4905635, 39.9740626]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617434877', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4709922, 39.9749098], [116.4712207, 39.9751144], [116.4714928, 39.974936], [116.4712643, 39.9747313], [116.4709922, 39.9749098]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617434878', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4714489, 39.9745843], [116.4717175, 39.9748132], [116.4720019, 39.9746173], [116.4717334, 39.9743883], [116.4714489, 39.9745843]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617434879', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4718784, 39.9748425], [116.4721086, 39.9750531], [116.472412, 39.9748582], [116.4721818, 39.9746477], [116.4718784, 39.9748425]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617434880', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4722175, 39.9751467], [116.4724647, 39.975361], [116.4727635, 39.9751586], [116.4725163, 39.9749443], [116.4722175, 39.9751467]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617434881', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4704624, 39.9749645], [116.4705583, 39.9750505], [116.4707961, 39.9748949], [116.4707002, 39.9748089], [116.4704624, 39.9749645]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617434882', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4702603, 39.9751137], [116.4703447, 39.9751941], [116.470528, 39.9750811], [116.4704436, 39.9750007], [116.4702603, 39.9751137]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617434883', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.474354, 39.9740446], [116.4746028, 39.9742553], [116.4747082, 39.9741868], [116.4745748, 39.9740635], [116.4747844, 39.9739209], [116.474712, 39.9738343], [116.4748454, 39.9737484], [116.4748008, 39.9737154], [116.4749174, 39.9736415], [116.4748416, 39.9735758], [116.4751362, 39.9734079], [116.4754081, 39.9736532], [116.475747, 39.9734211], [116.4754611, 39.9731035], [116.4752712, 39.9730757], [116.4748852, 39.9732775], [116.4749531, 39.9733545], [116.4746598, 39.9735361], [116.4747147, 39.973594], [116.4746407, 39.9736387], [116.4746875, 39.9736809], [116.4745205, 39.9737851], [116.4746183, 39.9738686], [116.474354, 39.9740446]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626841', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4806915, 39.9687512], [116.4806939, 39.9689134], [116.4813485, 39.9689077], [116.4813461, 39.9687455], [116.4806915, 39.9687512]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626843', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4806815, 39.9691511], [116.4806849, 39.9692978], [116.4813375, 39.9692891], [116.4813342, 39.9691424], [116.4806815, 39.9691511]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626845', code: 1500, fclass: 'building', name: '飘 HOME 小区 1 号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4817096, 39.9690813], [116.4821925, 39.9691314], [116.4822159, 39.9688127], [116.4817811, 39.9687749], [116.4817096, 39.9690813]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626847', code: 1500, fclass: 'building', name: '飘 HOME 小区 2 号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4822254, 39.9696307], [116.4827022, 39.9696679], [116.4827588, 39.9693551], [116.4822981, 39.9693179], [116.4822254, 39.9696307]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626848', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4816275, 39.9695812], [116.4821686, 39.9696388], [116.4822051, 39.9694372], [116.481664, 39.9693797], [116.4816275, 39.9695812]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626850', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.483211, 39.9694603], [116.4833789, 39.9694624], [116.4833869, 39.9690231], [116.483219, 39.9690231], [116.483211, 39.9694603]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626852', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4829077, 39.9690103], [116.4832103, 39.9690124], [116.4832116, 39.9688959], [116.482909, 39.9688938], [116.4829077, 39.9690103]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626854', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4824057, 39.9687948], [116.4828895, 39.9687956], [116.4828899, 39.9686444], [116.4824061, 39.9686436], [116.4824057, 39.9687948]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626856', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.480019, 39.9686525], [116.480019, 39.9688057], [116.4806668, 39.9688057], [116.4806668, 39.9686525], [116.480019, 39.9686525]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626858', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4801041, 39.9690806], [116.480106, 39.9692175], [116.480547, 39.9692139], [116.4805451, 39.969077], [116.4801041, 39.9690806]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626860', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4779239, 39.9691613], [116.4796293, 39.9691824], [116.4796341, 39.9689545], [116.4779287, 39.9689335], [116.4779239, 39.9691613]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626862', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4806652, 39.9696574], [116.4812182, 39.9696651], [116.4812213, 39.9695356], [116.4806683, 39.9695279], [116.4806652, 39.9696574]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626864', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4802074, 39.9694544], [116.4802102, 39.9695943], [116.4805607, 39.9695903], [116.480558, 39.9694504], [116.4802074, 39.9694544]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617626866', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4804593, 39.967326], [116.480805, 39.967381], [116.4807786, 39.9674785], [116.481123, 39.9675333], [116.4811616, 39.9673908], [116.4808753, 39.9673452], [116.4808986, 39.9672595], [116.4804948, 39.9671951], [116.4804593, 39.967326]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617723331', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4831053, 39.976556], [116.4832902, 39.9765572], [116.483304, 39.975299], [116.4831191, 39.9752978], [116.4831053, 39.976556]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617723332', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4783505, 39.9750665], [116.4784156, 39.9751662], [116.4789603, 39.9756647], [116.4791311, 39.9755681], [116.4785172, 39.9750104], [116.4785213, 39.974945], [116.4804239, 39.9737081], [116.4803502, 39.9736458], [116.4785091, 39.9748266], [116.4783709, 39.9749575], [116.4783505, 39.9750665]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617723333', code: 1500, fclass: 'building', name: '芳园里甲13号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4803502, 39.9736458], [116.4804576, 39.9737432], [116.4809203, 39.9734438], [116.4809479, 39.9734689], [116.4810695, 39.9733901], [116.4809574, 39.9732883], [116.4808199, 39.9733773], [116.480797, 39.9733566], [116.4803502, 39.9736458]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617723334', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4822578, 39.9717317], [116.4823431, 39.9718053], [116.4827411, 39.9715342], [116.4826558, 39.9714606], [116.4822578, 39.9717317]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617723335', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4816923, 39.972095], [116.4817894, 39.9721789], [116.4822494, 39.9718664], [116.4821523, 39.9717824], [116.4816923, 39.972095]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617723336', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.482818, 39.9709081], [116.4831712, 39.9709097], [116.4831697, 39.9711135], [116.4829253, 39.9712728], [116.4830222, 39.9713601], [116.483348, 39.9711477], [116.4833497, 39.9709162], [116.4833946, 39.9709164], [116.4833955, 39.9707911], [116.4833463, 39.9707909], [116.4833486, 39.9704817], [116.4831582, 39.9704808], [116.4831559, 39.9707777], [116.482819, 39.9707762], [116.482818, 39.9709081]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617723337', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4827984, 39.970256], [116.4831802, 39.9702578], [116.4831788, 39.9704333], [116.4833447, 39.9704341], [116.4833474, 39.9701063], [116.4827997, 39.9701037], [116.4827984, 39.970256]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '617927873', code: 1500, fclass: 'building', name: 'Kantine', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4714169, 39.9804217], [116.4716478, 39.9806402], [116.4718333, 39.9805251], [116.4716023, 39.9803066], [116.4714169, 39.9804217]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '621197847', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4962378, 39.9689615], [116.4965731, 39.9689738], [116.496616, 39.9687066], [116.4962593, 39.9686963], [116.4962378, 39.9689615]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '621197848', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.497291, 39.9688407], [116.4979977, 39.9688382], [116.4979944, 39.9685218], [116.4972943, 39.9685319], [116.497291, 39.9688407]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625068486', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4842813, 39.9782517], [116.4842928, 39.978625], [116.4843325, 39.9787276], [116.4844316, 39.9787922], [116.4845407, 39.978815], [116.4850543, 39.9788173], [116.4850478, 39.9786452], [116.4847272, 39.9786402], [116.484544, 39.9785199], [116.4845374, 39.9782491], [116.4842813, 39.9782517]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625068487', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4847014, 39.9781348], [116.4851202, 39.9781409], [116.4851322, 39.9778531], [116.4847096, 39.9778481], [116.4847014, 39.9781348]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625068488', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4852129, 39.9782027], [116.4852129, 39.9785298], [116.4855396, 39.978536], [116.4855524, 39.9782089], [116.4852129, 39.9782027]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625073975', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.471145, 39.9757529], [116.4714402, 39.9760232], [116.4717353, 39.9758367], [116.4714452, 39.975568], [116.471145, 39.9757529]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625073977', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4698173, 39.9747334], [116.4702133, 39.9750818], [116.4705971, 39.974827], [116.4702133, 39.974473], [116.4698173, 39.9747334]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625073979', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.469767, 39.9723585], [116.4699253, 39.9725075], [116.4704618, 39.9721728], [116.4703035, 39.9720238], [116.469767, 39.9723585]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625074949', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.469459, 39.9745717], [116.469643, 39.9747418], [116.4700555, 39.9744797], [116.4698715, 39.9743096], [116.469459, 39.9745717]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625194037', code: 1500, fclass: 'building', name: '宏源大厦', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4843266, 39.983549], [116.4843372, 39.9838636], [116.4844148, 39.9839235], [116.4843337, 39.9839802], [116.4846274, 39.9842269], [116.4847286, 39.9841657], [116.4848079, 39.9842269], [116.4851335, 39.9842297], [116.4851406, 39.9836575], [116.4850486, 39.9835653], [116.4843266, 39.983549]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625194540', code: 1500, fclass: 'building', name: '宏源公寓A座', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4840633, 39.9833873], [116.4843819, 39.9833879], [116.4843829, 39.9831182], [116.4840642, 39.9831176], [116.4840633, 39.9833873]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625194541', code: 1500, fclass: 'building', name: '宏源公寓B座', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4847932, 39.9833934], [116.4851119, 39.983394], [116.4851128, 39.9831243], [116.4847942, 39.9831236], [116.4847932, 39.9833934]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '625848706', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.482506, 39.9711873], [116.4826445, 39.97119], [116.4826652, 39.9705727], [116.4825267, 39.97057], [116.482506, 39.9711873]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626155031', code: 1500, fclass: 'building', name: '芳园里13号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4812323, 39.9731654], [116.4813711, 39.9732823], [116.4815436, 39.9731608], [116.4814544, 39.9730909], [116.4819778, 39.972743], [116.4818787, 39.9726564], [116.4813513, 39.9730089], [116.4813949, 39.9730453], [116.4812323, 39.9731654]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626535984', code: 1500, fclass: 'building', name: '芳园里12号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.481946, 39.9726159], [116.4820439, 39.9727064], [116.4826058, 39.9723446], [116.4825079, 39.9722583], [116.481946, 39.9726159]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626536191', code: 1500, fclass: 'building', name: '芳园里8号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4826501, 39.9721648], [116.4827306, 39.9722367], [116.4830095, 39.972064], [116.4829357, 39.97199], [116.4826501, 39.9721648]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626536254', code: 1500, fclass: 'building', name: '芳园里5号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4830873, 39.9720538], [116.4832107, 39.9720527], [116.4832107, 39.9720866], [116.4833247, 39.9720846], [116.48333, 39.9719479], [116.4830913, 39.971952], [116.4830873, 39.9720538]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626536603', code: 1500, fclass: 'building', name: '芳园里6号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4832388, 39.9721226], [116.4832388, 39.9724032], [116.4833689, 39.9724022], [116.4833676, 39.9721206], [116.4832388, 39.9721226]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626536649', code: 1500, fclass: 'building', name: '芳园里北区5号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4833247, 39.9724299], [116.483326, 39.972693], [116.4834521, 39.972692], [116.4834494, 39.9724279], [116.4833247, 39.9724299]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626536725', code: 1500, fclass: 'building', name: '芳园里7号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.482752, 39.9723364], [116.4831087, 39.9726447], [116.4832093, 39.9725728], [116.4828472, 39.9722665], [116.482752, 39.9723364]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626537302', code: 1500, fclass: 'building', name: '汉庭酒店', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4826367, 39.9735425], [116.4828244, 39.9735445], [116.4828405, 39.9732033], [116.4826528, 39.9732012], [116.4826367, 39.9735425]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626539356', code: 1500, fclass: 'building', name: '桔子水晶酒店', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4829743, 39.9748516], [116.4832775, 39.9748501], [116.4832799, 39.9741642], [116.4829821, 39.9741662], [116.4829743, 39.9748516]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626664810', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4842055, 39.9697625], [116.484268, 39.9697627], [116.4842676, 39.9698382], [116.4848486, 39.9698397], [116.4848494, 39.9696613], [116.484206, 39.9696595], [116.4842055, 39.9697625]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626664811', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4843504, 39.9701794], [116.4843943, 39.9702664], [116.4844053, 39.9703001], [116.4843943, 39.9703366], [116.4843613, 39.9704685], [116.4843797, 39.9705162], [116.4844309, 39.9705303], [116.4846507, 39.9704825], [116.4846873, 39.9704517], [116.4846946, 39.9704152], [116.4846763, 39.9703871], [116.4848228, 39.9703871], [116.484951, 39.9704292], [116.4851598, 39.9704292], [116.4851781, 39.9701541], [116.4843687, 39.9701541], [116.4843504, 39.9701794]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '626664812', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4828684, 39.9697663], [116.4833899, 39.9698105], [116.4834256, 39.9695631], [116.4829041, 39.9695189], [116.4828684, 39.9697663]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196401', code: 1500, fclass: 'building', name: '望京实验学校', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4699225, 39.9988481], [116.4715846, 39.9988979], [116.4716325, 39.997959], [116.4705353, 39.9979262], [116.4705528, 39.9975825], [116.4699879, 39.9975656], [116.4699225, 39.9988481]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196412', code: 1500, fclass: 'building', name: '望京西园315号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4685451, 39.9968844], [116.4686513, 39.9969874], [116.4688141, 39.9968681], [116.468984, 39.9968465], [116.4694441, 39.9968519], [116.4695998, 39.9968844], [116.4697343, 39.9969712], [116.4698617, 39.9969061], [116.4696564, 39.9967868], [116.4694511, 39.9967488], [116.468807, 39.9967488], [116.4685451, 39.9968844]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196415', code: 1500, fclass: 'building', name: '望京西园314号楼A座', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4699847, 39.9972125], [116.4701573, 39.9972975], [116.4702744, 39.9971701], [116.4708721, 39.9974108], [116.470946, 39.9973117], [116.4701881, 39.9970096], [116.4699847, 39.9972125]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196419', code: 1500, fclass: 'building', name: '望京西园314号楼B座', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4711001, 39.9974911], [116.4721476, 39.997661], [116.4721353, 39.9979018], [116.472351, 39.9979018], [116.4723571, 39.9976563], [116.4722339, 39.9975572], [116.4711617, 39.9973589], [116.4711001, 39.9974911]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196475', code: 1500, fclass: 'building', name: '望京西园325号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4679879, 40.0000928], [116.4681436, 40.0001964], [116.469158, 40.0002344], [116.4691654, 40.0001187], [116.4691213, 40.0001171], [116.4691283, 40.0000087], [116.4689163, 40.0000006], [116.4689092, 40.0001105], [116.4687802, 40.0001056], [116.4687861, 40.0000156], [116.4686386, 40.0000099], [116.4686322, 40.0001081], [116.4684808, 40.0001023], [116.4684869, 40.000009], [116.4683301, 40.0000029], [116.4683229, 40.0001117], [116.4681996, 40.000107], [116.4680698, 40.0000206], [116.4679879, 40.0000928]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196478', code: 1500, fclass: 'building', name: '望京西园304号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4696017, 40.0001929], [116.4701218, 40.0002013], [116.4701262, 40.0000412], [116.4700233, 40.0000396], [116.4700274, 39.9998903], [116.4698235, 39.999887], [116.469819, 40.0000497], [116.4696057, 40.0000463], [116.4696017, 40.0001929]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196482', code: 1500, fclass: 'building', name: '望京西园303号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4703833, 40.0001841], [116.4703833, 40.0003115], [116.4707962, 40.0003068], [116.4707962, 40.0001746], [116.4707037, 40.0001652], [116.4707037, 40.0000377], [116.4705004, 40.0000377], [116.4705004, 40.0001888], [116.4703833, 40.0001841]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196485', code: 1500, fclass: 'building', name: '望京西园302号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4711046, 40.0002781], [116.4715452, 40.0002921], [116.4715538, 40.000135], [116.47147, 40.0001323], [116.4714252, 40.0001309], [116.4714322, 40.0000032], [116.4712298, 39.9999968], [116.4712214, 40.0001699], [116.4711104, 40.0001699], [116.4711046, 40.0002781]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196488', code: 1500, fclass: 'building', name: '望京西园301号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4715767, 40.0001093], [116.471579, 40.0002334], [116.47163, 40.0002329], [116.4716289, 40.0001707], [116.471789, 40.000169], [116.4717904, 40.0002441], [116.4719512, 40.0002423], [116.4719486, 40.0001036], [116.4718331, 40.0001049], [116.4718304, 39.9999607], [116.4716318, 39.9999628], [116.4716345, 40.0001087], [116.4715767, 40.0001093]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196491', code: 1500, fclass: 'building', name: '望京西园307号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4696529, 39.9992335], [116.4700595, 39.9992437], [116.4700655, 39.9991034], [116.4699754, 39.9991011], [116.4699811, 39.9989684], [116.4697744, 39.9989632], [116.4697681, 39.9991112], [116.4696582, 39.9991085], [116.4696529, 39.9992335]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196494', code: 1500, fclass: 'building', name: '望京西园308号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4700972, 39.9992332], [116.4704573, 39.9992472], [116.4704658, 39.999118], [116.4703677, 39.9991142], [116.4703756, 39.9989941], [116.4701867, 39.9989867], [116.4701799, 39.9990885], [116.4701069, 39.9990857], [116.4700972, 39.9992332]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196497', code: 1500, fclass: 'building', name: '望京西园309号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4707577, 39.9991238], [116.4707577, 39.999249], [116.4711454, 39.9992633], [116.471164, 39.9991274], [116.4710706, 39.9991166], [116.4710753, 39.9990129], [116.4708698, 39.9990165], [116.4708652, 39.9991238], [116.4707577, 39.9991238]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196501', code: 1500, fclass: 'building', name: '望京西园310号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4711874, 39.9992705], [116.4715516, 39.9992812], [116.4715423, 39.9991667], [116.4714629, 39.9991667], [116.4714723, 39.9990272], [116.4712761, 39.9990236], [116.4712714, 39.9991632], [116.4712014, 39.9991632], [116.4711874, 39.9992705]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196504', code: 1500, fclass: 'building', name: '望京西园311号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4718541, 39.9992411], [116.4718551, 39.9993886], [116.4722548, 39.999387], [116.472254, 39.9992583], [116.4721696, 39.9992587], [116.4721684, 39.9990808], [116.4719734, 39.9990815], [116.4719745, 39.9992406], [116.4718541, 39.9992411]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196523', code: 1500, fclass: 'building', name: null, type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4703346, 39.9956772], [116.4704916, 39.9958087], [116.4708832, 39.9955343], [116.4707262, 39.9954028], [116.4703346, 39.9956772]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196524', code: 1500, fclass: 'building', name: null, type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4708802, 39.9953167], [116.471026, 39.9954591], [116.4714113, 39.9952277], [116.4712655, 39.9950852], [116.4708802, 39.9953167]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196526', code: 1500, fclass: 'building', name: null, type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4708258, 39.9964284], [116.4710082, 39.996601], [116.4714683, 39.9963044], [116.4712822, 39.9961348], [116.4708258, 39.9964284]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196527', code: 1500, fclass: 'building', name: null, type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4715301, 39.9966377], [116.4717232, 39.9968144], [116.4721953, 39.9965021], [116.4719915, 39.9963254], [116.4715301, 39.9966377]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627196529', code: 1500, fclass: 'building', name: null, type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4703123, 39.9961421], [116.4704832, 39.9963204], [116.4708855, 39.9960574], [116.4707031, 39.9958889], [116.4703123, 39.9961421]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627425284', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4669954, 39.9965589], [116.4675192, 39.9965636], [116.4675192, 39.9963464], [116.4670016, 39.9963323], [116.4669954, 39.9965589]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '627425286', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.467968, 39.9961057], [116.4682937, 39.9961165], [116.468322, 39.9956664], [116.4680176, 39.9956501], [116.467968, 39.9961057]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '628181596', code: 1500, fclass: 'building', name: '芳园里17号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.481264, 39.9739137], [116.4817959, 39.9739191], [116.4818011, 39.9737801], [116.4812655, 39.973777], [116.481264, 39.9739137]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '628181597', code: 1500, fclass: 'building', name: '芳园里甲17号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.481277, 39.9735674], [116.481809, 39.9735728], [116.4818142, 39.9734338], [116.4812786, 39.9734307], [116.481277, 39.9735674]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '628181710', code: 1500, fclass: 'building', name: '芳园里乙17号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4820243, 39.9737938], [116.4827582, 39.973798], [116.482765, 39.9736613], [116.4820259, 39.9736571], [116.4820243, 39.9737938]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '628182099', code: 1500, fclass: 'building', name: '芳园里丙17号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4821435, 39.9732388], [116.4821449, 39.9733611], [116.4821972, 39.97336], [116.4821972, 39.9733323], [116.4822803, 39.9733313], [116.482279, 39.97336], [116.4823594, 39.97336], [116.4823608, 39.9733313], [116.482448, 39.9733302], [116.482448, 39.97336], [116.4825284, 39.97336], [116.4825284, 39.9733364], [116.4826371, 39.9733364], [116.4826411, 39.9732377], [116.4826022, 39.9732388], [116.4826035, 39.9732233], [116.4825445, 39.9732233], [116.4825445, 39.9732388], [116.4824412, 39.9732398], [116.4824426, 39.9732213], [116.4823782, 39.9732213], [116.4823769, 39.9732388], [116.4822629, 39.9732388], [116.4822656, 39.9732192], [116.4822039, 39.9732213], [116.4822025, 39.9732388], [116.4821435, 39.9732388]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '632762951', code: 1500, fclass: 'building', name: '慧谷时空', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4635891, 40.0019501], [116.4647758, 40.0019556], [116.4647758, 40.0010466], [116.4661422, 40.0010907], [116.4661782, 40.0006389], [116.4636826, 40.0005618], [116.4636394, 40.0011293], [116.4635891, 40.0019501]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '632768142', code: 1500, fclass: 'building', name: '卓悦童', type: 'kindergarten'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4636372, 40.0020107], [116.4638856, 40.0024864], [116.4647316, 40.0024983], [116.4647471, 40.0020048], [116.4636372, 40.0020107]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '669450402', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4420002, 39.971486], [116.4423347, 39.9717765], [116.4429396, 39.9713674], [116.442428, 39.9709231], [116.4420394, 39.971186], [116.4420002, 39.971486]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '686527797', code: 1500, fclass: 'building', name: '一展空间（冠寓）', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4938327, 39.9742285], [116.4942394, 39.9742285], [116.4942579, 39.9736571], [116.4938389, 39.9736524], [116.4938327, 39.9742285]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '686529696', code: 1500, fclass: 'building', name: '北京国际画材中心', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4586542, 39.9840619], [116.4588517, 39.9842445], [116.4592161, 39.9840131], [116.4590186, 39.9838305], [116.4586542, 39.9840619]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '687317557', code: 1500, fclass: 'building', name: '新发地水果蔬菜超市', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4699693, 40.0003592], [116.4702786, 40.0003766], [116.4702892, 40.0002657], [116.4699799, 40.0002483], [116.4699693, 40.0003592]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '687359084', code: 1500, fclass: 'building', name: '兆维工业园C3号楼', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4872824, 39.9733643], [116.4888952, 39.9733836], [116.4889018, 39.9730611], [116.4872889, 39.9730419], [116.4872824, 39.9733643]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '687359085', code: 1500, fclass: 'building', name: '兆维工业园C2号楼', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4856054, 39.9733494], [116.4868319, 39.9733608], [116.4868371, 39.9730306], [116.4867022, 39.9730294], [116.486703, 39.9729787], [116.4864779, 39.9729766], [116.486477, 39.9730302], [116.4859873, 39.9730256], [116.4859881, 39.9729704], [116.4857639, 39.9729683], [116.4857629, 39.9730325], [116.4856105, 39.9730311], [116.4856054, 39.9733494]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '687359086', code: 1500, fclass: 'building', name: '兆维工业园C1号楼', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4848202, 39.9732155], [116.4848799, 39.9732161], [116.4848785, 39.9733046], [116.4854789, 39.9733103], [116.4854845, 39.9729663], [116.4848681, 39.9729604], [116.4848668, 39.9730395], [116.484823, 39.9730391], [116.4848202, 39.9732155]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '687359088', code: 1500, fclass: 'building', name: '北京电信酒仙桥数据中心', type: 'industrial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4886217, 39.9728412], [116.490792, 39.9728489], [116.4907941, 39.9725036], [116.4886238, 39.9724959], [116.4886217, 39.9728412]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '687360882', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4660212, 39.9997953], [116.466143, 39.9998451], [116.4662072, 39.9997492], [116.4660854, 39.9996994], [116.4660212, 39.9997953]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '687365081', code: 1500, fclass: 'building', name: '博泰国际大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4663042, 40.0010006], [116.4669757, 40.0010081], [116.4669793, 40.0008184], [116.4663078, 40.0008109], [116.4663042, 40.0010006]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '687365084', code: 1500, fclass: 'building', name: '望京西园一区129号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4685376, 40.0017537], [116.4692248, 40.0017791], [116.4692293, 40.0017078], [116.4693835, 40.0017136], [116.4694052, 40.0013699], [116.4692313, 40.0013635], [116.4692155, 40.0016134], [116.468548, 40.0015887], [116.4685376, 40.0017537]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '687365085', code: 1500, fclass: 'building', name: '凉亭', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4696502, 40.0016254], [116.4696572, 40.0016586], [116.4696688, 40.0016699], [116.4696905, 40.0016912], [116.4697261, 40.0017013], [116.4697726, 40.0017007], [116.4698159, 40.00169], [116.4698484, 40.0016746], [116.469874, 40.0016509], [116.469874, 40.0016248], [116.4698306, 40.0016242], [116.4698213, 40.0016444], [116.4697966, 40.0016586], [116.4697787, 40.0016675], [116.4697455, 40.0016663], [116.4697129, 40.0016562], [116.4696959, 40.0016396], [116.4696913, 40.0016242], [116.4696502, 40.0016254]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '691614969', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4758454, 39.9953176], [116.4758657, 39.9953556], [116.4759379, 39.9953694], [116.4760462, 39.9953815], [116.4762018, 39.9954143], [116.4764093, 39.995435], [116.4765897, 39.995423], [116.4766123, 39.9953919], [116.47661, 39.9953124], [116.4766912, 39.9952139], [116.4768581, 39.9950808], [116.4768897, 39.9950255], [116.4768581, 39.9949962], [116.4767859, 39.9949875], [116.4765784, 39.9950186], [116.4762559, 39.9951085], [116.4759108, 39.995264], [116.4758454, 39.9953176]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '691614971', code: 1500, fclass: 'building', name: '昆泰嘉瑞公寓', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4832264, 40.0004642], [116.4832264, 40.0005114], [116.4832505, 40.0005648], [116.4832559, 40.0006244], [116.4832505, 40.0006758], [116.4832586, 40.000721], [116.4832854, 40.0007456], [116.483335, 40.0007426], [116.48339, 40.000723], [116.4834839, 40.0007189], [116.4835509, 40.0007189], [116.4836341, 40.0007189], [116.4836716, 40.0006922], [116.4836823, 40.0006573], [116.483677, 40.0006121], [116.4836502, 40.0005607], [116.4836555, 40.0005032], [116.4836555, 40.0004395], [116.4836448, 40.0004087], [116.4836099, 40.0003881], [116.4835752, 40.000381], [116.4835429, 40.0003902], [116.4834678, 40.0004066], [116.4834034, 40.0004148], [116.483331, 40.0004087], [116.4832639, 40.0004231], [116.4832264, 40.0004642]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '691614972', code: 1500, fclass: 'building', name: '阿里B', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4835602, 40.0012276], [116.4835634, 40.0012716], [116.483608, 40.0012936], [116.4837069, 40.0012911], [116.4838026, 40.0013009], [116.483876, 40.0013302], [116.4839493, 40.0013449], [116.4840195, 40.00134], [116.484061, 40.001296], [116.4840737, 40.0012423], [116.4840833, 40.0011861], [116.4841152, 40.0011299], [116.4841471, 40.0010785], [116.4841471, 40.0010443], [116.4841184, 40.0010052], [116.4840769, 40.0009857], [116.483994, 40.0009857], [116.4839174, 40.0009808], [116.4838026, 40.0009466], [116.4837484, 40.0009319], [116.483691, 40.0009442], [116.4836654, 40.0009686], [116.4836431, 40.0010101], [116.4836367, 40.0010932], [116.4835921, 40.0011763], [116.4835602, 40.0012276]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '9694981', code: 1500, fclass: 'building', name: 'B36', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4903849, 39.9788133], [116.491942, 39.9788255], [116.4919545, 39.9778866], [116.4903974, 39.9778744], [116.4903849, 39.9788133]], [[116.4905325, 39.9786665], [116.4905346, 39.9784384], [116.4917711, 39.978445], [116.491769, 39.9786731], [116.4905325, 39.9786665]], [[116.4905546, 39.9782801], [116.4905567, 39.978052], [116.4917933, 39.9780586], [116.4917912, 39.9782867], [116.4905546, 39.9782801]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '699924716', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4928505, 39.9715468], [116.4928508, 39.9716034], [116.492936, 39.9716031], [116.4929357, 39.9715466], [116.4928505, 39.9715468]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '699924717', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4920237, 39.9715481], [116.492024, 39.9716047], [116.4921092, 39.9716044], [116.4921089, 39.9715479], [116.4920237, 39.9715481]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '699924718', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4927515, 39.9715494], [116.4930488, 39.9715512], [116.493052, 39.971228], [116.4927548, 39.9712262], [116.4927515, 39.9715494]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '699924719', code: 1500, fclass: 'building', name: '融合通信', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4918923, 39.9712255], [116.4918924, 39.9715539], [116.4921979, 39.9715539], [116.4921978, 39.9712255], [116.4918923, 39.9712255]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '699927273', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4898876, 39.9774349], [116.4902011, 39.9774373], [116.4902129, 39.9769252], [116.4898981, 39.9769212], [116.4898901, 39.9772519], [116.4898876, 39.9774349]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409351', code: 1500, fclass: 'building', name: '望京通信大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4605458, 39.9968821], [116.4612556, 39.9968954], [116.4612637, 39.9966401], [116.460554, 39.9966267], [116.4605458, 39.9968821]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409352', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4592125, 39.9969638], [116.4604077, 39.9969812], [116.4604287, 39.9961347], [116.4598301, 39.996126], [116.4598268, 39.9962613], [116.4602665, 39.9962676], [116.4602634, 39.9963943], [116.4602057, 39.9963934], [116.4601998, 39.9966353], [116.4601536, 39.9966346], [116.4601522, 39.996694], [116.4601942, 39.9966946], [116.4601897, 39.996876], [116.459862, 39.9968712], [116.4598649, 39.9967519], [116.45957, 39.9967477], [116.4595732, 39.9966199], [116.4592211, 39.9966148], [116.4592125, 39.9969638]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409353', code: 1500, fclass: 'building', name: '227号楼', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4588737, 39.9972552], [116.4588738, 39.9973012], [116.4589426, 39.9973012], [116.4589426, 39.9973171], [116.4590216, 39.9973171], [116.4590216, 39.9972657], [116.4590926, 39.9972657], [116.4590927, 39.9973071], [116.4592429, 39.9973071], [116.4592428, 39.9972436], [116.459209, 39.9972436], [116.459209, 39.9972106], [116.4592238, 39.9972106], [116.4592239, 39.9971748], [116.459186, 39.9971748], [116.459186, 39.9971572], [116.4592067, 39.9971573], [116.4592069, 39.9971219], [116.4591751, 39.9971219], [116.4591754, 39.997063], [116.4590799, 39.9970627], [116.4590797, 39.9971016], [116.459031, 39.9971014], [116.4590311, 39.9970634], [116.4589297, 39.9970631], [116.4589294, 39.9971244], [116.4588991, 39.9971243], [116.458899, 39.9971491], [116.458919, 39.9971492], [116.458919, 39.9971832], [116.4588866, 39.9971831], [116.4588866, 39.997205], [116.4589088, 39.997205], [116.4589088, 39.9972552], [116.4588737, 39.9972552]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409354', code: 1500, fclass: 'building', name: '226', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4583017, 39.9972545], [116.4583018, 39.9973005], [116.4583706, 39.9973004], [116.4583706, 39.9973164], [116.4584496, 39.9973163], [116.4584496, 39.997265], [116.4585206, 39.9972649], [116.4585207, 39.9973064], [116.4586709, 39.9973063], [116.4586708, 39.9972429], [116.458637, 39.9972429], [116.458637, 39.9972099], [116.4586518, 39.9972099], [116.4586519, 39.9971741], [116.458614, 39.997174], [116.458614, 39.9971565], [116.4586347, 39.9971565], [116.4586349, 39.9971212], [116.4586031, 39.9971211], [116.4586034, 39.9970622], [116.4585079, 39.997062], [116.4585077, 39.9971008], [116.458459, 39.9971007], [116.4584592, 39.9970626], [116.4583577, 39.9970623], [116.4583574, 39.9971237], [116.4583271, 39.9971236], [116.458327, 39.9971484], [116.4583471, 39.9971484], [116.458347, 39.9971824], [116.4583147, 39.9971824], [116.4583146, 39.9972042], [116.4583368, 39.9972042], [116.4583368, 39.9972545], [116.4583017, 39.9972545]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409355', code: 1500, fclass: 'building', name: '228', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4594628, 39.9972471], [116.4594628, 39.9972931], [116.4595316, 39.9972931], [116.4595316, 39.9973091], [116.4596107, 39.997309], [116.4596106, 39.9972576], [116.4596817, 39.9972576], [116.4596817, 39.9972991], [116.4598319, 39.997299], [116.4598319, 39.9972355], [116.459798, 39.9972355], [116.459798, 39.9972026], [116.4598129, 39.9972026], [116.4598129, 39.9971668], [116.459775, 39.9971667], [116.4597751, 39.9971492], [116.4597958, 39.9971492], [116.4597959, 39.9971139], [116.4597642, 39.9971138], [116.4597644, 39.9970549], [116.459669, 39.9970546], [116.4596688, 39.9970935], [116.45962, 39.9970934], [116.4596202, 39.9970553], [116.4595187, 39.997055], [116.4595184, 39.9971163], [116.4594882, 39.9971163], [116.4594881, 39.9971411], [116.4595081, 39.9971411], [116.459508, 39.9971751], [116.4594757, 39.997175], [116.4594757, 39.9971969], [116.4594979, 39.9971969], [116.4594979, 39.9972471], [116.4594628, 39.9972471]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409357', code: 1500, fclass: 'building', name: '216', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4592668, 39.9984767], [116.4592687, 39.9986027], [116.4593525, 39.998602], [116.4593528, 39.9986212], [116.4598928, 39.9986163], [116.4598914, 39.998531], [116.4599058, 39.9985261], [116.4599051, 39.9984665], [116.459781, 39.9984674], [116.4597816, 39.9985166], [116.4597569, 39.9985168], [116.4597563, 39.9984732], [116.4596623, 39.9984739], [116.4596629, 39.9985184], [116.4596335, 39.9985186], [116.4596328, 39.9984717], [116.4595577, 39.9984723], [116.4595584, 39.9985241], [116.4595028, 39.9985245], [116.4595022, 39.9984762], [116.4594396, 39.9984767], [116.4594403, 39.9985297], [116.4593795, 39.9985302], [116.4593787, 39.9984757], [116.4592668, 39.9984767]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409358', code: 1500, fclass: 'building', name: '215', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4592386, 39.9988206], [116.4592405, 39.9989466], [116.4593243, 39.9989459], [116.4593246, 39.9989652], [116.4598646, 39.9989603], [116.4598633, 39.9988749], [116.4598776, 39.9988701], [116.4598769, 39.9988104], [116.4597528, 39.9988113], [116.4597534, 39.9988605], [116.4597287, 39.9988607], [116.4597282, 39.9988172], [116.4596341, 39.9988179], [116.4596347, 39.9988623], [116.4596053, 39.9988625], [116.4596047, 39.9988156], [116.4595295, 39.9988162], [116.4595302, 39.998868], [116.4594747, 39.9988685], [116.459474, 39.9988201], [116.4594114, 39.9988206], [116.4594122, 39.9988737], [116.4593513, 39.9988742], [116.4593505, 39.9988197], [116.4592386, 39.9988206]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409359', code: 1500, fclass: 'building', name: '209', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4580377, 39.9983038], [116.4580396, 39.9984298], [116.4581234, 39.9984291], [116.4581237, 39.9984484], [116.4586637, 39.9984435], [116.4586623, 39.9983581], [116.4586767, 39.9983533], [116.458676, 39.9982936], [116.4585519, 39.9982945], [116.4585525, 39.9983437], [116.4585278, 39.9983439], [116.4585272, 39.9983004], [116.4584332, 39.9983011], [116.4584338, 39.9983455], [116.4584044, 39.9983457], [116.4584037, 39.9982988], [116.4583286, 39.9982994], [116.4583293, 39.9983512], [116.4582737, 39.9983517], [116.4582731, 39.9983033], [116.4582105, 39.9983038], [116.4582112, 39.9983569], [116.4581504, 39.9983574], [116.4581496, 39.9983029], [116.4580377, 39.9983038]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409360', code: 1500, fclass: 'building', name: '205', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4580457, 39.9986777], [116.4580475, 39.9988037], [116.4581314, 39.998803], [116.4581316, 39.9988222], [116.4586716, 39.9988173], [116.4586703, 39.998732], [116.4586847, 39.9987271], [116.4586839, 39.9986675], [116.4585598, 39.9986683], [116.4585604, 39.9987176], [116.4585358, 39.9987177], [116.4585352, 39.9986742], [116.4584412, 39.9986749], [116.4584417, 39.9987194], [116.4584123, 39.9987196], [116.4584117, 39.9986727], [116.4583366, 39.9986733], [116.4583373, 39.9987251], [116.4582817, 39.9987255], [116.458281, 39.9986772], [116.4582184, 39.9986777], [116.4582192, 39.9987307], [116.4581583, 39.9987312], [116.4581575, 39.9986767], [116.4580457, 39.9986777]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409361', code: 1500, fclass: 'building', name: '203', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4580983, 39.9991948], [116.4581807, 39.9991952], [116.4581806, 39.9992147], [116.4586033, 39.9992167], [116.4586028, 39.9992842], [116.4589267, 39.9992857], [116.4589271, 39.999242], [116.4590125, 39.9992424], [116.4590133, 39.9991476], [116.458578, 39.9991456], [116.4585786, 39.9990708], [116.4585075, 39.9990705], [116.4585072, 39.9991141], [116.4584589, 39.9991139], [116.4584593, 39.9990663], [116.4583995, 39.999066], [116.4583991, 39.999117], [116.4583325, 39.9991167], [116.4583328, 39.9990676], [116.4582773, 39.9990673], [116.458277, 39.9991195], [116.4582133, 39.9991192], [116.4582137, 39.9990638], [116.4580993, 39.9990633], [116.4580983, 39.9991948]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409362', code: 1500, fclass: 'building', name: '214', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4592679, 39.9991812], [116.4592698, 39.9993073], [116.4593536, 39.9993065], [116.4593539, 39.9993258], [116.4598939, 39.9993209], [116.4598926, 39.9992356], [116.4599069, 39.9992307], [116.4599062, 39.999171], [116.4597821, 39.9991719], [116.4597827, 39.9992211], [116.459758, 39.9992213], [116.4597575, 39.9991778], [116.4596634, 39.9991785], [116.459664, 39.9992229], [116.4596346, 39.9992231], [116.459634, 39.9991762], [116.4595588, 39.9991768], [116.4595595, 39.9992286], [116.459504, 39.9992291], [116.4595033, 39.9991807], [116.4594407, 39.9991813], [116.4594415, 39.9992343], [116.4593806, 39.9992348], [116.4593798, 39.9991803], [116.4592679, 39.9991812]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409363', code: 1500, fclass: 'building', name: '208', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.45726, 39.9982971], [116.4572618, 39.9984231], [116.4573457, 39.9984224], [116.457346, 39.9984416], [116.457886, 39.9984367], [116.4578846, 39.9983514], [116.457899, 39.9983465], [116.4578983, 39.9982869], [116.4577742, 39.9982877], [116.4577748, 39.9983369], [116.4577501, 39.9983371], [116.4577495, 39.9982936], [116.4576555, 39.9982943], [116.4576561, 39.9983387], [116.4576267, 39.998339], [116.457626, 39.9982921], [116.4575509, 39.9982926], [116.4575516, 39.9983445], [116.457496, 39.9983449], [116.4574954, 39.9982966], [116.4574328, 39.9982971], [116.4574335, 39.9983501], [116.4573727, 39.9983506], [116.4573719, 39.9982961], [116.45726, 39.9982971]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409364', code: 1500, fclass: 'building', name: '204', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4571615, 39.9986531], [116.4571621, 39.9987831], [116.4572453, 39.9987828], [116.4572454, 39.9988022], [116.4579047, 39.9988003], [116.4579039, 39.998653], [116.4577939, 39.9986533], [116.4577941, 39.9987019], [116.4577645, 39.998702], [116.4577642, 39.9986487], [116.4576821, 39.9986489], [116.4576823, 39.9986977], [116.4576494, 39.9986978], [116.4576492, 39.998654], [116.4575622, 39.9986543], [116.4575624, 39.9986985], [116.4575274, 39.9986986], [116.4575271, 39.9986514], [116.4574562, 39.9986516], [116.4574565, 39.9987031], [116.4573984, 39.9987033], [116.4573981, 39.9986546], [116.4573365, 39.9986548], [116.4573367, 39.9987075], [116.4572767, 39.9987076], [116.4572764, 39.9986527], [116.4571615, 39.9986531]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409365', code: 1500, fclass: 'building', name: '219', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4610112, 39.9991565], [116.4611211, 39.9991582], [116.4611222, 39.999116], [116.4611441, 39.9991163], [116.4611459, 39.9990479], [116.4612291, 39.9990492], [116.4612273, 39.9991182], [116.4612511, 39.9991186], [116.4612501, 39.9991569], [116.4613633, 39.9991586], [116.4613895, 39.9981638], [116.46128, 39.9981621], [116.4612792, 39.9981954], [116.4612607, 39.9981951], [116.461259, 39.998259], [116.4611615, 39.9982575], [116.4611642, 39.9981553], [116.4610381, 39.9981533], [116.4610112, 39.9991565]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409366', code: 1500, fclass: 'building', name: '202', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4581217, 39.9995673], [116.4588882, 39.9996863], [116.4589225, 39.9995566], [116.4581561, 39.9994375], [116.4581217, 39.9995673]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409367', code: 1500, fclass: 'building', name: '212', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4592787, 39.999686], [116.459451, 39.9997129], [116.4594414, 39.9997496], [116.4595179, 39.9997618], [116.4595307, 39.99973], [116.4598433, 39.999774], [116.4598561, 39.999818], [116.4600219, 39.999862], [116.4600761, 39.9997911], [116.4601144, 39.9997813], [116.4601336, 39.9996738], [116.4593042, 39.9995565], [116.4592787, 39.999686]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409368', code: 1500, fclass: 'building', name: '213', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4602197, 39.9998009], [116.4603728, 39.9998375], [116.4603792, 39.9998742], [116.4604525, 39.9998888], [116.4604653, 39.9998546], [116.4605929, 39.9998595], [116.4611319, 39.9995712], [116.4612181, 39.9994637], [116.4612276, 39.9992609], [116.4610554, 39.9992389], [116.4610107, 39.9994588], [116.4605227, 39.9997227], [116.4602548, 39.999686], [116.4602197, 39.9998009]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409369', code: 1500, fclass: 'building', name: '220', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4601096, 39.9979254], [116.4607017, 39.9979362], [116.4607048, 39.9978344], [116.4606461, 39.9978333], [116.4606487, 39.9977495], [116.4605183, 39.9977471], [116.460517, 39.9977881], [116.4604348, 39.9977866], [116.4604359, 39.9977524], [116.4603666, 39.9977512], [116.4603656, 39.9977866], [116.4602814, 39.9977851], [116.4602826, 39.9977465], [116.4601509, 39.9977441], [116.4601485, 39.99782], [116.4601967, 39.9978209], [116.4601964, 39.9978326], [116.4601125, 39.9978311], [116.4601096, 39.9979254]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409370', code: 1500, fclass: 'building', name: '218', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4600382, 39.998541], [116.4606303, 39.9985519], [116.4606335, 39.9984501], [116.4605747, 39.998449], [116.4605773, 39.9983652], [116.4604469, 39.9983628], [116.4604457, 39.9984038], [116.4603634, 39.9984023], [116.4603645, 39.9983681], [116.4602953, 39.9983669], [116.4602942, 39.9984023], [116.46021, 39.9984008], [116.4602112, 39.9983622], [116.4600795, 39.9983598], [116.4600772, 39.9984357], [116.4601253, 39.9984366], [116.460125, 39.9984483], [116.4600412, 39.9984467], [116.4600382, 39.998541]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409371', code: 1500, fclass: 'building', name: '217', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.460054, 39.9992132], [116.4606461, 39.999224], [116.4606493, 39.9991222], [116.4605905, 39.9991211], [116.4605931, 39.9990373], [116.4604627, 39.9990349], [116.4604615, 39.9990759], [116.4603792, 39.9990744], [116.4603803, 39.9990402], [116.4603111, 39.999039], [116.46031, 39.9990744], [116.4602258, 39.9990729], [116.460227, 39.9990343], [116.4600953, 39.9990319], [116.460093, 39.9991078], [116.4601411, 39.9991087], [116.4601408, 39.9991204], [116.460057, 39.9991189], [116.460054, 39.9992132]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409372', code: 1500, fclass: 'building', name: '怡家酒店', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4608541, 39.9977028], [116.4608591, 39.9979414], [116.4613467, 39.9979354], [116.4613414, 39.9976882], [116.4610967, 39.9976913], [116.4610993, 39.9978137], [116.460996, 39.997815], [116.4609936, 39.9977011], [116.4608541, 39.9977028]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409373', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4580233, 39.9977812], [116.4586038, 39.9977911], [116.4586494, 39.9975881], [116.45849, 39.9975797], [116.4584824, 39.9976826], [116.4581854, 39.9976788], [116.4581768, 39.9975594], [116.4584228, 39.9975515], [116.458424, 39.9974474], [116.4580328, 39.9974538], [116.4580233, 39.9977812]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409374', code: 1500, fclass: 'building', name: '211', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.458012, 39.9979854], [116.4580312, 39.9981149], [116.4587616, 39.9981002], [116.4588413, 39.9981516], [116.4589083, 39.9981516], [116.4589338, 39.9980612], [116.4588669, 39.9980098], [116.4588286, 39.9980465], [116.4587361, 39.9980392], [116.4587297, 39.9979805], [116.458012, 39.9979854]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409375', code: 1500, fclass: 'building', name: '206', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4587696, 39.9984436], [116.4587807, 39.9989849], [116.4589912, 39.9989824], [116.4589802, 39.9984411], [116.4587696, 39.9984436]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '712409376', code: 1500, fclass: 'building', name: '卧龙幼儿园', type: 'kindergarten'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4591807, 39.9982144], [116.459244, 39.9982147], [116.4592436, 39.9982556], [116.4593615, 39.9982562], [116.4593608, 39.9983358], [116.4596842, 39.9983375], [116.459685, 39.9982472], [116.4597709, 39.9982308], [116.4597711, 39.9981982], [116.4598336, 39.9981985], [116.4598349, 39.9980442], [116.4591821, 39.9980409], [116.4591807, 39.9982144]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '718381702', code: 1500, fclass: 'building', name: '花家地小区1号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4621053, 39.9839279], [116.4624411, 39.9842289], [116.4630851, 39.9842714], [116.4636042, 39.983922], [116.4634856, 39.9838051], [116.4631143, 39.98404], [116.4630851, 39.9840164], [116.4629865, 39.984086], [116.4625597, 39.9840565], [116.4622763, 39.9838157], [116.4621053, 39.9839279]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '719353918', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4854605, 39.9863435], [116.4872641, 39.9863544], [116.4872655, 39.9862103], [116.485462, 39.9861994], [116.4854605, 39.9863435]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '720128197', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4564308, 39.9832737], [116.45675, 39.9834327], [116.4567821, 39.9833942], [116.4571256, 39.9835647], [116.4570744, 39.983627], [116.45644, 39.9836445], [116.456471, 39.9842696], [116.4570348, 39.9842483], [116.4570225, 39.983808], [116.457056, 39.9838111], [116.4573218, 39.9834789], [116.4565684, 39.9831095], [116.4564308, 39.9832737]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282341', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.48716, 39.9803983], [116.4872878, 39.9803993], [116.4872935, 39.9799725], [116.4871657, 39.9799715], [116.48716, 39.9803983]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282343', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4873461, 39.9794013], [116.4880062, 39.9794049], [116.4880071, 39.9793063], [116.487347, 39.9793027], [116.4873461, 39.9794013]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282344', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4873082, 39.9804317], [116.4879452, 39.9804372], [116.4879487, 39.9802039], [116.4873116, 39.9801984], [116.4873082, 39.9804317]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282345', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4888517, 39.9797092], [116.4889524, 39.9797108], [116.4889583, 39.9794921], [116.4888577, 39.9794905], [116.4888517, 39.9797092]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282346', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4881586, 39.9793549], [116.4889885, 39.9793602], [116.4889893, 39.9792811], [116.4881594, 39.9792757], [116.4881586, 39.9793549]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282348', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4873508, 39.9806497], [116.4889628, 39.9806682], [116.4889654, 39.9805325], [116.4873535, 39.980514], [116.4873508, 39.9806497]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282349', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4881274, 39.9794566], [116.4881274, 39.9796951], [116.4888086, 39.9796951], [116.4888086, 39.9794566], [116.4881274, 39.9794566]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282350', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4881633, 39.9804156], [116.4888137, 39.9804162], [116.4888141, 39.9801615], [116.4881637, 39.9801608], [116.4881633, 39.9804156]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282351', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4881615, 39.980047], [116.4889432, 39.9800551], [116.4889473, 39.9798229], [116.4881656, 39.9798147], [116.4881615, 39.980047]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '723282352', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4873401, 39.9798461], [116.4873429, 39.9800829], [116.4879757, 39.9800784], [116.4879999, 39.9795111], [116.4873591, 39.9795032], [116.4873508, 39.9797249], [116.4876961, 39.9797292], [116.4876512, 39.9797783], [116.4876803, 39.9798525], [116.4873401, 39.9798461]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '727018818', code: 1500, fclass: 'building', name: 'Red Star Gallery', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4879953, 39.9845329], [116.4881324, 39.9845341], [116.4882521, 39.9845351], [116.4882607, 39.9839329], [116.4880039, 39.9839307], [116.4879953, 39.9845329]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '727018841', code: 1500, fclass: 'building', name: 'Zero Art Center', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4880114, 39.9838924], [116.4882635, 39.9838982], [116.4882801, 39.9834769], [116.488028, 39.9834711], [116.4880114, 39.9838924]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '727418648', code: 1500, fclass: 'building', name: '同程艺龙大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5014488, 39.9856434], [116.5014488, 39.9859475], [116.501996, 39.9859475], [116.5019852, 39.9853968], [116.5016741, 39.9853968], [116.5016741, 39.9856434], [116.5014488, 39.9856434]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '727682283', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4628514, 40.0104867], [116.4628631, 40.0106252], [116.4636414, 40.0105938], [116.4636292, 40.0104483], [116.4628514, 40.0104867]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '727682541', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4634931, 40.0116862], [116.4635114, 40.0122306], [116.4637406, 40.0122271], [116.4637261, 40.0116826], [116.4634931, 40.0116862]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '727682542', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4626135, 40.012098], [116.4626416, 40.0124634], [116.4633431, 40.012442], [116.4633351, 40.0122882], [116.4628622, 40.0123027], [116.4628511, 40.0120907], [116.4626135, 40.012098]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '727682543', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4616151, 40.0123413], [116.4616298, 40.0127228], [116.462333, 40.012707], [116.4623189, 40.0123417], [116.4616151, 40.0123413]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '741831229', code: 1500, fclass: 'building', name: 'C7', type: 'industrial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.486619, 39.9878243], [116.4898212, 39.9878377], [116.4898259, 39.9871661], [116.4866237, 39.9871532], [116.486619, 39.9878243]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '752332233', code: 1500, fclass: 'building', name: '#113', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4628509, 40.0056569], [116.4631832, 40.0056578], [116.4631843, 40.0054051], [116.4628521, 40.0054042], [116.4628509, 40.0056569]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '752332236', code: 1500, fclass: 'building', name: '#112', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4622173, 40.0057704], [116.4622173, 40.0060176], [116.4625484, 40.0060176], [116.4625484, 40.0057704], [116.4622173, 40.0057704]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '752332237', code: 1500, fclass: 'building', name: '#112', type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.46192, 40.0060777], [116.4621258, 40.0060786], [116.4621299, 40.0055537], [116.4619204, 40.0055523], [116.46192, 40.0060777]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '752332238', code: 1500, fclass: 'building', name: '体育场 & 食堂', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4653851, 40.006084], [116.4656029, 40.0060887], [116.4657376, 40.0060887], [116.4657467, 40.0056842], [116.4653884, 40.0056813], [116.4653851, 40.006084]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '752332239', code: 1500, fclass: 'building', name: '行政楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4656029, 40.0060887], [116.4656029, 40.0062343], [116.4658094, 40.0062343], [116.4661208, 40.0062343], [116.4661209, 40.0062057], [116.466252, 40.0062057], [116.466252, 40.0061122], [116.4661977, 40.0061122], [116.4659052, 40.0061121], [116.4659051, 40.0061575], [116.4657375, 40.0061575], [116.4657376, 40.0060887], [116.4656029, 40.0060887]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '752332240', code: 1500, fclass: 'building', name: '北楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4653426, 40.0055971], [116.4654899, 40.0055964], [116.4654899, 40.0056614], [116.4657881, 40.0056642], [116.4657903, 40.0056835], [116.4658095, 40.0057089], [116.4658449, 40.005714], [116.4658715, 40.0057027], [116.4658833, 40.0056846], [116.465884, 40.0056592], [116.4660117, 40.0056586], [116.4660171, 40.0056832], [116.4660412, 40.0056852], [116.4660786, 40.0056832], [116.4660947, 40.0056647], [116.4662339, 40.0056647], [116.4662339, 40.0055684], [116.466258, 40.0055684], [116.4662553, 40.0055069], [116.4662205, 40.0055048], [116.4662205, 40.0054269], [116.4658244, 40.005431], [116.4658227, 40.0053709], [116.4658217, 40.0053367], [116.4657655, 40.0053367], [116.4657667, 40.0052686], [116.465728, 40.0052687], [116.4657298, 40.0053709], [116.4657307, 40.0054228], [116.4657869, 40.0054228], [116.4657869, 40.0054864], [116.4655086, 40.0054884], [116.4653453, 40.0054884], [116.4653426, 40.0055971]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '752332241', code: 1500, fclass: 'building', name: '南楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4653849, 40.0051219], [116.4653857, 40.0052699], [116.465728, 40.0052687], [116.4657667, 40.0052686], [116.4657681, 40.0052202], [116.4661025, 40.005219], [116.4661027, 40.0052444], [116.4662231, 40.005244], [116.4662225, 40.0051192], [116.4657407, 40.0051207], [116.4653849, 40.0051219]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '752332262', code: 1500, fclass: 'building', name: '天然气站', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4652445, 40.0062084], [116.4652475, 40.006318], [116.4654559, 40.0063146], [116.4654529, 40.006205], [116.4652445, 40.0062084]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '753701046', code: 1500, fclass: 'building', name: '鸿运大厦', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4858891, 39.9749151], [116.4858895, 39.975096], [116.4865828, 39.9750952], [116.4865825, 39.9749143], [116.4858891, 39.9749151]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088595', code: 1500, fclass: 'building', name: '花家地北里15号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4591303, 39.9836808], [116.4592479, 39.9837869], [116.4599751, 39.9833076], [116.4600521, 39.9832935], [116.4606688, 39.9835226], [116.4608372, 39.9834234], [116.4602221, 39.9831958], [116.4601353, 39.9831636], [116.4600432, 39.9831526], [116.4599408, 39.983171], [116.4591303, 39.9836808]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088596', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4596251, 39.9838028], [116.4597606, 39.9839209], [116.4602478, 39.9836219], [116.4601088, 39.9835148], [116.4596251, 39.9838028]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088597', code: 1500, fclass: 'building', name: '花家地北里16号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4593364, 39.9838824], [116.4597246, 39.9842251], [116.4599604, 39.9842422], [116.4609036, 39.9836846], [116.4607511, 39.9835647], [116.4598506, 39.9841207], [116.4594685, 39.9838014], [116.4593364, 39.9838824]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088598', code: 1500, fclass: 'building', name: '望京大厦C座', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4609373, 39.9837436], [116.4612769, 39.9840454], [116.4614714, 39.9839304], [116.4611294, 39.9836329], [116.4609373, 39.9837436]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088599', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4610154, 39.9843341], [116.4613574, 39.9846174], [116.4617056, 39.9844002], [116.4617117, 39.9842373], [116.4610894, 39.9842019], [116.4610678, 39.9843034], [116.4610154, 39.9843341]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088600', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4602261, 39.9848259], [116.4603789, 39.9848382], [116.4603897, 39.9846718], [116.4606874, 39.9846861], [116.4606847, 39.9847313], [116.4607095, 39.9847328], [116.4609214, 39.9847456], [116.4610602, 39.984754], [116.461079, 39.9846142], [116.4602421, 39.984571], [116.4602261, 39.9848259]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088601', code: 1500, fclass: 'building', name: '教学楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.460218, 39.9852123], [116.4603307, 39.9853027], [116.4605828, 39.9851527], [116.4605426, 39.9851033], [116.4609127, 39.984869], [116.4610146, 39.9848444], [116.4610173, 39.984793], [116.4609214, 39.9847456], [116.4608081, 39.9848053], [116.4607095, 39.9847328], [116.4603999, 39.9849239], [116.4602395, 39.9849636], [116.4602287, 39.9850704], [116.460218, 39.9852123]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088602', code: 1500, fclass: 'building', name: '花家地北里4号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4602777, 39.9855469], [116.4602787, 39.9855905], [116.4603478, 39.9855921], [116.4603478, 39.9855407], [116.4604068, 39.9855438], [116.4604047, 39.9855936], [116.4604881, 39.9855967], [116.4605755, 39.9856022], [116.4605816, 39.9855523], [116.460547, 39.9855531], [116.460547, 39.985522], [116.4605816, 39.9855204], [116.4605836, 39.9854207], [116.4604799, 39.985413], [116.4604749, 39.9854698], [116.4603864, 39.985469], [116.4603864, 39.9854161], [116.460295, 39.9854114], [116.4602868, 39.98549], [116.4603265, 39.98549], [116.4603244, 39.9855485], [116.4602777, 39.9855469]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088603', code: 1500, fclass: 'building', name: '花家地北里3号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4607111, 39.9852693], [116.4607121, 39.9853129], [116.4607812, 39.9853145], [116.4607812, 39.9852631], [116.4608402, 39.9852662], [116.4608382, 39.985316], [116.4609215, 39.9853192], [116.4610089, 39.9853246], [116.461015, 39.9852748], [116.4609805, 39.9852755], [116.4609805, 39.9852444], [116.461015, 39.9852428], [116.461017, 39.9851432], [116.4609134, 39.9851354], [116.4609083, 39.9851922], [116.4608199, 39.9851914], [116.4608199, 39.9851385], [116.4607284, 39.9851338], [116.4607203, 39.9852125], [116.4607599, 39.9852125], [116.4607579, 39.9852709], [116.4607111, 39.9852693]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088604', code: 1500, fclass: 'building', name: '花家地北里2号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4611069, 39.9849952], [116.461108, 39.9850388], [116.4611771, 39.9850403], [116.4611771, 39.9849889], [116.461236, 39.984992], [116.461234, 39.9850419], [116.4613173, 39.985045], [116.4614047, 39.9850505], [116.4614108, 39.9850006], [116.4613763, 39.9850014], [116.4613763, 39.9849702], [116.4614108, 39.9849687], [116.4614129, 39.984869], [116.4613092, 39.9848612], [116.4613041, 39.9849181], [116.4612157, 39.9849173], [116.4612157, 39.9848643], [116.4611242, 39.9848597], [116.4611161, 39.9849383], [116.4611557, 39.9849383], [116.4611537, 39.9849967], [116.4611069, 39.9849952]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088605', code: 1500, fclass: 'building', name: '花家地北里1号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4605982, 39.9856861], [116.4606819, 39.9857552], [116.4618633, 39.9850028], [116.4619178, 39.9850037], [116.4619665, 39.9845976], [116.4618467, 39.9845914], [116.4618162, 39.9849368], [116.4605982, 39.9856861]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088606', code: 1500, fclass: 'building', name: '童欣幼儿园', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4590054, 39.9849859], [116.459227, 39.9850036], [116.4592255, 39.9850485], [116.4594471, 39.9850591], [116.4594517, 39.9849706], [116.4592639, 39.9849565], [116.4592701, 39.9848374], [116.4594271, 39.9848503], [116.4594456, 39.9847524], [116.4592024, 39.9847324], [116.4592024, 39.9847124], [116.4590592, 39.9847029], [116.4590423, 39.9848657], [116.4590192, 39.9848881], [116.4590054, 39.9849859]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088607', code: 1500, fclass: 'building', name: '花家地北里8号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.458416, 39.9856104], [116.4586492, 39.9857993], [116.4589949, 39.9855693], [116.4587537, 39.9853681], [116.458416, 39.9856104]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088608', code: 1500, fclass: 'building', name: '花家地西里207号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4567609, 39.985332], [116.4568493, 39.9853997], [116.4568069, 39.9854783], [116.4568069, 39.9855325], [116.4568988, 39.9855514], [116.4569978, 39.985481], [116.4570721, 39.9855379], [116.4571428, 39.9854837], [116.4570827, 39.9854078], [116.4571463, 39.9853211], [116.4570402, 39.9852669], [116.4569377, 39.9852995], [116.456867, 39.9852615], [116.4567609, 39.985332]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088609', code: 1500, fclass: 'building', name: '花家地西里206号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4574279, 39.9856739], [116.4575163, 39.9857416], [116.4574738, 39.9858202], [116.4574738, 39.9858744], [116.4575658, 39.9858933], [116.4576648, 39.9858229], [116.457739, 39.9858798], [116.4578097, 39.9858256], [116.4577496, 39.9857497], [116.4578133, 39.985663], [116.4577072, 39.9856088], [116.4576047, 39.9856414], [116.4575339, 39.9856034], [116.4574279, 39.9856739]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088610', code: 1500, fclass: 'building', name: '花家地西里205号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.457884, 39.9861236], [116.4579724, 39.9861913], [116.45793, 39.9862699], [116.45793, 39.9863241], [116.4580219, 39.9863431], [116.4581209, 39.9862726], [116.458174, 39.9863133], [116.4581952, 39.9863295], [116.4582659, 39.9862753], [116.4582058, 39.9861995], [116.4582694, 39.9861128], [116.4581633, 39.9860586], [116.4580608, 39.9860911], [116.4579901, 39.9860532], [116.457884, 39.9861236]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088611', code: 1500, fclass: 'building', name: '花家地西里209号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4561247, 39.9862337], [116.4568759, 39.9865233], [116.4569412, 39.9863874], [116.45633, 39.9861908], [116.4564373, 39.9859727], [116.4571559, 39.986223], [116.4572399, 39.9861014], [116.4563814, 39.9858119], [116.4561247, 39.9862337]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088612', code: 1500, fclass: 'building', name: '花家地西里203号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4571045, 39.9866198], [116.4576364, 39.9870381], [116.458117, 39.9867092], [116.4579257, 39.9865626], [116.4578044, 39.9866448], [116.4578604, 39.9866984], [116.4576318, 39.9868593], [116.4572445, 39.9865197], [116.4571045, 39.9866198]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088613', code: 1500, fclass: 'building', name: '中央美术学院', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4570876, 39.9824383], [116.457617, 39.9827095], [116.4579156, 39.9824099], [116.4573246, 39.9821434], [116.4570876, 39.9824383]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088614', code: 1500, fclass: 'building', name: '中央美术学院', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.457931, 39.9825774], [116.4584451, 39.9828345], [116.458546, 39.9827151], [116.4580234, 39.9824595], [116.457931, 39.9825774]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '764088615', code: 1500, fclass: 'building', name: '中央美术学院', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4582941, 39.9822995], [116.4596913, 39.9822559], [116.4596913, 39.9821955], [116.4597969, 39.9821856], [116.459736, 39.9811635], [116.4590333, 39.9812009], [116.4590414, 39.9815332], [116.4587355, 39.9815449], [116.4587259, 39.9813311], [116.4584931, 39.9813347], [116.4585134, 39.9814623], [116.4586353, 39.9814592], [116.4586474, 39.9815992], [116.4586497, 39.9816661], [116.458684, 39.9821334], [116.4582948, 39.9821448], [116.4582968, 39.9822242], [116.4582941, 39.9822995]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '786589522', code: 1500, fclass: 'building', name: '实验楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4700145, 40.009628], [116.470211, 40.0097034], [116.470229, 40.0096759], [116.4706694, 40.0098451], [116.4706528, 40.0098705], [116.4707166, 40.009895], [116.4708162, 40.0098638], [116.4708987, 40.0097101], [116.4708709, 40.0096688], [116.470929, 40.0095699], [116.4712351, 40.0096752], [116.4712673, 40.0097163], [116.471337, 40.0096999], [116.4713913, 40.0096153], [116.4709855, 40.0094566], [116.4710919, 40.0092909], [116.4716321, 40.0094944], [116.4716911, 40.0094698], [116.4717608, 40.0094122], [116.4717554, 40.009367], [116.4709508, 40.0090959], [116.4708834, 40.0092217], [116.4710405, 40.0092732], [116.4710137, 40.0093236], [116.4709582, 40.0093044], [116.4708956, 40.0093939], [116.4706581, 40.0093029], [116.470547, 40.009473], [116.4707957, 40.0095683], [116.470745, 40.0096458], [116.4706165, 40.0095965], [116.4705793, 40.0096534], [116.4701145, 40.0094751], [116.4700145, 40.009628]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '786589523', code: 1500, fclass: 'building', name: '教学楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4702189, 40.0085991], [116.4703446, 40.0085975], [116.4703469, 40.0087017], [116.4709636, 40.0086939], [116.4709672, 40.0088584], [116.4711057, 40.0088567], [116.4714906, 40.0088518], [116.4718514, 40.0088472], [116.4718411, 40.0083718], [116.4717889, 40.0083725], [116.4717886, 40.0083606], [116.4717868, 40.0082797], [116.471159, 40.0082882], [116.4711633, 40.0084738], [116.4711641, 40.0085065], [116.4706253, 40.0085138], [116.4706234, 40.0084333], [116.4706216, 40.0083533], [116.4704879, 40.0083551], [116.4704208, 40.0083072], [116.4702402, 40.0084554], [116.470272, 40.0085069], [116.4702189, 40.0085991]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '786589524', code: 1500, fclass: 'building', name: '食堂', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4711285, 40.0077967], [116.4712145, 40.0077898], [116.4712202, 40.0078315], [116.4713161, 40.0078238], [116.4712984, 40.0078916], [116.4713274, 40.0078899], [116.4713456, 40.0080821], [116.4714398, 40.0080769], [116.4715314, 40.0080718], [116.4715279, 40.0080341], [116.4718126, 40.0080184], [116.4717832, 40.0077072], [116.4716912, 40.0076503], [116.4716213, 40.0076296], [116.4715355, 40.0076042], [116.4712878, 40.0076123], [116.4712701, 40.0075771], [116.4712064, 40.0075798], [116.4711285, 40.0077967]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '786589525', code: 1500, fclass: 'building', name: '艺术楼', type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4690842, 40.0086737], [116.4690925, 40.0092814], [116.4693389, 40.0092794], [116.4693307, 40.0086718], [116.4690842, 40.0086737]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '786589528', code: 1500, fclass: 'building', name: '天象厅', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4708655, 40.009809], [116.4708713, 40.0098482], [116.4708891, 40.0098877], [116.4709317, 40.0099191], [116.4709846, 40.0099413], [116.4710442, 40.0099486], [116.4711035, 40.00994], [116.4711556, 40.0099166], [116.4711943, 40.0098812], [116.4712152, 40.0098378], [116.4712157, 40.0097916], [116.4711958, 40.0097479], [116.4711579, 40.0097119], [116.4711064, 40.0096879], [116.4710473, 40.0096785], [116.4709875, 40.009685], [116.4709341, 40.0097065], [116.4708983, 40.0097349], [116.4708747, 40.0097701], [116.4708655, 40.009809]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '10947904', code: 1500, fclass: 'building', name: '六佰本商业街', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4601731, 40.0100155], [116.4609129, 40.0100143], [116.461019, 40.007439], [116.4606543, 40.0074126], [116.460287, 40.0073935], [116.4601731, 40.0100155]]], [[[116.4603113, 40.0068828], [116.4604658, 40.0069015], [116.4607112, 40.0071168], [116.4610969, 40.006951], [116.4611535, 40.004972], [116.4608775, 40.0047822], [116.460382, 40.0047877], [116.4603113, 40.0068828]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '786589554', code: 1500, fclass: 'building', name: '六佰本商业街', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4603113, 40.0068828], [116.4604658, 40.0069015], [116.4607112, 40.0071168], [116.4610969, 40.006951], [116.4611535, 40.004972], [116.4608775, 40.0047822], [116.460382, 40.0047877], [116.4603113, 40.0068828]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791823408', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4871531, 39.9746314], [116.4871542, 39.9749995], [116.4878655, 39.9749983], [116.4878644, 39.9746301], [116.4871531, 39.9746314]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791823743', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4886478, 39.9750767], [116.4892531, 39.9750804], [116.489254, 39.9749896], [116.4886487, 39.9749859], [116.4886478, 39.9750767]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791823744', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.488081, 39.9750834], [116.4882883, 39.9750835], [116.4883841, 39.9750137], [116.4883842, 39.974622], [116.4880811, 39.974622], [116.488081, 39.9750834]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791823745', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4886308, 39.9747721], [116.4894425, 39.9747772], [116.4894436, 39.9746734], [116.4886319, 39.9746682], [116.4886308, 39.9747721]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791824761', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4827397, 39.9783713], [116.4832461, 39.9783784], [116.4832574, 39.9779065], [116.482751, 39.9778994], [116.4827397, 39.9783713]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791827299', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4827082, 39.9774368], [116.4830225, 39.9774424], [116.4830425, 39.9767718], [116.4827283, 39.9767663], [116.4827082, 39.9774368]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791828732', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.483948, 39.9768169], [116.4841511, 39.9768175], [116.4841529, 39.9764454], [116.4839498, 39.9764448], [116.483948, 39.9768169]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791830069', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4843027, 39.9753784], [116.4843029, 39.9756216], [116.4852593, 39.9756211], [116.4852591, 39.975378], [116.4843027, 39.9753784]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791830070', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4847325, 39.9769449], [116.4853741, 39.9769538], [116.485378, 39.9767898], [116.4847364, 39.9767809], [116.4847325, 39.9769449]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791830071', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4847539, 39.9764959], [116.4853955, 39.9765049], [116.4853994, 39.9763409], [116.4847579, 39.9763319], [116.4847539, 39.9764959]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791830072', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.484717, 39.9773878], [116.4857199, 39.9773997], [116.4857493, 39.975947], [116.4847685, 39.9759354], [116.4847653, 39.9760916], [116.4855252, 39.9761006], [116.4855021, 39.9772424], [116.4847201, 39.9772332], [116.484717, 39.9773878]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791830073', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4842965, 39.977312], [116.48451, 39.9773127], [116.4845126, 39.9768237], [116.4842991, 39.976823], [116.4842965, 39.977312]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '791830074', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4842919, 39.9764461], [116.4845054, 39.9764468], [116.484508, 39.9759578], [116.4842945, 39.9759571], [116.4842919, 39.9764461]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '793508723', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4573103, 39.9790324], [116.4576738, 39.9790351], [116.4576779, 39.9787062], [116.4573144, 39.9787035], [116.4573103, 39.9790324]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '793508724', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4579582, 39.9786079], [116.4583217, 39.9786105], [116.4583257, 39.9782816], [116.4579622, 39.978279], [116.4579582, 39.9786079]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '793508725', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4585317, 39.9781946], [116.4588952, 39.9781972], [116.4588993, 39.9778683], [116.4585358, 39.9778657], [116.4585317, 39.9781946]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '793508726', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.458954, 39.9776561], [116.4593175, 39.9776588], [116.4593215, 39.9773299], [116.458958, 39.9773272], [116.458954, 39.9776561]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '793508727', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4581294, 39.9775839], [116.4584929, 39.9775866], [116.458497, 39.9772577], [116.4581334, 39.977255], [116.4581294, 39.9775839]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '793508728', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4573965, 39.9775708], [116.45776, 39.9775735], [116.4577641, 39.9772445], [116.4574006, 39.9772419], [116.4573965, 39.9775708]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '793508729', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4566261, 39.9775625], [116.4569896, 39.9775651], [116.4569937, 39.9772362], [116.4566302, 39.9772336], [116.4566261, 39.9775625]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827546872', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4782373, 40.000425], [116.4783003, 40.0005199], [116.4783883, 40.0004856], [116.4784373, 40.0005594], [116.4793886, 40.0001888], [116.4793118, 40.0000732], [116.4790346, 40.0001811], [116.4789994, 40.0001281], [116.4782373, 40.000425]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827546873', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4777434, 40.0000716], [116.4779016, 40.0002993], [116.4782965, 40.0001383], [116.478167, 39.9999519], [116.4781383, 39.9999106], [116.4777434, 40.0000716]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827546874', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4783723, 39.9998292], [116.4784851, 39.9999971], [116.4789402, 39.9998176], [116.4791012, 40.0000572], [116.479237, 40.0000036], [116.4792052, 39.9999563], [116.4792978, 39.9999198], [116.4791076, 39.9996367], [116.4790119, 39.9996744], [116.4789601, 39.9995974], [116.4783723, 39.9998292]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827549844', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4810483, 39.9981987], [116.4816505, 39.9985779], [116.4820182, 39.9982352], [116.4814161, 39.997856], [116.4810483, 39.9981987]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827549910', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4828081, 39.9955179], [116.4833184, 39.9957118], [116.4833984, 39.9955882], [116.4828881, 39.9953944], [116.4828081, 39.9955179]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827549911', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4836916, 39.9954772], [116.4842019, 39.9956711], [116.4842819, 39.9955475], [116.4837715, 39.9953537], [116.4836916, 39.9954772]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827549912', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.482999, 39.9947669], [116.4834053, 39.9949092], [116.483471, 39.9947989], [116.4830648, 39.9946566], [116.482999, 39.9947669]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827549913', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4836129, 39.9947642], [116.4840191, 39.9949065], [116.4840849, 39.9947962], [116.4836787, 39.9946539], [116.4836129, 39.9947642]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550356', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.485288, 39.9973067], [116.485288, 39.9974353], [116.4861445, 39.9974353], [116.4861445, 39.9973067], [116.485288, 39.9973067]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550357', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.486392, 39.9962167], [116.4865501, 39.9962801], [116.4870627, 39.996283], [116.4871098, 39.9962426], [116.487107, 39.9961828], [116.4869912, 39.9961115], [116.4864541, 39.9961194], [116.486392, 39.9962167]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550358', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.485563, 39.9965727], [116.485564, 39.9966991], [116.4860992, 39.9966965], [116.4860981, 39.9965701], [116.485563, 39.9965727]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550359', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4857138, 39.9962015], [116.4857148, 39.9962765], [116.4861899, 39.9962729], [116.4861881, 39.9961365], [116.4857938, 39.9961396], [116.4857138, 39.9962015]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550360', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4863186, 39.9968084], [116.4871484, 39.9968102], [116.4871489, 39.9966816], [116.4863191, 39.9966798], [116.4863186, 39.9968084]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550361', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4862225, 39.9976638], [116.4862225, 39.9977924], [116.487079, 39.9977924], [116.487079, 39.9976638], [116.4862225, 39.9976638]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550815', code: 1500, fclass: 'building', name: 'T3', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4863802, 39.9954185], [116.4864024, 39.9955173], [116.4864479, 39.9956171], [116.4865261, 39.9957179], [116.4866368, 39.9958027], [116.48678, 39.9958406], [116.4868934, 39.9958057], [116.4869415, 39.9957009], [116.4869051, 39.9955882], [116.4868361, 39.9954914], [116.4867592, 39.9953906], [116.4866485, 39.9953198], [116.4865287, 39.9953018], [116.4864297, 39.9953317], [116.4863802, 39.9954185]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550816', code: 1500, fclass: 'building', name: 'T1', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.485516, 39.9945731], [116.4856186, 39.994713], [116.485789, 39.9948568], [116.4860064, 39.9949248], [116.4862133, 39.9948795], [116.4862498, 39.9947169], [116.4861507, 39.9945744], [116.4859873, 39.9944518], [116.4857769, 39.9943719], [116.4855665, 39.9944159], [116.485516, 39.9945731]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550817', code: 1500, fclass: 'building', name: 'T2', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4852658, 39.9954258], [116.485318, 39.9955591], [116.4854156, 39.9956737], [116.4855075, 39.9957349], [116.485624, 39.9957149], [116.4856988, 39.9955937], [116.4857162, 39.9954325], [116.4856692, 39.995278], [116.4855492, 39.9951647], [116.4854397, 39.9951474], [116.4853441, 39.9951821], [116.4852867, 39.995302], [116.4852658, 39.9954258]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550903', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4824515, 39.9962252], [116.4831362, 39.9964829], [116.4832168, 39.9963571], [116.4825322, 39.9960994], [116.4824515, 39.9962252]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '827550904', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4832381, 39.9966773], [116.4839227, 39.996935], [116.4840034, 39.9968092], [116.4833188, 39.9965514], [116.4832381, 39.9966773]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830306096', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4627026, 39.9805284], [116.463374, 39.9805668], [116.4635221, 39.980461], [116.4635319, 39.9803203], [116.4633974, 39.9803148], [116.4633895, 39.9804283], [116.4633132, 39.980468], [116.4627118, 39.9804335], [116.4627026, 39.9805284]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830306097', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4628167, 39.9808833], [116.4636401, 39.9809241], [116.4637882, 39.9808183], [116.4638139, 39.980559], [116.4636788, 39.9805511], [116.4636555, 39.9807856], [116.4635792, 39.9808253], [116.4628248, 39.9807879], [116.4628167, 39.9808833]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830306098', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4622016, 39.9807025], [116.4623421, 39.9808363], [116.4626774, 39.9808503], [116.462684, 39.9807584], [116.4623907, 39.9807461], [116.4623299, 39.9806774], [116.4623611, 39.9803376], [116.4622357, 39.9803309], [116.4622016, 39.9807025]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830306099', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4624164, 39.9801017], [116.4624989, 39.9801761], [116.4626587, 39.9800723], [116.4631817, 39.9801064], [116.463193, 39.9800051], [116.4626222, 39.9799679], [116.4624164, 39.9801017]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830306581', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4609966, 39.9804918], [116.4617533, 39.9805338], [116.4618077, 39.9805856], [116.4617965, 39.9807082], [116.4619434, 39.9807161], [116.4619568, 39.9805683], [116.4617952, 39.9804325], [116.4610063, 39.9803887], [116.4609966, 39.9804918]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830306582', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.461112, 39.9808375], [116.4617211, 39.9808713], [116.4617755, 39.980923], [116.4617643, 39.9810457], [116.4619111, 39.9810535], [116.4619246, 39.9809058], [116.461763, 39.9807699], [116.4611218, 39.9807344], [116.461112, 39.9808375]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830306583', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4605534, 39.9804833], [116.4605842, 39.9810634], [116.4607597, 39.9812284], [116.4617304, 39.9812866], [116.4617402, 39.9811904], [116.4608359, 39.9811362], [116.4607697, 39.9810676], [116.4607384, 39.9804775], [116.4605534, 39.9804833]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830307866', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4627908, 39.9812446], [116.4635475, 39.9812867], [116.4636019, 39.9813384], [116.4635815, 39.9815766], [116.4637186, 39.9815835], [116.4637399, 39.9813351], [116.4635894, 39.9811853], [116.4628005, 39.9811415], [116.4627908, 39.9812446]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830307867', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4620865, 39.9815689], [116.4622227, 39.9815761], [116.4622548, 39.98122], [116.4623299, 39.9811747], [116.4626308, 39.9811946], [116.4626414, 39.9811], [116.4623041, 39.9810778], [116.4621197, 39.9812003], [116.4620865, 39.9815689]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830307868', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4626002, 39.9815775], [116.4632093, 39.9816113], [116.4632637, 39.981663], [116.4632525, 39.9817857], [116.4633993, 39.9817935], [116.4634128, 39.9816458], [116.4632512, 39.9815099], [116.46261, 39.9814744], [116.4626002, 39.9815775]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '830307869', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4620923, 39.9817975], [116.4622738, 39.9819556], [116.4629986, 39.9819979], [116.4630079, 39.9819039], [116.4623408, 39.981865], [116.4621887, 39.9817326], [116.4620923, 39.9817975]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '832188804', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4636934, 39.9830776], [116.4644495, 39.9831336], [116.4644943, 39.9831554], [116.4645268, 39.9831835], [116.4645146, 39.983305], [116.4646325, 39.9833174], [116.4646284, 39.9832645], [116.4646406, 39.9831866], [116.4646122, 39.9831243], [116.4645552, 39.9830776], [116.4644495, 39.9830402], [116.4637096, 39.9829841], [116.4636934, 39.9830776]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '832188805', code: 1500, fclass: 'building', name: null, type: 'residential'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4631486, 39.9830184], [116.4631689, 39.9830994], [116.4632746, 39.9831928], [116.4633681, 39.9831305], [116.4632746, 39.9830402], [116.4632705, 39.9829841], [116.4632909, 39.9829405], [116.4636771, 39.9826944], [116.4640308, 39.9827162], [116.464043, 39.9826196], [116.4636405, 39.9825916], [116.4631852, 39.9828688], [116.4631486, 39.9830184]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '836335936', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5003718, 39.9728345], [116.5015347, 39.9728196], [116.501549, 39.9726033], [116.5003827, 39.972633], [116.5003718, 39.9728345]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '836335937', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4990773, 39.9724189], [116.5000729, 39.9727927], [116.5001243, 39.9725862], [116.4991352, 39.9722171], [116.4990773, 39.9724189]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '836335938', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5003606, 39.972318], [116.5015055, 39.9723088], [116.5015373, 39.9720582], [116.5003619, 39.972071], [116.5003606, 39.972318]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '836335939', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5003648, 39.9717782], [116.5015233, 39.971788], [116.5015688, 39.9715093], [116.5003808, 39.9715137], [116.5003648, 39.9717782]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '836335940', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4990534, 39.9719521], [116.5001089, 39.9719507], [116.5001873, 39.9716808], [116.4990588, 39.9716997], [116.4990534, 39.9719521]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '836335941', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4990694, 39.9711764], [116.4990836, 39.9714087], [116.5001355, 39.9714134], [116.5001564, 39.9711817], [116.4990694, 39.9711764]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719749', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.498778, 39.9784563], [116.4987785, 39.9786216], [116.4999062, 39.97862], [116.4999058, 39.9784545], [116.498778, 39.9784563]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719750', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4987673, 39.9773495], [116.4987677, 39.9775148], [116.4998954, 39.9775132], [116.499895, 39.9773477], [116.4987673, 39.9773495]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719751', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5001173, 39.9773605], [116.5001173, 39.9775248], [116.5009808, 39.9775248], [116.5009808, 39.9773604], [116.5001173, 39.9773605]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719752', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5001254, 39.9784405], [116.5001254, 39.9786048], [116.5009889, 39.9786049], [116.5009889, 39.9784404], [116.5001254, 39.9784405]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719753', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5010124, 39.9780866], [116.5013536, 39.9780879], [116.5013539, 39.9780382], [116.5016724, 39.9780393], [116.5016728, 39.9779836], [116.5019812, 39.9779847], [116.501982, 39.9778589], [116.5016591, 39.9778578], [116.5016588, 39.9779012], [116.5013382, 39.9779001], [116.5013379, 39.9779476], [116.5010133, 39.9779464], [116.5010124, 39.9780866]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719754', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5011975, 39.9773733], [116.5011984, 39.9774842], [116.5019474, 39.9774806], [116.5019465, 39.9773673], [116.5017992, 39.977368], [116.5017988, 39.9773108], [116.5013444, 39.9773128], [116.5013449, 39.9773726], [116.5011975, 39.9773733]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719755', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.500172, 39.9764145], [116.5001734, 39.9765437], [116.5010711, 39.976538], [116.5010719, 39.9766029], [116.5017233, 39.9765986], [116.5017219, 39.9764695], [116.5010799, 39.9764737], [116.5010792, 39.9764086], [116.500172, 39.9764145]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719756', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5001771, 39.97571], [116.5001784, 39.9758646], [116.5011962, 39.9758595], [116.5011965, 39.975888], [116.501619, 39.9758858], [116.5016179, 39.9757602], [116.5011783, 39.9757625], [116.5011778, 39.9757049], [116.5001771, 39.97571]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719757', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5001494, 39.9750049], [116.5014138, 39.9750094], [116.5014149, 39.9748284], [116.5001504, 39.9748241], [116.5001494, 39.9750049]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845719758', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5001556, 39.9733074], [116.5012749, 39.9733112], [116.501273, 39.9736361], [116.5015303, 39.973637], [116.5015321, 39.973329], [116.5014585, 39.9733287], [116.5014596, 39.9731435], [116.5001566, 39.9731389], [116.5001556, 39.9733074]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845720410', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4989255, 39.9707757], [116.4998813, 39.9707844], [116.4998801, 39.970856], [116.500139, 39.9708584], [116.500138, 39.9709206], [116.500368, 39.9709228], [116.50037, 39.9707973], [116.5001384, 39.9707951], [116.5001394, 39.9707305], [116.4998899, 39.9707282], [116.4998911, 39.9706517], [116.4989276, 39.9706429], [116.4989255, 39.9707757]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845720411', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4984887, 39.9740969], [116.4989452, 39.9740932], [116.4988745, 39.9737606], [116.4985608, 39.9737596], [116.4984887, 39.9740969]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845720412', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4993219, 39.9741199], [116.4997785, 39.9741162], [116.4997078, 39.9737835], [116.499394, 39.9737826], [116.4993219, 39.9741199]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845720413', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4990931, 39.975045], [116.4995216, 39.9750485], [116.4994509, 39.9747158], [116.4991815, 39.9747185], [116.4990931, 39.975045]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845720414', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4982829, 39.9750482], [116.4987114, 39.9750517], [116.4986407, 39.974719], [116.4983713, 39.9747216], [116.4982829, 39.9750482]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845720415', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4974773, 39.9750249], [116.4979058, 39.9750284], [116.4978351, 39.9746957], [116.4975657, 39.9746984], [116.4974773, 39.9750249]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845720416', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4986508, 39.9759305], [116.4990116, 39.9759286], [116.4989385, 39.9756013], [116.4987111, 39.9756058], [116.4986508, 39.9759305]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '845720417', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4992964, 39.9759361], [116.4996572, 39.9759342], [116.4995842, 39.9756069], [116.4993568, 39.9756113], [116.4992964, 39.9759361]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848891034', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4571521, 39.9878668], [116.457493, 39.9878664], [116.4574705, 39.9876234], [116.4571739, 39.9876241], [116.4571521, 39.9878668]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848891035', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.455823, 39.9879363], [116.4561616, 39.9879666], [116.4561765, 39.9877233], [116.4558819, 39.9876973], [116.455823, 39.9879363]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848891036', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4551001, 39.9877726], [116.4554387, 39.9878029], [116.4554535, 39.9875596], [116.4551589, 39.9875336], [116.4551001, 39.9877726]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848891037', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4565672, 39.9880958], [116.4569047, 39.9881324], [116.4569274, 39.9878895], [116.4566338, 39.9878579], [116.4565672, 39.9880958]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848893222', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4551707, 39.9892241], [116.4557114, 39.9893048], [116.4557452, 39.9891718], [116.4552044, 39.9890911], [116.4551707, 39.9892241]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848893223', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4547897, 39.9908015], [116.4557503, 39.9909394], [116.4557804, 39.9908164], [116.4548198, 39.9906785], [116.4547897, 39.9908015]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848893224', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4549278, 39.9902676], [116.4558151, 39.9903896], [116.4558438, 39.9902674], [116.4549565, 39.9901454], [116.4549278, 39.9902676]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848893225', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4550532, 39.989762], [116.4559405, 39.9898841], [116.4559691, 39.9897619], [116.4550818, 39.9896398], [116.4550532, 39.989762]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848894599', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4538116, 39.9898958], [116.4547991, 39.9900414], [116.4548329, 39.9899069], [116.4538454, 39.9897612], [116.4538116, 39.9898958]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848894600', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4539147, 39.9894815], [116.4549022, 39.9896272], [116.454936, 39.9894926], [116.4539485, 39.989347], [116.4539147, 39.9894815]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848894601', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4535966, 39.9906955], [116.4546198, 39.9908504], [116.4546496, 39.9907348], [116.4536265, 39.9905798], [116.4535966, 39.9906955]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848894602', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4540807, 39.9890278], [116.4546176, 39.9891036], [116.4546077, 39.9891449], [116.4549999, 39.9892003], [116.4550299, 39.9890752], [116.4546522, 39.9890219], [116.4546626, 39.9889788], [116.4541112, 39.988901], [116.4540807, 39.9890278]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848894603', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4539035, 39.9903231], [116.4547057, 39.9904399], [116.454737, 39.9903133], [116.4539349, 39.9901965], [116.4539035, 39.9903231]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848894604', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4542669, 39.9882695], [116.4548116, 39.988341], [116.4548025, 39.9883819], [116.4555455, 39.9884794], [116.4555737, 39.9883533], [116.4548485, 39.9882581], [116.4548581, 39.9882155], [116.4542954, 39.9881416], [116.4542669, 39.9882695]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '848894605', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4542065, 39.9886502], [116.4547484, 39.9887233], [116.454739, 39.9887644], [116.4556299, 39.9888845], [116.4556588, 39.9887586], [116.4547847, 39.9886407], [116.4547945, 39.9885979], [116.4542358, 39.9885226], [116.4542065, 39.9886502]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279443', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4774639, 40.0025297], [116.4774656, 40.0026597], [116.4776324, 40.0026584], [116.4776338, 40.0027673], [116.477733, 40.0027666], [116.4777317, 40.0026644], [116.4779838, 40.0026625], [116.4779852, 40.0027706], [116.478079, 40.0027699], [116.4780776, 40.0026641], [116.4782362, 40.0026629], [116.4782344, 40.0025239], [116.4774639, 40.0025297]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279444', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4782925, 40.0022465], [116.4782941, 40.0023764], [116.4784609, 40.0023751], [116.4784623, 40.002484], [116.4785615, 40.0024833], [116.4785602, 40.0023811], [116.4788123, 40.0023792], [116.4788137, 40.0024873], [116.4789075, 40.0024866], [116.4789061, 40.0023808], [116.4790647, 40.0023796], [116.4790629, 40.0022406], [116.4782925, 40.0022465]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279445', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4769982, 40.0022989], [116.4771664, 40.0022993], [116.4771659, 40.0024121], [116.4772804, 40.0024124], [116.477281, 40.0022938], [116.477414, 40.0022941], [116.4774145, 40.0021846], [116.4769987, 40.0021835], [116.4769982, 40.0022989]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279446', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4791869, 40.0021048], [116.4793551, 40.0021053], [116.4793546, 40.0022181], [116.4794691, 40.0022184], [116.4794696, 40.0020998], [116.4796026, 40.0021001], [116.4796031, 40.0019906], [116.4791874, 40.0019895], [116.4791869, 40.0021048]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279447', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4802784, 40.0010747], [116.4802793, 40.0011848], [116.4803806, 40.0011843], [116.4803809, 40.0012303], [116.4805982, 40.0012293], [116.4805978, 40.0011817], [116.480695, 40.0011813], [116.4806941, 40.0010726], [116.4802784, 40.0010747]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279448', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4797754, 40.0007909], [116.4797764, 40.000901], [116.4798776, 40.0009006], [116.479878, 40.0009466], [116.4800953, 40.0009455], [116.4800949, 40.000898], [116.4801921, 40.0008975], [116.4801912, 40.0007888], [116.4797754, 40.0007909]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279449', code: 1500, fclass: 'building', name: '浦项大厦', type: 'office'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4817581, 39.9973051], [116.4818117, 39.9975434], [116.4820558, 39.9975372], [116.4824045, 39.9976009], [116.4826485, 39.9976626], [116.4828202, 39.9977078], [116.4829087, 39.9977961], [116.4829436, 39.9978742], [116.4829436, 39.9980386], [116.4827746, 39.9982996], [116.4829946, 39.9984105], [116.4832064, 39.9982728], [116.483362, 39.9980982], [116.4833727, 39.9977818], [116.4831474, 39.9975146], [116.4827344, 39.9973646], [116.482214, 39.9972907], [116.4817581, 39.9973051]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279450', code: 1500, fclass: 'building', name: null, type: 'office'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4840509, 40.0000587], [116.4840643, 40.0001943], [116.4842038, 40.0003094], [116.4843191, 40.0003402], [116.4844532, 40.0002868], [116.4846464, 40.0003196], [116.4848019, 40.0003813], [116.4849441, 40.0004059], [116.4850675, 40.0003443], [116.4851399, 40.0002025], [116.4850219, 40.0000279], [116.4847188, 39.9998717], [116.4843594, 39.999847], [116.4841448, 39.9999005], [116.4840509, 40.0000587]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279451', code: 1500, fclass: 'building', name: '绿地中心', type: 'office'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4820419, 39.9988096], [116.4824657, 39.9990604], [116.4827232, 39.9988188], [116.4823168, 39.9985538], [116.4820419, 39.9988096]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279452', code: 1500, fclass: 'building', name: '阿里健康', type: 'office'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4803869, 39.9997635], [116.4805908, 39.9999279], [116.4809287, 39.9998457], [116.4811111, 39.9997388], [116.4808322, 39.9994635], [116.4803869, 39.9997635]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279453', code: 1500, fclass: 'building', name: null, type: '写字楼'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4804113, 39.9986892], [116.4807734, 39.9990016], [116.4810752, 39.9988002], [116.4806755, 39.9985146], [116.4804113, 39.9986892]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279454', code: 1500, fclass: 'building', name: '中旺大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4855925, 39.9989682], [116.4855957, 39.9990122], [116.4856403, 39.9990342], [116.4857392, 39.9990317], [116.4858349, 39.9990415], [116.4859083, 39.9990708], [116.4859816, 39.9990855], [116.4860518, 39.9990806], [116.4860933, 39.9990366], [116.486106, 39.9989829], [116.4861156, 39.9989267], [116.4861475, 39.9988705], [116.4861794, 39.9988191], [116.4861794, 39.9987849], [116.4861507, 39.9987458], [116.4861092, 39.9987263], [116.4860263, 39.9987263], [116.4859497, 39.9987214], [116.4858349, 39.9986872], [116.4857807, 39.9986725], [116.4857233, 39.9986848], [116.4856977, 39.9987092], [116.4856754, 39.9987507], [116.485669, 39.9988338], [116.4856244, 39.9989169], [116.4855925, 39.9989682]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279455', code: 1500, fclass: 'building', name: '中航资本大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4846859, 39.9992353], [116.4846891, 39.9992793], [116.4847337, 39.9993013], [116.4848326, 39.9992988], [116.4849283, 39.9993086], [116.4850017, 39.9993379], [116.485075, 39.9993526], [116.4851452, 39.9993477], [116.4851867, 39.9993037], [116.4851994, 39.99925], [116.485209, 39.9991938], [116.4852409, 39.9991376], [116.4852728, 39.9990862], [116.4852728, 39.999052], [116.4852441, 39.9990129], [116.4852026, 39.9989934], [116.4851197, 39.9989934], [116.4850431, 39.9989885], [116.4849283, 39.9989543], [116.4848741, 39.9989396], [116.4848167, 39.9989519], [116.4847911, 39.9989763], [116.4847688, 39.9990178], [116.4847624, 39.9991009], [116.4847178, 39.999184], [116.4846859, 39.9992353]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '860279456', code: 1500, fclass: 'building', name: '金辉大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4821539, 40.0014503], [116.4821571, 40.0014943], [116.4822017, 40.0015163], [116.4823006, 40.0015138], [116.4823963, 40.0015236], [116.4824697, 40.0015529], [116.482543, 40.0015676], [116.4826132, 40.0015627], [116.4826547, 40.0015187], [116.4826674, 40.001465], [116.482677, 40.0014088], [116.4827089, 40.0013526], [116.4827408, 40.0013012], [116.4827408, 40.001267], [116.4827121, 40.0012279], [116.4826706, 40.0012084], [116.4825877, 40.0012084], [116.4825111, 40.0012035], [116.4823963, 40.0011693], [116.4823421, 40.0011546], [116.4822847, 40.0011669], [116.4822591, 40.0011913], [116.4822368, 40.0012328], [116.4822304, 40.0013159], [116.4821858, 40.001399], [116.4821539, 40.0014503]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '908328961', code: 1500, fclass: 'building', name: '5号楼', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.466474, 39.9857899], [116.4665365, 39.9857978], [116.4665266, 39.985844], [116.4665885, 39.9858518], [116.4665797, 39.985893], [116.4666555, 39.9859024], [116.466666, 39.9858531], [116.4666302, 39.9858486], [116.4666397, 39.9858034], [116.4666003, 39.9857985], [116.466619, 39.9857459], [116.4666706, 39.9857197], [116.4667544, 39.9857408], [116.4667465, 39.9857801], [116.4667779, 39.9857838], [116.4667698, 39.9858241], [116.4667917, 39.9858266], [116.4668131, 39.9857173], [116.4667709, 39.9857125], [116.4667846, 39.9856422], [116.4667489, 39.9856381], [116.4667366, 39.9856145], [116.4666072, 39.9856086], [116.4666211, 39.9856452], [116.4665456, 39.9857007], [116.4664946, 39.9856942], [116.466474, 39.9857899]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '912220110', code: 1500, fclass: 'building', name: '国家乡村振兴局', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.44849, 39.9697333], [116.4484941, 39.9697444], [116.4486515, 39.9698714], [116.4488347, 39.9697381], [116.4488632, 39.969532], [116.4485217, 39.9695043], [116.44849, 39.9697333]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '912220111', code: 1500, fclass: 'building', name: '共济大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.448514, 39.9696577], [116.4485182, 39.9696933], [116.448537, 39.9697259], [116.4485685, 39.9697523], [116.4486093, 39.9697695], [116.4486552, 39.9697758], [116.4487027, 39.9697702], [116.4487451, 39.9697529], [116.4487777, 39.9697258], [116.4487969, 39.969692], [116.4488004, 39.9696553], [116.4487879, 39.9696197], [116.4487607, 39.9695893], [116.448722, 39.9695674], [116.4486761, 39.9695566], [116.4486295, 39.9695577], [116.4485859, 39.9695702], [116.4485497, 39.9695927], [116.4485249, 39.969623], [116.448514, 39.9696577]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '916885296', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4678046, 39.9833728], [116.4684927, 39.9834027], [116.4685029, 39.9832756], [116.4678101, 39.9832482], [116.4678046, 39.9833728]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '916885297', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4675772, 39.9831324], [116.4680278, 39.9831571], [116.4680449, 39.9829869], [116.4675879, 39.9829614], [116.4675772, 39.9831324]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '916885298', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4683035, 39.9823716], [116.4686934, 39.982396], [116.4687238, 39.982081], [116.4683353, 39.9820531], [116.4683035, 39.9823716]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '916885299', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4675131, 39.9823781], [116.4680859, 39.9824162], [116.4681, 39.9822764], [116.4675258, 39.9822383], [116.4675131, 39.9823781]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '916885300', code: 1500, fclass: 'building', name: null, type: 'school'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4676235, 39.9828742], [116.4680509, 39.9828987], [116.4680764, 39.982664], [116.4676426, 39.9826445], [116.4676235, 39.9828742]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '931307322', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5002385, 39.9979205], [116.5003507, 39.9979374], [116.5003755, 39.997841], [116.5002633, 39.9978241], [116.5002385, 39.9979205]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '931307323', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5002643, 39.9977972], [116.500397, 39.9978173], [116.5004237, 39.997714], [116.500291, 39.9976939], [116.5002643, 39.9977972]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '931307324', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5004131, 39.9978099], [116.5005268, 39.9978278], [116.5005551, 39.9977223], [116.5004413, 39.9977044], [116.5004131, 39.9978099]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '931307325', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5005436, 39.9977144], [116.5006613, 39.9977364], [116.5006869, 39.9976556], [116.500694, 39.9976325], [116.5005723, 39.9976124], [116.5005436, 39.9977144]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '931307326', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5005768, 39.9976023], [116.500699, 39.9976198], [116.5007224, 39.9975239], [116.5006002, 39.9975064], [116.5005768, 39.9976023]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '931307327', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5004577, 39.9975967], [116.500564, 39.9976127], [116.5005737, 39.9975751], [116.5005401, 39.99757], [116.500546, 39.9975472], [116.5005769, 39.9975518], [116.500584, 39.9975239], [116.5004804, 39.9975083], [116.5004577, 39.9975967]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '931307328', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.5006613, 39.9977364], [116.5008721, 39.9977756], [116.5008977, 39.9976948], [116.5006869, 39.9976556], [116.5006613, 39.9977364]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509839', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4541505, 40.0086709], [116.4541526, 40.008824], [116.4543455, 40.0088225], [116.454346, 40.0088597], [116.4550898, 40.0088537], [116.4550879, 40.0087105], [116.454347, 40.0087164], [116.4543463, 40.0086693], [116.4541505, 40.0086709]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509840', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4552763, 40.0088133], [116.4561532, 40.0088162], [116.456154, 40.0086857], [116.455277, 40.0086828], [116.4552763, 40.0088133]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509841', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4552126, 40.0085162], [116.4554669, 40.0085165], [116.455467, 40.0084733], [116.4561614, 40.0084742], [116.4561616, 40.0083602], [116.4554503, 40.0083593], [116.4554502, 40.0084042], [116.4552129, 40.0084039], [116.4552126, 40.0085162]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509842', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4543137, 40.0083797], [116.4543143, 40.0084932], [116.4548446, 40.0084917], [116.4548447, 40.0085279], [116.4550832, 40.0085272], [116.4550826, 40.0084222], [116.4548315, 40.0084229], [116.4548313, 40.0083782], [116.4543137, 40.0083797]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509843', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4545478, 40.0097521], [116.455027, 40.0098286], [116.4550655, 40.0096871], [116.4545862, 40.0096107], [116.4545478, 40.0097521]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509844', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4552004, 40.0094616], [116.4558666, 40.0094627], [116.455867, 40.0093277], [116.4552008, 40.0093266], [116.4552004, 40.0094616]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509845', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4551736, 40.009833], [116.4559177, 40.0098338], [116.4559179, 40.0096919], [116.4551738, 40.0096912], [116.4551736, 40.009833]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509846', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4539373, 40.0090041], [116.4539404, 40.0091509], [116.4541481, 40.0091483], [116.4541495, 40.0092151], [116.4543746, 40.0092123], [116.454376, 40.0092766], [116.4545835, 40.009274], [116.4545847, 40.0093288], [116.4548058, 40.009326], [116.4548026, 40.0091764], [116.4545868, 40.0091791], [116.4545855, 40.0091156], [116.4543674, 40.0091183], [116.4543662, 40.0090618], [116.4541418, 40.0090646], [116.4541405, 40.0090016], [116.4539373, 40.0090041]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509847', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4552763, 40.0091427], [116.4561532, 40.0091456], [116.456154, 40.0090151], [116.4552771, 40.0090122], [116.4552763, 40.0091427]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '934509848', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4537002, 40.0094411], [116.4543632, 40.009699], [116.4544497, 40.0095685], [116.4537868, 40.0093106], [116.4537002, 40.0094411]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '941787659', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4725386, 39.9921701], [116.4728127, 39.9923092], [116.472997, 39.9920959], [116.472723, 39.9919569], [116.4725386, 39.9921701]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '941787660', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4714937, 39.9914112], [116.4717591, 39.9914405], [116.4717854, 39.9913187], [116.4715081, 39.9912952], [116.4714937, 39.9914112]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '941787661', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4702005, 39.9928638], [116.4704302, 39.9930695], [116.4705653, 39.9929809], [116.4705026, 39.9929248], [116.4704493, 39.9929598], [116.4702823, 39.9928102], [116.4702005, 39.9928638]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '941787662', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4726716, 39.992852], [116.4728314, 39.9930001], [116.4731966, 39.9927688], [116.4729737, 39.9925622], [116.4728706, 39.9926275], [116.4729338, 39.992686], [116.4726716, 39.992852]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '941787663', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4689485, 39.9889541], [116.4697248, 39.9896547], [116.4700856, 39.98942], [116.4700342, 39.9893736], [116.4701349, 39.9893081], [116.4694101, 39.9886539], [116.4689485, 39.9889541]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '941787664', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4719568, 39.993383], [116.472136, 39.9935364], [116.4724658, 39.9931284], [116.4722429, 39.9930241], [116.4719568, 39.993383]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '941787666', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4729647, 39.9924482], [116.4731788, 39.9926562], [116.4733891, 39.9925036], [116.4731355, 39.9922684], [116.4729647, 39.9924482]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '941823651', code: 1500, fclass: 'building', name: '方恒时代中心', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.477836, 39.9919452], [116.4781801, 39.9919495], [116.4781801, 39.9915598], [116.4778392, 39.9915632], [116.477836, 39.9919452]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '941823652', code: 1500, fclass: 'building', name: '联网大厦', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4779357, 39.9905949], [116.4779358, 39.9908666], [116.4784365, 39.9908721], [116.4785567, 39.9909142], [116.4786468, 39.9907714], [116.4784299, 39.9906964], [116.4784278, 39.9906333], [116.4783808, 39.9906248], [116.4783675, 39.9905928], [116.4779357, 39.9905949]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '945348472', code: 1500, fclass: 'building', name: null, type: 'grandstand'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4892407, 39.9993938], [116.4893578, 39.9994373], [116.4894198, 39.9993395], [116.4893027, 39.999296], [116.4892407, 39.9993938]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '946034870', code: 1500, fclass: 'building', name: null, type: 'ruins'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4477579, 39.9848187], [116.4478272, 39.9850435], [116.447985, 39.9852403], [116.4482154, 39.9853893], [116.4484951, 39.9854755], [116.4487863, 39.9854909], [116.4490697, 39.9854377], [116.4493188, 39.9853212], [116.4495099, 39.9851522], [116.449625, 39.9849467], [116.4496533, 39.9847242], [116.449592, 39.9845056], [116.449447, 39.9843115], [116.449232, 39.9841604], [116.4489578, 39.9840643], [116.4486581, 39.984039], [116.4483632, 39.9840869], [116.4481027, 39.9842032], [116.4479029, 39.9843762], [116.447784, 39.9845885], [116.4477579, 39.9848187]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '947114736', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4723912, 39.988523], [116.4726289, 39.9887398], [116.4730093, 39.9885011], [116.4727796, 39.9882844], [116.4723912, 39.988523]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '947114737', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4717127, 39.9889354], [116.4719504, 39.9891523], [116.4723308, 39.9889136], [116.4721011, 39.9886969], [116.4717127, 39.9889354]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '947114738', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4711541, 39.9892823], [116.4713917, 39.9894991], [116.4717721, 39.9892604], [116.4715425, 39.9890437], [116.4711541, 39.9892823]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948302252', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.46855, 40.0068113], [116.4685625, 40.0071176], [116.4689104, 40.007124], [116.4688354, 40.0068043], [116.46855, 40.0068113]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948302254', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4702817, 40.0067825], [116.4702942, 40.0070889], [116.4706421, 40.0070952], [116.4705671, 40.0067755], [116.4702817, 40.0067825]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948302258', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4709551, 40.0068023], [116.4709676, 40.0071086], [116.4713155, 40.007115], [116.4712405, 40.0067953], [116.4709551, 40.0068023]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948302260', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4714596, 40.0064033], [116.4714721, 40.0067096], [116.47182, 40.006716], [116.471745, 40.0063963], [116.4714596, 40.0064033]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948302263', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4697568, 40.0064214], [116.4697693, 40.0067278], [116.4701172, 40.0067342], [116.4700422, 40.0064145], [116.4697568, 40.0064214]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948302264', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4690719, 40.0064329], [116.4690844, 40.0067393], [116.4694323, 40.0067456], [116.4693573, 40.0064259], [116.4690719, 40.0064329]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948302265', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4673625, 40.0063262], [116.467375, 40.0066326], [116.4677229, 40.0066389], [116.467648, 40.0063192], [116.4673625, 40.0063262]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948302266', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4678689, 40.0067945], [116.4678814, 40.0071008], [116.4682293, 40.0071072], [116.4681543, 40.0067875], [116.4678689, 40.0067945]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948302268', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4790625, 40.0016692], [116.4790635, 40.0017793], [116.4791647, 40.0017789], [116.4791651, 40.0018249], [116.4793824, 40.0018238], [116.479382, 40.0017763], [116.4794792, 40.0017758], [116.4794783, 40.0016671], [116.4790625, 40.0016692]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303397', code: 1500, fclass: 'building', name: '402', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4653177, 39.9978079], [116.4654348, 39.9978102], [116.4654438, 39.9977127], [116.4655014, 39.9977132], [116.4655012, 39.9977727], [116.4657533, 39.9977752], [116.46575, 39.9978375], [116.4658698, 39.9978388], [116.465881, 39.9977061], [116.4657839, 39.9976199], [116.465389, 39.9976017], [116.4653272, 39.9976301], [116.4653177, 39.9978079]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303398', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4671612, 39.9955977], [116.4672916, 39.9957099], [116.467438, 39.99561], [116.4675671, 39.9957211], [116.4677061, 39.9956263], [116.4675742, 39.9955129], [116.4676981, 39.9954283], [116.4675705, 39.9953185], [116.4674386, 39.9954085], [116.4673144, 39.9953017], [116.4671782, 39.9953947], [116.4673023, 39.9955014], [116.4671612, 39.9955977]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303399', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4665583, 39.9948135], [116.4666887, 39.9949257], [116.4668351, 39.9948258], [116.4669642, 39.994937], [116.4671032, 39.9948421], [116.4669713, 39.9947287], [116.4670952, 39.9946441], [116.4669676, 39.9945343], [116.4668357, 39.9946243], [116.4667115, 39.9945175], [116.4665753, 39.9946105], [116.4666993, 39.9947172], [116.4665583, 39.9948135]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303400', code: 1500, fclass: 'building', name: '417', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4649134, 39.9945934], [116.4650439, 39.9947056], [116.4651902, 39.9946058], [116.4653194, 39.9947169], [116.4654584, 39.9946221], [116.4653265, 39.9945086], [116.4654504, 39.9944241], [116.4653227, 39.9943143], [116.4651909, 39.9944042], [116.4650667, 39.9942974], [116.4649304, 39.9943904], [116.4650545, 39.9944971], [116.4649134, 39.9945934]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303401', code: 1500, fclass: 'building', name: '420', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4622056, 39.9948816], [116.462336, 39.9949938], [116.4624824, 39.994894], [116.4626115, 39.9950051], [116.4627505, 39.9949103], [116.4626186, 39.9947968], [116.4627425, 39.9947123], [116.4626149, 39.9946025], [116.462483, 39.9946924], [116.4623588, 39.9945856], [116.4622226, 39.9946786], [116.4623466, 39.9947853], [116.4622056, 39.9948816]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303402', code: 1500, fclass: 'building', name: '414', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4654471, 39.9955278], [116.4655776, 39.9956401], [116.4657239, 39.9955402], [116.4658531, 39.9956513], [116.465992, 39.9955565], [116.4658601, 39.995443], [116.465984, 39.9953585], [116.4658564, 39.9952487], [116.4657245, 39.9953387], [116.4656004, 39.9952319], [116.4654641, 39.9953248], [116.4655882, 39.9954316], [116.4654471, 39.9955278]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303403', code: 1500, fclass: 'building', name: '405', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.46405, 39.9969164], [116.4641671, 39.9969187], [116.4641761, 39.9968212], [116.4642338, 39.9968217], [116.4642335, 39.9968813], [116.4644856, 39.9968838], [116.4644823, 39.996946], [116.4646022, 39.9969474], [116.4646133, 39.9968147], [116.4645162, 39.9967285], [116.4641213, 39.9967102], [116.4640595, 39.9967386], [116.46405, 39.9969164]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303404', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4660237, 39.9973567], [116.4661408, 39.997359], [116.4661498, 39.9972615], [116.4662074, 39.997262], [116.4662072, 39.9973215], [116.4664593, 39.997324], [116.4664559, 39.9973863], [116.4665758, 39.9973876], [116.466587, 39.9972549], [116.4664899, 39.9971688], [116.466095, 39.9971505], [116.4660332, 39.9971789], [116.4660237, 39.9973567]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303405', code: 1500, fclass: 'building', name: '426', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4631933, 39.9933049], [116.4633238, 39.9934172], [116.4634701, 39.9933173], [116.4635993, 39.9934284], [116.4637382, 39.9933336], [116.4636063, 39.9932201], [116.4637303, 39.9931356], [116.4636026, 39.9930258], [116.4634708, 39.9931158], [116.4633466, 39.993009], [116.4632103, 39.9931019], [116.4633344, 39.9932087], [116.4631933, 39.9933049]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303406', code: 1500, fclass: 'building', name: '418', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4657206, 39.9940683], [116.4658511, 39.9941805], [116.4659974, 39.9940807], [116.4661266, 39.9941918], [116.4662655, 39.994097], [116.4661335, 39.9939835], [116.4662575, 39.9938989], [116.4661299, 39.9937892], [116.4659981, 39.9938791], [116.4658739, 39.9937723], [116.4657376, 39.9938653], [116.4658617, 39.993972], [116.4657206, 39.9940683]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303407', code: 1500, fclass: 'building', name: '421', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4636389, 39.9950646], [116.4637694, 39.9951768], [116.4639157, 39.995077], [116.4640448, 39.9951881], [116.4641838, 39.9950933], [116.4640519, 39.9949798], [116.4641758, 39.9948952], [116.4640482, 39.9947854], [116.4639163, 39.9948754], [116.4637922, 39.9947686], [116.4636559, 39.9948616], [116.46378, 39.9949683], [116.4636389, 39.9950646]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303408', code: 1500, fclass: 'building', name: '419', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4627055, 39.9956833], [116.462836, 39.9957956], [116.4629823, 39.9956957], [116.4631115, 39.9958068], [116.4632505, 39.995712], [116.4631185, 39.9955985], [116.4632425, 39.995514], [116.4631148, 39.9954042], [116.462983, 39.9954942], [116.4628588, 39.9953874], [116.4627225, 39.9954803], [116.4628466, 39.9955871], [116.4627055, 39.9956833]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303409', code: 1500, fclass: 'building', name: '404', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4646313, 39.9974493], [116.4647484, 39.9974516], [116.4647574, 39.9973541], [116.4648151, 39.9973546], [116.4648148, 39.9974142], [116.4650669, 39.9974167], [116.4650636, 39.997479], [116.4651835, 39.9974803], [116.4651946, 39.9973476], [116.4650975, 39.9972614], [116.4647026, 39.9972432], [116.4646408, 39.9972716], [116.4646313, 39.9974493]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303410', code: 1500, fclass: 'building', name: '411', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4661561, 39.9962221], [116.4662866, 39.9963343], [116.4664329, 39.9962345], [116.4665621, 39.9963456], [116.4667011, 39.9962508], [116.4665692, 39.9961373], [116.4666931, 39.9960528], [116.4665655, 39.995943], [116.4664336, 39.9960329], [116.4663094, 39.9959261], [116.4661732, 39.9960191], [116.4662972, 39.9961258], [116.4661561, 39.9962221]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303411', code: 1500, fclass: 'building', name: '424', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4623192, 39.9938457], [116.4624496, 39.993958], [116.462596, 39.9938581], [116.4627251, 39.9939692], [116.4628641, 39.9938744], [116.4627322, 39.9937609], [116.4628561, 39.9936764], [116.4627285, 39.9935666], [116.4625966, 39.9936566], [116.4624724, 39.9935498], [116.4623362, 39.9936427], [116.4624603, 39.9937495], [116.4623192, 39.9938457]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303412', code: 1500, fclass: 'building', name: '427', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4639415, 39.994051], [116.464072, 39.9941632], [116.4642183, 39.9940634], [116.4643475, 39.9941745], [116.4644864, 39.9940797], [116.4643545, 39.9939662], [116.4644784, 39.9938816], [116.4643508, 39.9937718], [116.4642189, 39.9938618], [116.4640948, 39.993755], [116.4639585, 39.993848], [116.4640826, 39.9939547], [116.4639415, 39.994051]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '948303413', code: 1500, fclass: 'building', name: '406', type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4634743, 39.9963925], [116.4635913, 39.9963948], [116.4636003, 39.9962973], [116.463658, 39.9962978], [116.4636578, 39.9963573], [116.4639098, 39.9963598], [116.4639065, 39.9964221], [116.4640264, 39.9964234], [116.4640375, 39.9962907], [116.4639404, 39.9962045], [116.4635455, 39.9961863], [116.4634837, 39.9962147], [116.4634743, 39.9963925]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '949051583', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.474767, 39.997416], [116.4748899, 39.9976311], [116.4751656, 39.9975385], [116.4750426, 39.9973235], [116.474767, 39.997416]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '949051584', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4735521, 39.9976752], [116.4735983, 39.9978141], [116.4748231, 39.9975752], [116.474777, 39.9974363], [116.4735521, 39.9976752]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '949052750', code: 1500, fclass: 'building', name: '新城幼儿园', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4647734, 39.9965714], [116.4652838, 39.9970054], [116.4653215, 39.9969793], [116.4654003, 39.9970463], [116.4654459, 39.9970148], [116.4655313, 39.9970874], [116.465685, 39.9969813], [116.4655712, 39.9968846], [116.4655171, 39.9969219], [116.4653658, 39.9967932], [116.465411, 39.9967621], [116.465332, 39.9966949], [116.4651938, 39.9967903], [116.4649239, 39.9965608], [116.4649952, 39.9965116], [116.4649347, 39.9964601], [116.4647734, 39.9965714]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '949052751', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.465342, 39.9966449], [116.4654989, 39.9967447], [116.4656463, 39.996657], [116.4659177, 39.9966462], [116.4659177, 39.9965621], [116.4653594, 39.9965477], [116.465342, 39.9966449]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '951842773', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4711216, 39.993278], [116.4712766, 39.9934158], [116.4715345, 39.9932563], [116.4714284, 39.9931644], [116.4712243, 39.9932029], [116.4711216, 39.993278]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '951842776', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4715246, 39.9936433], [116.4718233, 39.9937837], [116.4719583, 39.9936342], [116.4716621, 39.993495], [116.4715246, 39.9936433]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '951974055', code: 1500, fclass: 'building', name: '关帝庙', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4476782, 40.0019832], [116.4479758, 40.0020415], [116.4480599, 40.0018167], [116.4477614, 40.0017499], [116.4476782, 40.0019832]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '951993631', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4814566, 40.0009169], [116.4815952, 40.0009501], [116.4817459, 40.0009076], [116.4818464, 40.0008479], [116.4818949, 40.0007909], [116.4821201, 40.0007258], [116.482257, 40.0006289], [116.4824077, 40.0005148], [116.4821842, 40.0003582], [116.4818239, 40.0005679], [116.4814566, 40.0009169]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332484', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4593065, 39.9892762], [116.459611, 39.989485], [116.4597279, 39.9893935], [116.4594186, 39.9891772], [116.4593065, 39.9892762]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332485', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.459201, 39.9903453], [116.4592019, 39.9904763], [116.4598165, 39.9904738], [116.4598155, 39.9903427], [116.459201, 39.9903453]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332486', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4597589, 39.9923038], [116.45977, 39.9924346], [116.4603832, 39.9924039], [116.4603721, 39.9922732], [116.4597589, 39.9923038]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332487', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4599812, 39.9912898], [116.4599825, 39.9914209], [116.460597, 39.9914173], [116.4605957, 39.9912862], [116.4599812, 39.9912898]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332488', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4589291, 39.9903697], [116.4590989, 39.9903858], [116.4591165, 39.9900472], [116.4589513, 39.9900311], [116.4589291, 39.9903697]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332489', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4587647, 39.9893463], [116.458886, 39.9894387], [116.4593193, 39.9891047], [116.4591979, 39.9890123], [116.4587647, 39.9893463]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332490', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4597019, 39.9896019], [116.4599916, 39.9898227], [116.4601146, 39.9897361], [116.4598206, 39.9895076], [116.4597019, 39.9896019]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332491', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4602695, 39.9908484], [116.4602728, 39.9909795], [116.4606814, 39.9909785], [116.4606884, 39.9908506], [116.4602695, 39.9908484]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332492', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4609235, 39.9913514], [116.4610944, 39.9913576], [116.4611236, 39.9908873], [116.4609527, 39.9908811], [116.4609235, 39.9913514]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332493', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4600034, 39.9906052], [116.4601118, 39.9907065], [116.4605871, 39.990408], [116.4604786, 39.9903066], [116.4600034, 39.9906052]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332494', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.459067, 39.9898702], [116.4591755, 39.9899716], [116.4596507, 39.989673], [116.4595422, 39.9895717], [116.459067, 39.9898702]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332495', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4603007, 39.9921918], [116.4604584, 39.9922426], [116.4606224, 39.9919558], [116.4604725, 39.9918992], [116.4603007, 39.9921918]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332496', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4606013, 39.9904841], [116.4608805, 39.9907127], [116.4610076, 39.9906296], [116.4607245, 39.9903931], [116.4606013, 39.9904841]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332497', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4597404, 39.9917829], [116.4597468, 39.9919139], [116.4603609, 39.9918966], [116.4603546, 39.9917656], [116.4597404, 39.9917829]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332498', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4605557, 39.9917922], [116.4607008, 39.9918616], [116.4610264, 39.9914623], [116.4608813, 39.9913929], [116.4605557, 39.9917922]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332499', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4596892, 39.9901728], [116.4597977, 39.9902741], [116.4602729, 39.9899756], [116.4601645, 39.9898742], [116.4596892, 39.9901728]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952332501', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4586196, 39.9899867], [116.458787, 39.990014], [116.458915, 39.9895535], [116.4587477, 39.9895262], [116.4586196, 39.9899867]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952333080', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.464176, 39.9871371], [116.4642812, 39.9872413], [116.4647307, 39.9869567], [116.4647637, 39.986808], [116.4643979, 39.9864975], [116.4642867, 39.9865633], [116.4646172, 39.986857], [116.464176, 39.9871371]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952333081', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4637026, 39.986817], [116.4640787, 39.9871562], [116.4642494, 39.9870469], [116.4638813, 39.9867117], [116.4637026, 39.986817]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952334333', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4735947, 39.9995371], [116.4736529, 39.9996153], [116.4738402, 39.9995345], [116.473921, 39.9995371], [116.4740352, 39.9996011], [116.4741126, 39.9995206], [116.4739955, 39.9994606], [116.4740549, 39.9993712], [116.4739698, 39.999358], [116.4739436, 39.9993099], [116.473748, 39.9993287], [116.4737114, 39.9993731], [116.4736386, 39.9994106], [116.4736926, 39.9994887], [116.4735947, 39.9995371]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952334530', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4744464, 39.9994514], [116.4745046, 39.9995295], [116.4746919, 39.9994488], [116.4747727, 39.9994514], [116.4748869, 39.9995154], [116.4749643, 39.9994349], [116.4748472, 39.9993749], [116.4749066, 39.9992854], [116.4748215, 39.9992723], [116.4747953, 39.9992242], [116.4745997, 39.999243], [116.4745631, 39.9992874], [116.4744903, 39.9993249], [116.4745443, 39.999403], [116.4744464, 39.9994514]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952334531', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4734554, 40.0004025], [116.4735135, 40.0004806], [116.4737008, 40.0003999], [116.4737817, 40.0004025], [116.4738958, 40.0004665], [116.4739326, 40.0004282], [116.4739732, 40.0003859], [116.4738561, 40.0003259], [116.4739155, 40.0002365], [116.4738305, 40.0002234], [116.4738042, 40.0001752], [116.4736086, 40.0001941], [116.473572, 40.0002384], [116.4734992, 40.0002759], [116.4735532, 40.000354], [116.4734554, 40.0004025]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952334532', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4752954, 40.0002385], [116.4753535, 40.0003166], [116.4755408, 40.0002359], [116.4756217, 40.0002385], [116.4757358, 40.0003025], [116.4758133, 40.0002219], [116.4756961, 40.0001619], [116.4757556, 40.0000725], [116.4756705, 40.0000594], [116.4756443, 40.0000113], [116.4754487, 40.0000301], [116.4754121, 40.0000745], [116.4753392, 40.000112], [116.4753933, 40.0001901], [116.4752954, 40.0002385]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952334533', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.474741, 40.0003381], [116.4747991, 40.0004162], [116.4749864, 40.0003355], [116.4750673, 40.0003381], [116.4751814, 40.0004021], [116.4752589, 40.0003216], [116.4751417, 40.0002616], [116.4752012, 40.0001721], [116.4751161, 40.000159], [116.4750899, 40.0001109], [116.4748943, 40.0001297], [116.4748577, 40.0001741], [116.4747848, 40.0002116], [116.4748389, 40.0002897], [116.474741, 40.0003381]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952334534', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4739979, 40.0003815], [116.474056, 40.0004596], [116.4742433, 40.0003789], [116.4743242, 40.0003815], [116.4744383, 40.0004455], [116.4745157, 40.0003649], [116.4743986, 40.000305], [116.474458, 40.0002155], [116.474373, 40.0002024], [116.4743467, 40.0001543], [116.4741511, 40.0001731], [116.4741145, 40.0002175], [116.4740417, 40.000255], [116.4740958, 40.0003331], [116.4739979, 40.0003815]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952820150', code: 1500, fclass: 'building', name: '303号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4686885, 39.9935209], [116.4688092, 39.993624], [116.4693101, 39.9933097], [116.4696879, 39.9936373], [116.4698245, 39.9935601], [116.469436, 39.9931772], [116.4692747, 39.9931523], [116.4686885, 39.9935209]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952820151', code: 1500, fclass: 'building', name: '302号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4696079, 39.9942723], [116.4697285, 39.9943753], [116.4702294, 39.9940611], [116.4706649, 39.9944339], [116.4708002, 39.9943556], [116.4703302, 39.9939248], [116.4701866, 39.9938922], [116.4696079, 39.9942723]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '952820152', code: 1500, fclass: 'building', name: '306号楼', type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4676352, 39.9929081], [116.4677533, 39.9930189], [116.4684508, 39.9925796], [116.4688286, 39.9929071], [116.4689652, 39.9928299], [116.4685822, 39.9924633], [116.4683797, 39.9924302], [116.4676352, 39.9929081]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '957702450', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4684772, 39.990766], [116.4688035, 39.9910594], [116.4691303, 39.990846], [116.468804, 39.9905527], [116.4684772, 39.990766]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '957702452', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4687021, 39.9875524], [116.4687769, 39.987628], [116.468823, 39.9876001], [116.4690078, 39.9877604], [116.4691468, 39.9876766], [116.4690689, 39.9876045], [116.4690235, 39.9876329], [116.4688445, 39.9874686], [116.4687021, 39.9875524]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '958030983', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4698269, 39.9904406], [116.4699979, 39.9905946], [116.4709179, 39.989995], [116.4702067, 39.9893544], [116.4700113, 39.9894818], [116.4705516, 39.9899683], [116.4698269, 39.9904406]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '958030984', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4692001, 39.9899127], [116.4695433, 39.990228], [116.4700562, 39.9899004], [116.469713, 39.989585], [116.4692001, 39.9899127]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '958040951', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4629127, 39.9898187], [116.4633026, 39.9901699], [116.46326, 39.9902016], [116.4633315, 39.9902617], [116.4634703, 39.9901707], [116.4630117, 39.9897516], [116.4629127, 39.9898187]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '958040952', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4637924, 39.9902507], [116.4650598, 39.9903395], [116.4650802, 39.9902062], [116.4638058, 39.9901342], [116.4637924, 39.9902507]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '958040953', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4634971, 39.9904285], [116.4637479, 39.9906361], [116.4646505, 39.9906767], [116.465006, 39.9904362], [116.4634971, 39.9904285]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '958040954', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4631826, 39.9896078], [116.4636553, 39.9900338], [116.463747, 39.9899835], [116.4632794, 39.9895405], [116.4631826, 39.9896078]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '958040955', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4656768, 39.989614], [116.4656917, 39.989717], [116.4658499, 39.9896934], [116.4659518, 39.9897787], [116.4660592, 39.9897191], [116.4659585, 39.9896348], [116.4660109, 39.9895157], [116.4658765, 39.9894776], [116.4658137, 39.9895968], [116.4656768, 39.989614]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '958040956', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4661664, 39.9892622], [116.4661813, 39.9893652], [116.4663395, 39.9893416], [116.4664414, 39.9894269], [116.4665488, 39.9893673], [116.4664481, 39.989283], [116.4665005, 39.9891639], [116.4663661, 39.9891258], [116.4663033, 39.989245], [116.4661664, 39.9892622]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '958040957', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4635122, 39.9894145], [116.463985, 39.9898405], [116.4640766, 39.9897902], [116.463609, 39.9893472], [116.4635122, 39.9894145]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969549827', code: 1500, fclass: 'building', name: '国际竹藤大厦', type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4733853, 39.9877147], [116.473446, 39.9877509], [116.4735761, 39.9877867], [116.4737577, 39.987809], [116.4738779, 39.9878149], [116.4738867, 39.9878614], [116.4739906, 39.9878484], [116.4740741, 39.987818], [116.4741109, 39.9877925], [116.4741231, 39.9877701], [116.4741255, 39.9877509], [116.4741167, 39.98772], [116.4740904, 39.9876892], [116.4740496, 39.9876655], [116.4739807, 39.9876391], [116.4739217, 39.9876225], [116.4739007, 39.9876601], [116.4738528, 39.9876521], [116.4737787, 39.9876391], [116.4736713, 39.9876346], [116.4736362, 39.9876368], [116.4735551, 39.9876418], [116.4734617, 39.9876722], [116.4733853, 39.9877147]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969549828', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4739217, 39.9876225], [116.4739807, 39.9876391], [116.4740496, 39.9876655], [116.4740904, 39.9876892], [116.4741167, 39.98772], [116.4741301, 39.9876131], [116.4739299, 39.9876015], [116.4739217, 39.9876225]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969549829', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4739299, 39.9876015], [116.4741301, 39.9876131], [116.4741167, 39.98772], [116.4741255, 39.9877509], [116.4741231, 39.9877701], [116.4743321, 39.9876319], [116.474105, 39.9874347], [116.4740437, 39.9874785], [116.473983, 39.9875349], [116.4739299, 39.9876015]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969549830', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4731672, 39.9875266], [116.4733853, 39.9877147], [116.4734617, 39.9876722], [116.4735551, 39.9876418], [116.4736362, 39.9876368], [116.4734101, 39.987402], [116.4733034, 39.9874581], [116.4732831, 39.9874409], [116.4732302, 39.9874698], [116.4732363, 39.987483], [116.4731672, 39.9875266]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969549831', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.472509, 39.9868332], [116.4727618, 39.9868424], [116.4727904, 39.9863773], [116.4725377, 39.9863682], [116.472509, 39.9868332]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969549832', code: 1500, fclass: 'building', name: null, type: 'commercial'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4730542, 39.9867616], [116.4734541, 39.9871074], [116.4736276, 39.9869962], [116.4732294, 39.9866382], [116.4730542, 39.9867616]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550789', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4618148, 39.9769412], [116.4619375, 39.9769483], [116.4619553, 39.9768001], [116.4618316, 39.97679], [116.4618148, 39.9769412]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550790', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4619375, 39.9769483], [116.4622369, 39.9769694], [116.4622547, 39.9768212], [116.4619553, 39.9768001], [116.4619375, 39.9769483]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550791', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4621008, 39.9767826], [116.4624996, 39.9768042], [116.4625134, 39.976656], [116.4621145, 39.9766343], [116.4621008, 39.9767826]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550792', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4623528, 39.976611], [116.4627488, 39.9766356], [116.4627641, 39.9764904], [116.4623682, 39.9764658], [116.4623528, 39.976611]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550793', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4626136, 39.9764486], [116.4630082, 39.9764696], [116.463014, 39.9764144], [116.4630233, 39.9763245], [116.4626251, 39.9762991], [116.4626136, 39.9764486]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550794', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4629786, 39.9762797], [116.4630262, 39.9763232], [116.4631029, 39.9763933], [116.4636112, 39.9760617], [116.4635102, 39.9759682], [116.4634882, 39.9759478], [116.4629786, 39.9762797]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550795', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.463014, 39.9764144], [116.4630596, 39.976413], [116.4631029, 39.9763933], [116.4630262, 39.9763232], [116.4630233, 39.9763245], [116.463014, 39.9764144]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550796', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4634882, 39.9759478], [116.4635102, 39.9759682], [116.4637458, 39.9759791], [116.4637608, 39.975816], [116.4635418, 39.9758095], [116.4634989, 39.9758082], [116.4634882, 39.9759478]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550797', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4633821, 39.9757994], [116.4634989, 39.9758082], [116.4635418, 39.9758095], [116.4635466, 39.9757308], [116.4635494, 39.9756855], [116.4634807, 39.9756824], [116.4633941, 39.9756784], [116.4633821, 39.9757994]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550798', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4634807, 39.9756824], [116.4635494, 39.9756855], [116.4635466, 39.9757308], [116.4636237, 39.975734], [116.4636339, 39.9756113], [116.4634865, 39.9756025], [116.4634844, 39.975631], [116.4634807, 39.9756824]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550799', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.463379, 39.9756777], [116.4633941, 39.9756784], [116.4634807, 39.9756824], [116.4634844, 39.975631], [116.4633826, 39.9756262], [116.463379, 39.9756777]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550800', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4626878, 39.9762344], [116.4629526, 39.9762478], [116.4629591, 39.9761715], [116.4628507, 39.976166], [116.4628542, 39.9761255], [116.4626979, 39.9761176], [116.4626878, 39.9762344]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '969550801', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4628641, 39.9761307], [116.4630211, 39.9761425], [116.4630264, 39.9760768], [116.4630486, 39.9760645], [116.4631755, 39.9760662], [116.4631791, 39.9759781], [116.4632043, 39.9759649], [116.4632866, 39.9759666], [116.4632981, 39.9758449], [116.4631415, 39.9758405], [116.4631348, 39.9758988], [116.4631025, 39.9759198], [116.4629928, 39.975912], [116.4629835, 39.9760164], [116.4628809, 39.9760123], [116.4628641, 39.9761307]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '977732521', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4896184, 39.9978226], [116.4897241, 39.9978286], [116.4897364, 39.9977013], [116.4896307, 39.9976953], [116.4896184, 39.9978226]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '993479701', code: 1500, fclass: 'building', name: null, type: 'apartments'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4669179, 39.9856206], [116.4669865, 39.9856771], [116.4670687, 39.9856302], [116.4670826, 39.9855015], [116.4672391, 39.9855115], [116.4673217, 39.9854567], [116.4672523, 39.9853901], [116.4670194, 39.9853753], [116.466938, 39.9854348], [116.4669179, 39.9856206]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '996260871', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4535277, 39.986117], [116.4542395, 39.9862502], [116.4541598, 39.9864856], [116.4536372, 39.9863973], [116.4535916, 39.9865176], [116.4542706, 39.9866329], [116.4544571, 39.9861546], [116.4535661, 39.9859885], [116.4535277, 39.986117]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '996260872', code: 1500, fclass: 'building', name: null, type: null
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4524784, 39.9863542], [116.4533751, 39.9865012], [116.4534088, 39.9863719], [116.4526924, 39.986254], [116.4527635, 39.986017], [116.4532892, 39.986094], [116.4533303, 39.9859728], [116.4526474, 39.9858721], [116.4524784, 39.9863542]]]] }
    },
    {
      type: 'Feature',
      properties: {
        osm_id: '996319871', code: 1500, fclass: 'building', name: '宜家家居', type: 'retail'
      },
      geometry: { type: 'MultiPolygon', coordinates: [[[[116.4563122, 39.9760855], [116.4571405, 39.9769438], [116.4581211, 39.9769159], [116.4584859, 39.9766692], [116.4571598, 39.97551], [116.4563122, 39.9760855]]]] }
    }
  ]
}

export default buildings
