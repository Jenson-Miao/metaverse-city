/* eslint-disable import/prefer-default-export */
const roads = {
  type: 'FeatureCollection',
  name: 'wangjing_roads_primary_multipolyline',
  crs: {
    type: 'name',
    properties: {
      name: 'urn:ogc:def:crs:OGC:1.3:CRS84'
    }
  },
  features: [{
    type: 'Feature',
    properties: {
      osm_id: '5169171',
      code: 5115,
      fclass: 'tertiary',
      name: '将台路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5027255, 39.9714307],
          [116.5013275, 39.9708941],
          [116.5010843, 39.9706734],
          [116.5006282, 39.9704824],
          [116.5003176, 39.9703832],
          [116.5001824, 39.9703458],
          [116.5000811, 39.9703261],
          [116.4999609, 39.9703193],
          [116.499751, 39.9703325],
          [116.497471, 39.9705526],
          [116.4971601, 39.9705745],
          [116.4970377, 39.970586],
          [116.4968248, 39.9705994],
          [116.4962337, 39.9706367],
          [116.4953278, 39.9706433],
          [116.4936973, 39.9706451],
          [116.4934797, 39.9706528],
          [116.49144, 39.9706497],
          [116.4885016, 39.9706453],
          [116.4870791, 39.9706609],
          [116.4868645, 39.9706599],
          [116.484969, 39.9706686],
          [116.4848335, 39.9706852],
          [116.4846868, 39.9707337],
          [116.4843877, 39.9708439],
          [116.4841203, 39.9709783],
          [116.4838136, 39.9711655]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '16266618',
      code: 5114,
      fclass: 'secondary',
      name: '曙光西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4408924, 39.9615164],
          [116.4410596, 39.9616713],
          [116.4411083, 39.9617163],
          [116.4426989, 39.9631893],
          [116.44291, 39.9633784],
          [116.4429354, 39.9634012],
          [116.4430532, 39.9635067],
          [116.4458091, 39.9659759],
          [116.4469717, 39.9670176],
          [116.4470295, 39.9670757],
          [116.4471056, 39.9671352]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '16266662',
      code: 5114,
      fclass: 'secondary',
      name: '广泽路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4719588, 40.0101406],
          [116.472025, 40.0089245],
          [116.4720493, 40.0084782],
          [116.4720775, 40.0073859],
          [116.4721652, 40.0063629],
          [116.4722235, 40.0047116],
          [116.4722508, 40.0038321],
          [116.4722579, 40.0035927]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '22760069',
      code: 5114,
      fclass: 'secondary',
      name: '北土城东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4292487, 39.9754814],
          [116.4264059, 39.9757592],
          [116.4256562, 39.9758302],
          [116.4252803, 39.9758401],
          [116.4248999, 39.9758399]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '22771829',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5095499, 39.9802185],
          [116.5043638, 39.9868656],
          [116.5028189, 39.9887069],
          [116.5016646, 39.9900058],
          [116.4983127, 39.9936059],
          [116.4965858, 39.9952503],
          [116.4950571, 39.9967955]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '23116001',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 3,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4537755, 40.0140369],
          [116.4530736, 40.0140874],
          [116.4524772, 40.0141977],
          [116.4519515, 40.0143127]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '23383764',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.461859, 39.9717303],
          [116.46175, 39.971686],
          [116.4615906, 39.9716747],
          [116.4614634, 39.9717085],
          [116.4605581, 39.972268],
          [116.4577729, 39.9740219],
          [116.4569024, 39.9745973]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '23720836',
      code: 5115,
      fclass: 'tertiary',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4640404, 39.9677312],
          [116.4653737, 39.9679217],
          [116.4656743, 39.9679156],
          [116.4659357, 39.9678263],
          [116.4678577, 39.9666178],
          [116.4681426, 39.9664473],
          [116.4682625, 39.9663756],
          [116.4683989, 39.9663595],
          [116.4689311, 39.9660131],
          [116.4704434, 39.9650365],
          [116.4714321, 39.964398],
          [116.4727478, 39.9635089],
          [116.4747197, 39.9622152],
          [116.4747921, 39.9621676],
          [116.4754569, 39.9617166],
          [116.4769041, 39.9607257],
          [116.4771413, 39.9605587]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '23892295',
      code: 5115,
      fclass: 'tertiary',
      name: '小营北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4312904, 39.996968],
          [116.4311797, 39.9969715],
          [116.4291397, 39.9969838],
          [116.4271511, 39.9969958],
          [116.4253165, 39.9970069],
          [116.4240729, 39.9970661],
          [116.421914, 39.9972512],
          [116.4199773, 39.997396],
          [116.4190892, 39.9974601],
          [116.4183103, 39.9975016]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '23892322',
      code: 5115,
      fclass: 'tertiary',
      name: '西坝河路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4420425, 39.9815238],
          [116.4416803, 39.9813883],
          [116.4410057, 39.9810761],
          [116.4388608, 39.9800657],
          [116.4387246, 39.979997],
          [116.4372261, 39.9792411],
          [116.4371731, 39.9788836],
          [116.4368413, 39.9766257],
          [116.4366104, 39.975089],
          [116.4365512, 39.9747908],
          [116.4366659, 39.9746143],
          [116.4366642, 39.9744501],
          [116.4366566, 39.9743084],
          [116.4365737, 39.9715306],
          [116.4365943, 39.9713531],
          [116.436796, 39.9703302],
          [116.4367872, 39.970208],
          [116.4367156, 39.9701005],
          [116.4365869, 39.9699918],
          [116.4352638, 39.9692041],
          [116.4347288, 39.9689016],
          [116.4345007, 39.9687335],
          [116.4343296, 39.9685498],
          [116.4330283, 39.9668743],
          [116.4329331, 39.9667528]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '23892323',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫北街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4499524, 39.9694171],
          [116.4499457, 39.9694221],
          [116.4497854, 39.9695269],
          [116.4496052, 39.9696584],
          [116.4471862, 39.9714579],
          [116.4463775, 39.9720598],
          [116.4460395, 39.9723114],
          [116.4439356, 39.9738773],
          [116.443855, 39.9739358],
          [116.4437689, 39.9740096],
          [116.4437033, 39.9740939],
          [116.4426522, 39.9754453],
          [116.4408836, 39.9775966],
          [116.4388608, 39.9800657],
          [116.4376636, 39.9815474],
          [116.437433, 39.9817646],
          [116.4369863, 39.9821854],
          [116.4363404, 39.9823824],
          [116.4358491, 39.9824045],
          [116.435164, 39.9824186],
          [116.4349069, 39.9825079],
          [116.4346908, 39.9827054]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '23892332',
      code: 5115,
      fclass: 'tertiary',
      name: '南湖中园二条',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4569437, 39.9887742],
          [116.4568597, 39.988726],
          [116.4560266, 39.9882662],
          [116.4558659, 39.9882165],
          [116.4557222, 39.9881603],
          [116.4555776, 39.9881247],
          [116.4541891, 39.9879257],
          [116.4509897, 39.9874797]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '24806468',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽东二路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4777715, 40.0115642],
          [116.4771889, 40.0106473],
          [116.4771513, 40.0105881],
          [116.4764365, 40.0093222],
          [116.4763596, 40.0091861]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '26320965',
      code: 5115,
      fclass: 'tertiary',
      name: '万红西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4829703, 39.9790645],
          [116.482756, 39.9790983],
          [116.4825906, 39.9791935],
          [116.4820688, 39.9795438],
          [116.4819494, 39.9796232],
          [116.4804923, 39.9806142],
          [116.479844, 39.9810271],
          [116.4778031, 39.9823486],
          [116.4769939, 39.9829318],
          [116.4767855, 39.983082]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '29257863',
      code: 5114,
      fclass: 'secondary',
      name: '阜通东大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4766441, 39.9912067],
          [116.476457, 39.9911318],
          [116.4748145, 39.9896444],
          [116.4736107, 39.9885543],
          [116.471585, 39.9867371],
          [116.4714912, 39.9866651],
          [116.4712994, 39.9865179],
          [116.4707218, 39.9860746],
          [116.4703239, 39.9858133],
          [116.4699845, 39.985557],
          [116.4693803, 39.9851028],
          [116.4688056, 39.9846861],
          [116.4683474, 39.9843474],
          [116.4681574, 39.9841981],
          [116.4679567, 39.9840358],
          [116.4672778, 39.9834146],
          [116.4663904, 39.9826074],
          [116.4663288, 39.9825514],
          [116.4647423, 39.9811081],
          [116.4615682, 39.9782207],
          [116.4596874, 39.9765097],
          [116.4581508, 39.975133]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '29258169',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4607322, 39.9715889],
          [116.4614718, 39.9711045]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '29258170',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4608786, 39.9717355],
          [116.4580063, 39.9735835],
          [116.457048, 39.9741843],
          [116.4560813, 39.9748671],
          [116.4549013, 39.9756327]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '29258263',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4663919, 39.968108],
          [116.4640636, 39.9696444]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '29258265',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4661243, 39.9680277],
          [116.4673372, 39.9672614],
          [116.4679631, 39.9668485]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30186348',
      code: 5115,
      fclass: 'tertiary',
      name: '望京西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4509705, 39.9828125],
          [116.4506933, 39.9823408],
          [116.4504826, 39.9819271],
          [116.4501747, 39.9814289],
          [116.4496026, 39.9806459],
          [116.4489672, 39.9796808]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30186429',
      code: 5115,
      fclass: 'tertiary',
      name: '京承高速辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.430066, 39.968203],
          [116.4297608, 39.9683494],
          [116.4296526, 39.9684013],
          [116.4288962, 39.968511],
          [116.4277889, 39.9685375],
          [116.4276565, 39.9685464],
          [116.427591, 39.9685747],
          [116.4275373, 39.9686101],
          [116.4275069, 39.9686679],
          [116.4275036, 39.9687437],
          [116.427628, 39.9704745],
          [116.4276667, 39.9713144],
          [116.4278085, 39.9715451],
          [116.4281325, 39.9720725],
          [116.4282578, 39.9723232],
          [116.4282917, 39.9725328],
          [116.428312, 39.9728029],
          [116.4283369, 39.9730245],
          [116.4283888, 39.9732167],
          [116.4287989, 39.9738613],
          [116.4289607, 39.9741351],
          [116.4291255, 39.9744138],
          [116.4295171, 39.9751008]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30879674',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.48384, 39.9653474],
          [116.4838974, 39.9657165],
          [116.4839108, 39.965803]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30879880',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4838929, 39.9662872],
          [116.4838705, 39.967227],
          [116.4838717, 39.9672456],
          [116.48383, 39.9699461],
          [116.4838136, 39.9711655]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30879928',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.483807, 39.965803],
          [116.4838138, 39.965709],
          [116.48384, 39.9653474]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30879931',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4837902, 39.9662793],
          [116.483807, 39.965803]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30879935',
      code: 5121,
      fclass: 'unclassified',
      name: '方园南街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4758138, 39.9661097],
          [116.475768, 39.9660343],
          [116.4757326, 39.9659856],
          [116.4756147, 39.9658861],
          [116.4754173, 39.9657195],
          [116.4752983, 39.9656191],
          [116.4732108, 39.9638577]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30879941',
      code: 5121,
      fclass: 'unclassified',
      name: '方园南街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4758138, 39.9661097],
          [116.4760264, 39.9665122]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30879951',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'T',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.470139, 39.9679444],
          [116.470607, 39.9683487]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30879952',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.470607, 39.9683487],
          [116.4706673, 39.9684042],
          [116.471145, 39.9688444],
          [116.4731405, 39.9706832],
          [116.4761261, 39.9733845],
          [116.4762105, 39.9734629],
          [116.4779186, 39.9750498]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30880238',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5165202, 39.9712232],
          [116.5160735, 39.9718512]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30880240',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5190111, 39.9671953],
          [116.5182557, 39.9685307],
          [116.5168567, 39.9707475],
          [116.5165202, 39.9712232]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30880252',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5193848, 39.966546],
          [116.5190111, 39.9671953]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30880254',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5231371, 39.9578652],
          [116.5217046, 39.9613493],
          [116.5216285, 39.9615284],
          [116.5203815, 39.9643797],
          [116.5193848, 39.966546]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30880271',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5160735, 39.9718512],
          [116.515771, 39.9723097],
          [116.5154062, 39.972826],
          [116.514961, 39.9734049],
          [116.5141101, 39.9745033],
          [116.5129171, 39.9760095]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '30880273',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5129171, 39.9760095],
          [116.5095499, 39.9802185]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '32920077',
      code: 5115,
      fclass: 'tertiary',
      name: '河荫西路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4505279, 40.0012858],
          [116.4514872, 40.0017226],
          [116.4516287, 40.0017611],
          [116.4518196, 40.0018435],
          [116.4519712, 40.001966],
          [116.4538104, 40.0028984],
          [116.454711, 40.0033198],
          [116.4553263, 40.0035014],
          [116.455927, 40.0035863],
          [116.456811, 40.0035846]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '32920090',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽中二路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4671555, 40.0040115],
          [116.4671241, 40.0044298]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '32920164',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽中一路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4641551, 40.0073318],
          [116.4640863, 40.0101801],
          [116.464082, 40.0103029],
          [116.4639421, 40.0128528]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '32920675',
      code: 5115,
      fclass: 'tertiary',
      name: '南湖北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.456811, 40.0035846],
          [116.4567763, 40.0027076],
          [116.4567188, 40.001257],
          [116.4565607, 39.999779],
          [116.4565554, 39.9996741]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '32920681',
      code: 5115,
      fclass: 'tertiary',
      name: '河荫西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4614574, 40.0036619],
          [116.45697, 40.0036271],
          [116.456811, 40.0035846]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '32920748',
      code: 5115,
      fclass: 'tertiary',
      name: '湖光北街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4615729, 40.0003595],
          [116.4600314, 40.0001315],
          [116.4579359, 39.9998229],
          [116.4567142, 39.9996811],
          [116.4565554, 39.9996741]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33407971',
      code: 5122,
      fclass: 'residential',
      name: '万红路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.502478, 39.9717995],
          [116.5024782, 39.9729269],
          [116.5021586, 39.9738992],
          [116.5017467, 39.9751033],
          [116.5020624, 39.9772178],
          [116.5022893, 39.9788012],
          [116.5024391, 39.9792908],
          [116.5003411, 39.9792581],
          [116.4977506, 39.9792176]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33545269',
      code: 5115,
      fclass: 'tertiary',
      name: '将台路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4720439, 39.9788255],
          [116.4724405, 39.978566],
          [116.4729977, 39.9781938],
          [116.4733998, 39.9779392],
          [116.47353, 39.9779128]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33656118',
      code: 5141,
      fclass: 'service',
      name: '798西路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4852673, 39.9849792],
          [116.4853167, 39.9830194]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33656151',
      code: 5115,
      fclass: 'tertiary',
      name: '将台路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5036758, 39.9718307],
          [116.5045329, 39.9722927],
          [116.5048122, 39.9723684],
          [116.5049278, 39.9723864],
          [116.5051265, 39.9724173],
          [116.505565, 39.9726343],
          [116.506505, 39.9731862],
          [116.5075128, 39.9737866],
          [116.50873, 39.9745521],
          [116.5094601, 39.9750112],
          [116.5100432, 39.9753561],
          [116.5103652, 39.9755628],
          [116.5111852, 39.9760636],
          [116.5118812, 39.9764249],
          [116.5119565, 39.976464],
          [116.5120639, 39.9765197],
          [116.5121592, 39.9765501],
          [116.5124178, 39.9766324],
          [116.5127527, 39.9766863],
          [116.5130757, 39.9767286],
          [116.5138335, 39.9770612],
          [116.5145785, 39.9772446],
          [116.5153097, 39.9773038],
          [116.5158901, 39.9772601],
          [116.5162793, 39.9771912],
          [116.5170477, 39.9769718],
          [116.5183579, 39.9765293],
          [116.521181, 39.9756811],
          [116.5218648, 39.9754813],
          [116.5246119, 39.9748485],
          [116.5250943, 39.9747282],
          [116.5259799, 39.9745413],
          [116.5267607, 39.9742699],
          [116.5275398, 39.9739973],
          [116.5280548, 39.9738565],
          [116.5300478, 39.9739594]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33656211',
      code: 5141,
      fclass: 'service',
      name: '798东路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4933746, 39.9792046],
          [116.4933659, 39.9793876],
          [116.4933258, 39.9794514],
          [116.4932457, 39.9795104],
          [116.4931263, 39.9795587],
          [116.49308, 39.9810247],
          [116.4930543, 39.9819416],
          [116.4930461, 39.982235],
          [116.4930409, 39.9825584],
          [116.4930146, 39.9830918],
          [116.4929904, 39.985069],
          [116.4929328, 39.9866993]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33656216',
      code: 5141,
      fclass: 'service',
      name: 'Xiqing Lu',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4906299, 39.9812943],
          [116.4901873, 39.9812873],
          [116.4897138, 39.9812798],
          [116.4892341, 39.9812722],
          [116.4886985, 39.9812723],
          [116.4879589, 39.9812725],
          [116.487463, 39.9812726],
          [116.4872278, 39.9812722]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33869024',
      code: 5115,
      fclass: 'tertiary',
      name: '湖光中街',
      ref: null,
      oneway: 'B',
      maxspeed: 40,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4467866, 39.9947791],
          [116.4453042, 39.9945346],
          [116.4446656, 39.9943771],
          [116.4444796, 39.9943311],
          [116.4443825, 39.994307]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33869069',
      code: 5113,
      fclass: 'primary',
      name: '姜庄路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4311299, 39.9921969],
          [116.4321232, 39.9922208],
          [116.4325031, 39.9922596],
          [116.4329647, 39.9923393],
          [116.433415, 39.9924471],
          [116.4338963, 39.9925938],
          [116.4353252, 39.9930913],
          [116.4367591, 39.9935858],
          [116.4369238, 39.9936426],
          [116.4378571, 39.9939234],
          [116.4385543, 39.994049],
          [116.4392041, 39.9941643],
          [116.4399379, 39.994285]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33869095',
      code: 5115,
      fclass: 'tertiary',
      name: '湖光中街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4620058, 39.9959123],
          [116.4617563, 39.9959521],
          [116.4616446, 39.9959498],
          [116.4598449, 39.9959122],
          [116.4593838, 39.9959025],
          [116.4589649, 39.9958938],
          [116.4566581, 39.9958456],
          [116.4565421, 39.9958432],
          [116.4530726, 39.9957694],
          [116.4517341, 39.9957435],
          [116.4515205, 39.9957399],
          [116.4513302, 39.995737],
          [116.4511936, 39.9957342],
          [116.4491852, 39.995693],
          [116.4484397, 39.9956788],
          [116.4469819, 39.995651],
          [116.4459092, 39.9954645],
          [116.4443746, 39.9951899]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '33869235',
      code: 5122,
      fclass: 'residential',
      name: '屏翠西路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4610247, 40.0129531],
          [116.4598649, 40.0129486],
          [116.4565407, 40.0129349],
          [116.4563149, 40.0129346]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '34150615',
      code: 5115,
      fclass: 'tertiary',
      name: '鼎成路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4313108, 39.9880147],
          [116.4313077, 39.9880863],
          [116.4311299, 39.9921969],
          [116.4311258, 39.9922926]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '34150653',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4542951, 39.9761962],
          [116.4540345, 39.9763648],
          [116.4533952, 39.9767782],
          [116.4514985, 39.9780416],
          [116.4509714, 39.9783883],
          [116.4507021, 39.9785607],
          [116.4494967, 39.9793325],
          [116.4490608, 39.9796159]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '35028007',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽东街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.476371, 40.0073887],
          [116.4785377, 40.007422]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '35028008',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽东街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4814602, 40.0090216],
          [116.4786988, 40.0075103],
          [116.4785377, 40.007422]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '35028012',
      code: 5115,
      fclass: 'tertiary',
      name: '屏翠东路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4668206, 40.0130192],
          [116.469443, 40.0129312],
          [116.4716781, 40.0128562],
          [116.4720785, 40.0128738],
          [116.4729037, 40.0128251],
          [116.4730715, 40.0128056],
          [116.4736649, 40.0127365],
          [116.4749657, 40.0124897],
          [116.4753714, 40.0123516],
          [116.4777715, 40.0115642],
          [116.4798138, 40.0104493],
          [116.4814602, 40.0090216],
          [116.4823278, 40.0081583],
          [116.4825754, 40.0079073],
          [116.4837496, 40.006734],
          [116.4845764, 40.0058886]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '35028013',
      code: 5115,
      fclass: 'tertiary',
      name: '溪阳东路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4818147, 40.0041721],
          [116.4814825, 40.004075],
          [116.4810491, 40.0039172],
          [116.4799392, 40.0039473],
          [116.4764995, 40.0042896],
          [116.472327, 40.0047014],
          [116.4722235, 40.0047116],
          [116.4689168, 40.0050444],
          [116.4687212, 40.005065],
          [116.4670793, 40.0050264]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '39456572',
      code: 5122,
      fclass: 'residential',
      name: '南皋路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5164731, 40.0007092],
          [116.5169922, 40.0015988],
          [116.5171013, 40.0016725],
          [116.5172715, 40.0017293],
          [116.5184174, 40.0019429],
          [116.5199706, 40.0021867],
          [116.5216335, 40.0025539],
          [116.5228202, 40.0027982],
          [116.5234793, 40.0029641],
          [116.5237345, 40.0030861],
          [116.5239818, 40.0032495],
          [116.5240683, 40.0033551],
          [116.5241136, 40.0034692],
          [116.5240771, 40.0040217],
          [116.5239824, 40.0046191],
          [116.5239893, 40.0047403]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '42355924',
      code: 5115,
      fclass: 'tertiary',
      name: '文学馆路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.431048, 39.9821203],
          [116.4307138, 39.9821452],
          [116.4283717, 39.9821773],
          [116.4262819, 39.9821622],
          [116.4247394, 39.9821459],
          [116.4215106, 39.9821171],
          [116.4180529, 39.9820847]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '42746558',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4614068, 40.0138771],
          [116.4605748, 40.013872]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '42746559',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4605764, 40.0137302],
          [116.4614255, 40.01373]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '42746560',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 90,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4614255, 40.01373],
          [116.4643803, 40.0137663],
          [116.4664189, 40.0137748],
          [116.4684159, 40.0137572],
          [116.4692645, 40.0137722],
          [116.4713955, 40.0137438],
          [116.472532, 40.0136802],
          [116.4730794, 40.0136264],
          [116.4738605, 40.0135288],
          [116.4743004, 40.0134565],
          [116.4751843, 40.0133113],
          [116.4761038, 40.0130575],
          [116.4767017, 40.0128826],
          [116.4780827, 40.0123763],
          [116.4798834, 40.0114679],
          [116.4803632, 40.0111264],
          [116.4809082, 40.0107205],
          [116.4817325, 40.0100705],
          [116.4830534, 40.008823],
          [116.4843149, 40.0075349],
          [116.4863217, 40.0055069],
          [116.4874547, 40.0043268],
          [116.4888871, 40.0028284]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '42746575',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4891349, 40.0028077],
          [116.4882701, 40.0037563],
          [116.4869864, 40.0050874],
          [116.4852499, 40.0068781],
          [116.4835357, 40.0086359],
          [116.4821981, 40.0099616],
          [116.4808988, 40.0110329],
          [116.4802691, 40.0114626],
          [116.4791431, 40.0120427],
          [116.4782379, 40.0124586],
          [116.4773259, 40.0128516],
          [116.4768592, 40.0130095],
          [116.4764689, 40.0131415],
          [116.4754507, 40.0134057],
          [116.474332, 40.0136454],
          [116.4731265, 40.0138285],
          [116.4725664, 40.013873],
          [116.4713996, 40.0138967],
          [116.4687295, 40.0138937],
          [116.4663903, 40.0139185],
          [116.4642661, 40.0139073],
          [116.4614068, 40.0138771]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '42746576',
      code: 5111,
      fclass: 'motorway',
      name: '五元桥',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 2,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4950571, 39.9967955],
          [116.494777, 39.9970873],
          [116.4925789, 39.9992919],
          [116.4911115, 40.0008006]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '42746577',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4948914, 39.9966492],
          [116.4971939, 39.9943968],
          [116.4988176, 39.9927493],
          [116.5014413, 39.9900095],
          [116.5035861, 39.9875113],
          [116.5094097, 39.9800747]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '42746580',
      code: 5111,
      fclass: 'motorway',
      name: '五元桥',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 2,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4909988, 40.0006828],
          [116.4924541, 39.9991948],
          [116.4941696, 39.9974182],
          [116.4948914, 39.9966492]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '42746709',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5094097, 39.9800747],
          [116.5127873, 39.9759974]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51267740',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4507092, 40.0191423],
          [116.4495371, 40.0154296],
          [116.4487166, 40.0131906],
          [116.4473626, 40.0091312],
          [116.4470257, 40.0079978],
          [116.4467092, 40.0069334]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51269072',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4467092, 40.0069334],
          [116.4466037, 40.0066187]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51269073',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4466037, 40.0066187],
          [116.4461914, 40.0054037],
          [116.445914, 40.004569],
          [116.4447478, 40.0010358]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51269075',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4471258, 40.0066707],
          [116.4471475, 40.006736],
          [116.4472188, 40.006957]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51269167',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.44509, 40.0004414],
          [116.4453152, 40.0011325]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51269168',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4447478, 40.0010358],
          [116.4445142, 40.0003716]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51269169',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 120,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4445142, 40.0003716],
          [116.4434477, 39.9979798],
          [116.442868, 39.9968371],
          [116.4408768, 39.9937598],
          [116.439815, 39.9921436],
          [116.4387081, 39.9904489],
          [116.4369204, 39.9877122]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51269178',
      code: 5113,
      fclass: 'primary',
      name: '辛店路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.446434, 40.0068013],
          [116.4440669, 40.0067194],
          [116.4432227, 40.0066982],
          [116.4420448, 40.006669],
          [116.441199, 40.0066723],
          [116.4408168, 40.0066855],
          [116.4395089, 40.0067511],
          [116.4367667, 40.0068886],
          [116.4343557, 40.0070031],
          [116.4330795, 40.0070492],
          [116.4328361, 40.00706]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51269196',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4453152, 40.0011325],
          [116.4468969, 40.0059685],
          [116.4471258, 40.0066707]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51269764',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4472188, 40.006957],
          [116.4473921, 40.0073766],
          [116.4475228, 40.0077519],
          [116.4480231, 40.0091889],
          [116.4485376, 40.01074],
          [116.4513868, 40.0191288]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51333847',
      code: 5122,
      fclass: 'residential',
      name: '容瑞路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4561839, 40.0192504],
          [116.4561895, 40.0191645],
          [116.4562036, 40.0189459],
          [116.4562522, 40.0169553],
          [116.456265, 40.0159977],
          [116.4562883, 40.0147231],
          [116.4562981, 40.014314]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51383612',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 3,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4489641, 40.0149814],
          [116.4508314, 40.0144032],
          [116.4519043, 40.0141484]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51383613',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4519043, 40.0141484],
          [116.4530223, 40.0139555],
          [116.453647, 40.0138806],
          [116.454344, 40.0138299],
          [116.4552772, 40.0137693],
          [116.4562817, 40.0137511],
          [116.4605764, 40.0137302]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '51383614',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 3,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4519515, 40.0143127],
          [116.4504988, 40.014699],
          [116.4489567, 40.0151826]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '54669971',
      code: 5115,
      fclass: 'tertiary',
      name: '南春路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5076525, 39.9995332],
          [116.507664, 39.9994534],
          [116.5078511, 39.9981488],
          [116.5079788, 39.9969077],
          [116.5082406, 39.9943647],
          [116.5082711, 39.994041],
          [116.5082857, 39.9938853],
          [116.5082878, 39.993758],
          [116.50824, 39.9936359],
          [116.5081819, 39.9935373],
          [116.5080148, 39.993301],
          [116.5061918, 39.9912009],
          [116.5058429, 39.9906601],
          [116.5045051, 39.9891255],
          [116.5044666, 39.9889913],
          [116.5044705, 39.9887931],
          [116.5044844, 39.9885195],
          [116.5044755, 39.988303],
          [116.5041426, 39.9878084],
          [116.5041023, 39.9876255],
          [116.5039881, 39.9875445]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '58867974',
      code: 5115,
      fclass: 'tertiary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4929328, 39.9866993],
          [116.4925976, 39.9867436],
          [116.4911598, 39.9867299],
          [116.4900051, 39.9867189],
          [116.4864966, 39.9866855],
          [116.4855918, 39.9866769],
          [116.4835475, 39.9866574],
          [116.4834329, 39.9865436]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '59946415',
      code: 5115,
      fclass: 'tertiary',
      name: '望京西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4516653, 40.002303],
          [116.4516287, 40.0017611],
          [116.4515374, 40.000033],
          [116.4515382, 39.9997629],
          [116.4515389, 39.9994975],
          [116.451538, 39.9994038],
          [116.4515291, 39.9992066],
          [116.4515151, 39.9988963],
          [116.4514655, 39.9978707],
          [116.4513915, 39.9967034],
          [116.4513302, 39.995737],
          [116.4513163, 39.9956014],
          [116.4512721, 39.9936687],
          [116.4511973, 39.992583],
          [116.4511156, 39.9913693],
          [116.4510649, 39.9905602],
          [116.4510494, 39.9901345],
          [116.4510505, 39.989755],
          [116.451068, 39.9887734],
          [116.4510716, 39.9884639],
          [116.4510352, 39.9881294],
          [116.450977, 39.9878088],
          [116.4509897, 39.9874797]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '59946626',
      code: 5122,
      fclass: 'residential',
      name: '香宾路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4594094, 40.0146311],
          [116.456404, 40.0147232],
          [116.4562883, 40.0147231],
          [116.4563992, 40.014687],
          [116.4594002, 40.0145835],
          [116.4609277, 40.0145308]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '60051360',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4971216, 40.0024224],
          [116.4929775, 39.9986663],
          [116.4924571, 39.9981946],
          [116.4919417, 39.9977275],
          [116.489501, 39.9951383]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '60051366',
      code: 5115,
      fclass: 'tertiary',
      name: '花家地南街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4671608, 39.9745307],
          [116.4663284, 39.9750544],
          [116.4659676, 39.9752814],
          [116.4640301, 39.9766208],
          [116.4617414, 39.9781081],
          [116.4616486, 39.9781684],
          [116.4615682, 39.9782207],
          [116.4614689, 39.9782852]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '60065807',
      code: 5121,
      fclass: 'unclassified',
      name: '宜居南路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4560491, 39.9760888],
          [116.4574016, 39.9751555],
          [116.4577046, 39.9749688]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '60065808',
      code: 5121,
      fclass: 'unclassified',
      name: '宜居西路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4553863, 39.9755012],
          [116.4560491, 39.9760888],
          [116.4562861, 39.9763102],
          [116.4563769, 39.9764895],
          [116.4563871, 39.9771272],
          [116.4563239, 39.9772845],
          [116.456149, 39.9777197],
          [116.4560012, 39.9781405]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '60110696',
      code: 5115,
      fclass: 'tertiary',
      name: '西坝河南路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4340858, 39.9484204],
          [116.4339531, 39.9483589],
          [116.4338368, 39.9483148],
          [116.4337016, 39.9482693],
          [116.4298648, 39.9482563],
          [116.4296667, 39.9483321],
          [116.4295865, 39.9484289],
          [116.4292549, 39.9491117],
          [116.4291745, 39.9493276],
          [116.4286166, 39.9507053],
          [116.4283406, 39.9513437],
          [116.4282363, 39.9516337],
          [116.4281813, 39.9519246],
          [116.4281989, 39.9531463],
          [116.4281711, 39.9540417],
          [116.4281814, 39.954859],
          [116.4282244, 39.9550655],
          [116.428373, 39.9557792],
          [116.4284252, 39.9560979],
          [116.4285549, 39.9566745],
          [116.4286809, 39.9572327],
          [116.4287568, 39.9574803],
          [116.4288084, 39.9576489],
          [116.4289457, 39.9580891],
          [116.42896, 39.9581394],
          [116.4289808, 39.9582243],
          [116.4289989, 39.9582839],
          [116.4291912, 39.9589397],
          [116.4294534, 39.9597144],
          [116.4294574, 39.9597261],
          [116.4295891, 39.9599362],
          [116.4300509, 39.960673],
          [116.430907, 39.9615993],
          [116.4312471, 39.9618559],
          [116.4317784, 39.9619285],
          [116.4328596, 39.9629075],
          [116.4342597, 39.9641568],
          [116.43481, 39.9646246],
          [116.4350553, 39.9648208]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '60251615',
      code: 5121,
      fclass: 'unclassified',
      name: '宜居南路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4560491, 39.9760888],
          [116.454713, 39.9769143]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '60254896',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4780283, 39.974983],
          [116.4793511, 39.9762076],
          [116.4808972, 39.9775946],
          [116.4821478, 39.9787164],
          [116.4823783, 39.9788726],
          [116.4826231, 39.9789477]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '60254907',
      code: 5115,
      fclass: 'tertiary',
      name: '万红西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4766534, 39.9829673],
          [116.4777848, 39.9822188],
          [116.4797869, 39.980912],
          [116.4802185, 39.9806303],
          [116.4807336, 39.9802941],
          [116.4818242, 39.9795822],
          [116.4819714, 39.9794806],
          [116.4820867, 39.9793396],
          [116.4820983, 39.9789093],
          [116.4820698, 39.9787591]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '60254998',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.483783, 39.9790399],
          [116.4835815, 39.9790659],
          [116.4829703, 39.9790645],
          [116.4826083, 39.9790223],
          [116.4822993, 39.9789245],
          [116.4820698, 39.9787591],
          [116.4792377, 39.9762799],
          [116.4779186, 39.9750498]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '67979592',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4539583, 39.9626267],
          [116.4508354, 39.9646902],
          [116.4471056, 39.9671352]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '68522954',
      code: 5121,
      fclass: 'unclassified',
      name: '宜居北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4583129, 39.9771447],
          [116.4574458, 39.9771243],
          [116.4563871, 39.9771272]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '68522956',
      code: 5121,
      fclass: 'unclassified',
      name: '宜居北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4594344, 39.9765192],
          [116.458917, 39.9768996],
          [116.458685, 39.9770721],
          [116.4585367, 39.9771178],
          [116.4583129, 39.9771447]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '68522957',
      code: 5141,
      fclass: 'service',
      name: '宜居西路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4560012, 39.9781405],
          [116.4553, 39.9793278]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '72631490',
      code: 5115,
      fclass: 'tertiary',
      name: '阜通东大街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4766441, 39.9912067],
          [116.4772894, 39.9919081],
          [116.4775528, 39.9924009],
          [116.4777631, 39.9929899],
          [116.4777969, 39.9932611],
          [116.4776802, 39.9940017],
          [116.4776045, 39.9944315],
          [116.4774869, 39.9948229],
          [116.477368, 39.9951109],
          [116.4772155, 39.9953733],
          [116.4770358, 39.9955983],
          [116.4768394, 39.9958232],
          [116.4766013, 39.9960628],
          [116.4764019, 39.9962372],
          [116.476256, 39.9963439],
          [116.4761146, 39.9964473]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '73533570',
      code: 5115,
      fclass: 'tertiary',
      name: '西坝河南路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4353141, 39.9651493],
          [116.4355923, 39.965431],
          [116.4356974, 39.9655779],
          [116.4357732, 39.9657343],
          [116.4358257, 39.9659464],
          [116.4359103, 39.9660636],
          [116.4360607, 39.9662189],
          [116.4366864, 39.966753],
          [116.4368484, 39.9668773],
          [116.4370703, 39.967018],
          [116.4372539, 39.9671135],
          [116.4373506, 39.9671546],
          [116.4375862, 39.9672405],
          [116.4379242, 39.967324],
          [116.4382248, 39.967381]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '84718463',
      code: 5115,
      fclass: 'tertiary',
      name: '望花路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4677599, 39.9892847],
          [116.467666, 39.9892005],
          [116.4674889, 39.989047],
          [116.467041, 39.9886387],
          [116.4662728, 39.9879449],
          [116.4640542, 39.9859968],
          [116.4629282, 39.9849227],
          [116.4628787, 39.9848755],
          [116.4627707, 39.9847809],
          [116.4627108, 39.9847195],
          [116.4618818, 39.9839622],
          [116.4612103, 39.9833426],
          [116.4610207, 39.9831233],
          [116.4607886, 39.9827301]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '84718475',
      code: 5115,
      fclass: 'tertiary',
      name: '花家地东路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4700962, 39.9802332],
          [116.4717652, 39.9815902],
          [116.4737002, 39.9826335],
          [116.4739139, 39.9828349],
          [116.4750835, 39.9839374]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '108383923',
      code: 5121,
      fclass: 'unclassified',
      name: '酒仙桥中路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4934086, 39.9753356],
          [116.4931938, 39.9753367],
          [116.4903311, 39.9753514],
          [116.4884979, 39.9753569],
          [116.4869712, 39.9753614],
          [116.4868586, 39.9753618],
          [116.4858993, 39.9753736],
          [116.4857985, 39.9753727],
          [116.4837914, 39.9753551],
          [116.4836396, 39.9753564]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '114080000',
      code: 5122,
      fclass: 'residential',
      name: '彩溢路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5061918, 39.9912009],
          [116.5052175, 39.9941726]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115536556',
      code: 5141,
      fclass: 'service',
      name: '798路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4853167, 39.9830194],
          [116.4838637, 39.9829984],
          [116.4837186, 39.9829918]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115536558',
      code: 5141,
      fclass: 'service',
      name: '797路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4836482, 39.9849844],
          [116.4838155, 39.9849755],
          [116.4852673, 39.9849792],
          [116.4854665, 39.9849809],
          [116.4862749, 39.9849878],
          [116.4874264, 39.9849976],
          [116.4882604, 39.9850077],
          [116.488812, 39.9850179],
          [116.4893727, 39.9850252],
          [116.4903262, 39.9850505],
          [116.4906969, 39.9850531],
          [116.4912938, 39.9850572],
          [116.4915952, 39.9850593],
          [116.4921243, 39.9850628],
          [116.4929904, 39.985069],
          [116.4939497, 39.985078],
          [116.4940398, 39.985078],
          [116.4951585, 39.9850759],
          [116.4951826, 39.9843814]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115536559',
      code: 5141,
      fclass: 'service',
      name: '798路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4853167, 39.9830194],
          [116.4854014, 39.9830177],
          [116.4873975, 39.9830297],
          [116.4878952, 39.9830415],
          [116.4883337, 39.9830373],
          [116.4886413, 39.9830553],
          [116.4894256, 39.9830671],
          [116.489676, 39.9830689],
          [116.4903804, 39.9830738],
          [116.4906219, 39.9830742],
          [116.490904, 39.9830737],
          [116.4915939, 39.9830724],
          [116.4918241, 39.9830755],
          [116.4930146, 39.9830918],
          [116.4941516, 39.983094]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115584358',
      code: 5115,
      fclass: 'tertiary',
      name: '万红路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4858623, 39.9790417],
          [116.4857428, 39.9790664],
          [116.483783, 39.9790399]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115584359',
      code: 5115,
      fclass: 'tertiary',
      name: '万红路',
      ref: null,
      oneway: 'T',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4858623, 39.9790417],
          [116.4857282, 39.9790186],
          [116.4855273, 39.9790143],
          [116.483787, 39.9789797],
          [116.4835782, 39.9789581]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115645281',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4826231, 39.9789477],
          [116.4829485, 39.9789603],
          [116.4835782, 39.9789581]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115645596',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4762175, 39.9733304],
          [116.4762627, 39.9733717],
          [116.4763031, 39.9734085],
          [116.4772172, 39.9742428],
          [116.4780283, 39.974983]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115646459',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4706941, 39.9683104],
          [116.4707427, 39.9683552],
          [116.4732225, 39.9706401],
          [116.4762175, 39.9733304]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115646460',
      code: 5114,
      fclass: 'secondary',
      name: '霄云路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.45384, 39.9531234],
          [116.4563131, 39.9553666],
          [116.4598434, 39.9585686],
          [116.4606412, 39.9592943],
          [116.4616084, 39.960174],
          [116.4616591, 39.9602201],
          [116.4619756, 39.9604947],
          [116.4629249, 39.9613182],
          [116.4634793, 39.9619168],
          [116.4643373, 39.9631132],
          [116.4647565, 39.9636793],
          [116.4652635, 39.9642436],
          [116.4653399, 39.9643286],
          [116.4656962, 39.9646364],
          [116.4659405, 39.9648474],
          [116.4665658, 39.9652785],
          [116.4683989, 39.9663595]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115646461',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4702255, 39.9679139],
          [116.4706941, 39.9683104]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '115677923',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4688737, 39.9666949],
          [116.4699388, 39.9676491],
          [116.4700495, 39.9677552],
          [116.4702255, 39.9679139]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120027180',
      code: 5122,
      fclass: 'residential',
      name: '荣轩街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4527189, 39.9834708],
          [116.4524988, 39.9816904],
          [116.4524768, 39.9811117],
          [116.4524963, 39.9807499],
          [116.4527388, 39.9793762],
          [116.4529561, 39.9783383]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120495322',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4605652, 39.9676093],
          [116.4511862, 39.9591635]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120495324',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4610992, 39.9680827],
          [116.4605652, 39.9676093]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120495325',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4598364, 39.9679861],
          [116.4592009, 39.96737],
          [116.4587699, 39.9669997],
          [116.4566488, 39.965048],
          [116.4540297, 39.962691],
          [116.4539583, 39.9626267],
          [116.4538631, 39.9625413],
          [116.4537987, 39.9624835],
          [116.4531461, 39.9618977],
          [116.4519647, 39.960839],
          [116.4512871, 39.9602317],
          [116.4511749, 39.9601311],
          [116.4511233, 39.9600849],
          [116.4504942, 39.9595235],
          [116.4501908, 39.9593075],
          [116.4498998, 39.959111],
          [116.4493958, 39.9588534],
          [116.4488572, 39.9585964],
          [116.4487914, 39.9585681],
          [116.4487192, 39.9585357],
          [116.4484443, 39.9583922],
          [116.4481372, 39.9581962]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120495327',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4603178, 39.9684239],
          [116.4598364, 39.9679861]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120495438',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4679631, 39.9668485],
          [116.4688949, 39.9662384]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120495440',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4688949, 39.9662384],
          [116.4723383, 39.9639915],
          [116.4750624, 39.9621913],
          [116.4768167, 39.9609918]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120495441',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4680994, 39.966983],
          [116.4672559, 39.9675427],
          [116.4663919, 39.968108]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120496989',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4607431, 39.9675538],
          [116.4612344, 39.9680097]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120496998',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4599713, 39.9679038],
          [116.4604548, 39.9683113]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '120497149',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4690363, 39.9663912],
          [116.4680994, 39.966983]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '121114832',
      code: 5115,
      fclass: 'tertiary',
      name: '创达二路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4684784, 40.0169415],
          [116.4685248, 40.0150482],
          [116.46854, 40.0146895]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '121510825',
      code: 5115,
      fclass: 'tertiary',
      name: '望京北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4668246, 40.0103041],
          [116.4665077, 40.0103026],
          [116.464082, 40.0103029],
          [116.4613834, 40.0103185],
          [116.4611373, 40.0103201],
          [116.4594004, 40.010341],
          [116.457746, 40.0103208],
          [116.4566356, 40.0103096]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '121513015',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4605748, 40.013872],
          [116.4564511, 40.0139048]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '127714571',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4482317, 39.9580377],
          [116.448561, 39.9583039],
          [116.4488118, 39.9584468],
          [116.4494734, 39.9587813],
          [116.449974, 39.9590476],
          [116.4502839, 39.9592397],
          [116.4504605, 39.9593626],
          [116.4507187, 39.9595618],
          [116.4508742, 39.9596885],
          [116.451045, 39.9598297],
          [116.4511436, 39.9599215],
          [116.451241, 39.960012],
          [116.451289, 39.9600567],
          [116.4518925, 39.9606115],
          [116.4528199, 39.9614529],
          [116.4532415, 39.9618354],
          [116.4599713, 39.9679038]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '129622317',
      code: 5112,
      fclass: 'trunk',
      name: '京密路 Jingmi Road',
      ref: 'G101',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4604548, 39.9683113],
          [116.4607185, 39.9685336]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724100',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.478042, 39.9831364],
          [116.4794155, 39.9843788],
          [116.4820789, 39.9867882]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724101',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路 Airport Expressway',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4993433, 40.0023934],
          [116.501242, 40.0041181]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724102',
      code: 5111,
      fclass: 'motorway',
      name: 'Airport Expressway',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.499184, 40.0024946],
          [116.4984537, 40.0018385]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724104',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.472483, 39.9783232],
          [116.4688425, 39.9750774],
          [116.4610992, 39.9680827]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724106',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4726477, 39.9782416],
          [116.4730759, 39.9786278]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724108',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4612344, 39.9680097],
          [116.4628542, 39.9694099]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724109',
      code: 5111,
      fclass: 'motorway',
      name: 'Airport Expressway',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4985776, 40.0017166],
          [116.4993433, 40.0023934]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724110',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4729305, 39.9787347],
          [116.472483, 39.9783232]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724112',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4984537, 40.0018385],
          [116.4982837, 40.0017021],
          [116.4951751, 39.998849],
          [116.4847942, 39.9894288],
          [116.4830763, 39.9878844]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724115',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4819647, 39.986875],
          [116.4814004, 39.9863696],
          [116.4778783, 39.9832103]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724116',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4730759, 39.9786278],
          [116.476416, 39.9816603],
          [116.4768399, 39.9820451]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '148724127',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4972397, 40.0023722],
          [116.499405, 40.0041865]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '150529289',
      code: 5112,
      fclass: 'trunk',
      name: '北三环',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4393308, 39.9623859],
          [116.4379987, 39.9632541],
          [116.4372444, 39.9637482],
          [116.4366404, 39.9641438],
          [116.4358741, 39.9646458]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '154117612',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路 Airport Expressway',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5218452, 40.0182992],
          [116.5181474, 40.0169984],
          [116.5167882, 40.0164556],
          [116.5154103, 40.0158424],
          [116.5139928, 40.0150998],
          [116.5129669, 40.0145091],
          [116.512533, 40.0142252],
          [116.5120748, 40.0138963],
          [116.5115561, 40.013504],
          [116.5111858, 40.0132254],
          [116.5106333, 40.0127873],
          [116.5101892, 40.0123992],
          [116.5083646, 40.0107932]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '154117616',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4832109, 39.9878112],
          [116.4864749, 39.9907594],
          [116.488982, 39.9929865],
          [116.4953063, 39.9987431],
          [116.4972792, 40.0004794],
          [116.4985776, 40.0017166]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '154117629',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4820789, 39.9867882],
          [116.4832109, 39.9878112]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '154117633',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4830763, 39.9878844],
          [116.4819647, 39.986875]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '154117646',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4998536, 40.0046876],
          [116.4992334, 40.0042082]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '154117648',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.499405, 40.0041865],
          [116.4999978, 40.0046635]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '154117672',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4992334, 40.0042082],
          [116.4971216, 40.0024224]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '154117673',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4999978, 40.0046635],
          [116.5009816, 40.0054761],
          [116.5022772, 40.0065444],
          [116.5072837, 40.0110448],
          [116.5096253, 40.0131256],
          [116.5100107, 40.0134937],
          [116.5103491, 40.0138679],
          [116.5106783, 40.0143019],
          [116.5130209, 40.0177413]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '158035590',
      code: 5112,
      fclass: 'trunk',
      name: '北三环',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4302365, 39.9679973],
          [116.4295913, 39.9681313],
          [116.4289985, 39.9682229],
          [116.428415, 39.9682601],
          [116.4276597, 39.968246]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '158035591',
      code: 5112,
      fclass: 'trunk',
      name: '北三环',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4358741, 39.9646458],
          [116.4348177, 39.9653404]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '158245758',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4276091, 39.9726587],
          [116.428253, 39.9735257]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '159782788',
      code: 5115,
      fclass: 'tertiary',
      name: '文学馆路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.431048, 39.9821203],
          [116.4318278, 39.9821166]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '159782789',
      code: 5115,
      fclass: 'tertiary',
      name: '文学馆路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4180841, 39.981993],
          [116.4181803, 39.981994],
          [116.4182923, 39.9819952],
          [116.4211913, 39.9820262],
          [116.4247403, 39.9820641],
          [116.4267089, 39.9820839],
          [116.4307426, 39.9820605],
          [116.431048, 39.9821203]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161685581',
      code: 5114,
      fclass: 'secondary',
      name: '花家地街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4720069, 39.9789455],
          [116.4711454, 39.9795261],
          [116.4700962, 39.9802332],
          [116.4665499, 39.9825053],
          [116.4664701, 39.9825563],
          [116.4663904, 39.9826074],
          [116.4663012, 39.9826646],
          [116.4655436, 39.9831601],
          [116.4640117, 39.9841277],
          [116.4628787, 39.9848755],
          [116.4597357, 39.9869372],
          [116.45964, 39.9869989]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161685584',
      code: 5115,
      fclass: 'tertiary',
      name: '将台路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.47353, 39.9779128],
          [116.4734677, 39.9779981],
          [116.4730677, 39.9782581],
          [116.4721092, 39.9788809]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161685598',
      code: 5115,
      fclass: 'tertiary',
      name: '望京街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4766441, 39.9912067],
          [116.4750373, 39.9922512],
          [116.4743274, 39.9927292]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161685599',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.489501, 39.9951383],
          [116.4891728, 39.994785],
          [116.4871139, 39.9925829],
          [116.4838443, 39.9896306],
          [116.4818924, 39.9878681],
          [116.4817957, 39.9877783]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161685600',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4719446, 39.9788921],
          [116.4711205, 39.9781589],
          [116.4708051, 39.9778732],
          [116.4705886, 39.9776771],
          [116.4699854, 39.9771307],
          [116.4689344, 39.9761785],
          [116.4671608, 39.9745307]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161701272',
      code: 5114,
      fclass: 'secondary',
      name: '曙光西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4484705, 39.9686292],
          [116.447019, 39.9673027],
          [116.446924, 39.9672092]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161701273',
      code: 5115,
      fclass: 'tertiary',
      name: '曙光西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4566881, 39.974046],
          [116.4563186, 39.9739239],
          [116.4540108, 39.972531],
          [116.4536736, 39.9721448]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161701274',
      code: 5115,
      fclass: 'tertiary',
      name: '曙光西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4538195, 39.9720805],
          [116.454346, 39.9721941],
          [116.4567272, 39.9736194],
          [116.4569205, 39.9738964]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161701275',
      code: 5114,
      fclass: 'secondary',
      name: '曙光西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4495595, 39.9693158],
          [116.4497854, 39.9695269],
          [116.4505869, 39.9701277],
          [116.4538195, 39.9720805],
          [116.4557154, 39.973195]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161701276',
      code: 5114,
      fclass: 'secondary',
      name: '曙光西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4556244, 39.9732822],
          [116.4536736, 39.9721448],
          [116.4504298, 39.9702467],
          [116.4496052, 39.9696584],
          [116.44919, 39.9692818]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161701303',
      code: 5114,
      fclass: 'secondary',
      name: '阜通东大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4582645, 39.9750627],
          [116.460223, 39.9768709],
          [116.4616486, 39.9781684],
          [116.4648222, 39.9810566],
          [116.4664083, 39.9825001],
          [116.4664701, 39.9825563],
          [116.4680418, 39.9839846],
          [116.4682447, 39.9841512],
          [116.468425, 39.9842918],
          [116.4689025, 39.9846421],
          [116.4694676, 39.9850547],
          [116.4700495, 39.9855147],
          [116.4704084, 39.9857374],
          [116.4708306, 39.9860136],
          [116.4714079, 39.9864444],
          [116.4716016, 39.9865889],
          [116.4716993, 39.9866619],
          [116.4737241, 39.9884805],
          [116.4748601, 39.9895141],
          [116.4749371, 39.9895842],
          [116.4765625, 39.9910631],
          [116.4766441, 39.9912067]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161701304',
      code: 5114,
      fclass: 'secondary',
      name: '曙光西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4581508, 39.975133],
          [116.4574691, 39.9745431],
          [116.4565731, 39.9738595],
          [116.4556244, 39.9732822]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161701307',
      code: 5114,
      fclass: 'secondary',
      name: '阜通东大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4557154, 39.973195],
          [116.4566743, 39.9737758],
          [116.4575904, 39.9744538],
          [116.4582645, 39.9750627]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161701312',
      code: 5115,
      fclass: 'tertiary',
      name: '北三环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4407731, 39.9616002],
          [116.4406173, 39.9617012],
          [116.440543, 39.9617494],
          [116.440419, 39.9618298],
          [116.4401758, 39.9619874],
          [116.4400588, 39.9620633],
          [116.4398341, 39.962209],
          [116.4395663, 39.9623827],
          [116.4381808, 39.963281],
          [116.4376039, 39.9637137],
          [116.4374779, 39.9638082],
          [116.4373057, 39.9639205],
          [116.4369567, 39.964128],
          [116.4364894, 39.964406],
          [116.4353141, 39.9651493]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '161702302',
      code: 5115,
      fclass: 'tertiary',
      name: '北三环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4353141, 39.9651493],
          [116.4349015, 39.9654166]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '162586530',
      code: 5115,
      fclass: 'tertiary',
      name: '北三环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4345938, 39.9651423],
          [116.4350553, 39.9648208],
          [116.4365239, 39.9638666],
          [116.4372069, 39.963417],
          [116.4372505, 39.9633889],
          [116.4373309, 39.9633369]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '162586531',
      code: 5115,
      fclass: 'tertiary',
      name: '北三环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4344179, 39.96573],
          [116.4343478, 39.9657804],
          [116.4336977, 39.9662471],
          [116.4336188, 39.9663047],
          [116.4332715, 39.9665317],
          [116.4330789, 39.9666575],
          [116.4329331, 39.9667528],
          [116.4322484, 39.9672002],
          [116.4321752, 39.9672481],
          [116.4316852, 39.967527],
          [116.4313282, 39.9677302],
          [116.4312366, 39.9677657],
          [116.4307068, 39.9679711],
          [116.430066, 39.968203],
          [116.4296482, 39.9683165],
          [116.4291104, 39.968405],
          [116.4285725, 39.9684514],
          [116.4280706, 39.968459]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '162586532',
      code: 5112,
      fclass: 'trunk',
      name: '北三环',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4347217, 39.96525],
          [116.4360949, 39.9643345],
          [116.4371744, 39.9636274],
          [116.4373567, 39.963508],
          [116.4379537, 39.9631169]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '162586533',
      code: 5112,
      fclass: 'trunk',
      name: '北三环',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4343294, 39.9656614],
          [116.4339535, 39.9659085],
          [116.4329822, 39.9665311],
          [116.4328361, 39.9666252],
          [116.4326912, 39.9667185],
          [116.4317473, 39.9673265],
          [116.4314028, 39.9675042],
          [116.4310459, 39.9676976],
          [116.4302365, 39.9679973]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '164910459',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4382248, 39.967381],
          [116.4382552, 39.9674804]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '164910461',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4381513, 39.9674717],
          [116.4382248, 39.967381]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '164910477',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4381649, 39.9679908],
          [116.4381716, 39.9681084],
          [116.4382303, 39.9682296],
          [116.4383703, 39.9683991],
          [116.4398011, 39.9697792],
          [116.4403318, 39.9702911],
          [116.4411966, 39.9711253],
          [116.4412801, 39.9712058],
          [116.4414187, 39.9713467]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204609',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4766869, 39.9821326],
          [116.4729305, 39.9787347]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204611',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4483214, 39.9563736],
          [116.4487511, 39.9567641],
          [116.4501915, 39.9580911],
          [116.4607431, 39.9675538]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204612',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4628542, 39.9694099],
          [116.4639909, 39.9704082]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204615',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4768399, 39.9820451],
          [116.478042, 39.9831364]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204617',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4778783, 39.9832103],
          [116.4766869, 39.9821326]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204621',
      code: 5115,
      fclass: 'tertiary',
      name: '尚家楼路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.450764, 39.9646278],
          [116.4508354, 39.9646902],
          [116.4508999, 39.9647488],
          [116.4534906, 39.9671031]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204622',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4455548, 39.9682101],
          [116.446924, 39.9672092]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204627',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4607185, 39.9685336],
          [116.4656078, 39.9728937],
          [116.4720439, 39.9788255]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204629',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4638395, 39.9695531],
          [116.4661243, 39.9680277]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169204630',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4635507, 39.9697244],
          [116.4638395, 39.9695531]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169207726',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路 Airport Expressway',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5085295, 40.0106942],
          [116.5107546, 40.0126832],
          [116.511309, 40.0131041],
          [116.5117375, 40.0134213],
          [116.5123203, 40.0138701],
          [116.5128243, 40.0142129],
          [116.5136356, 40.0147009],
          [116.5143291, 40.0150847],
          [116.5154949, 40.0156988],
          [116.5169169, 40.0163441],
          [116.518421, 40.0169069],
          [116.5218756, 40.0181498]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169207728',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路 Airport Expressway',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5016489, 40.0044877],
          [116.5082907, 40.0104809]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169207729',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路 Airport Expressway',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5081335, 40.0105729],
          [116.5013815, 40.004512]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169207731',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路 Airport Expressway',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5009843, 40.0041474],
          [116.499184, 40.0024946]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169212593',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4819236, 39.9876974],
          [116.4820258, 39.9877872],
          [116.4850702, 39.9905343],
          [116.4871954, 39.9925045],
          [116.4895949, 39.9950925],
          [116.4920451, 39.997683],
          [116.4972397, 40.0023722]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169212594',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4817957, 39.9877783],
          [116.4767615, 39.9832488],
          [116.476668, 39.9831707]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169612116',
      code: 5115,
      fclass: 'tertiary',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.47748, 39.9609499],
          [116.4771063, 39.9612459],
          [116.4762033, 39.9618514],
          [116.4748548, 39.9627555],
          [116.4732108, 39.9638577],
          [116.4718183, 39.9647108],
          [116.4709184, 39.9653172],
          [116.4688737, 39.9666949],
          [116.468777, 39.9667626],
          [116.4686921, 39.9668377],
          [116.4684467, 39.9670547],
          [116.4679459, 39.9673961],
          [116.4673467, 39.9678046],
          [116.4669781, 39.9679888],
          [116.4666511, 39.9682235]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169612234',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.448372, 39.9799015],
          [116.4432293, 39.9832469],
          [116.4422137, 39.9839093],
          [116.441804, 39.9841597],
          [116.4412623, 39.9844142],
          [116.4404747, 39.9847531],
          [116.4370416, 39.9860953]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169612318',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4768056, 39.9612205],
          [116.4752049, 39.9623004],
          [116.472582, 39.9640265],
          [116.4690363, 39.9663912]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169612332',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4549013, 39.9756327],
          [116.4495414, 39.9791499]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169612334',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4494301, 39.9790117],
          [116.4545021, 39.9757024]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169612348',
      code: 5112,
      fclass: 'trunk',
      name: '望京桥',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4482549, 39.9797628],
          [116.4494301, 39.9790117]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169612351',
      code: 5112,
      fclass: 'trunk',
      name: '望京桥',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4495414, 39.9791499],
          [116.448372, 39.9799015]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616112',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4350223, 39.9848322],
          [116.4344577, 39.9840159],
          [116.4327065, 39.9812811]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616113',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4358512, 39.9861022],
          [116.4352283, 39.9851347]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616114',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4368673, 39.9864088],
          [116.437463, 39.9873059]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616115',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4358534, 39.9848975],
          [116.4364959, 39.9858826]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616116',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4368208, 39.9875545],
          [116.4362894, 39.9867424],
          [116.4362475, 39.9866785]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616117',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 120,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4375811, 39.9874783],
          [116.4392205, 39.9899909],
          [116.4397992, 39.9908779],
          [116.4425383, 39.9950756],
          [116.4438293, 39.9973368],
          [116.4445322, 39.9987157],
          [116.44509, 40.0004414]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616118',
      code: 5115,
      fclass: 'tertiary',
      name: '京承高速辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4386287, 39.9869623],
          [116.4387084, 39.9875586],
          [116.4388178, 39.9880988],
          [116.4389842, 39.988514],
          [116.4393025, 39.9892099],
          [116.4396923, 39.9901119],
          [116.4397052, 39.9901418],
          [116.4398771, 39.9905039],
          [116.4400808, 39.9908431],
          [116.4411943, 39.9925039],
          [116.4416516, 39.9931663]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616119',
      code: 5115,
      fclass: 'tertiary',
      name: '京承高速辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4347596, 39.9851918],
          [116.4347411, 39.9850165],
          [116.4346979, 39.9848588],
          [116.4346348, 39.9847047],
          [116.4342347, 39.9841338],
          [116.4336557, 39.9832392],
          [116.4331941, 39.9825569],
          [116.4326729, 39.9818022],
          [116.4315613, 39.9801498],
          [116.4312801, 39.9797352],
          [116.4300761, 39.9777244],
          [116.4292307, 39.9763124],
          [116.4288607, 39.9757668]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616124',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4356605, 39.9846119],
          [116.4358534, 39.9848975]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616129',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4369204, 39.9877122],
          [116.4368208, 39.9875545]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616137',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4352283, 39.9851347],
          [116.4350223, 39.9848322]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616146',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4313108, 39.9880147],
          [116.4311976, 39.9880237],
          [116.43005, 39.9880788],
          [116.4287523, 39.9880802],
          [116.4275921, 39.9880485],
          [116.4263097, 39.9880204],
          [116.4247292, 39.9879656],
          [116.4234464, 39.9879203],
          [116.4203662, 39.9878115],
          [116.4184116, 39.9877425],
          [116.4179397, 39.9877305]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616162',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4362475, 39.9866785],
          [116.4358512, 39.9861022]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169616163',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4364959, 39.9858826],
          [116.4368673, 39.9864088]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169620645',
      code: 5115,
      fclass: 'tertiary',
      name: '湖光中街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4448106, 39.99512],
          [116.4469707, 39.9954892],
          [116.4511753, 39.9955978],
          [116.4513163, 39.9956014],
          [116.4515138, 39.9956057],
          [116.4517282, 39.995612],
          [116.4533205, 39.9956586],
          [116.4540458, 39.9956798],
          [116.4543102, 39.9956875],
          [116.4552063, 39.9957138],
          [116.4565424, 39.9957529],
          [116.4566601, 39.995755],
          [116.4580537, 39.9957793],
          [116.4599332, 39.995812],
          [116.4616527, 39.995842],
          [116.4617638, 39.9958439],
          [116.4620058, 39.9959123]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169620673',
      code: 5113,
      fclass: 'primary',
      name: '姜庄路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4399444, 39.9944028],
          [116.4398143, 39.9943797],
          [116.4385558, 39.9941615],
          [116.437833, 39.994023],
          [116.4370281, 39.9937847],
          [116.435277, 39.9931949],
          [116.4347853, 39.993017],
          [116.43386, 39.9926823],
          [116.4333675, 39.9925271],
          [116.432934, 39.9924107],
          [116.4325006, 39.9923438],
          [116.4321207, 39.9923179],
          [116.4311258, 39.9922926]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169620682',
      code: 5113,
      fclass: 'primary',
      name: '育慧东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4240977, 39.992179],
          [116.427475, 39.992196],
          [116.4310503, 39.9921955]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169620683',
      code: 5113,
      fclass: 'primary',
      name: '育慧东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.431047, 39.9922897],
          [116.427475, 39.9923004],
          [116.4252258, 39.9923071],
          [116.4243004, 39.9923098],
          [116.42416, 39.9922912]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '169620684',
      code: 5114,
      fclass: 'secondary',
      name: '鼎成路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4311258, 39.9922926],
          [116.4311234, 39.9924019],
          [116.4310528, 39.9956126],
          [116.4311196, 39.9961493],
          [116.4312518, 39.9968472]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170244830',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽西街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4520577, 40.0069546],
          [116.4521674, 40.0069588],
          [116.4543188, 40.0070417],
          [116.4565412, 40.0071273],
          [116.4567332, 40.0071347],
          [116.4568456, 40.0071398],
          [116.457731, 40.0071585],
          [116.4591245, 40.0072429],
          [116.4602252, 40.0072948],
          [116.4612898, 40.0073298]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170244831',
      code: 5113,
      fclass: 'primary',
      name: '北湖渠路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4323061, 40.0012598],
          [116.4325554, 40.0022822],
          [116.4326473, 40.0026593],
          [116.4326867, 40.0029226],
          [116.4327123, 40.0032082],
          [116.4327414, 40.0035855],
          [116.4328307, 40.0069491],
          [116.4328361, 40.00706],
          [116.432895, 40.0114595],
          [116.4329207, 40.0119787],
          [116.4329754, 40.0124189],
          [116.4330883, 40.0128433],
          [116.4334128, 40.0139319],
          [116.4338835, 40.0155113]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170244832',
      code: 5113,
      fclass: 'primary',
      name: '北湖渠路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.433741, 40.0155076],
          [116.4329079, 40.0129688],
          [116.4328129, 40.0126804],
          [116.432696, 40.0123008],
          [116.4326795, 40.0114562],
          [116.4326954, 40.0070551],
          [116.4326962, 40.0069656]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170244833',
      code: 5114,
      fclass: 'secondary',
      name: '北湖渠路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4312904, 39.996968],
          [116.4318733, 39.9994271],
          [116.432222, 40.0008982]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170244834',
      code: 5114,
      fclass: 'secondary',
      name: '北湖渠路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4321215, 40.0009273],
          [116.4320944, 40.0008135],
          [116.4317666, 39.9994367],
          [116.4311797, 39.9969715]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170244841',
      code: 5115,
      fclass: 'tertiary',
      name: '辛店路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4328361, 40.00706],
          [116.4326954, 40.0070551],
          [116.4297561, 40.0071569],
          [116.4287442, 40.0072109],
          [116.4281742, 40.00726],
          [116.4273798, 40.0073324],
          [116.4273013, 40.0073396],
          [116.4242111, 40.0076213],
          [116.4235772, 40.0076717],
          [116.422542, 40.0077467],
          [116.4219624, 40.0077859],
          [116.4214884, 40.0078031],
          [116.4208977, 40.0078132],
          [116.4166907, 40.007888],
          [116.4160295, 40.0078998],
          [116.4156827, 40.0079059],
          [116.410412, 40.0079997]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170244842',
      code: 5115,
      fclass: 'tertiary',
      name: '辛店路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4104287, 40.007909],
          [116.4130771, 40.0078674],
          [116.4166887, 40.0078107],
          [116.4208955, 40.0077447],
          [116.4214855, 40.0077309],
          [116.4219588, 40.0077066],
          [116.4225385, 40.0076667],
          [116.423625, 40.0075791],
          [116.424623, 40.0074995],
          [116.4252437, 40.0074467],
          [116.4272903, 40.0072728],
          [116.4273684, 40.0072662],
          [116.4281764, 40.0071913],
          [116.4287408, 40.0071454],
          [116.4292985, 40.0071082],
          [116.4297567, 40.0070865],
          [116.4326962, 40.0069656]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170244843',
      code: 5114,
      fclass: 'secondary',
      name: '鼎成路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4311395, 39.9968477],
          [116.4310063, 39.996147],
          [116.430954, 39.9956151],
          [116.4309863, 39.9936931],
          [116.4310446, 39.9923753],
          [116.431047, 39.9922897],
          [116.4310503, 39.9921955],
          [116.431195, 39.9880986],
          [116.4311976, 39.9880237]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246448',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4613888, 40.01018],
          [116.4613834, 40.0103185],
          [116.4613094, 40.0120795],
          [116.4613008, 40.012284],
          [116.4612978, 40.0123552],
          [116.4612936, 40.0128824],
          [116.4612719, 40.0129924],
          [116.4612492, 40.0135487]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246449',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4615306, 40.0073278],
          [116.4614784, 40.0085509],
          [116.4614726, 40.0086554],
          [116.4614671, 40.0087555],
          [116.4613888, 40.01018]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246450',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4611422, 40.0101795],
          [116.4611476, 40.0100768],
          [116.4612198, 40.0086896],
          [116.4612217, 40.0086532],
          [116.4612285, 40.0085217],
          [116.4612898, 40.0073298]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246453',
      code: 5122,
      fclass: 'residential',
      name: '香宾路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4609206, 40.014601],
          [116.4605552, 40.014615],
          [116.4594094, 40.0146311]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246457',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽中街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4615306, 40.0073278],
          [116.4641551, 40.0073318],
          [116.4645037, 40.0073309],
          [116.4669876, 40.0073196],
          [116.4720775, 40.0073859],
          [116.4722077, 40.007386],
          [116.476371, 40.0073887]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246458',
      code: 5114,
      fclass: 'secondary',
      name: '利泽西二路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4567898, 40.0047017],
          [116.4567718, 40.0054299],
          [116.4567529, 40.0055628],
          [116.4567417, 40.0059171],
          [116.4567332, 40.0071347],
          [116.4567047, 40.0078067]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246459',
      code: 5115,
      fclass: 'tertiary',
      name: '望京北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4520577, 40.0069546],
          [116.4520725, 40.0077233],
          [116.452081, 40.0078459],
          [116.4520986, 40.0079503],
          [116.452127, 40.0080794],
          [116.4521773, 40.0082167],
          [116.452233, 40.0083459],
          [116.4523919, 40.0086411],
          [116.4525764, 40.0089196],
          [116.4527683, 40.0091379],
          [116.4530258, 40.0093603],
          [116.4533035, 40.0095492],
          [116.4536561, 40.0097535],
          [116.4539794, 40.0099004],
          [116.4544453, 40.0100418],
          [116.454807, 40.0101201],
          [116.4552089, 40.0101579],
          [116.4556601, 40.0101803],
          [116.4564467, 40.010171]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246462',
      code: 5115,
      fclass: 'tertiary',
      name: '望京西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4518806, 40.0068949],
          [116.4517826, 40.0045463],
          [116.4517269, 40.003213],
          [116.4516969, 40.0027698]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246463',
      code: 5115,
      fclass: 'tertiary',
      name: '望京西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4518903, 40.0028747],
          [116.4519172, 40.0032679],
          [116.4519658, 40.004542],
          [116.4520577, 40.0069546]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246464',
      code: 5115,
      fclass: 'tertiary',
      name: '望京西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4509897, 39.9874797],
          [116.4511237, 39.9878027],
          [116.4511752, 39.98813],
          [116.4512009, 39.9884671],
          [116.4512009, 39.9887825],
          [116.4511843, 39.9897655],
          [116.451188, 39.9901391],
          [116.4512061, 39.9905545],
          [116.4512765, 39.9913864],
          [116.4513951, 39.9930395],
          [116.4514395, 39.9939988],
          [116.4515138, 39.9956057],
          [116.4515205, 39.9957399],
          [116.4516049, 39.9978705],
          [116.4516615, 39.9992051],
          [116.45167, 39.9994086],
          [116.4516707, 39.9995012],
          [116.4516842, 39.999763],
          [116.451698, 40.0000303],
          [116.4517787, 40.0010569],
          [116.4518196, 40.0018435],
          [116.4518587, 40.0024143]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170246465',
      code: 5115,
      fclass: 'tertiary',
      name: '辛店路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4518894, 40.0069666],
          [116.4507791, 40.006942],
          [116.4500101, 40.0069383],
          [116.4494601, 40.006887],
          [116.446434, 40.0068013]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258801',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4615729, 40.0003595],
          [116.4615768, 40.0002841],
          [116.4615796, 40.0002075],
          [116.4615982, 39.9996945],
          [116.4616594, 39.9980111],
          [116.4617504, 39.9960397],
          [116.4617563, 39.9959521],
          [116.4617638, 39.9958439],
          [116.4617676, 39.9957719],
          [116.4618361, 39.9944892],
          [116.4618418, 39.9943829],
          [116.4618466, 39.9940081],
          [116.4618643, 39.9938553],
          [116.4619047, 39.9935437],
          [116.4620007, 39.9932265],
          [116.4621472, 39.9928992],
          [116.4623259, 39.9926411],
          [116.462431, 39.9925082],
          [116.4625847, 39.9923139],
          [116.4628427, 39.9921117],
          [116.4632921, 39.9917813],
          [116.4640112, 39.991316]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258804',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4616639, 40.0044999],
          [116.4616592, 40.0047427],
          [116.4615878, 40.0061773],
          [116.4615306, 40.0073278]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258806',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4614303, 40.0041306],
          [116.4614574, 40.0036619],
          [116.461457, 40.0035823],
          [116.4615304, 40.0015418],
          [116.4615331, 40.0014674],
          [116.4615729, 40.0003595]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258822',
      code: 5115,
      fclass: 'tertiary',
      name: '南湖北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4565554, 39.9996741],
          [116.456551, 39.9995646],
          [116.4565559, 39.9994869],
          [116.4565319, 39.9978627],
          [116.456537, 39.9959564],
          [116.4565421, 39.9958432]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258823',
      code: 5115,
      fclass: 'tertiary',
      name: '南湖北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4566581, 39.9958456],
          [116.4566713, 39.995963],
          [116.4566482, 39.9965733],
          [116.4565994, 39.997861],
          [116.4567095, 39.9994912],
          [116.4567186, 39.9995773],
          [116.4567142, 39.9996811],
          [116.4568573, 40.0010936],
          [116.456969, 40.0035402],
          [116.45697, 40.0036271]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258826',
      code: 5114,
      fclass: 'secondary',
      name: '南湖南路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.459546, 39.9870562],
          [116.4582921, 39.9878926],
          [116.4569437, 39.9887742]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258828',
      code: 5114,
      fclass: 'secondary',
      name: '南湖南路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4565424, 39.9957529],
          [116.4565473, 39.9956382],
          [116.4566591, 39.993028],
          [116.4566933, 39.992055],
          [116.4567293, 39.9913355],
          [116.4568597, 39.988726],
          [116.4582195, 39.9878239],
          [116.4593739, 39.9869484]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258830',
      code: 5115,
      fclass: 'tertiary',
      name: '宏泰西街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4618176, 40.0003037],
          [116.4666222, 40.0004782],
          [116.4670696, 40.0004945],
          [116.4672692, 40.0005551]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258831',
      code: 5115,
      fclass: 'tertiary',
      name: '宏泰西街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4672692, 40.0005551],
          [116.4670671, 40.000571],
          [116.4663433, 40.0005437],
          [116.4636371, 40.0004417],
          [116.4618135, 40.0003803]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258832',
      code: 5115,
      fclass: 'tertiary',
      name: '宏泰西街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4672692, 40.0005551],
          [116.469426, 40.0006612],
          [116.4700908, 40.0006661],
          [116.4724451, 40.0006655],
          [116.4725542, 40.0006738],
          [116.4737344, 40.0007036],
          [116.47501, 40.0005836],
          [116.4758001, 40.0004252],
          [116.4760674, 40.0003716],
          [116.4765464, 40.0002594]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258833',
      code: 5115,
      fclass: 'tertiary',
      name: '河荫中路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4616902, 40.0036523],
          [116.4636785, 40.0037786],
          [116.4671555, 40.0040115],
          [116.4681996, 40.0040289],
          [116.4696882, 40.0038784],
          [116.4722579, 40.0035927],
          [116.4723805, 40.0035894]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258835',
      code: 5115,
      fclass: 'tertiary',
      name: '河荫西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.456811, 40.0035846],
          [116.456969, 40.0035402],
          [116.4590859, 40.0035579],
          [116.461457, 40.0035823]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258837',
      code: 5115,
      fclass: 'tertiary',
      name: '湖光北街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4565554, 39.9996741],
          [116.4540997, 39.9995871],
          [116.4519273, 39.9995103],
          [116.4516707, 39.9995012],
          [116.4515389, 39.9994975]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170258839',
      code: 5115,
      fclass: 'tertiary',
      name: '湖光北街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.451538, 39.9994038],
          [116.45167, 39.9994086],
          [116.4519282, 39.999418],
          [116.4540996, 39.9994974],
          [116.4552076, 39.9995212],
          [116.456551, 39.9995646],
          [116.4567186, 39.9995773],
          [116.4591315, 39.9999204],
          [116.4615768, 40.0002841]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170261272',
      code: 5115,
      fclass: 'tertiary',
      name: '望京西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4490608, 39.9796159],
          [116.4504148, 39.9815107],
          [116.4507873, 39.9820492],
          [116.4509463, 39.9822587],
          [116.4511845, 39.9826463]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170261278',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4486735, 39.9793186],
          [116.4475719, 39.978077],
          [116.4454872, 39.9758696],
          [116.4437689, 39.9740096],
          [116.4426562, 39.9728232],
          [116.4413978, 39.9714814],
          [116.4413133, 39.9713913],
          [116.4412122, 39.9712922]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170261279',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4489672, 39.9796808],
          [116.4488417, 39.9797651],
          [116.4486508, 39.9798934],
          [116.447882, 39.9803954],
          [116.4474058, 39.9807281],
          [116.4468903, 39.981073],
          [116.4426726, 39.9838718],
          [116.4417086, 39.9844069],
          [116.4407245, 39.9848496],
          [116.4397101, 39.9853204],
          [116.4393302, 39.9855816],
          [116.4390161, 39.9858501],
          [116.4387495, 39.9862635],
          [116.4386549, 39.986595],
          [116.4386287, 39.9869623]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170261280',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4386287, 39.9869623],
          [116.4356976, 39.9880657]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170261282',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4356976, 39.9880657],
          [116.4351492, 39.9879012],
          [116.4347763, 39.9878275],
          [116.4343003, 39.9877835],
          [116.43362, 39.9877827],
          [116.4330142, 39.9878042],
          [116.4319499, 39.9879253],
          [116.4313108, 39.9880147]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170261284',
      code: 5114,
      fclass: 'secondary',
      name: '阜通西大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4593739, 39.9869484],
          [116.4581792, 39.985756],
          [116.4579404, 39.9855614],
          [116.4575437, 39.985275],
          [116.4570458, 39.9849756],
          [116.456781, 39.9848528],
          [116.4563945, 39.9846803],
          [116.4559457, 39.9845264],
          [116.4538779, 39.9839797],
          [116.4535416, 39.9838837],
          [116.4529431, 39.9837147],
          [116.4524397, 39.9836099],
          [116.4520812, 39.9834944],
          [116.4516992, 39.9833103],
          [116.4513124, 39.983097],
          [116.4509705, 39.9828125]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170261285',
      code: 5114,
      fclass: 'secondary',
      name: '阜通西大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4511845, 39.9826463],
          [116.4514171, 39.9829034],
          [116.4516955, 39.9831598],
          [116.4519783, 39.9833228],
          [116.4523276, 39.983465],
          [116.4529607, 39.9836406],
          [116.4535925, 39.983812],
          [116.4543701, 39.9840229],
          [116.4559698, 39.9844568],
          [116.4563094, 39.9845534],
          [116.4567005, 39.9846953],
          [116.4571347, 39.9849092],
          [116.4576242, 39.9852138],
          [116.457969, 39.9854523],
          [116.4582571, 39.9856731],
          [116.4585093, 39.9859097],
          [116.4594362, 39.9868097],
          [116.4595013, 39.9868729],
          [116.45964, 39.9869989],
          [116.4596847, 39.9870383],
          [116.4600878, 39.987464],
          [116.4606944, 39.9881046],
          [116.461165, 39.9885796],
          [116.4611873, 39.9886021],
          [116.4614363, 39.9888256],
          [116.464136, 39.9912482],
          [116.4642497, 39.9914217]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170261286',
      code: 5114,
      fclass: 'secondary',
      name: '阜通西大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4642497, 39.9914217],
          [116.4640112, 39.991316],
          [116.4639242, 39.9912411],
          [116.4616918, 39.9891903],
          [116.4613148, 39.988835],
          [116.4606034, 39.9881647],
          [116.459546, 39.9870562]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170292687',
      code: 5121,
      fclass: 'unclassified',
      name: '方园南街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.476684, 39.9696997],
          [116.4765389, 39.9678535],
          [116.4764938, 39.9674928],
          [116.4760504, 39.9665593],
          [116.4760264, 39.9665122]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170292690',
      code: 5121,
      fclass: 'unclassified',
      name: '红霞路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.487477, 39.9602131],
          [116.4847588, 39.9619976],
          [116.4825116, 39.9634878],
          [116.482319, 39.9636025],
          [116.4822814, 39.9637875],
          [116.4838602, 39.9651447]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170628885',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4545021, 39.9757024],
          [116.4581358, 39.9732944],
          [116.4582888, 39.9731871],
          [116.4607322, 39.9715889]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170628887',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4535745, 39.9760942],
          [116.4566881, 39.974046],
          [116.4569205, 39.9738964],
          [116.4579546, 39.9732175],
          [116.4586365, 39.972639],
          [116.4594228, 39.97188],
          [116.4598432, 39.9715585]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '170628901',
      code: 5115,
      fclass: 'tertiary',
      name: '西坝河路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4445553, 39.9819715],
          [116.4426817, 39.9817248],
          [116.4422554, 39.9816194],
          [116.4420425, 39.9815238],
          [116.4422715, 39.9815749],
          [116.4427027, 39.9816437],
          [116.4446519, 39.9819093]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '171731747',
      code: 5115,
      fclass: 'tertiary',
      name: '阜安路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4830787, 40.0028122],
          [116.4812807, 40.0012543],
          [116.4811595, 40.0011555],
          [116.4800339, 40.0001036],
          [116.4790352, 39.9991703],
          [116.4774441, 39.9977529],
          [116.4764536, 39.9968134],
          [116.4763009, 39.9966589],
          [116.4761146, 39.9964473]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '171731750',
      code: 5115,
      fclass: 'tertiary',
      name: '宏泰东街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4765464, 40.0002594],
          [116.4778781, 39.999778],
          [116.4788056, 39.9992894],
          [116.4790352, 39.9991703],
          [116.4791529, 39.9990888],
          [116.4796114, 39.9987593],
          [116.4805007, 39.9980966],
          [116.4812324, 39.9974652],
          [116.4814307, 39.9972714]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '171860081',
      code: 5115,
      fclass: 'tertiary',
      name: '阜通东大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4577729, 39.9740219],
          [116.4577377, 39.9741637],
          [116.4577377, 39.9742803],
          [116.4577715, 39.9743948],
          [116.4578533, 39.9744833],
          [116.4598885, 39.9764136],
          [116.4617414, 39.9781081],
          [116.4632252, 39.9794754],
          [116.466488, 39.9824487],
          [116.4665499, 39.9825053],
          [116.468116, 39.9839399],
          [116.4683063, 39.9841143],
          [116.4684776, 39.9842481],
          [116.4688283, 39.9844944],
          [116.4689972, 39.9845991],
          [116.4695751, 39.9849906],
          [116.4701205, 39.9854367],
          [116.4704466, 39.9856514],
          [116.4707889, 39.9858555],
          [116.4715114, 39.9863744],
          [116.4717284, 39.9865014],
          [116.4718368, 39.9865688],
          [116.4738369, 39.9884071],
          [116.4739084, 39.9884726],
          [116.4756064, 39.9900272],
          [116.4756427, 39.9900605],
          [116.4761855, 39.9905574]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '171860084',
      code: 5115,
      fclass: 'tertiary',
      name: '阜通东大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4758491, 39.9907625],
          [116.475264, 39.9902384],
          [116.4734795, 39.9886397],
          [116.4720232, 39.9873345],
          [116.4714537, 39.9868235],
          [116.4713707, 39.9867484],
          [116.4711946, 39.9865889],
          [116.4705176, 39.9860644],
          [116.4701571, 39.985823],
          [116.4698778, 39.9856064],
          [116.4692833, 39.985146],
          [116.4687189, 39.9847255],
          [116.4685891, 39.9846306],
          [116.4683066, 39.9844361],
          [116.4681022, 39.9842716],
          [116.4678726, 39.9840865],
          [116.4671877, 39.9834679],
          [116.4665399, 39.9828809],
          [116.4663012, 39.9826646],
          [116.4662398, 39.9826088],
          [116.4646496, 39.9811678],
          [116.4630144, 39.9796858],
          [116.4614689, 39.9782852]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '171861194',
      code: 5115,
      fclass: 'tertiary',
      name: '阜安路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.476256, 39.9963439],
          [116.4766108, 39.9967173],
          [116.4776132, 39.9976294],
          [116.4777802, 39.9977735],
          [116.4791529, 39.9990888],
          [116.4803082, 40.0001282],
          [116.4814084, 40.0011571]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '172936630',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.48384, 39.9653474],
          [116.4838524, 39.9652227],
          [116.4838602, 39.9651447],
          [116.4843397, 39.9647453],
          [116.4853672, 39.964054],
          [116.4863508, 39.9634209],
          [116.487445, 39.9626968],
          [116.4890466, 39.9616538],
          [116.4898479, 39.9611423],
          [116.4899668, 39.9610664],
          [116.4912403, 39.9602535],
          [116.4917707, 39.9599149],
          [116.4917979, 39.9598248],
          [116.491945, 39.9593378]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '172936640',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5127873, 39.9759974],
          [116.5131243, 39.9756024],
          [116.5147476, 39.9734083],
          [116.5159161, 39.9718105]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '172944166',
      code: 5115,
      fclass: 'tertiary',
      name: '北皋街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.504948, 40.0067243],
          [116.505759, 40.00618],
          [116.5092219, 40.0061311],
          [116.5096184, 40.0061255],
          [116.511424, 40.00621],
          [116.5115968, 40.0062181],
          [116.5141073, 40.0066125],
          [116.51722, 40.0063945],
          [116.5187178, 40.0062683],
          [116.5197905, 40.0061716],
          [116.5218168, 40.0060126],
          [116.5235235, 40.0060728],
          [116.5239143, 40.0061425]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '172944168',
      code: 5114,
      fclass: 'secondary',
      name: '南皋路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5111564, 40.0001111],
          [116.510903, 40.0000834],
          [116.5099516, 39.9999301],
          [116.509441, 39.9998389],
          [116.5076525, 39.9995332],
          [116.5075893, 39.999526],
          [116.504566, 39.9994245],
          [116.5027324, 39.9993562],
          [116.5020112, 39.9993514],
          [116.5016556, 39.9993643],
          [116.5011604, 39.9994058],
          [116.5008395, 39.999456],
          [116.5005094, 39.9995219],
          [116.5000003, 39.9996489],
          [116.4999348, 39.9996728],
          [116.4995239, 39.9998225],
          [116.4990615, 40.0000297],
          [116.4981676, 40.0005058]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '173019246',
      code: 5115,
      fclass: 'tertiary',
      name: '阜通西大街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4761146, 39.9964473],
          [116.4757277, 39.996649],
          [116.4753728, 39.9968017],
          [116.4750099, 39.9969514],
          [116.474647, 39.9970767],
          [116.4741733, 39.9971704],
          [116.4736995, 39.9972275],
          [116.4732497, 39.9972601],
          [116.4727535, 39.9972391],
          [116.4726111, 39.9972514],
          [116.4724013, 39.9972586],
          [116.4720028, 39.9972104],
          [116.4716161, 39.997146],
          [116.4712014, 39.9970332],
          [116.4702305, 39.9966334],
          [116.4693156, 39.9960275],
          [116.4678825, 39.9947217],
          [116.4663204, 39.9932984],
          [116.4650608, 39.9921581]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '173019252',
      code: 5115,
      fclass: 'tertiary',
      name: '阜荣街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.47736, 39.9861144],
          [116.4759775, 39.9870141],
          [116.475419, 39.9873775],
          [116.4752949, 39.9874583],
          [116.4747179, 39.9878338],
          [116.4738369, 39.9884071],
          [116.4737241, 39.9884805],
          [116.4736107, 39.9885543],
          [116.4734795, 39.9886397],
          [116.4714331, 39.9899714],
          [116.4685673, 39.9918311],
          [116.4684677, 39.9918961],
          [116.4663204, 39.9932984]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '173019258',
      code: 5115,
      fclass: 'tertiary',
      name: '花家地南街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4612103, 39.9833426],
          [116.460941, 39.9831976],
          [116.460407, 39.9830754],
          [116.4600767, 39.9830342],
          [116.4598763, 39.983093],
          [116.4592892, 39.9834911],
          [116.4584419, 39.9840534],
          [116.4574492, 39.9847123],
          [116.4571347, 39.9849092],
          [116.4570458, 39.9849756]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '173019927',
      code: 5122,
      fclass: 'residential',
      name: '方园南街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.480644, 39.9732573],
          [116.4768127, 39.9697005]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '173019932',
      code: 5115,
      fclass: 'tertiary',
      name: '将台西路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4686342, 39.973675],
          [116.4688698, 39.9735067],
          [116.4705674, 39.9723677],
          [116.4719569, 39.9714581],
          [116.4730891, 39.970716],
          [116.4731405, 39.9706832],
          [116.4732225, 39.9706401],
          [116.4734242, 39.9705344],
          [116.4741535, 39.9701892],
          [116.4742436, 39.9701585],
          [116.4747312, 39.9699923],
          [116.4761903, 39.9696963],
          [116.476684, 39.9696997],
          [116.4768127, 39.9697005],
          [116.4774027, 39.9697045],
          [116.4789047, 39.9697868],
          [116.4801749, 39.9698048],
          [116.4806428, 39.9698114],
          [116.4813831, 39.9698196],
          [116.482522, 39.9698768],
          [116.4836876, 39.9699429],
          [116.48383, 39.9699461]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176576438',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4270496, 39.9876792],
          [116.4289147, 39.9877395],
          [116.4295304, 39.9877472],
          [116.4304464, 39.9877454],
          [116.4310997, 39.9876907],
          [116.4318676, 39.987589],
          [116.4325646, 39.9874687],
          [116.4334295, 39.987237],
          [116.4348324, 39.9867662],
          [116.4357232, 39.9864214]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176576439',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4444103, 39.9820649],
          [116.4445553, 39.9819715],
          [116.4446519, 39.9819093],
          [116.445547, 39.9813327],
          [116.4462525, 39.9808781],
          [116.4464152, 39.9807733],
          [116.4467243, 39.9805741],
          [116.4482867, 39.9795676],
          [116.4485898, 39.9793725],
          [116.4486735, 39.9793186],
          [116.4487685, 39.9792555],
          [116.4488733, 39.9791866],
          [116.4491412, 39.9790104],
          [116.4514189, 39.9775122],
          [116.4533088, 39.976269],
          [116.4535745, 39.9760942]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176576440',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4358251, 39.9865741],
          [116.4349784, 39.9869057],
          [116.4334577, 39.9874192],
          [116.43261, 39.9876472],
          [116.4318913, 39.9877768],
          [116.4311242, 39.9878875],
          [116.4305133, 39.9879189],
          [116.4296525, 39.9879298],
          [116.4282218, 39.9879188],
          [116.4270528, 39.987878],
          [116.4186235, 39.9876061]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176576442',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4370416, 39.9860953],
          [116.4358251, 39.9865741]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176576443',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4357232, 39.9864214],
          [116.4369552, 39.9859447]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176576444',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4441149, 39.982464],
          [116.4482549, 39.9797628]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176576445',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4263519, 39.9874976],
          [116.4279284, 39.9875413],
          [116.4295233, 39.9875856],
          [116.4301914, 39.9875606],
          [116.4304264, 39.9875361],
          [116.4310416, 39.9874719],
          [116.4317709, 39.9873624],
          [116.4323087, 39.9872003],
          [116.4327907, 39.9869761],
          [116.4332931, 39.9866729],
          [116.4339577, 39.9861841],
          [116.4343636, 39.9858914],
          [116.4345623, 39.9856986],
          [116.4346846, 39.9855059],
          [116.4347277, 39.9853533],
          [116.4347596, 39.9851918],
          [116.4366885, 39.9844507],
          [116.437002, 39.9846298],
          [116.4373389, 39.9847636],
          [116.4377154, 39.9848417],
          [116.438022, 39.9848747],
          [116.4383659, 39.9848717],
          [116.4386778, 39.9848395],
          [116.4389858, 39.9847913],
          [116.4395865, 39.9846452],
          [116.4399715, 39.9845266],
          [116.4411592, 39.9840693],
          [116.4417686, 39.9837668],
          [116.4419672, 39.9836388],
          [116.4444103, 39.9820649]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176576447',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4369552, 39.9859447],
          [116.4403984, 39.9846121],
          [116.4411642, 39.9842722],
          [116.4416847, 39.9840231],
          [116.4421056, 39.983775],
          [116.4425205, 39.9835043],
          [116.4441149, 39.982464]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176579600',
      code: 5112,
      fclass: 'trunk',
      name: '东四环北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4619131, 39.9708154],
          [116.4635507, 39.9697244]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '176579602',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4614718, 39.9711045],
          [116.4619131, 39.9708154]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '177931393',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.438467, 39.9686033],
          [116.438079, 39.9684194],
          [116.4378761, 39.9683423],
          [116.4376157, 39.96827],
          [116.4370613, 39.9680337],
          [116.4367913, 39.9678672],
          [116.4365581, 39.9676872],
          [116.4343478, 39.9657804]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '178747920',
      code: 5122,
      fclass: 'residential',
      name: '七圣路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4296724, 39.9678282],
          [116.4295387, 39.9673243],
          [116.4295304, 39.9672044],
          [116.4295973, 39.9656169],
          [116.4296883, 39.9634587],
          [116.4297189, 39.9624668],
          [116.4297681, 39.9623769],
          [116.4298488, 39.9622975],
          [116.4302546, 39.962011],
          [116.4303148, 39.9619748]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '181460545',
      code: 5114,
      fclass: 'secondary',
      name: '芳园西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.470139, 39.9679444],
          [116.468777, 39.9667626]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '181460561',
      code: 5114,
      fclass: 'secondary',
      name: '霄云路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4682625, 39.9663756],
          [116.466508, 39.9653282],
          [116.4659502, 39.9649457],
          [116.4658688, 39.9648898],
          [116.465268, 39.964366],
          [116.465072, 39.9641497],
          [116.4646748, 39.9637113],
          [116.4642635, 39.96315],
          [116.4634064, 39.9619517],
          [116.4628379, 39.9613611],
          [116.4616004, 39.9602471],
          [116.4597808, 39.9586053],
          [116.4563229, 39.9554887],
          [116.4562417, 39.9554121],
          [116.4548326, 39.9541408],
          [116.4538336, 39.9532467],
          [116.4537604, 39.9531812]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '187024627',
      code: 5115,
      fclass: 'tertiary',
      name: '小营北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4182175, 39.9974021],
          [116.4183578, 39.9973995],
          [116.4190842, 39.997389],
          [116.4199781, 39.9973308],
          [116.4240817, 39.9969883],
          [116.4253254, 39.9969247],
          [116.4271517, 39.9969005],
          [116.4291598, 39.9968739],
          [116.4311395, 39.9968477],
          [116.4312518, 39.9968472]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '190527232',
      code: 5114,
      fclass: 'secondary',
      name: '南影路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5111564, 40.0001111],
          [116.5111711, 40.0000446],
          [116.5112958, 39.9994372],
          [116.51136, 39.9992415],
          [116.5114865, 39.9990666],
          [116.5118151, 39.9988026],
          [116.512864, 39.9980723],
          [116.5139923, 39.9972915],
          [116.5150129, 39.9966151]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '190527233',
      code: 5114,
      fclass: 'secondary',
      name: '首都机场辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4885153, 39.9917654],
          [116.4839155, 39.987646],
          [116.4836985, 39.9873776]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199823445',
      code: 5111,
      fclass: 'motorway',
      name: '首都机场高速公路',
      ref: 'S12',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4639909, 39.9704082],
          [116.4681939, 39.9742472],
          [116.4726477, 39.9782416]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199823447',
      code: 5112,
      fclass: 'trunk',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4640636, 39.9696444],
          [116.4637625, 39.9698497],
          [116.4621224, 39.9709314],
          [116.4608786, 39.9717355]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199827587',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4642497, 39.9914217],
          [116.4629333, 39.9923333],
          [116.4626413, 39.9926115],
          [116.4623934, 39.9929552],
          [116.4622297, 39.9932856],
          [116.4621333, 39.9935833],
          [116.4620741, 39.9939737],
          [116.4620673, 39.9942031],
          [116.4620058, 39.9959123],
          [116.461821, 40.0002156],
          [116.4618176, 40.0003037],
          [116.4618135, 40.0003803],
          [116.4617311, 40.0020911],
          [116.4616902, 40.0036523],
          [116.4616668, 40.0041492]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199827588',
      code: 5114,
      fclass: 'secondary',
      name: '花家地街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4595013, 39.9868729],
          [116.4607476, 39.9860754],
          [116.4627707, 39.9847809],
          [116.464451, 39.983763],
          [116.4662398, 39.9826088],
          [116.4663288, 39.9825514],
          [116.4664083, 39.9825001],
          [116.466488, 39.9824487],
          [116.4687885, 39.9809643],
          [116.4708973, 39.979605],
          [116.4719446, 39.9788921]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199827591',
      code: 5114,
      fclass: 'secondary',
      name: '广顺南大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.464136, 39.9912482],
          [116.4674889, 39.989047],
          [116.4690653, 39.9880219],
          [116.469405, 39.987801],
          [116.4711946, 39.9865889],
          [116.4712994, 39.9865179],
          [116.4714079, 39.9864444],
          [116.4715114, 39.9863744],
          [116.4732635, 39.9851877],
          [116.4744204, 39.9844041],
          [116.4746979, 39.9842106],
          [116.4750835, 39.9839374]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199827592',
      code: 5115,
      fclass: 'tertiary',
      name: '阜通西大街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4650608, 39.9921581],
          [116.4643348, 39.9915008],
          [116.4642497, 39.9914217]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199829833',
      code: 5115,
      fclass: 'tertiary',
      name: '宏昌路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4620058, 39.9959123],
          [116.4621393, 39.9959197],
          [116.4623147, 39.9959321],
          [116.4624346, 39.9959567],
          [116.462549, 39.9959976],
          [116.4626455, 39.9960537],
          [116.4629218, 39.9962683],
          [116.4655009, 39.9985169],
          [116.4662895, 39.9992877],
          [116.4666888, 39.999678],
          [116.4672157, 40.000193],
          [116.4672973, 40.000294],
          [116.4673538, 40.0004047],
          [116.4672692, 40.0005551]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199830204',
      code: 5115,
      fclass: 'tertiary',
      name: '溪阳中路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4670793, 40.0050264],
          [116.4657422, 40.0049593],
          [116.4633156, 40.004838],
          [116.4616592, 40.0047427]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199830767',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.46097, 40.0141089],
          [116.460997, 40.0135393]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199830769',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4612492, 40.0135487],
          [116.4612259, 40.0141165]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199830770',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.460997, 40.0135393],
          [116.4610247, 40.0129531],
          [116.4610486, 40.0123948],
          [116.461053, 40.0122912],
          [116.4610567, 40.0122053],
          [116.4611373, 40.0103201],
          [116.4611422, 40.0101795]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199830771',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4612259, 40.0141165],
          [116.46121, 40.014506],
          [116.4612058, 40.0145769],
          [116.4610549, 40.0167124],
          [116.4610301, 40.0172221],
          [116.4610389, 40.0180402],
          [116.4609498, 40.0191583]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199896001',
      code: 5115,
      fclass: 'tertiary',
      name: '屏翠西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4612936, 40.0128824],
          [116.4639421, 40.0128528],
          [116.4646373, 40.012873],
          [116.4668206, 40.0130192]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199903005',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4612898, 40.0073298],
          [116.4613958, 40.004725],
          [116.4614094, 40.0044894]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '199903012',
      code: 5115,
      fclass: 'tertiary',
      name: '辛店路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4494843, 40.0066343],
          [116.4495655, 40.0066508],
          [116.4503704, 40.0068246],
          [116.4507728, 40.0068572],
          [116.4516327, 40.006876],
          [116.4518806, 40.0068949]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '205500980',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4327065, 39.9812811],
          [116.4318809, 39.9799917],
          [116.4305494, 39.9780628],
          [116.4301557, 39.9774925],
          [116.4290412, 39.9757466]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '205501140',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.428253, 39.9735257],
          [116.4293328, 39.9751213]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '210697372',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4839108, 39.965803],
          [116.4839044, 39.9659756],
          [116.4838987, 39.9661314],
          [116.4838929, 39.9662872]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '219923065',
      code: 5122,
      fclass: 'residential',
      name: '南皋路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5111564, 40.0001111],
          [116.5112806, 40.000122],
          [116.5117008, 40.0001608],
          [116.5161066, 40.0005009],
          [116.5162406, 40.000527],
          [116.516354, 40.0005788],
          [116.5164233, 40.0006547],
          [116.5164731, 40.0007092]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '219923071',
      code: 5115,
      fclass: 'tertiary',
      name: '庄园西路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5222202, 39.9815049],
          [116.5222565, 39.9819131],
          [116.5221009, 39.9820898],
          [116.5210495, 39.9821679],
          [116.5196867, 39.9823159],
          [116.5181728, 39.9825243],
          [116.5171332, 39.9827434],
          [116.5158243, 39.9833106],
          [116.5143008, 39.9842642],
          [116.5128202, 39.9852918],
          [116.5122409, 39.9859084],
          [116.5112216, 39.9872812],
          [116.5108089, 39.9881679],
          [116.5107395, 39.9883584],
          [116.5104481, 39.9891582],
          [116.5102623, 39.990774]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '221266740',
      code: 5115,
      fclass: 'tertiary',
      name: '望京东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4818147, 40.0041721],
          [116.4830787, 40.0028122],
          [116.4831784, 40.0027049],
          [116.4833837, 40.002484],
          [116.4843909, 40.0015426],
          [116.4864119, 39.9995149],
          [116.4867903, 39.9989951],
          [116.4870577, 39.9985727],
          [116.4871918, 39.9982604],
          [116.4874499, 39.9976732],
          [116.4875509, 39.9970685],
          [116.4875428, 39.9969024],
          [116.4875213, 39.9964621],
          [116.4874322, 39.9959385],
          [116.4871928, 39.9953161],
          [116.4867323, 39.9945731],
          [116.4859205, 39.9937275]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '221267221',
      code: 5121,
      fclass: 'unclassified',
      name: '远安路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4872101, 39.9986393],
          [116.4870577, 39.9985727],
          [116.4849674, 39.9978614],
          [116.4840669, 39.997507],
          [116.4818357, 39.9966287],
          [116.4817276, 39.9965862],
          [116.4797716, 39.9962871]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '221926322',
      code: 5115,
      fclass: 'tertiary',
      name: '容创路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4612058, 40.0145769],
          [116.4613789, 40.0146078],
          [116.463184, 40.0146571],
          [116.4638383, 40.0146821],
          [116.4651738, 40.0147377],
          [116.46854, 40.0146895],
          [116.4716943, 40.0146725],
          [116.4720372, 40.0146659],
          [116.4726372, 40.0146332],
          [116.4732723, 40.0145581],
          [116.4746254, 40.0143853],
          [116.4756353, 40.0141646],
          [116.4766067, 40.0139214],
          [116.4769278, 40.0138062]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '229125969',
      code: 5121,
      fclass: 'unclassified',
      name: '京旺北街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5184117, 40.0114067],
          [116.5195502, 40.0115745],
          [116.521508, 40.0123075],
          [116.5217184, 40.0124032],
          [116.5230614, 40.0127334],
          [116.5246943, 40.0134447],
          [116.5247748, 40.0134759]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '229125973',
      code: 5115,
      fclass: 'tertiary',
      name: '首都机场辅路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5023845, 40.0043659],
          [116.504948, 40.0067243],
          [116.5088794, 40.01031],
          [116.5122437, 40.0132268],
          [116.5126758, 40.0134821],
          [116.5165353, 40.0149129],
          [116.5170478, 40.0150931],
          [116.5207246, 40.0164076],
          [116.522081, 40.0168845]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '229125977',
      code: 5115,
      fclass: 'tertiary',
      name: '京旺中路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5197905, 40.0061716],
          [116.519768, 40.0079792],
          [116.5196304, 40.009843],
          [116.5184117, 40.0114067],
          [116.5177792, 40.012661],
          [116.5175432, 40.0131371],
          [116.5174932, 40.0133408],
          [116.5165353, 40.0149129]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '230683394',
      code: 5121,
      fclass: 'unclassified',
      name: '坝河北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4707427, 39.9683552],
          [116.4707906, 39.9682966],
          [116.4708978, 39.9682031],
          [116.4737249, 39.9672865],
          [116.4760504, 39.9665593],
          [116.4761418, 39.9665434],
          [116.4762209, 39.9665424],
          [116.4763414, 39.9665442],
          [116.478269, 39.966664],
          [116.4801155, 39.9667891],
          [116.4805968, 39.966862],
          [116.4808333, 39.9668898],
          [116.4835511, 39.9672088],
          [116.4835765, 39.9672125],
          [116.4836684, 39.9672392],
          [116.4837318, 39.9672531],
          [116.4838717, 39.9672456],
          [116.4839731, 39.9672075],
          [116.4839922, 39.9669185],
          [116.4839949, 39.9665341],
          [116.484062, 39.9664416],
          [116.4842283, 39.9663635],
          [116.4848479, 39.9663964],
          [116.485419, 39.9664477],
          [116.4862706, 39.9665329],
          [116.4879725, 39.9666986],
          [116.4889542, 39.9669082],
          [116.4891406, 39.9669481],
          [116.4893887, 39.9669987],
          [116.4894786, 39.9671578],
          [116.4895912, 39.9673181],
          [116.4900096, 39.9674168],
          [116.4905568, 39.9675319],
          [116.4907446, 39.9676306],
          [116.4909323, 39.9676635],
          [116.4911093, 39.967758],
          [116.4912488, 39.9678156],
          [116.4916136, 39.9678814],
          [116.4919154, 39.9680101],
          [116.4920482, 39.9681349],
          [116.492103, 39.9682397],
          [116.4922527, 39.9683381],
          [116.4923968, 39.9683377],
          [116.4926972, 39.9683213],
          [116.4929668, 39.9683513],
          [116.493227, 39.9683266],
          [116.4935783, 39.9682876],
          [116.4937835, 39.9683624],
          [116.4941027, 39.9684529],
          [116.494387, 39.968457],
          [116.4946445, 39.9684857],
          [116.4951179, 39.9684829],
          [116.4952347, 39.9684693],
          [116.4953093, 39.9684659],
          [116.4956959, 39.9684487],
          [116.4958421, 39.9684376],
          [116.4964458, 39.9684397],
          [116.4967059, 39.9685506]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '230684654',
      code: 5115,
      fclass: 'tertiary',
      name: '南湖西园南路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4469707, 39.9954892],
          [116.4469603, 39.9954431],
          [116.4467866, 39.9947791],
          [116.4467972, 39.9937821],
          [116.4468822, 39.9931249],
          [116.4471224, 39.9922187],
          [116.4476014, 39.9901233],
          [116.4477929, 39.9893342],
          [116.4481454, 39.9883905],
          [116.4491191, 39.987741],
          [116.4493937, 39.987634]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '230684656',
      code: 5115,
      fclass: 'tertiary',
      name: '南湖西园中街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4450377, 39.9929114],
          [116.4458406, 39.9930073],
          [116.4468822, 39.9931249],
          [116.4498439, 39.9933863],
          [116.4511388, 39.9936496],
          [116.4512721, 39.9936687]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237576353',
      code: 5115,
      fclass: 'tertiary',
      name: '西坝河东路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4382248, 39.967381],
          [116.439413, 39.9675284],
          [116.4398987, 39.9675862],
          [116.4408991, 39.967698],
          [116.4414791, 39.9677585],
          [116.4427891, 39.9679151],
          [116.4445823, 39.9681048],
          [116.4452291, 39.9681748],
          [116.4455548, 39.9682101]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581585',
      code: 5115,
      fclass: 'tertiary',
      name: '京承高速辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4298771, 39.9756528],
          [116.4303457, 39.9763159],
          [116.430662, 39.9767774],
          [116.4309609, 39.9772038],
          [116.4319667, 39.978729],
          [116.4322757, 39.979117],
          [116.4328165, 39.9797879],
          [116.432855, 39.9798454],
          [116.4333057, 39.9805179],
          [116.4335374, 39.9810243],
          [116.4338035, 39.9814189],
          [116.4341279, 39.9818893],
          [116.4346908, 39.9827054],
          [116.4353676, 39.983502],
          [116.4358257, 39.9838575],
          [116.4363416, 39.984185],
          [116.4365516, 39.9843507],
          [116.4366885, 39.9844507]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581588',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4293328, 39.9751213],
          [116.4296932, 39.9756734]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581591',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4290412, 39.9757466],
          [116.4286819, 39.9751941]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581593',
      code: 5115,
      fclass: 'tertiary',
      name: '京承高速辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4295171, 39.9751008],
          [116.4298771, 39.9756528]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581594',
      code: 5115,
      fclass: 'tertiary',
      name: '京承高速辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4288607, 39.9757668],
          [116.4284927, 39.9752152]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581595',
      code: 5115,
      fclass: 'tertiary',
      name: '京承高速辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4284927, 39.9752152],
          [116.4280723, 39.9745895],
          [116.4277175, 39.9740254],
          [116.4269366, 39.9729571]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581598',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4296932, 39.9756734],
          [116.4309742, 39.9775684],
          [116.4344506, 39.9827327],
          [116.4356605, 39.9846119]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581599',
      code: 5111,
      fclass: 'motorway',
      name: '京承高速',
      ref: 'S11',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4286819, 39.9751941],
          [116.4280713, 39.9743501],
          [116.4275399, 39.9735833]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581600',
      code: 5114,
      fclass: 'secondary',
      name: '北土城东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.388232, 39.9754384],
          [116.3883806, 39.9754388],
          [116.3885102, 39.9754399],
          [116.391533, 39.9754647],
          [116.3916592, 39.9754658],
          [116.3918145, 39.975467],
          [116.3953436, 39.975496],
          [116.3954225, 39.9754977],
          [116.395545, 39.9755003],
          [116.3957338, 39.9754992],
          [116.3981602, 39.9755102],
          [116.4009157, 39.9755235],
          [116.4010022, 39.9755239],
          [116.4011468, 39.9755246],
          [116.4012513, 39.975527],
          [116.4014222, 39.975528],
          [116.4075841, 39.9755637],
          [116.4111361, 39.9755876],
          [116.4112345, 39.9755886],
          [116.4113654, 39.975592],
          [116.4115048, 39.9755916],
          [116.414282, 39.9756235],
          [116.4182313, 39.9756664],
          [116.4183568, 39.9756678],
          [116.4184936, 39.975669],
          [116.4216323, 39.9756967],
          [116.4249023, 39.9757256],
          [116.425442, 39.9757111],
          [116.4257705, 39.9757009],
          [116.4263885, 39.9756477],
          [116.4291657, 39.9753669]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581601',
      code: 5114,
      fclass: 'secondary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4449115, 39.968792],
          [116.4415095, 39.9712803],
          [116.4414187, 39.9713467]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581602',
      code: 5114,
      fclass: 'secondary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4451636, 39.9683654],
          [116.4452876, 39.9682747],
          [116.4455548, 39.9682101]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581603',
      code: 5114,
      fclass: 'secondary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4455548, 39.9682101],
          [116.4454875, 39.9683707],
          [116.4454293, 39.9684132]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237581604',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.446924, 39.9672092],
          [116.4470295, 39.9670757],
          [116.450764, 39.9646278],
          [116.4538631, 39.9625413]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237582262',
      code: 5114,
      fclass: 'secondary',
      name: '曙光西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.446924, 39.9672092],
          [116.4468235, 39.967116],
          [116.4455746, 39.9660083],
          [116.4452767, 39.9657441],
          [116.443132, 39.9638194],
          [116.44302, 39.9637189],
          [116.4428783, 39.9635918],
          [116.4427564, 39.9634766],
          [116.4409331, 39.9617516],
          [116.4407731, 39.9616002]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237584841',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4381716, 39.9681084],
          [116.4384817, 39.9683329],
          [116.439698, 39.9694959],
          [116.4404507, 39.9702154],
          [116.4413131, 39.97104],
          [116.441397, 39.9711202],
          [116.4415095, 39.9712803],
          [116.441576, 39.97135],
          [116.4422277, 39.9720329],
          [116.4425953, 39.9724299],
          [116.4439356, 39.9738773],
          [116.4449283, 39.9749447]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237584843',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4446896, 39.974835],
          [116.4456845, 39.9759133],
          [116.4487685, 39.9792555]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237584844',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4449283, 39.9749447],
          [116.4458665, 39.9759536],
          [116.4465332, 39.9766704],
          [116.4468738, 39.9770366],
          [116.4488733, 39.9791866],
          [116.4489141, 39.979435]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237584847',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4411308, 39.9713516],
          [116.4410265, 39.9712498],
          [116.4401867, 39.9704297],
          [116.4395484, 39.9698064],
          [116.4393805, 39.9696424],
          [116.4386051, 39.9688853],
          [116.4384898, 39.9687917],
          [116.4383723, 39.9686861],
          [116.4382435, 39.9685961],
          [116.4380831, 39.9684887],
          [116.4378761, 39.9683423]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237584849',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4488181, 39.9794969],
          [116.4485898, 39.9793725],
          [116.4471858, 39.9778559],
          [116.4453109, 39.9758306],
          [116.4442473, 39.9746817],
          [116.4437033, 39.9740939],
          [116.4423218, 39.9726075],
          [116.441325, 39.971535],
          [116.4412408, 39.9714444],
          [116.4411308, 39.9713516]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237584851',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4412122, 39.9712922],
          [116.441108, 39.9711901],
          [116.4403512, 39.9704483],
          [116.4397895, 39.9698978],
          [116.4386859, 39.9688161],
          [116.438467, 39.9686033],
          [116.4382734, 39.968415],
          [116.4381333, 39.9682382],
          [116.438062, 39.9681055],
          [116.4380514, 39.9679746]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '237584854',
      code: 5114,
      fclass: 'secondary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4412122, 39.9712922],
          [116.4412801, 39.9712058],
          [116.441397, 39.9711202],
          [116.4446377, 39.9687501]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '242347378',
      code: 5115,
      fclass: 'tertiary',
      name: '北土城东路辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.425442, 39.9757111],
          [116.4260057, 39.975599],
          [116.4264821, 39.9755286],
          [116.4291034, 39.9752597]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '242347383',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫南街辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4349667, 39.9750649],
          [116.4339626, 39.9751478],
          [116.4335409, 39.9751826],
          [116.4331216, 39.97522],
          [116.4324141, 39.9752757],
          [116.4293124, 39.9755802]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '243914203',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5129404, 40.0178583],
          [116.51057, 40.0143461],
          [116.5102238, 40.0139314],
          [116.5099007, 40.013572]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '244010335',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.476668, 39.9831707],
          [116.4765076, 39.9830347],
          [116.4720069, 39.9789455]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '244010336',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4834329, 39.9865436],
          [116.4834804, 39.9850163],
          [116.4835257, 39.9828234],
          [116.4835597, 39.9810306],
          [116.4835802, 39.9797936],
          [116.4835815, 39.9790659],
          [116.4835782, 39.9789581],
          [116.4835968, 39.9778674],
          [116.4836039, 39.9774489],
          [116.4836396, 39.9753564],
          [116.4836511, 39.9741059],
          [116.4836771, 39.9712808],
          [116.4836876, 39.9699429],
          [116.4837318, 39.9672531],
          [116.4837295, 39.9672271],
          [116.4837688, 39.9664256],
          [116.4837902, 39.9662793]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '244010351',
      code: 5115,
      fclass: 'tertiary',
      name: '南湖南路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4569437, 39.9887742],
          [116.4567972, 39.9920723],
          [116.4567804, 39.9930804],
          [116.4566671, 39.9956471],
          [116.4566601, 39.995755]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '244010352',
      code: 5114,
      fclass: 'secondary',
      name: '广顺南大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.476668, 39.9831707],
          [116.4757185, 39.9838099],
          [116.4755046, 39.9839381],
          [116.475212, 39.9841335]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '244010353',
      code: 5115,
      fclass: 'tertiary',
      name: '望京西路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4509705, 39.9828125],
          [116.450908, 39.9836193],
          [116.4509024, 39.985203],
          [116.4509014, 39.985485],
          [116.4508989, 39.9861812],
          [116.4508955, 39.9871248],
          [116.4509897, 39.9874797]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '244010354',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4819236, 39.9876974],
          [116.4821422, 39.9875499],
          [116.4834104, 39.9866942],
          [116.4834329, 39.9865436]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '244010355',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4835872, 39.9865886],
          [116.4835475, 39.9866574],
          [116.4834721, 39.9868318],
          [116.4831296, 39.9870312],
          [116.4828706, 39.9872131],
          [116.4820258, 39.9877872]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '244031353',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4606604, 40.0191552],
          [116.460763, 40.0181352],
          [116.460769, 40.0172323],
          [116.4607978, 40.016735],
          [116.460837, 40.0160602],
          [116.4609206, 40.014601]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324777',
      code: 5114,
      fclass: 'secondary',
      name: '曙光西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4471056, 39.9671352],
          [116.4471697, 39.9672013],
          [116.4487915, 39.9686531],
          [116.4488203, 39.9686831]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324797',
      code: 5115,
      fclass: 'tertiary',
      name: '北土城东路辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4293124, 39.9755802],
          [116.4264135, 39.9758753],
          [116.4257723, 39.9759555],
          [116.425525, 39.9759519],
          [116.4248977, 39.9759451]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324798',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫中路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4414187, 39.9713467],
          [116.441485, 39.9714171],
          [116.4427038, 39.9727124],
          [116.443855, 39.9739358],
          [116.4446001, 39.9747386],
          [116.4446896, 39.974835]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324799',
      code: 5114,
      fclass: 'secondary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4291657, 39.9753669],
          [116.433527, 39.9749259],
          [116.4350793, 39.9747856],
          [116.4358106, 39.9746929],
          [116.436297, 39.9745836],
          [116.4366642, 39.9744501],
          [116.4369051, 39.9743466],
          [116.437152, 39.9742244],
          [116.4373202, 39.9741293],
          [116.4375496, 39.9739636],
          [116.4393898, 39.9726214],
          [116.4411308, 39.9713516]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324800',
      code: 5114,
      fclass: 'secondary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4414187, 39.9713467],
          [116.4413133, 39.9713913],
          [116.4412408, 39.9714444],
          [116.4402902, 39.9721396],
          [116.4384675, 39.9734725],
          [116.4376666, 39.9740581],
          [116.4374294, 39.9742295],
          [116.4372486, 39.9743317],
          [116.4369933, 39.9744581],
          [116.4366659, 39.9746143],
          [116.4363388, 39.9747148],
          [116.4358506, 39.9748198],
          [116.4354409, 39.9748765]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324801',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4450535, 39.9688036],
          [116.4432174, 39.9701397],
          [116.441576, 39.97135],
          [116.441485, 39.9714171],
          [116.4413978, 39.9714814],
          [116.441325, 39.971535],
          [116.4382677, 39.9737894],
          [116.4377401, 39.9741728],
          [116.4373998, 39.9744137],
          [116.4369793, 39.9746272],
          [116.4365512, 39.9747908],
          [116.4363293, 39.974847],
          [116.4356695, 39.9749909],
          [116.4349667, 39.9750649]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324802',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫南街辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4291034, 39.9752597],
          [116.4301021, 39.9751503],
          [116.4322363, 39.9749536],
          [116.4334126, 39.974825],
          [116.4335209, 39.9748142],
          [116.4351362, 39.9746534],
          [116.4355894, 39.9746019],
          [116.4360894, 39.9744966],
          [116.4366566, 39.9743084],
          [116.4370601, 39.9741112],
          [116.4374432, 39.9738714],
          [116.4389686, 39.9727554],
          [116.4397908, 39.9721538],
          [116.4410265, 39.9712498],
          [116.441108, 39.9711901],
          [116.4411966, 39.9711253],
          [116.4413131, 39.97104],
          [116.4444657, 39.9687334]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324803',
      code: 5114,
      fclass: 'secondary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4354409, 39.9748765],
          [116.4335344, 39.9750626],
          [116.4292487, 39.9754814]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324804',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4449764, 39.9683598],
          [116.4452291, 39.9681748],
          [116.4468235, 39.967116],
          [116.4469717, 39.9670176],
          [116.4537244, 39.9625328],
          [116.4537987, 39.9624835]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '251324805',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫南街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4540297, 39.962691],
          [116.4508999, 39.9647488],
          [116.4471697, 39.9672013],
          [116.447019, 39.9673027],
          [116.4457953, 39.9682354],
          [116.4456199, 39.9683915],
          [116.4455659, 39.9684308]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '277744185',
      code: 5121,
      fclass: 'unclassified',
      name: '河荫中路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4765515, 40.0032314],
          [116.4771984, 40.0031464],
          [116.4785263, 40.0028883],
          [116.4795337, 40.0023663],
          [116.4812807, 40.0012543],
          [116.4814084, 40.0011571]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '297832767',
      code: 5114,
      fclass: 'secondary',
      name: '阜安西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4665164, 39.9899521],
          [116.4666162, 39.9900401],
          [116.4685673, 39.9918311],
          [116.4715311, 39.9945153],
          [116.472096, 39.995039],
          [116.4724436, 39.9954437],
          [116.4727402, 39.9959208],
          [116.4728172, 39.9964602],
          [116.4727535, 39.9972391],
          [116.4726447, 39.9988681],
          [116.4726386, 39.9990083],
          [116.4725542, 40.0006738],
          [116.4724689, 40.0019418],
          [116.4723805, 40.0035894]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '337511665',
      code: 5121,
      fclass: 'unclassified',
      name: '锦轩街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.454713, 39.9769143],
          [116.4541183, 39.9772721],
          [116.4540123, 39.977334],
          [116.4539191, 39.9776365],
          [116.4535307, 39.9778956],
          [116.4532485, 39.9780647],
          [116.4530642, 39.9782234],
          [116.4529561, 39.9783383],
          [116.4526185, 39.9787345],
          [116.452356, 39.9789046],
          [116.4498607, 39.9805217]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '344054171',
      code: 5141,
      fclass: 'service',
      name: '环博路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5058429, 39.9906601],
          [116.508186, 39.9906578],
          [116.5083196, 39.990661],
          [116.5084122, 39.9906582],
          [116.5084766, 39.9906564],
          [116.5085267, 39.9906571],
          [116.5087138, 39.9906588],
          [116.509306, 39.9906715],
          [116.5094703, 39.99073],
          [116.5095683, 39.9907365],
          [116.5098542, 39.99075],
          [116.5102623, 39.990774]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '344054173',
      code: 5115,
      fclass: 'tertiary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5009054, 39.9867858],
          [116.500285, 39.9867888],
          [116.4989708, 39.9867535],
          [116.4978195, 39.9867323],
          [116.4971192, 39.9867274],
          [116.496674, 39.9867244],
          [116.4959874, 39.9867196],
          [116.4943995, 39.9867086],
          [116.4940078, 39.9867059],
          [116.4929328, 39.9866993]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '344478672',
      code: 5115,
      fclass: 'tertiary',
      name: '望京东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4785377, 40.007422],
          [116.4792796, 40.0066517],
          [116.4811295, 40.0047711],
          [116.4818147, 40.0041721]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '344478673',
      code: 5114,
      fclass: 'secondary',
      name: '望京北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4668248, 40.0101624],
          [116.4684854, 40.0101553],
          [116.4694796, 40.010159],
          [116.4719588, 40.0101406],
          [116.4721338, 40.0101317],
          [116.4731481, 40.0101112],
          [116.4740678, 40.0099986],
          [116.4748514, 40.0098165],
          [116.4755982, 40.0095467],
          [116.4763596, 40.0091861],
          [116.4771436, 40.0087047],
          [116.4785377, 40.007422]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '355996063',
      code: 5115,
      fclass: 'tertiary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4835872, 39.9865886],
          [116.4854037, 39.9866058],
          [116.4874225, 39.986625],
          [116.4903026, 39.9866482],
          [116.4911589, 39.9866551],
          [116.4926016, 39.9866667],
          [116.4929328, 39.9866993]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '433775607',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽东二路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4763596, 40.0091861],
          [116.476371, 40.0073887],
          [116.4764676, 40.0060188],
          [116.4764754, 40.005217],
          [116.4764995, 40.0042896]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '435480702',
      code: 5115,
      fclass: 'tertiary',
      name: '望京东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4819256, 40.004236],
          [116.4813173, 40.0048491],
          [116.4795867, 40.006612],
          [116.4786988, 40.0075103]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '435480703',
      code: 5114,
      fclass: 'secondary',
      name: '望京北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4786988, 40.0075103],
          [116.4779305, 40.0083095],
          [116.4773833, 40.0087409],
          [116.476977, 40.0090265],
          [116.4764365, 40.0093222],
          [116.4759712, 40.0095668],
          [116.4753888, 40.0097912],
          [116.4749033, 40.0099494],
          [116.4744339, 40.0100748],
          [116.4741321, 40.0101177],
          [116.4735756, 40.0102165],
          [116.4729238, 40.0102679],
          [116.4721772, 40.0102936],
          [116.472113, 40.0102921],
          [116.4719727, 40.0102872],
          [116.4718788, 40.0102896],
          [116.4710999, 40.0102987],
          [116.4695315, 40.0102877],
          [116.4668246, 40.0103041]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '441729863',
      code: 5122,
      fclass: 'residential',
      name: '七圣中街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4278085, 39.9715451],
          [116.4292879, 39.9715275],
          [116.4312359, 39.9715043],
          [116.4328121, 39.9714387],
          [116.4348685, 39.9713839],
          [116.4365737, 39.9715306],
          [116.4373274, 39.9714159],
          [116.438132, 39.9708863],
          [116.4395484, 39.9698064]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '441729864',
      code: 5122,
      fclass: 'residential',
      name: '丰和园路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4373274, 39.9714159],
          [116.4389686, 39.9727554]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '474563700',
      code: 5121,
      fclass: 'unclassified',
      name: '河荫中路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4814084, 40.0011571],
          [116.4820373, 40.0009216],
          [116.482951, 40.000356],
          [116.4834536, 40.000068],
          [116.4843076, 39.9993379],
          [116.4848891, 39.9983764],
          [116.4849674, 39.9978614]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '474563956',
      code: 5115,
      fclass: 'tertiary',
      name: '溪阳东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4845764, 40.0058886],
          [116.4843545, 40.005802],
          [116.4841079, 40.0055043],
          [116.4840298, 40.005273],
          [116.484001, 40.0049898]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '474564176',
      code: 5115,
      fclass: 'tertiary',
      name: '阜安路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.484001, 40.0049898],
          [116.484021, 40.0043177],
          [116.4839582, 40.0039281],
          [116.4838254, 40.0035671],
          [116.483664, 40.0033744],
          [116.4834618, 40.0031792],
          [116.4832242, 40.0029583],
          [116.4830787, 40.0028122]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '478368008',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5099007, 40.013572],
          [116.5095165, 40.0131822],
          [116.5092867, 40.0129791],
          [116.507169, 40.0111068],
          [116.5064732, 40.0104674],
          [116.5063719, 40.0103743],
          [116.5062272, 40.0102414],
          [116.502149, 40.0066172],
          [116.500873, 40.0055451],
          [116.5004575, 40.0051952],
          [116.5000232, 40.0048302],
          [116.4999601, 40.0047772],
          [116.4998536, 40.0046876]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '534224179',
      code: 5115,
      fclass: 'tertiary',
      name: '望京街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4743274, 39.9927292],
          [116.4715311, 39.9945153],
          [116.4714338, 39.9945865],
          [116.4707598, 39.9950794],
          [116.4693156, 39.9960275],
          [116.4678627, 39.9969618],
          [116.4672559, 39.9973595],
          [116.4655009, 39.9985169]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '541459586',
      code: 5114,
      fclass: 'secondary',
      name: '利泽西二路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4566655, 40.0083013],
          [116.4565971, 40.0100674],
          [116.4565932, 40.0101693]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '541459587',
      code: 5114,
      fclass: 'secondary',
      name: '利泽西二路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4567047, 40.0078067],
          [116.4566655, 40.0083013]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '571518214',
      code: 5112,
      fclass: 'trunk',
      name: '京密路 Jingmi Road',
      ref: 'G101',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4720439, 39.9788255],
          [116.4721092, 39.9788809]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '571518215',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4721092, 39.9788809],
          [116.4766534, 39.9829673],
          [116.4767855, 39.983082],
          [116.4792215, 39.9852099],
          [116.4811584, 39.986993],
          [116.4819236, 39.9876974]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '571518216',
      code: 5112,
      fclass: 'trunk',
      name: '京密路 Jingmi Road',
      ref: 'G101',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4720069, 39.9789455],
          [116.4719446, 39.9788921]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '587347187',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5188518, 39.9671537],
          [116.5191994, 39.9664922]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '587347188',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5191994, 39.9664922],
          [116.5202572, 39.964298],
          [116.5214583, 39.9614822],
          [116.5217133, 39.9608546],
          [116.5227944, 39.9582671],
          [116.5229723, 39.9578465]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '653309112',
      code: 5113,
      fclass: 'primary',
      name: '辛店路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4326962, 40.0069656],
          [116.4328307, 40.0069491],
          [116.4368542, 40.0067947],
          [116.439505, 40.0066556],
          [116.4400407, 40.0066275],
          [116.4410181, 40.0065945],
          [116.4418523, 40.0066],
          [116.4420528, 40.0066024],
          [116.4427725, 40.006611],
          [116.4440077, 40.0066604]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '653309231',
      code: 5113,
      fclass: 'primary',
      name: '北湖渠路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4326962, 40.0069656],
          [116.4326782, 40.006102],
          [116.4326271, 40.0035882],
          [116.4326083, 40.0032187],
          [116.432581, 40.0029327],
          [116.4325339, 40.0026715],
          [116.4324328, 40.0022866],
          [116.4322111, 40.0012995]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '722816075',
      code: 5114,
      fclass: 'secondary',
      name: '广泽路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4718488, 40.0134304],
          [116.4718644, 40.0131815],
          [116.4718797, 40.0128575],
          [116.4718948, 40.0124607],
          [116.4719657, 40.0111339],
          [116.4719824, 40.0107763],
          [116.4719727, 40.0102872],
          [116.4719588, 40.0101406]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '722816092',
      code: 5114,
      fclass: 'secondary',
      name: '广泽桥',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.47172, 40.0159442],
          [116.4717415, 40.0154833],
          [116.4717779, 40.0148196],
          [116.4718488, 40.0134304]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '742470687',
      code: 5115,
      fclass: 'tertiary',
      name: '北三环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4275243, 39.9679704],
          [116.4280237, 39.9679903],
          [116.4287855, 39.9679784],
          [116.4296724, 39.9678282],
          [116.4308672, 39.9674805],
          [116.4315775, 39.9671228],
          [116.4329246, 39.9662437],
          [116.4331873, 39.9660722],
          [116.434121, 39.9654592]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '742470689',
      code: 5112,
      fclass: 'trunk',
      name: '北三环',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4269505, 39.9680911],
          [116.4276439, 39.968106]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '742470691',
      code: 5112,
      fclass: 'trunk',
      name: '北三环',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4276439, 39.968106],
          [116.4284169, 39.9681297],
          [116.4289175, 39.9680968],
          [116.4293365, 39.9680353],
          [116.4298675, 39.9679359],
          [116.4301501, 39.9678656],
          [116.4309206, 39.967595],
          [116.431647, 39.967233],
          [116.4342269, 39.9655691]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '742470692',
      code: 5112,
      fclass: 'trunk',
      name: '北三环',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4276597, 39.968246],
          [116.4269445, 39.9682249]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '759027417',
      code: 5115,
      fclass: 'tertiary',
      name: '北三环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4280706, 39.968459],
          [116.4274903, 39.9684335]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '759033263',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫北街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4506706, 39.9689484],
          [116.4499524, 39.9694171]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '759033264',
      code: 5115,
      fclass: 'tertiary',
      name: '太阳宫北街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4566488, 39.965048],
          [116.4553814, 39.9658694],
          [116.4534906, 39.9671031],
          [116.4506815, 39.9689407],
          [116.4506706, 39.9689484]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '786589555',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽中二路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4670904, 40.0048782],
          [116.4670793, 40.0050264],
          [116.4670043, 40.006338],
          [116.4669876, 40.0073196],
          [116.4668248, 40.0101624],
          [116.4668246, 40.0103041],
          [116.4668206, 40.0130192]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '786589556',
      code: 5115,
      fclass: 'tertiary',
      name: '利泽中二路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4671241, 40.0044298],
          [116.4670904, 40.0048782]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '821799842',
      code: 5115,
      fclass: 'tertiary',
      name: '将台路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4838136, 39.9711655],
          [116.4836771, 39.9712808],
          [116.4830589, 39.9716837],
          [116.4824221, 39.9720986],
          [116.4817727, 39.9725218],
          [116.4810016, 39.9730243],
          [116.480644, 39.9732573],
          [116.4799799, 39.97369],
          [116.4793578, 39.9740929],
          [116.4787717, 39.9744853],
          [116.4780283, 39.974983],
          [116.4779186, 39.9750498],
          [116.4778715, 39.97508],
          [116.4763869, 39.9760335],
          [116.4753841, 39.9766775],
          [116.474692, 39.9771386],
          [116.4741073, 39.9775244],
          [116.47353, 39.9779128]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '821799843',
      code: 5114,
      fclass: 'secondary',
      name: '酒仙桥路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4838136, 39.9711655],
          [116.4838324, 39.9725546],
          [116.4838223, 39.9734955],
          [116.4837914, 39.9753551],
          [116.4837823, 39.9756811],
          [116.4837482, 39.9776181],
          [116.483787, 39.9789797],
          [116.483783, 39.9790399],
          [116.4837629, 39.9794453],
          [116.4837408, 39.9812183],
          [116.4837186, 39.9829918],
          [116.4836742, 39.984342],
          [116.4836482, 39.9849844],
          [116.4835872, 39.9865886]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '821799844',
      code: 5112,
      fclass: 'trunk',
      name: '京密路',
      ref: 'G101;G111',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4671608, 39.9745307],
          [116.4668608, 39.9742829],
          [116.4654765, 39.9729842],
          [116.463136, 39.9709545],
          [116.4620245, 39.9699761],
          [116.4603178, 39.9684239]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '821799845',
      code: 5115,
      fclass: 'tertiary',
      name: '花家地南街',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4614689, 39.9782852],
          [116.4606008, 39.9788493],
          [116.4604258, 39.9790982],
          [116.4603262, 39.9792399],
          [116.4602666, 39.9799387],
          [116.4602929, 39.9802713],
          [116.4604026, 39.9816589],
          [116.4604248, 39.9819408],
          [116.4607886, 39.9827301]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '821799846',
      code: 5115,
      fclass: 'tertiary',
      name: '阜通东大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4614689, 39.9782852],
          [116.4599428, 39.9769021],
          [116.4595865, 39.9766385],
          [116.4594344, 39.9765192],
          [116.459226, 39.9763722],
          [116.4577046, 39.9749688],
          [116.457332, 39.974653],
          [116.457178, 39.9746061],
          [116.4570452, 39.9745966],
          [116.4569024, 39.9745973]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '821799847',
      code: 5115,
      fclass: 'tertiary',
      name: '北四环东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4569024, 39.9745973],
          [116.4553863, 39.9755012],
          [116.4542951, 39.9761962]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '823713321',
      code: 5115,
      fclass: 'tertiary',
      name: '望京北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4565932, 40.0101693],
          [116.4577494, 40.0101971],
          [116.4600657, 40.0101851],
          [116.4611422, 40.0101795],
          [116.4613888, 40.01018],
          [116.4640863, 40.0101801],
          [116.4668248, 40.0101624]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '823713322',
      code: 5115,
      fclass: 'tertiary',
      name: '望京北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4564467, 40.010171],
          [116.4565932, 40.0101693]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '823713323',
      code: 5115,
      fclass: 'tertiary',
      name: '望京北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4564354, 40.0103133],
          [116.4553242, 40.0102985],
          [116.4549457, 40.0102714],
          [116.4545511, 40.0102042],
          [116.4539543, 40.0100494],
          [116.4535372, 40.0098294],
          [116.4530906, 40.0095567],
          [116.4528969, 40.0094109],
          [116.4527954, 40.0093345],
          [116.4525205, 40.0090521],
          [116.4522993, 40.0087468],
          [116.452193, 40.0085654],
          [116.452127, 40.0084312],
          [116.4520676, 40.0082959],
          [116.4520163, 40.0081462],
          [116.4519823, 40.0080224],
          [116.451957, 40.007889],
          [116.4519477, 40.0078048],
          [116.4518894, 40.0069666]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '823713324',
      code: 5115,
      fclass: 'tertiary',
      name: '望京北路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4566356, 40.0103096],
          [116.4564354, 40.0103133]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '823732386',
      code: 5114,
      fclass: 'secondary',
      name: '广顺南大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.475212, 39.9841335],
          [116.474856, 39.9843607],
          [116.4740385, 39.9849421],
          [116.4737211, 39.9851678],
          [116.473163, 39.9855545],
          [116.4730485, 39.9856301],
          [116.4717284, 39.9865014],
          [116.4716016, 39.9865889],
          [116.4714912, 39.9866651],
          [116.4713707, 39.9867484],
          [116.469837, 39.98777],
          [116.469369, 39.9880783],
          [116.467666, 39.9892005],
          [116.4665164, 39.9899521],
          [116.4664197, 39.9900139],
          [116.4652657, 39.9907516],
          [116.4642497, 39.9914217]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '823732387',
      code: 5115,
      fclass: 'tertiary',
      name: '花家地东路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.475212, 39.9841335],
          [116.4753135, 39.9842365],
          [116.4759424, 39.9847852],
          [116.475992, 39.9848317],
          [116.47736, 39.9861144],
          [116.4802594, 39.9887792]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '823732388',
      code: 5115,
      fclass: 'tertiary',
      name: '花家地东路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4750835, 39.9839374],
          [116.475212, 39.9841335]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '823732389',
      code: 5114,
      fclass: 'secondary',
      name: '广顺南大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4750835, 39.9839374],
          [116.4752498, 39.9838362],
          [116.4755309, 39.9836651],
          [116.4765076, 39.9830347]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '829561404',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5163466, 39.9711724],
          [116.5184746, 39.9678712],
          [116.5188518, 39.9671537]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '829561405',
      code: 5111,
      fclass: 'motorway',
      name: '东五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5159161, 39.9718105],
          [116.5163466, 39.9711724]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '834691550',
      code: 5115,
      fclass: 'tertiary',
      name: '将台路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: -2,
      bridge: 'F',
      tunnel: 'T'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5027255, 39.9714307],
          [116.5036758, 39.9718307]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '845035290',
      code: 5115,
      fclass: 'tertiary',
      name: '望京东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4860464, 39.9936439],
          [116.4866476, 39.9941892],
          [116.4869972, 39.9946314],
          [116.4873346, 39.9951952],
          [116.4875623, 39.9957776],
          [116.4876883, 39.9962821],
          [116.487729, 39.9967991],
          [116.4877087, 39.997232],
          [116.48763, 39.9976604],
          [116.487467, 39.9981747],
          [116.4872101, 39.9986393],
          [116.4869971, 39.9990245],
          [116.4866366, 39.9994635],
          [116.4854134, 40.0007121],
          [116.4837663, 40.0024249],
          [116.4833506, 40.002834],
          [116.4832242, 40.0029583],
          [116.4819256, 40.004236]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '862991914',
      code: 5115,
      fclass: 'tertiary',
      name: '辛店路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4440077, 40.0066604],
          [116.4447989, 40.006696],
          [116.4459124, 40.0067234],
          [116.4483184, 40.0067947],
          [116.4488684, 40.0068056],
          [116.4494133, 40.0068266],
          [116.4495822, 40.0068331],
          [116.4503704, 40.0068246]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '878063390',
      code: 5115,
      fclass: 'tertiary',
      name: '溪阳东路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.484001, 40.0049898],
          [116.4826806, 40.0045687],
          [116.4823052, 40.0044425],
          [116.4819256, 40.004236]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '878063391',
      code: 5115,
      fclass: 'tertiary',
      name: '溪阳东路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4819256, 40.004236],
          [116.4818147, 40.0041721]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '879493404',
      code: 5115,
      fclass: 'tertiary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5028343, 39.9860326],
          [116.5012282, 39.9866605],
          [116.5009054, 39.9867858]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '879493405',
      code: 5115,
      fclass: 'tertiary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: -1,
      bridge: 'F',
      tunnel: 'T'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5032261, 39.9858801],
          [116.5028343, 39.9860326]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '879493406',
      code: 5115,
      fclass: 'tertiary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5034437, 39.9857871],
          [116.5032261, 39.9858801]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '879493407',
      code: 5115,
      fclass: 'tertiary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5034966, 39.9873797],
          [116.5033394, 39.9873121],
          [116.5033261, 39.9872147],
          [116.5036502, 39.9866646],
          [116.5039131, 39.986494],
          [116.5041465, 39.9862598],
          [116.5047741, 39.9853555],
          [116.5048224, 39.9852178],
          [116.5048385, 39.9850842],
          [116.5048251, 39.9849691],
          [116.5047848, 39.9848952],
          [116.5047204, 39.984813],
          [116.5046105, 39.9847472],
          [116.5044925, 39.984702],
          [116.5043208, 39.9846568],
          [116.5041867, 39.9846547],
          [116.5040579, 39.9846691],
          [116.5039024, 39.9847266],
          [116.5038004, 39.9847945],
          [116.5037361, 39.9848684],
          [116.5036824, 39.9850061],
          [116.5036315, 39.9853432],
          [116.503543, 39.9856699],
          [116.5034437, 39.9857871]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '879493408',
      code: 5115,
      fclass: 'tertiary',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5039881, 39.9875445],
          [116.5034966, 39.9873797]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '888429326',
      code: 5115,
      fclass: 'tertiary',
      name: '花家地街辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4594362, 39.9868097],
          [116.4606843, 39.986013],
          [116.4627108, 39.9847195]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '888429329',
      code: 5115,
      fclass: 'tertiary',
      name: '花家地街辅路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4629282, 39.9849227],
          [116.4597761, 39.9869787],
          [116.4596847, 39.9870383]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '906804671',
      code: 5115,
      fclass: 'tertiary',
      name: '万红路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4977506, 39.9792176],
          [116.4933746, 39.9792046],
          [116.4930576, 39.9791925],
          [116.4903372, 39.9791665],
          [116.4903192, 39.9791663],
          [116.4892588, 39.9790761],
          [116.4880886, 39.9790601],
          [116.487209, 39.979048],
          [116.4871521, 39.9790502],
          [116.4858623, 39.9790417]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '906804673',
      code: 5122,
      fclass: 'residential',
      name: '酒仙桥北路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5028471, 39.9871553],
          [116.5024324, 39.9870694],
          [116.5011358, 39.9868117],
          [116.5009054, 39.9867858]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '943519202',
      code: 5114,
      fclass: 'secondary',
      name: '广顺北大街',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4609206, 40.014601],
          [116.4609277, 40.0145308],
          [116.46097, 40.0141089]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '943519203',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4564511, 40.0139048],
          [116.4551966, 40.0139292],
          [116.4545083, 40.0139688],
          [116.4537755, 40.0140369]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '947989186',
      code: 5114,
      fclass: 'secondary',
      name: '阜安西路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4722579, 40.0035927],
          [116.4723831, 40.0019223],
          [116.4724451, 40.0006655],
          [116.4725418, 39.9990003],
          [116.4725487, 39.9988602],
          [116.4726111, 39.9972514],
          [116.4726625, 39.9964779],
          [116.4725855, 39.9959385],
          [116.472345, 39.995434],
          [116.4719643, 39.9950391],
          [116.4714338, 39.9945865],
          [116.4684677, 39.9918961],
          [116.4665109, 39.9901062],
          [116.4664197, 39.9900139]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '973020542',
      code: 5115,
      fclass: 'tertiary',
      name: '首都机场辅路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4885153, 39.9917654],
          [116.4926044, 39.9954879],
          [116.4937222, 39.9956773],
          [116.4941873, 39.9960886],
          [116.4954238, 39.9972395],
          [116.4954081, 39.9974052],
          [116.4954241, 39.997637],
          [116.4954837, 39.9978598],
          [116.4955348, 39.9980502],
          [116.4956131, 39.998229],
          [116.4957679, 39.9984326],
          [116.4980949, 40.0004405],
          [116.4981676, 40.0005058],
          [116.4997409, 40.0019338],
          [116.5020088, 40.0040203]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '973020543',
      code: 5115,
      fclass: 'tertiary',
      name: '首都机场辅路',
      ref: null,
      oneway: 'B',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5020088, 40.0040203],
          [116.5023845, 40.0043659]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '987659180',
      code: 5114,
      fclass: 'secondary',
      name: '南皋路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4980949, 40.0004405],
          [116.4989749, 39.9999871],
          [116.499412, 39.9997942],
          [116.4997264, 39.999665],
          [116.4999894, 39.999578],
          [116.5005799, 39.9994338],
          [116.5012002, 39.9993285],
          [116.5017103, 39.9992759],
          [116.5022994, 39.9992615],
          [116.5026991, 39.9992871],
          [116.5040834, 39.9993317],
          [116.5045785, 39.9993608],
          [116.5063025, 39.9994146],
          [116.5073476, 39.9994338],
          [116.507664, 39.9994534],
          [116.5082157, 39.9995613],
          [116.5099828, 39.9998455],
          [116.5109115, 40.000011],
          [116.5111711, 40.0000446],
          [116.5112806, 40.000122]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '987659181',
      code: 5114,
      fclass: 'secondary',
      name: '南影路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.5150129, 39.9966151],
          [116.5149548, 39.996741],
          [116.5137393, 39.9975546],
          [116.5127763, 39.9982354],
          [116.5118554, 39.998884],
          [116.5115657, 39.9991457],
          [116.5114222, 39.9993922],
          [116.5113652, 39.9996786],
          [116.5112806, 40.000122]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '995551874',
      code: 5114,
      fclass: 'secondary',
      name: '广泽路',
      ref: null,
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4723805, 40.0035894],
          [116.472327, 40.0047014],
          [116.4722537, 40.0063651],
          [116.4722077, 40.007386],
          [116.4721439, 40.0089255],
          [116.4721082, 40.0098598],
          [116.4721338, 40.0101317],
          [116.472113, 40.0102921],
          [116.4720923, 40.0105148],
          [116.4720529, 40.0111075],
          [116.4719922, 40.0122278],
          [116.4719345, 40.0132597],
          [116.4718769, 40.0144729],
          [116.4718526, 40.0152794],
          [116.4718283, 40.0158442],
          [116.4718132, 40.0159488]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '996313087',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4904239, 40.0014916],
          [116.4896795, 40.0022315]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '996313088',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4896795, 40.0022315],
          [116.4891349, 40.0028077]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '996313089',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 90,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.489446, 40.0022473],
          [116.4902051, 40.0014315],
          [116.4903145, 40.0013283]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '996313090',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 90,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4888871, 40.0028284],
          [116.489446, 40.0022473]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '996313091',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4911115, 40.0008006],
          [116.4906133, 40.0013013]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '996313092',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 90,
      layer: 0,
      bridge: 'F',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4905089, 40.0011449],
          [116.4909988, 40.0006828]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '996313094',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 90,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4903145, 40.0013283],
          [116.4905089, 40.0011449]
        ]
      ]
    }
  },
  {
    type: 'Feature',
    properties: {
      osm_id: '996313097',
      code: 5111,
      fclass: 'motorway',
      name: '北五环',
      ref: 'S50',
      oneway: 'F',
      maxspeed: 0,
      layer: 1,
      bridge: 'T',
      tunnel: 'F'
    },
    geometry: {
      type: 'MultiLineString',
      coordinates: [
        [
          [116.4906133, 40.0013013],
          [116.4904239, 40.0014916]
        ]
      ]
    }
  }
  ]
}

export {
  roads
}
