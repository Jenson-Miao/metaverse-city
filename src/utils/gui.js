import { gui as dat } from 'dat.gui'

export function initGui ({
  threeLayer,
  effects
}) {
  var params = {
  }
  var gui = new dat.GUI()
  effects.forEach(item => {
    params = Object.assign(params, item.getGuiParams ? item.getGuiParams() : {})
    if (item.guiFun) {
      item.guiFun(gui, params, threeLayer)
    }
  })
}

// function animateShow (meshes) {
//   meshes.forEach(function (mesh) {
//     mesh.animateShow({
//       duration: 3000
//     })
//   })
// }
