import Stats from 'stats.js'

export default function initStats ({ el }) {
  const stats = new Stats()
  stats.domElement.style.zIndex = 100
  el.appendChild(stats.domElement)
  return stats
}
