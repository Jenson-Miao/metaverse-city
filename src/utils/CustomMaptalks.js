import * as maptalks from 'maptalks'
import { ThreeLayer } from 'maptalks.three'
import * as THREE from 'three'
import { initGui } from '@/utils/gui'
import initStats from '@/utils/stats'
// import sky from '@/assets/sky.jpg'
export default class CustomMaptalks {
  // 需要增加的插件
  effects = []
  /**
   *
   * @param el
   * @param options
   *
   */
  constructor (el, options) {
    this.el = el
    const config = {
      showStats: false, // 是否显示左上角的状态
      showGui: false// 是否显示右上角的配置项
    }

    this.options = Object.assign(config, options)
  }

  config = {
    center: [116.4733853, 39.9877147], // 中心点位
    zoom: 10, // 缩放层级，越大放大越大
    pitch: 70, // 倾斜度
    bearing: 180, // 旋转度

    centerCross: false, // 在地图中央显示一个红十字
    doubleClickZoom: false, // 是否允许地图通过双击事件缩放。
    baseLayer: new maptalks.TileLayer('tile', {
      // dark_all light_all
      urlTemplate: 'https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png',
      subdomains: ['a', 'b', 'c', 'd'],
      attribution: "<span style='color:#fff'>智慧城市</span>"
    })
    // baseLayer: new maptalks.TileLayer('tile', {
    //   urlTemplate: 'http://{s}.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejh2N21nMzAxMmQzMnA5emRyN2lucW0ifQ.jSE-g2vsn48Ry928pqylcg',
    //   subdomains: ['a', 'b', 'c', 'd'],
    //   attribution: '&copy; <a href="http://osm.org">OpenStreetMap</a> contributors, &copy; <a href="https://mapbox.com/">mapbox</a>'
    // })
  }

  // 初始化maptalks
  initMapTalks (option) {
    // 更多配置：https://maptalks.org/maptalks.js/api/0.x/Map.html
    option = Object.assign(this.config, option)
    // 创建地图
    const map = new maptalks.Map(this.el, option)
    this.map = map
    map.on('click', e => {
      // 点击坐标
      this.moveMap(e.coordinate.toArray())
    })

    // 继承自 CanvasLayer类 文档：https://maptalks.org/maptalks.js/api/0.x/CanvasLayer.html
    const threeLayer = new ThreeLayer('t', {
      forceRenderOnMoving: true, // 地图移动时强制渲染图层
      forceRenderOnRotating: true // 地图旋转时强制渲染图层
    })

    threeLayer.addTo(map)
    this.map = map
    this.threeLayer = threeLayer
  }

  /**
   * Buildings 建筑
   * Outline 建筑物轮廓线
   */
  initEffets (effects = []) {
    this.meshes = []
    this.threeLayer.prepareToDraw = (gl, scene, camera) => {
      this.camera = camera
      // var light = new THREE.DirectionalLight(0xffffff)
      // light.position.set(0, 10, 10).normalize()
      // scene.add(light)
      // camera.add(new THREE.PointLight('#fff', 0.5))

      var ambientLight, pointLight, light
      light = new THREE.DirectionalLight(0xffffff, 0.4)
      light.position.set(0, 10, 10).normalize()
      scene.add(light)
      ambientLight = new THREE.AmbientLight(0xffffff, 0.44)
      scene.add(ambientLight)
      pointLight = new THREE.PointLight(0xffffff, 0)
      camera.add(pointLight)
      // 天空
      // var urls = [
      //   sky,
      //   sky,
      //   sky,
      //   sky,
      //   sky,
      //   sky
      // ]
      // var r = 'http://stemkoski.github.io/Three.js/images/dawnmountain-'
      // var urls = [
      //   r + 'xpos.png',
      //   r + 'xneg.png',
      //   r + 'ypos.png',
      //   r + 'yneg.png',
      //   r + 'zpos.png',
      //   r + 'zneg.png'
      // ]
      // var skyMap = new THREE.CubeTextureLoader().load(urls)
      // scene.background = skyMap
      // scene.rotation.x = -Math.PI / 2

      // 增加部分组件栏
      if (effects.length) {
        effects.forEach(Item => {
          // eslint-disable-next-line no-new
          this.effects.push(new Item(this))
        })
      }
      if (this.options.showStats) {
        // 左上角的状态栏
        this.stats = initStats(this)
      }
      if (this.options.showGui) {
        // 右上角配置
        initGui(this)
      }
      setTimeout(() => {
        this.moveMap()
      }, 1000)
      this.animation(this)
    }
  }

  moveMap (center) {
    this.config.zoom = 15
    if (center) {
      this.config.center = center
      this.config.zoom = 17
    }

    this.map.animateTo(this.config, {
      duration: 1000
    })
  }

  animation () {
    const clock = new THREE.Clock()
    // layer animation support Skipping frames
    if (!this.map.isInteracting()) {
      this.threeLayer._needsUpdate = !this.threeLayer._needsUpdate
      if (this.threeLayer._needsUpdate) {
        this.threeLayer.renderScene()
      }
    }
    if (this.options.showStats) {
      this.stats.update()
    }
    if (this.effects.length) {
      this.effects.forEach(item => {
        const dt = clock.getDelta()
        if (item._animation) {
          item._animation(dt)
        }
      })
    }
    requestAnimationFrame(() => { this.animation() })
  }

  getBuildingsMaterial (color = 'red') {
    const material = new THREE.MeshPhongMaterial({
      // map: texture,
      transparent: true,
      color: '#fff'
    })
    return material
  }
}
